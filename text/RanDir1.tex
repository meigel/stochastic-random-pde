\documentclass{article}%
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}%
\setcounter{MaxMatrixCols}{30}
%TCIDATA{OutputFilter=latex2.dll}
%TCIDATA{Version=5.50.0.2890}
%TCIDATA{CSTFile=40 LaTeX article.cst}
%TCIDATA{Created=Friday, July 25, 2014 13:29:45}
%TCIDATA{LastRevised=Friday, July 25, 2014 17:22:36}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{<META NAME="DocumentShell" CONTENT="Standard LaTeX\Blank - Standard LaTeX Article">}
%BeginMSIPreambleData
\providecommand{\U}[1]{\protect\rule{.1in}{.1in}}
%EndMSIPreambleData
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\begin{document}
\section{Elliptic Dirichlet problem with random coefficients}

Consider for $R\in\mathcal{R}$ the random Dirichlet problem%
\begin{align}
\frac{1}{2}\sum_{i,j=1}^{d}a^{ij}(x,R)\partial_{x^{i}x^{j}}u+\sum_{i=1}%
^{d}b^{i}(x,R)\partial_{x^{i}}u+c(x,R)u+g(x,R) &  =0,\text{ \ \ }x\in
G,\nonumber\\
\left.  u\right\vert _{\partial G} &  =\varphi(x,R),\label{RD}%
\end{align}
where $R$ is a random variable in $\mathcal{R}$, and for each $R,$
$a^{ij}(\cdot,R)=a^{ji}(\cdot,R)$ satisfies the strong ellipticity condition
in $\overline{G}:=G\cup\partial G$ with $G$ open and simply connected. For a
particular $R\in\mathcal{R},$ the solution of (\ref{RD}) has the following
probabilistic representation:%
\[
u(x,R)=E\left[  \left.  \varphi(X_{\tau^{x}}^{0,x},R)Y_{\tau^{x}}%
^{0,x,1}+Z_{\tau^{x}}^{0,x,1,0}\right\vert R\right]
\]
where%
\begin{align*}
dX &  =b(X,R)ds+\sigma(X,R)dW,\text{ \ \ \ }X(0)=x,\\
dY &  =c(X,R)Yds,\text{ \ \ }Y(0)=1,\\
dZ &  =g(X,R)Yds,\text{ \ \ }Z(0)=0,
\end{align*}
$\tau^{x}$ is the first hitting time of $X$ to the boundary $G$ and
$a=\sigma\sigma^{T}.$ We want to compute%
\[
u(x):=E\left[  u(x,R)\right]  =E\left[  \varphi(X_{\tau^{x}}^{0,x}%
,R)Y_{\tau^{x}}^{0,x,1}+Z_{\tau^{x}}^{0,x,1,0}\right]  ,\text{ \ \ }x\in G.
\]
For fixed $x\in G,$ $u(x)$ can be simulated by simulating for $m=1,...,M$
independently $R_{m},$ and%
\begin{align*}
dX_{m} &  =b(X_{m},R_{m})ds+\sigma(X_{m},R_{m})dW,\text{ \ \ \ }X_{m}(0)=x,\\
dY_{m} &  =c(X_{m},R_{m})Y_{m}ds,\text{ \ \ }Y_{m}(0)=1,\\
dZ_{m} &  =g(X_{m},R_{m})Y_{m}ds,\text{ \ \ }Z_{m}(0)=0,
\end{align*}
%
\[
\widehat{u}_{M}(x):=\frac{1}{M}\sum_{m=1}^{M}\left(  \varphi(X_{m,\tau^{x}%
},R_{m})Y_{m,\tau^{x}}+Z_{m,\tau^{x}}\right)
\]
Now let $\mu$ be some probability measure on $G.$ Let $\mathfrak{x}$ be
distributed according to $\mu.$ Let $\psi_{k}(x),$ $k=1,...,K\,$\ be a system
of basis functions on $G.$ Sample $\mathfrak{x}_{1},...,\mathfrak{x}_{N}$ from
$\mu$ and consider%
\[
\widehat{\alpha}^{N,M}:=\underset{\alpha\in\mathbb{R}^{k}}{\arg\min}\frac
{1}{N}\sum_{n=1}^{N}\left(  u_{M}(\mathfrak{x}_{n})-\sum_{k=1}^{K}\alpha
_{k}\psi_{k}(\mathfrak{x}_{n})\right)  ^{2}%
\]
Next consider%
\[
\widehat{u}_{N,M}(x):=\sum_{k=1}^{K}\widehat{\alpha}_{k}^{N,M}\psi_{k}(x)
\]
as approximation to%
\[
u(x),\text{ \ \ }x\in G
\]
Indeed, heuristically,%
\begin{align*}
\sum_{k=1}^{K}\widehat{\alpha}_{k}^{N,M}\psi_{k}(x)  & \approx E\left[
\left.  u_{M}(\mathfrak{x})\right\vert \mathfrak{x}=x\right]  \\
& \approx E\left[  \varphi(X_{\tau^{x}},R)Y_{\tau^{x}}+Z_{\tau^{x}}\right]
=u(x)
\end{align*}
In principle one may take $M=1,$ hence%
\[
\widehat{\alpha}^{N}:=\widehat{\alpha}^{N,1}:=\underset{\alpha\in
\mathbb{R}^{k}}{\arg\min}\frac{1}{N}\sum_{n=1}^{N}\left(  \varphi
(X_{\tau^{\mathfrak{x}_{n}}},R_{n})Y_{\tau^{\mathfrak{x}_{n}}}+Z_{\tau
^{\mathfrak{x}_{n}}}-\sum_{k=1}^{K}\alpha_{k}\psi_{k}(\mathfrak{x}%
_{n})\right)  ^{2}%
\]



\end{document}