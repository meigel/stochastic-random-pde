For the benchmark problem at hand we consider a constant right-hand side $f \equiv 1$ on the unit square domain $D = [0,1]^2 \subset \mathbb{R}^2$. The Dirichlet boundary data $g = \sin(\pi x_1) + \sin(\pi x_2)$ is enforced on the whole boundary $\partial D$ of the domain. Here, $x_i$ denotes component $i$ of the coordinate vector $x$.

The permeability tensor $\kappa$ is constructed in a form resembling a
Karhunen-Lo\`{e}ve expansion.
It exhibits the characteristics of a separable covariance function on the unit square and is easily controlled with respect to amplitude and frequency of the field.
More precisely, in~\eqref{eq:KL} we set $E[\kappa]=1$ and consider the coefficients $a_m$ with $m = 1, 2, \ldots$ and
\begin{align}\label{eq:fourier_modes}
\begin{split}
a_m(x) &= \alpha_m \cos(2\pi \beta_1(m) x_1)\cos(2\pi \beta_2(m) x_2),\quad
\alpha_m = A_\alpha m^{-\sigma_\alpha},\\
\beta_1(m) &= m - k(m) (k(m) + 1) /2,\quad
\beta_2(m) = k(m) - \beta_1(m),\\
k(m) &= \lfloor -1/2 + \sqrt{1/4 + 2m}\rfloor.
\end{split}
\end{align}
The parameters must verify $\sigma_\alpha > 1$ and $0 < A_\alpha < 1/\zeta(\sigma_\alpha)$ with the Riemann zeta function $\zeta$. For uniformly distributed random variables $\varphi_m\sim U(-1,1)$, the parameters $c_a, \varepsilon_a > 0$ and the truncation length $t_a\in\mathbb{N}$ determine the  (computational) random field $\kappa$ by
\begin{align}\label{eq:bench_field}
\kappa(x) = \frac{c_a}{\alpha_{\min}} \left( \sum_{m=1}^{t_a} a_m(x) \varphi_m + \alpha_{\min} \right) + \varepsilon_a.
\end{align}
\begin{remark}
We suppose the model~\eqref{eq:bench_field} of the field $\kappa$ in a $t_a$-term expansion to be the exact representation.
This, of course, is usually not the case and one has to consider how the truncation affects the solution accuracy.
Here, the representation merely serves the purpose of an easy to compute miscellaneous random field which fulfills Assumption~\ref{ass:conditionons-FK-stoch}.
However, an explicit representation of $\kappa$, based on a countable set of random variables, is neither necessary nor used in the presented method.
\end{remark}
The constant $\alpha_{\min}$ is set to the absolute value of the minimum for the sum over the $a_m$, i.e. $\alpha_{\min} = \sum_{m = 1}^{t_a} \alpha_m = \left| \min_{x\in D} \sum_{m = 1}^{t_a} a_m(x) \varphi_m \right| = \sum_{m = 1}^{t_a} \alpha_m$.

The basis functions $\psi_k$ for $k = 1, \ldots, K$ in the regression methods from Subsection~\ref{ssec:recap_regest} are chosen as the Legendre polynomials. We choose the polynomial degree 4 for each spatial direction which results in $K=25$ basis functions.
In case that locations $x$ are drawn uniformly in $D$, this choice admits the advantage of $G= I$ with the identity matrix $I$ in ~\eqref{dkl} such that no inverse of $G$ needs to be computed in ~\eqref{gre2}. 

A reference solution for this problem is obtained with a simple Monte Carlo approach utilizing a standard finite element solver. A set of $N=10^6$ samples $\kappa^i$ for $i = 1,\ldots,N$ is drawn from the random variable $\kappa$ and the finite element method gives a discrete solution $u^i_h$ on some very fine mesh with approximately $3\cdot 10^6$ degrees of freedom for each sample.
Subsequently, the stochastic estimator for the expected value is given by $\hat{u}_h = N^{-1} \sum_{i=1}^N u^i_h$ and we assume that $E[u] \approx \hat{u}_h$ is a sufficiently close approximation.

As described in Section~\ref{sec:conv-compl-analys}, we note that we only consider the
error from the time-discretization and the regression steps of our
algorithm. In particular, we work with an exact stochastic field given by a truncated expansion (``finite dimensional noise assumption'') and also fix the number of basis
functions. This implies that the error will converge to a positive
value given by the bias, i.e., by the error induced by projection to the fixed set of basis functions.

In the following subsections, we first demonstrate point-wise convergence of the scheme at an arbitrary location (the center) in the domain.
Then, global convergence of the regression approximation is examined with two different coefficient fields.
First, a ``smooth'' field with few expansion terms and thus only low frequencies is considered.
Second, a ``rough'' field with many expansion terms is employed.
This also contains high frequency components and thus exhibits large gradients.
With both settings, the convergence for a structured (deterministic) and a random (stochastic) selection of regression points in the domain $D$ is depicted.

\begin{remark}[Notes regarding the implementation]
  We implemented the Euler-Maruyama Monte-Carlo solver in C++ as a Python module. Using \texttt{OpenMP}, the solver calculates the trajectories in parallel on an arbitrary amount of processors. To sample the discrete Brownian motion we use a 64-bit Mersenne-Twister random number generator (RNG) from the C++ standard library. To ensure independently sampled random variables, each thread has his own distinctly seeded RNG. Being a Python module, we can easily compare our solution with the reference solution computed with the finite element solver \texttt{FEniCS} \cite{logg2012automated,es_fenics_2015} and references therein. Moreover, the fully/semi stochastic and deterministic global regression methods from Subsection~\ref{ssec:recap_regest} are also implemented in Python using the \texttt{NumPy} package.
\end{remark}






\subsection{Convergence in one point}
To visualize the outcome of the simulation algorithm detailed in the preceding subsection, we set $\sigma_\alpha = 2$ and $A_\alpha = 0.6$ in ~\eqref{eq:fourier_modes} and $t_a = 5$, $c_a = 1$ and $\varepsilon_a = 5 \cdot 10^{-4}$ in~\eqref{eq:bench_field}. This yields a rather smooth coefficient field.
This first test compares the results of the implemented solver with the reference solution at a single point $x \in D$.
We examine convergence in two ways in Figure~\ref{fig:pwc}.
On the left, we observe a convergence rate of $1$ in the Euler-Maruyama scheme by decreasing the initial time step $\Delta t_0$. 
Note that an adaptive time step calculation is applied which reduces the time steps close to the boundary $\partial D$.
On the right, we can see a convergence rate of $1/2$ due to increasing the number of trajectories $M$ in the Monte Carlo simulation.

\begin{figure}
\centering
\includegraphics[width=0.49\textwidth]{{{pic/PointwiseConvergency_dt_x0}}}
\includegraphics[width=0.49\textwidth]{{{pic/PointwiseConvergency_N_x0}}}
\caption{Convergence of the solution in $x = [0.5, 0.5]$.}
\label{fig:pwc}
\end{figure}

\subsection{Experiment - smooth benchmark field}\label{ssec:smoothKappa}
We again choose $\sigma_\alpha = 2$ and $A_\alpha = 0.6$ in ~\eqref{eq:fourier_modes} and $t_a = 5$, $c_a = 1$ and $\varepsilon_a = 5 \cdot 10^{-4}$ in ~\eqref{eq:bench_field}.
This time we are interested in convergence in the whole domain.
We thus measure the errors in the $L^2(D)$ and $H^1(D)$ norms.

\subsubsection*{Deterministic global regression}\label{sssec:det_grid_smooth}
The first part of this experiment applies the deterministic global regression from~\eqref{reg1} to solutions with the above example problem data.
On a given uniform triangulation of $D$, a single trajectory is computed for each vertex.
The convergence of the errors in the $L^2(D)$ and $H^1(D)$ norms is depicted in
Figure~\ref{fig:detRand} with square markers. Here, $N$ is
the total number of grid points and $M = 1$ is the number of trajectories starting from each grid point.
We also set an initial time step $\Delta t_0$ small enough such that the first term on the right-hand side in~\eqref{eq:error-decomp} is smaller than the other error contributions.
Hence, Figure~\ref{fig:detRand} shows the error resulting from the global regression. From Proposition~\ref{propd} we expect to
see a RMSE of order $\frac{1}{\sqrt{MN}}$ in terms of the total number of
trajectories $MN$, i.e., a convergence rate of $1/2$.
Hence, the numerical results are in line with the theory.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=\textwidth]{./pic/paper_detRand_N}
  \caption{The $L^2$ and $H^1$ errors from the experiments with deterministic global regression (square) and semi stochastic
  global regression (circle) on a smooth benchmark field as well as the $L^2$ and $H^1$ error of 
  the projection into the polynomial regression space (i.e., the bias, no marker).}
  \label{fig:detRand}
\end{figure}

\subsubsection*{Semi stochastic global regression}\label{sssec:stoch_grid_smooth}
The second part of this experiment uses uniformly distributed points in the domain $D$
together with the semi stochastic global regression from~\eqref{dkl}. Once again, for each sample point a single trajectory is
computed.
The results are depicted in Figure~\ref{fig:detRand} with circular markers.
They show the same behavior of the $L^2$ error as in the first
experiment with a slightly improved performance in the $H^1$ norm.
The convergence rate is the same as in the previous part of the experiment, as predicted by Proposition~\ref{propv}. 

\subsection{Experiment - rough benchmark field}\label{ssec:roughKappa}
We now choose $\sigma_\alpha = 1.1$ and $A_\alpha = 0.0009$ in \eqref{eq:fourier_modes}.
Furthermore, we drop the first 1000 terms of the sum in \eqref{eq:bench_field} and take
$t_a = 1020$, $c_a = 1$ and $\varepsilon_a = 5 \cdot 10^{-4}$.
This results in a coefficient field which only includes higher frequencies in the expansion~\eqref{eq:KL}.
We thus call this field ``rough'' when compared to the ``smooth'' first field.
A sample realization is depicted in Figure~\ref{fig:rough_field}.
With this, the experiment from Section~\ref{ssec:smoothKappa} are repeated in the following.

\begin{figure}[!htbp]
  \centering
  \includegraphics[width=\textwidth]{./pic/kappaRough500.png}
  \caption{Example realization of the rough benchmark field}
  \label{fig:rough_field}
\end{figure}

\subsubsection*{Deterministic global regression}\label{sssec:det_grid_rough}
As in the former experiment, we launch one trajectory from each vertex of a uniform grid of the domain $D$.
The errors in the $L^2(D)$ and $H^1(D)$ norms are again depicted with square markers in Figure~\ref{fig:detRandRough}.
Although the stochastic field now exhibits high oscillations, we can still observe the anticipated convergence rate of $1/2$ in both error norms.

\subsubsection*{Semi stochastic global regression}\label{sssec:stoch_grid_rough}
To conclude this second experiment, as before, we sample on uniformly distributed points in $D$ and compute one trajectory from each point.
Then again, the semi stochastic global regression is applied and we observe a convergence rate of $1/2$ (circular markers in Figure~\ref{fig:detRandRough}) and a slight performance improvement compared to the deterministic global regression.

\subsection{Comparison}
Comparing the two experiments in Sections~\ref{ssec:smoothKappa} and ~\ref{ssec:roughKappa}, we can see that the projection error in the $H^1(D)$ norm is reached at around $10^7$ and the $L^2(D)$ error around $2 \cdot 10^{-3}$  sampled points in both cases.
However, the $L^2(D)$ error of the projection is approximately one order of magnitude lower in the case of the rough field.
A possible explanation for this last observation could be that realizations of the rough field exhibit much smaller global $L^2(D)$ norms than the smooth fields.
This directly leads to smaller absolute values of the solutions (high frequencies of the coefficient are smoothed by the differential operator) and hence smaller $L^2$ errors.


\begin{figure}[!htbp]
  \centering
  \includegraphics[width=\textwidth]{./pic/detVSstoRough_N}
  \caption{The $L^2$ and $H^1$ errors from the experiments with deterministic global regression (square) and semi stochastic
  global regression (circle) on a rough benchmark field as well as the $L^2$ and $H^1$ error of 
  the projection into the polynomial regression space (no marker).}
  \label{fig:detRandRough}
\end{figure}


In summary, the experiments of this section illustrate that the described algorithm exhibits the predicted convergence behavior for stochastic fields of different smoothness.

Certainly, further experiments and a more detailed analysis will have to be carried out to assess the possibilities and the limitations for the application of this numerical method.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "spde_paper"
%%% End: 
