The construction of global approximate solutions based on probabilistic
representations for different random PDEs may generically be carried out via
solving a regression problem connected with a probabilistic representation%
\begin{equation}
v(x)=E\left[  \Phi^{x}\right]  ,\text{ \ \ }x\in D\subset\mathbb{R}%
^{d}.\label{pr}%
\end{equation}
In (\ref{pr}), $x\in D$ is a generic point in space and
$\Phi^{x}$ is a real valued random variable on a probability space $\left(
\Omega,\mathcal{F},\mathbb{P}\right)  $ such that the map%
\[
\left(  x,\omega\right)  \in D\times\Omega\rightarrow\Phi^{x}\left(
\omega\right) %
\]
is $\mathcal{B}\left( D\right) \otimes \mathcal{F}$ measurable.

In the context of this paper, we consider $v(x) = E[u(x)]$, $u$ being the
solution of the random PDE~\eqref{eq:RPDE}. By~\eqref{eq:stoch-rep-Eu}, the
above relation holds for
\begin{equation*}
  \Phi^x \coloneqq g\left( X_{\tau_x}^x\right)
  Y^x_{\tau_x} + Z^x_{\tau_x}.
\end{equation*}

For each fixed $x\in D$, we consider the sequence of i.i.d. copies
$\left( \Phi_{m}^{x},m=1,2,\ldots\right) .$ Let further $\mu$ be some probability
measure on $\left( D,\mathcal{B}\left( D\right) \right) ,$ $U$ be a random
variable on $D$ with distribution $\mu,$ and $\left( U_{n},n=1,2,\ldots\right) $
a sequence of i.i.d. copies of $U$ that are independent of any $\Phi_{m}^{x},$
$m=1,2,\ldots,$ $x\in D.$ One now may estimate $v(x)$ via regression procedures
based on a Monte Carlo simulation of%
\begin{align*}
&  \Phi_{1}^{U_{1}},\ldots,\Phi_{M}^{U_{1}},\\
&  \Phi_{M+1}^{U_{2}},\ldots,\Phi_{2M}^{U_{2}},\\
&  \cdot\cdot\cdot\\
&  \Phi_{(N-1)M+1}^{U_{N}},\ldots,\Phi_{NM}^{U_{N}},
\end{align*}
i.e. in condensed notation%
\begin{equation}
\left(  \Phi_{(n-1)M+1}^{U_{n}},\ldots,\Phi_{nM}^{U_{n}%
}\right)  ,\text{ \ \ }n=1,\ldots,N.\label{sa}%
\end{equation}
Notice once more that all these random variables are, by construction,
independent.

Generally one distinguishes between regression estimators of local and global
nature. Below we give a concise recap, where we introduce for ease of notation
the (point-wise) averages
\begin{equation}
  \barPhi^x_n \coloneqq \overline{\Phi}_{n,M}^{x} \coloneqq
  \frac{1}{M}\sum_{m=1}^{M}\Phi_{(n-1)M+m}^{x}, \quad x\in D.\label{av}%
\end{equation}

\begin{remark}
  Although the boundary data is typically known, a regression approach as
  presented here does not enforce or impose boundary conditions explicitly.
  In the numerical experiments, we manually place a set of ``exact solution
  points'' on the entire boundary.  There are several ways to get a more
  accurate matching of boundary conditions.  However, this does not affect the
  presented error analysis.
\end{remark}


\subsection{Recap of regression estimators}\label{ssec:recap_regest}

In this section we recall some common regression estimators starting with some local ones.

\subsubsection*{Local regression}

Let $\mathcal{K}(x)\geq0$ be some kernel
function on $\mathbb{R}^{d}$ satisfying $\int\mathcal{K}(x)dx=1$ and $\int
x^{i}\mathcal{K}(x)dx=0,$ $i=1,\ldots,d.$ For a band-width $\delta>0$ with
$\delta\downarrow0$ in relation to $N\rightarrow\infty,$ in a suitable way,
one defines the kernel estimator%
\[
\widehat{v}(x) \coloneqq \frac{\sum_{n=1}^{N}\mathcal{K}\left(
\frac{x-U_{n}}{\delta}\right)  \overline{\Phi}_{n,M}^{U%
_{n}}}{\sum_{n^{\prime}=1}^{N}\mathcal{K}\left(  \frac{x-U%
_{n^{\prime}}}{\delta}\right)  }.
\]

Another related type of local regression is the so called \textit{local
  polynomial regression, }where one solves the following weighted least
squares problem for some polynomial degree $p$ at a fixed point $x_{0}:$%
\[
 \underset{\left\{  \beta_{\eta};\,\left\vert
\eta\right\vert \leq p\right\}  }{\arg\min}\sum_{n=1}^{N}\mathcal{K}\left(
\frac{x_{0}-U_{n}}{\delta}\right)  \cdot\left[  \overline{\Phi
}_{n,M}^{U_{n}}-\sum_{\left\vert \eta\right\vert \leq p}\beta_{\eta
}(U_{n}-x_{0})^{\eta}\right]  ^{2}%
\]
with $\eta\in\mathbb{N}_{0}^{d}$ being a generic multi-index, which gives the
approximations
\[
\widehat{v}%
(x_{0})\coloneqq \widehat{\beta}_{0},\quad
\partial_{\eta}\widehat{v}(x_{0})\coloneqq \eta!\widehat{\beta}_{\eta}.
\]


It should be noted that local polynomial regression is particularly favorable
if one needs to estimate derivatives at a fixed point. As some further local
estimators we mention the \textit{Nadaraya-Watson estimator,} and the
$k$\textit{-nearest neighbor estimators }(e.g. see \cite{liero89} for
more details).

An application of local regression methods to the random PDE problem at hand
is postponed to a future paper. Here, we focus on \emph{global} regression.

\subsubsection*{Fully stochastic global regression}

Global Monte Carlo regression estimators are extremely popular in finance
where they are used for the pricing of American options (\cite{long/schw01}
and \cite{tsit/vroy01}). The general procedure is as follows. One takes a
system of (continuous basis) functions $\psi_{k}: D \to \R,$ $k=1,2,\ldots,\,$
and considers for a Monte Carlo sample (\ref{sa}) and a fixed number $K,$ the
solution of the regression problem
\begin{equation}
\hgamma \coloneqq \underset{\gamma\in\mathbb{R}^{K}}{\arg\min}\frac
{1}{N}\sum_{n=1}^{N}\left(  \overline{\Phi}_{n,M}^{U_{n}}-\sum
_{k=1}^{K}\gamma_{k}\psi_{k}(U_{n})\right)  ^{2}.\label{gre}%
\end{equation}
The solution of (\ref{gre}) is straightforward and given by%
\begin{equation}
\hgamma = \left(  \mathcal{M}^{\top
}\mathcal{M}\right)  ^{-1}\mathcal{M}^{\top}\mathcal{Y},\label{gh}%
\end{equation}
where $\mathcal{M} \in \mathbb{R}^{N\times K}$ with entries
given by
\begin{equation*}
  \mathcal{M}_{n,k} \coloneqq \psi_{k}(U_{n}), \quad n=1,\ldots,N,\quad
  k=1,\ldots,K,\label{des}
\end{equation*}
and
\[
\mathcal{Y}\coloneqq \left[  \overline{\Phi}_{1,M}^{U_{1}},\ldots,\overline
{\Phi}_{N,M}^{U_{N}}\right]  ^{\top}.
\]
One thus obtains the approximation%
\begin{equation}
  \widehat{v}(x)\coloneqq \sum_{k=1}^{K}\widehat{\gamma}_{k}%
  \psi_{k}(x),\label{gre1}%
\end{equation}
which, of course, depends on $N$, $M$, $K$ and the choice of basis functions
$\psi_1, \ldots, \psi_K$.

\subsubsection*{Semi stochastic global regression}

In several applications, in particular in finance, the random variable $U$ can
be simulated (e.g. via an SDE) but its distribution is not explicitly known
otherwise. Therefore, the construction of $\widehat{\gamma}$ via (\ref{gh})
requires the inversion of a random matrix $\mathcal{M}^\top \mathcal{M}$. In
some situations this inversion may be ill-conditioned and may be the source of
suboptimal convergence properties unless some kind of regularization is
included.  In the present context of random PDEs we, however, have full
control of the choice of the distribution of $U$. Note that
\begin{equation}
\frac{1}{N}\left[  \mathcal{M}^{\top}\mathcal{M}\right]  _{kl}   =\frac{1}%
{N}\sum_{n=1}^{N}\psi_{k}(U_{n})\psi_{l}(U_{n})
 \xrightarrow{N\to\infty} \int_{D}\psi_{k}(x)\psi_{l}(x)\mu\left(  dx\right)
  \eqqcolon
  \mathcal{G}_{kl}.\label{dkl}
\end{equation}
For favorable choices of $\mu$ and/or $\psi_k$, the matrix $\mathcal{G}$ may
be pre-computed by some quadrature method or even be known in closed form. In
particular, if the basis functions $\psi_k$ are chosen orthonormal
w.r.t.~$\mu$, then $\mathcal{G}$ is the identity matrix. So it is natural to
replace (\ref{gh}) with the estimate
\begin{equation}
  \overline{\gamma} \coloneqq \frac{1}{N} \mathcal{G}^{-1} \mathcal{M}^{\top}
  \mathcal{Y}\label{gre2}
\end{equation}
leading to the approximate solution
\begin{equation}
  \overline{v}(x) \coloneqq \sum_{k=1}^{K} \overline{\gamma}_{k}
  \psi_{k}(x). \label{gre3}
\end{equation}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{{{pic/regExample}}}
  \caption{Subset of the regression points $\Phi_i^{U_i}$ and the solution of
    the regression procedure.}
  \label{fig:regExample}
\end{figure}

The (semi-)stochastic global regression method is visualized in
Figure~\ref{fig:regExample}. The dots in the three-dimensional plot correspond
to the samples $\left(U_i, \Phi^{U_i}_i \right)$ (or a subset thereof), and the
surface is the outcome of the regression procedure, i.e., the graph of
$\overline{v}$ on the domain $D$. Notice that Figure~\ref{fig:regExample} is
based on the actual calculations in Section~\ref{sec:examples}.

\subsubsection*{Deterministic global regression}

As an alternative to the semi stochastic procedure (\ref{gre2}),
(\ref{gre3}), we may alternatively carry out spatial regression with respect
to some deterministic set of, e.g. uniform, grid points $x_{n},$ $n=1,\ldots,N.$
This leads to the regression problem%

\begin{equation}
\widetilde{\gamma} \coloneqq \underset{\gamma\in\mathbb{R}^{K}}{\arg\inf}%
\frac{1}{N}\sum_{n=1}^{N}\left(  \overline{\Phi}_{n,M}^{x_{n}}-\sum_{k=1}%
^{K}\gamma_{k}\psi_{k}(x_{n})\right)  ^{2}.\label{reg1}%
\end{equation}
By the deterministic design matrix by $\mathcal{N} \in \mathbb{R}^{N\times K}$
with entries given by
\[
\mathcal{N}_{n,k}\coloneqq \psi_{k}(x_{n}),\quad n=1,\ldots,N, \quad k=1,\ldots,K,
\]
the solution is given by%
\begin{equation}
\widetilde{\gamma} = \left(  \mathcal{N}^{\top
}\mathcal{N}\right)  ^{-1}\mathcal{N}^{\top}\widetilde{\mathcal{Y}}, \quad \widetilde{\mathcal{Y}}\coloneqq \left[
\overline{\Phi}_{1,M}^{x_{1} },\ldots,\overline{\Phi}_{N,M}^{x_{N}}\right]
^{\top},\label{eq:def-det-reg-coef}
\end{equation}
yielding the approximation%
\begin{equation}
\label{eq:det-reg-sol}
\widetilde{v}(x) \coloneqq \sum_{k=1}^{K}\widetilde{\gamma}_{k}\psi_{k}(x).
\end{equation}



\subsection{Convergence analysis of global regression estimators}

The convergence analysis of the full stochastic regression is highly
nontrivial and relies on the theory of empirical processes. For instance, in
the case of American options see \cite{clem/lamb/prot02}, and in the case of
Markovian control problems \cite{bel/kol/sch10}. The convergence analysis for
the semi stochastic regression and the deterministic regression approach is
significantly easier. Since the latter procedures are more suitable for our
purposes, we will restrict our analysis to them.

\begin{remark}
  \label{rem:notation-D}
  In what follows, we assume that the probability space $\left( \Omega, \F, P
  \right)$ is large enough to carry a sequence of independent samples of
  $\Phi^{U}$. Given an $\F$-measurable random field $\alpha = \alpha(x)$, $x
  \in D$, which may depend on a sequence of random variables $U_n$, we denote
  \begin{equation*}
    \norm{\alpha}_{L^2(\Omega \times D; P \otimes \mu)}^2 \coloneqq \int_D
    E[\alpha(x)^2] \mu(dx).
  \end{equation*}
  Hence, $\mu$ is used in a double role here: it denotes the distribution of
  the random variables $U_n$ (defined on $\Omega$) as well as a measure on $(D,
  \mathcal{B}(D))$.
\end{remark}

\subsubsection*{Semi stochastic regression statistics}

For the scalar product $f,g\in L_{\mu}^{2}(D)$ defined by
\[
\left\langle f,g\right\rangle _{L_{\mu}^{2}}\coloneqq \left\langle f,g\right\rangle
\coloneqq \int_{D}f(x)g(x)\mu(dx),
\]
and the corresponding norm $\norm{\cdot}_{L_\mu^2}$, let
\begin{equation}
  \label{eq:gamma-circ}
  \gamma^{\circ}\coloneqq \underset{\gamma\in\mathbb{R}^{K}}{\arg \min
  }\left\Vert v-\sum_{k=1}^{K}\gamma_{k}\psi_{k}\right\Vert ^{2}
\end{equation}
and define
\begin{equation}
  \label{eq:v_K-projection}
  v_K(x) \coloneqq \sum_{k=1}^K \gamma_k^\circ \psi_k(x), \quad x \in D.
\end{equation}
$v_K$ is the (exact) $L^2_\mu$-projection of $v$ to
$\operatorname{span} \set{\psi_1, \ldots, \psi_k}$. Hence, we have that
\[
\left\langle v- v_K,\psi_{k}\right\rangle=0, \quad k=1,\ldots,K,
\]
i.e.,%
\[
\left\langle v,\psi_{k}\right\rangle -\sum_{l=1}^{K}\mathcal{G}_{kl}\gamma_{l}^{\circ
}=0.
\]
By defining%
\[
\left[  w\right]  _{k}\coloneqq \left\langle v,\psi_{k}\right\rangle ,\quad
k=1,\ldots,K,
\]
we thus have%
\begin{equation*}
\gamma^{\circ}=\mathcal{G}^{-1}w.\label{cir}%
\end{equation*}

Note that the semi-stochastic regression estimator $\barv$ is a standard Monte
Carlo estimator of the projection $v_K$, as (for simplicity for $\mathcal{G} =
I_K$)
\begin{equation*}
  \bargamma_k = \f{1}{N} \sum_{n=1}^N \psi_k(U_n) \overline{\Phi}^{U_n}_{n,M}
  \approx \ip{\psi_k}{v}.
\end{equation*}
Hence, the standard Monte Carlo error formula shows that
\begin{equation*}
  E\left[\left( \bargamma_k - \ip{\psi_k}{v} \right)^2 \right] = \f{\var
    \left[\psi_k(U_n) \overline{\Phi}^{U_n}_{n,M} \right]}{N}.
\end{equation*}
On the other hand, we are really interested in $E\left[ \norm{\barv - v_K}^2
\right]$. An immediate application of the standard formula gives an error
estimate of the order $K^2/N$, as the $K$ estimates $\bargamma_1, \ldots,
\bargamma_K$ are not independent. However, we shall see that a more careful
analysis will give us an estimate of order $K/N$, see Proposition~\ref{propv}
below.

Let us write%
\begin{align}
\frac{1}{N}\left[  \mathcal{M}^{\top}\mathcal{Y}\right]  _{k} &  =\frac{1}%
{N}\sum_{n=1}^{N}\psi_{k}(U_{n})\mathcal{Y}_{n}=\frac{1}{N}%
\sum_{n=1}^{N}\psi_{k}(U_{n})\overline{\Phi}_{n,M}^{U_{n}%
}\nonumber\\
&  =:\frac{1}{N}\sum_{n=1}^{N}\psi_{k}(U_{n})\left(  v(U%
_{n})+\frac{\eta_{n}}{\sqrt{M}}\right)  \label{pe}%
\end{align}
with i.i.d. random variables%
\[
\eta_{n}\coloneqq \sqrt{M}\left(  \overline{\Phi}_{n,M}^{U%
_{n}}-v(U_{n})\right)  =\frac{1}{\sqrt{M}}\sum_{m=1}^{M}\left(
\Phi_{(n-1)M+m}^{U_{n}}-v(U_{n})\right)
\]
satisfying $E\left[  \eta_{1}|U_{1}\right]  =0.$ For the
variances we have
\begin{align*}
\var\left[ \left. \eta_{1} \right| U_1 \right]   &  =\var\left[
                                                   \left. \Phi^{U} \right| U\right]  \\
\var\left[  \eta_{1}\right]   &  =\var\left[  \Phi^{U%
}-v(U)\right]  = E \left[  \left(  \Phi^{U%
}-v(U)\right)  ^{2}\right]  \\
&  =\int\mu(dx) E \left[  \left(  \Phi^{x}-v(x)\right)  ^{2}\right]
=\int\mu(dx)\var\left[  \Phi^{x}\right]  .
\end{align*}
From (\ref{pe}) we then define%
\[
\frac{1}{N}\left[  \mathcal{M}^{\top}\mathcal{Y}\right]  _{k} \eqqcolon \left\langle
v,\psi_{k}\right\rangle +\frac{\xi_{k}}{\sqrt{N}}+\frac{\vartheta_{k}}%
{\sqrt{NM}}%
\]%
with
\begin{align*}
\xi_{k} &\coloneqq \frac{1}{\sqrt{N}}\sum_{n=1}^{N}\left(  \psi
_{k}(U_{n})v(U_{n})-\left\langle v,\psi_{k}\right\rangle
\right),\\
\vartheta_{k} &\coloneqq \frac{1}{\sqrt{N}}\sum_{n=1}^{N}%
\psi_{k}(U_{n})\eta_{n}=\frac{1}{\sqrt{NM}}\sum_{n=1}^{N}\sum
_{m=1}^{M}\psi_{k}(U_{n})\left(  \Phi_{(n-1)M+m}^{U_{n}%
}-v(U_{n})\right),
\end{align*}
where the $\xi_{k}$ and $\vartheta_{k}$ have zero mean for each $k=1,\ldots,K,$
and variances%
\begin{align}
\var\left[  \xi_{k}\right]   &  =\var\left[  \psi_{k}%
(U)v(U)\right]  =E\left[  \psi_{k}^{2}%
(U)v^{2}(U)\right]  -E\left[  \psi_{k}%
(U)v(U)\right]  ^{2}\nonumber\\
&  =\int\mu(dx)\psi_{k}^{2}(x)v^{2}(x)- \left(\int\mu(dx)\psi_{k}%
(x)v(x) \right)^2,\label{varxi}\\
\var\left[  \vartheta_{k}\right]   &  =\var\left[  \psi
_{k}(U_{1})\eta_{1}\right]  =\int\mu(dx)\psi_{k}^{2}(x)\var\left[
                                     \Phi^{x}\right]  ,\label{varth}%
\end{align}
respectively. From (\ref{gre2}) we then have%
\begin{align}
\overline{\gamma} &  =\mathcal{G}^{-1}\left(  w+\frac{\xi}{\sqrt{N}}%
+\frac{\vartheta}{\sqrt{MN}}\right)  \nonumber\\
&  =\gamma^{\circ}+\frac{\mathcal{G}^{-1}\xi}{\sqrt{N}}+\frac{\mathcal{G}^{-1}\vartheta}{\sqrt
{MN}}\nonumber
\end{align}
with $\xi\coloneqq \left[  \xi_{1},\ldots,\xi_{K}\right]  ^{\top}$ and $\vartheta
\coloneqq \left[  \vartheta_{1},\ldots,\vartheta_{K}\right]  ^{\top}.$ We so obtain a
point-wise approximation error,%
\begin{align*}
 \barv(x) - v(x)
&  =\psi^{\top}(x)\left(  \overline{\gamma}-\gamma^{\circ}\right)  + v_K(x)-v(x)\\
&  =\psi^{\top}(x)\left(  \frac{\mathcal{G}^{-1}\xi}{\sqrt{N}}+\frac{\mathcal{G}^{-1}\vartheta
}{\sqrt{MN}}\right)  + v_K(x) - v(x).
\end{align*}
with $\psi\coloneqq \left[  \psi_{1},\ldots,\psi_{K}\right]  ^{\top}.$ Since $\xi$ and
$\vartheta$ have zero mean, the point-wise bias---i.e., the error from
projecting $v$ to the span of the basis function $\psi_1$, \ldots, $\psi_K$---is equal to%
\[
\overline{\delta}(x)\coloneqq v_K(x) - v(x)
\]
and the $L_{\mu}^{2}(D)$-norm of the bias equals,%
\begin{equation}
\left\Vert \overline{\delta}\right\Vert =\left\Vert v- v_K\right\Vert _{L_{\mu}^{2}}.\label{bs}%
\end{equation}
The bias depends on properties of $v$ and the basis functions
  $\psi$ and shall not be analyzed in this section.

On the other hand, let $\overline{\varepsilon}(x)$ denote the regression error
on top of the bias $\overline{\delta}$, i.e.,
\begin{equation*}
  \overline{\varepsilon}(x) \coloneqq \barv(x) - v_K(x).
\end{equation*}
Clearly, $\overline{\varepsilon}(x)$ is related with the ``statistical error''
of a standard Monte Carlo procedure. Let us denote
\begin{equation*}
  \overline{\var}_{\mu} \coloneqq \norm{\overline{\varepsilon}}_{L^2\left(
      \Omega \times D; P \otimes \mu \right)}^2,
\end{equation*}
which can be seen as the average (w.r.t.~$\mu$) over the point-wise variance
of $\barv$. We have
\begin{align}
\overline{\var}_{\mu} &= \int_D\mu(dx)E\left[  \left(
\frac{\mathcal{G}^{-1}\xi}{\sqrt{N}}+\frac{\mathcal{G}^{-1}\vartheta}{\sqrt{MN}}\right)  ^{\top
}\psi(x)\psi^{\top}(x)\left(  \frac{\mathcal{G}^{-1}\xi}{\sqrt{N}}+\frac{\mathcal{G}^{-1}%
\vartheta}{\sqrt{MN}}\right)  \right]  \nonumber\\
&  =E\left[  \left(  \frac{\mathcal{G}^{-1}\xi}{\sqrt{N}}+\frac{\mathcal{G}^{-1}%
\vartheta}{\sqrt{MN}}\right)  ^{\top}\left(  \frac{\xi}{\sqrt{N}}%
+\frac{\vartheta}{\sqrt{MN}}\right)  \right]  \label{var}\\
&  =\frac{1}{N}E\left[  \xi^{\top}\mathcal{G}^{-1}\xi\right]  +\frac{2}%
{N\sqrt{M}}E\left[  \vartheta^{\top}\mathcal{G}^{-1}\xi\right]  +\frac{1}%
{MN}E\left[  \vartheta^{\top}\mathcal{G}^{-1}\vartheta\right]  ,\nonumber
\end{align}
by using (\ref{dkl}) and the symmetry of $\mathcal{G}.$

Let $\barlambda_{\min}^{K}>0$ be the smallest eigenvalue of $\mathcal{G}=\mathcal{G}^{K}.$ We then
have by (\ref{varxi}), (\ref{varth}), and Cauchy-Schwartz, the estimate%
\begin{equation}
  \label{varb}
  \overline{\var}_{\mu} \leq \frac{1}{\barlambda_{\min}^{K}N}
  \sum_{k=1}^{K}\var\left[  \xi_{k}\right]
  +\frac{2}{\barlambda_{\min}^{K}N\sqrt{M}}\sum_{k=1}^{K}\sqrt{\var\left[
      \xi_{k}\right]}\sqrt{\var\left[\vartheta_{k}\right]}
  +\frac{1}{\barlambda_{\min}^{K}MN}\sum_{k=1}^{K}\var\left[ \vartheta_{k}\right].
\end{equation}
The following proposition is now obvious.

\begin{proposition}
\label{propv} Suppose that the set of basis functions is such that the
variances (\ref{varxi}) and (\ref{varth}) are bounded by $\mathcal{V}$ for all
$k$ if $K\rightarrow\infty,$ and that $\barlambda_{\min}^{K}\geq \barlambda_{\min
}>0$ if $K\rightarrow\infty.$ We then have the estimate%
\[
 \norm{\overline{\varepsilon}}_{L^2\left(\Omega \times D; P \otimes \mu
   \right)}^2 \leq\frac{\mathcal{V}}{\barlambda_{\min}%
}\left(  1+\frac{2}{\sqrt{M}}+\frac{1}{M}\right)  \frac{K}{N}.
\]
\end{proposition}

\begin{remark}
  In view of the above proposition, it is inefficient to take $M$ larger than
  one or two in the semi stochastic regression. For example, $M=2$ doubles the
  computation time but reduces the variance bound approximately by a factor
  $0.73$ only. Moreover, assuming a choice of $\mu$-orthonormal basis
  functions $\psi_1, \ldots, \psi_K$, the matrix $\mathcal{G}$ is the identity
  matrix and, consequently, the parameter $\barlambda_{\min} = 1$.
\end{remark}

\subsubsection*{Deterministic regression statistics}

Similar to (\ref{dkl}) we introduce for a uniform grid $x_n$ with $n=1,...,N,$ the matrix $F$ and the vector $z$ defined
by
\begin{align}
\left[  F\right]  _{kl} &\coloneqq \frac{1}{N}\sum_{n=1}^{N}\psi_{k}(x_{n}%
)\psi_{l}(x_{n}),\label{fm}\\
\left[  z\right]_{l} &\coloneqq 
                       \frac{1}{N}\sum_{n=1}^{N} v(x_{n}) \psi_{l}(x_{n}),
                       \quad k,l=1,\ldots,K.\nonumber
\end{align}
We thus have that%
\[
\frac{1}{N}\left[  \mathcal{N}^{T}\mathcal{N}\right]  _{kl}=\frac{1}{N}%
\sum_{n=1}^{N}\psi_{k}(x_{n})\psi_{l}(x_{n})=\left[  F\right]  _{kl},
\]%
and
\begin{align*}
\frac{1}{N}\left[  \mathcal{N}^{T}\widetilde{\mathcal{Y}}\right]  _{k} &
= \frac{1}{N}\sum_{n=1}^{N}\psi_{k}(x_{n})\widetilde{\mathcal{Y}}_{n}\\
&  = \frac{1}{N}\sum_{n=1}^{N}\psi_{k}(x_{n})\overline{\Phi}_{n,M}^{x_{n}}\\
&  = \frac{1}{N}\sum_{n=1}^{N}\psi_{k}(x_{n})\left(  v(x_{n})+\frac
{\widetilde{\eta}_{n}}{\sqrt{M}}\right) 
\end{align*}
with independent random variables
\[
\widetilde{\eta}_{n}\coloneqq \widetilde{\eta}(x_{n}%
)\coloneqq \sqrt{M}\left(  \overline{\Phi}_{n,M}^{x_{n}}-v(x_{n})\right)  =\frac
{1}{\sqrt{M}}\sum_{m=1}^{M}\left(  \Phi_{(n-1)M+m}^{x_{n}}-v(x_{n})\right)  ,
\]
satisfying $E\left[  \widetilde{\eta}_{n}\right]  =0$ and%
\[
\var\left[  \widetilde{\eta}_{n}\right]  =\var\left[  \Phi^{x_{n}%
}\right]  \text{ }=E \left[  \left(  \Phi^{x_{n}}-v(x_{n})\right)
^{2}\right]  .
\]
So we can write%
\[
\frac{1}{N}\left[  \mathcal{N}^{T}\widetilde{\mathcal{Y}}\right]
_{k}  =  z_{k}
 +\frac{\widetilde{\vartheta}%
_{k}^{N,M}}{\sqrt{NM}},
\]
where for $k=1,\ldots,K,$
\[
\widetilde{\vartheta}_{k} \coloneqq  \frac{1}{\sqrt{N}}%
\sum_{n=1}^{N}\psi_{k}(x_{n})\widetilde{\eta}(x_{n})
\]
has zero mean and variance,%
\begin{equation}
\var\left[  \widetilde{\vartheta}_{k}\right]  =\frac{1}{N}\sum_{n=1}%
^{N}\psi_{k}^{2}(x_{n})\var\left[  \Phi^{x_{n}}\right].\label{vartil}%
\end{equation}
Next we proceed with%
\begin{equation*}
\widetilde{\gamma}   =\left(  \frac{1}{N}\mathcal{N}^{T}\mathcal{N}\right)
^{-1}\frac{1}{N}\mathcal{N}^{T}\widetilde{\mathcal{Y}}=F^{-1}z+\frac{1}%
{\sqrt{NM}}F^{-1}\widetilde{\vartheta}.
\end{equation*}



We obtain the random (point-wise) error
\begin{align}
\widetilde{\varepsilon}(x) &  \coloneqq \psi^{\top}(x)\widetilde{\gamma}-v(x)\notag\\
&  =\psi^{\top}(x)F^{-1}z-v(x)+\frac{1}{\sqrt{NM}}\psi^{\top}(x)F^{-1}%
\widetilde{\vartheta}.\label{etil}%
\end{align}
Since $\widetilde{\vartheta}=\left[  \widetilde{\vartheta}%
_{1}, \ldots, \widetilde{\vartheta}_{k}\right]$ has zero mean, the point-wise bias is thus equal to%
\begin{equation}
\widetilde{\delta}(x):=\widetilde{\delta}^{N,K}(x):=\psi^{\top}(x)\left(
F^{N,K}\right)  ^{-1}z^{N,K}-v(x).\label{bt}%
\end{equation}

Obviously, with%
\[
\mathcal{A}_{D}:=\int_{D}dx
\]
it holds that%
\[
F_{kl}^{N,K}\underset{N\rightarrow\infty}{\longrightarrow}F_{kl}^{\infty
,K}:=\frac{1}{\mathcal{A}_{D}}\int_{D}\psi_{k}(y)\psi_{l}(y)dy\text{ \ \ and
\ \ }z_{k}^{N,K}\underset{N\rightarrow\infty}{\longrightarrow}z_{k}^{\infty
,K}:=\frac{1}{\mathcal{A}_{D}}\int_{D}v(y)\psi_{k}(y)dy,
\]
hence we may define%
\begin{align}
\widetilde{\delta}^{\infty,K}(x)  & :=\psi^{\top}(x)\left(  F^{\infty
,K}\right)  ^{-1}z^{\infty,K}-v(x)\nonumber\\
& =:\psi^{\top}(x)\gamma^{\dag,K}-v(x),\label{per}%
\end{align}
where we introduce%
\[
\gamma^{\dag,K}:=\left(  F^{\infty,K}\right)  ^{-1}z^{\infty,K}.
\]
In fact, $\psi^{\top}\gamma^{\dag,K}$ is the projection of the true solution
$v$ on the span of the basis functions, with respect to the scalar product%
\[
\left\langle f,g\right\rangle _{L_{D}^{2}}:=\int_{D}f(y)g(y)dy.
\]
Thus, the (global) projection error with respect to the corresponding norm
satisfies%
\[
\left\Vert \widetilde{\delta}^{\infty,K}\right\Vert _{L_{D}^{2}}%
^{2}:=\left\Vert \psi^{\top}\gamma^{\dag,K}-v\right\Vert _{L_{D}^{2}}^{2}%
=\int_{D}\left(  \psi^{\top}(y)\gamma^{\dag,K}-v(y)\right)  ^{2}dy.
\]
It is natural to measure the global bias (\ref{bt}) with respect to this norm
also, that is,%
\[
\left\Vert \widetilde{\delta}^{N,K}\right\Vert _{L_{D}^{2}}^{2}=\left\Vert
\widetilde{\delta}\right\Vert _{L_{D}^{2}}^{2}=\int_{D}\left(  \psi^{\top
}(y)\left(  F^{N,K}\right)  ^{-1}z^{N,K}-v(y)\right)  ^{2}dy.
\]
By writing%
\begin{align*}
\widetilde{\delta}^{N,K}(x)  & =\widetilde{\delta}^{\infty,K}(x)+\psi^{\top
}(x)\left(  \left(  F^{\infty,K}\right)  ^{-1}\left(  z^{N,K}-z^{\infty
,K}\right)  +\left(  \left(  F^{N,K}\right)  ^{-1}-\left(  F^{\infty
,K}\right)  ^{-1}\right)  z^{N,K}\right)  \\
& =:\widetilde{\delta}^{\infty,K}(x)+\mathcal{R}_{N,K}(x),
\end{align*}
we obtain the global estimate%
\[
\left\Vert \widetilde{\delta}^{N,K}\right\Vert _{L_{D}^{2}}=\left\Vert
\widetilde{\delta}^{\infty,K}+\mathcal{R}_{N,K}\right\Vert _{L_{D}^{2}}%
\leq\left\Vert \widetilde{\delta}^{\infty,K}\right\Vert _{L_{D}^{2}%
}+\left\Vert \mathcal{R}_{N,K}\right\Vert _{L_{D}^{2}},
\]
where the residual term can be bounded from above by%
\begin{equation}
\left\Vert \mathcal{R}_{N,K}\right\Vert _{L_{D}^{2}}\leq\left\Vert
\psi\right\Vert _{L_{D}^{2}}\left(  \left\Vert \left(  F^{\infty,K}\right)
^{-1}\left(  z^{N,K}-z^{\infty,K}\right)  \right\Vert _{K}+\left\Vert \left(
\left(  F^{N,K}\right)  ^{-1}-\left(  F^{\infty,K}\right)  ^{-1}\right)
z^{N,K}\right\Vert _{K}\right),\label{res2}
\end{equation}
and where%
\[
z^{N,K}-z^{\infty,K}=\text{const}\frac{d}{N^{1/d}}\text{ \ \ and \ }\left[
\left(  F^{N,K}\right)  ^{-1}-\left(  F^{\infty,K}\right)  ^{-1}\right]
_{kl}=\text{const}\frac{d}{N^{1/d}},
\]
i.e.,
\begin{equation}
\left\Vert \widetilde{\delta}^{N,K}\right\Vert _{L_{D}^{2}}\approx\left\Vert
\widetilde{\delta}^{\infty,K}\right\Vert _{L_{D}^{2}}+\text{const}\frac
{d}{N^{1/d}}.\label{errd}%
\end{equation}
So, unlike in the semi stochastic case where the bias only depended on
$K,$ the bias (\ref{bt}) depends on $N$ and $K.$

\begin{remark}
  Similarly, to the semi-stochastic regression analyzed above, we could now
  also switch to a ``semi-deterministic'' regression by replacing
  $F_{kl}$ with $\mathcal{A}^{-1}_D\int_D \psi_k(y)\psi_l(y) dy$. This would help avoid any
  stability issues in the above linear system when $N < K$. Apart from this,
  the second term in (\ref{res2}) would disappear, but  the subsequent complexity analysis would remain unchanged.
\end{remark}


\bigskip We now proceed with the estimation of the variance in (\ref{etil}).
Similar to (\ref{var}) we obtain for the $L_{D}^{2}$-norm of the point-wise
variance,$\frac{1}{\sqrt{NM}}\psi^{\top}(x)F^{-1}\widetilde{\vartheta}$%
\begin{align*}
\widetilde{\text{Var}}_{L_{D}^{2}} &  :=\frac{1}{NM}\int_{G} E\left[
\left(  F^{-1}\widetilde{\vartheta}\right)  ^{\top}\psi(y)\psi^{\top}%
(y)F^{-1}\widetilde{\vartheta}\right]  dy\\
&  =\frac{1}{NM} E \left[  \widetilde{\vartheta}^{\top}F^{-1}%
F^{\infty,K}F^{-1}\widetilde{\vartheta}\right]
\end{align*}
by using the symmetry of $F.$ Let $\lambda_{\min}^{K}$ be the smallest
eigenvalue of $F^{\infty,K}.$ Since for fixed $K,$%
\[
F^{-1}F^{\infty,K}F^{-1}=\left(  F^{N,K}\right)  ^{-1}F^{\infty,K}\left(
F^{N,K}\right)  ^{-1}\rightarrow F^{\infty,K},\text{ \ \ if \ }N\rightarrow
\infty,
\]
we may assume that $N$ is large enough, such that the smallest eigenvalue of
$F^{-1}F^{\infty,K}F^{-1},$ $\lambda_{\min}^{N,K}$ say, satisfies
$\lambda_{\min}^{N,K}>\lambda_{\min}^{K}/2.$ Then, analogue to (\ref{varb}),
we obtain the variance bound,%
\[
\widetilde{\text{Var}}_{L_{D}^{2}}\leq\frac{2}{\lambda_{\min}^{K}NM}\sum
_{k=1}^{K}\text{Var}\left[  \widetilde{\vartheta}_{k}\right]  .
\]
We so have the following proposition.

\begin{proposition}
\label{propd} Suppose that the set of basis functions is such that the
variances (\ref{vartil}) are bounded by $\widetilde{\mathcal{V}}$ for all $N$
and $k$ if $N,K\rightarrow\infty.$ Suppose that $\lambda_{\min}^{K}%
>\widetilde{\lambda}_{\min}$ for each $K.$ Let us take $N>K$ %
such that $\lambda_{\min}^{N,K}\geq\widetilde{\lambda}_{\min}^{K}%
/2>\widetilde{\lambda}_{\min}/2.$ We then have the estimate%
\[
\widetilde{\mathrm{Var}}_{L_{D}^{2}}\leq\frac{2\widetilde{\mathcal{V}}%
}{\widetilde{\lambda}_{\min}}\frac{K}{NM}.
\]

\end{proposition}


Let us have a look at the bias of the deterministic regression procedure
again. It is clear that, even if $K$ is such that $\left\Vert \mathbb{\,}%
\widetilde{\delta}^{\infty,K}\right\Vert _{L_D^2}$ is small, a
too small $N$ causes extra bias due to the deterministic integration error,
cf. (\ref{errd}). This in contrast to the semi stochastic method where the
bias (\ref{bs}) is independent of the number $N$ of Monte Carlo samples of the
random variable $\mathcal{U}.$ Therefore, for larger dimensions $d$ the semi
stochastic method outperforms the deterministic one.

For a closer look at the bias of the deterministic regression, observe that
\[
\left\Vert \mathbb{\,}\widetilde{\delta}^{N,K}\right\Vert _{L^2_D}
\longrightarrow
\left\Vert \mathbb{\,}\widetilde{\delta}^{\infty,K}\right\Vert _{L_D^{2}}.
\]
It is clear that, even if $K$ is such that $\left\Vert \mathbb{\,}%
\widetilde{\delta}^{\infty,K}\right\Vert _{L_D^2}$ is small, a
too small $N$ causes extra bias due to the deterministic integration error.
This is in contrast to the semi stochastic method where the bias (\ref{bs}) is
independent of the number $N$ of Monte Carlo samples of the random variable
$U.$

The size of the deterministic integration error as a function of the
number $N$ of grid points depends on the nature of the point set $x_1, \ldots,
x_N$. For instance, in the case of a uniform tensor grid $x_1, \ldots, x_N$ in
dimension $d$, the integration error is, in principle, of order
$\f{1}{N^{1/d}}$, and, hence, the bias gives
\begin{subequations}
\label{eq:bias-det-reg}
\begin{equation}
\left\Vert \mathbb{\,}\widetilde{\delta}^{N,K}\right\Vert _{L^2_D}\approx
\left\Vert \mathbb{\,}\widetilde{\delta}^{\infty,K}\right\Vert _{L_D^2}+\const \frac{d}{N^{1/d}},\label{eq:bias-det-reg-tensor}
\end{equation}
where the second term amounts to the deterministic integration error of the
present integration method based on $\rho_{N}.$ On the other hand, if we
choose $x_1, \ldots, x_N$ as the first $N$ points of a $d$-dimensional
low-discrepancy sequence, then the integration error can be reduced to
$\f{\log^d N}{N}$, leading to
\begin{equation}
\left\Vert \mathbb{\,}\widetilde{\delta}^{N,K}\right\Vert _{L^2_D}\approx
\left\Vert \mathbb{\,}\widetilde{\delta}^{\infty,K}\right\Vert _{L^2_D}+\const\frac{\log^d(N)}{N}.\label{eq:bias-det-reg-qmc}
\end{equation}
\end{subequations}

We refer to~\cite{MN15} for a detailed exposition of the low-discrepancy case
and to~\cite{ZNX14} for an analysis based on collocation grids.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spde_paper"
%%% End:
