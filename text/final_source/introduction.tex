Many applications in the applied sciences, e.g. in engineering and computational biology, involve uncertainties of model parameters.
These can for instance be related to coefficients of media, i.e. material properties, the domains and boundary data.
The uncertainties may result from heterogeneities of media and incomplete knowledge or inherent stochasticity of parameters.
With steadily increasing computing power, the research field of uncertainty quantification has become a rapidly growing and vividly active area which covers many aspects of dealing with such uncertainties for problems of practical interest.

In this work, we are concerned with the description of a novel numerical approach for the solution of PDEs with stochastic data.
More specifically, we consider the Darcy equation related to the modeling of groundwater flow given by
\begin{subequations}
\label{eq:model}
\begin{align}
  -\nabla \cdot \left( \kappa(x) \nabla u(x) \right) &= f(x), \quad x \in D,\\
  u(x) &= g(x), \quad x \in \partial D.
\end{align}
\end{subequations}
Here, the solution $u$ is the hydraulic head, $\kappa$ denotes the conductivity coefficient describing the porosity of the medium, $f$ is a source term and the Dirichlet boundary data is defined by $g$.
The computational domain in $d$ dimensions is $D \subset \R^d$.
In what follows, we suppose that $D$ is a convex polygon and all data is sufficiently smooth such that the problem always exhibits a unique solution which itself is smooth.
A detailed regularity analysis is beyond the scope of this paper.
In principle, although we restrict our investigations to a stochastic coefficient $\kappa$, any data of the PDE can be modeled as being stochastic.
This model is quite popular for analytical and numerical examinations since it is one of the simplest models which reveals some major difficulties that also arise in more complex stochastic models.
Moreover, it is of practical relevance, e.g. in the context of groundwater contamination, and the deterministic second order elliptic PDE is a well-studied model problem.

When random data is assumed, an adequate description of the stochastic fields has to be chosen.
This can for instance be based on actual measurements, expert-knowledge or simplifying assumptions regarding the statistics.
For actual computations, a suitable representation amenable for the employed numerical method is required.
It is a common assumption that the considered fields are transformed Gaussian fields and are thus completely specified by the first two moments.
Another usual simplification is the finite dimensional noise assumption which states that a field only depends on a finite number of random variables.

In fact, any stochastic field $\kappa:\Omega\times D\to\mathbb{R}$ with finite variance can be represented in terms of
\begin{equation}\label{eq:KL}
\kappa(\omega,x) = E[\kappa] + \sum_{m=1}^\infty a_m(x) \xi_m(\omega)
\end{equation}
where the product of the sum separates the dependence on $\omega\in\Omega$ and $x\in D$.
A typical method to obtain such a representation is the Karhunen-Lo\`eve expansion (KL) which will be used in the numerical examples with a finite number of terms (truncated KL).
In this case, the basis $a_m$ consists of eigenfunctions of the covariance integral operator weighted by the eigenvalues of this operator.
The smoothness of the $a_m$ is directly related to the covariance function used to model the respective stochastic field, see e.g.~\cite{christakos2012random,MR2317004,lord2014introduction}.
A common choice is the Whittle-Mat\`ern covariance which includes the smooth Gaussian covariance and the rather rough exponential covariance.


A variety of numerical methods is available to obtain approximate solutions of the model problem \eqref{eq:model} with random data and we only refer to~\cite{lord2014introduction, smith2013uncertainty,le2010introduction} for an overview in the context of uncertainty quantification (UQ).
These methods often rely on the separation of the deterministic and the stochastic space and introduce separate discretizations~\cite{MR2805155}.
Common methods are based on sampling of the stochastic space, the projection onto an appropriate stochastic basis or a perturbation analysis.
The most well-known sampling approach is the Monte Carlo (MC) method which is very robust and easy to implement.
Recent developments include the quite successful application of multilevel ideas for variance reduction and advances with structured point sequences (Quasi-MC), cf.~\cite{MR3033013,MR2835612,MR2783812,MR3024159,MR3029293}.
(Pseudo-)Spectral methods represent a popular class of projection techniques which can e.g. be based on interpolation (Stochastic Collocation)~\cite{MR2318799,MR2421041,MR2421037} or orthogonal projections with respect to the energy norm induced by the differential operator of the random PDE (Stochastic Galerkin FEM)~\cite{ghanem2003stochastic,MR2121216,MR2121215,MR2084236,MR2105161,MR3154028}.
These methods are more involved to analyze and implement but offer the benefit of possibly drastically improved convergence rates when compared to standard Monte Carlo sampling.
The deterministic discretization often relies on the finite element method (FEM) which also holds for MC.


The aim of this paper is the description of a novel numerical approach which is based on the observation that the random PDE~\eqref{eq:model} is directly related to a stochastic differential equation driven by a stochastic process, namely
\begin{equation}\label{eq:sde}
dX_t = b(X_t)dt + \sigma(X_t)dW_t
\end{equation}
with appropriate coefficients $b$ and $\sigma$, Brownian motion $W$ and  additional boundary conditions.
For deterministic data $\kappa, f, g$, for any $x\in D$, the Feynman-Kac formula leads to a collection of random variables $\phi^x=\phi^x(\kappa,f,g)$ such that $u(x)=E[\phi^x]$, i.e. the deterministic solution at $x$ is equivalent to the expectation of the random variable.
When the data is stochastic, the solution $u(\omega, x)$ of the random PDE at $x\in D$ can be expressed as the {\em conditional expectation} $u(\omega,x)=E[\phi^x\,\vert\,\kappa,f,g]$ and the variance of $u(x)$ can be bounded by the variance of $\phi^x$.
To determine $\phi^x$ at points $x\in D$, a classical Euler method can be employed.
Given a finite set of sampling points in $D$, the approximate expectation of the solution $u(\cdot)=E[u(\omega,\cdot)]$ is determined by solving a regression problem with respect to a basis of orthogonal global polynomials.
By this, we obtain a representation of the (expectation of the) solution field which is globally defined and smooth.
One can regard the proposed method as a combination of sampling and reconstruction techniques, making use of classical stochastic solution methods point-wise and a global polynomial projection in a least squares sense.
When compared to MC which samples a stochastic space $(\Omega,\mathcal{F},P)$ by (typically) determining a FEM solution at every point and subsequently averaging the solutions, our method determines realizations of stochastic solutions at points in the physical domain $D$ and determines the expectation by a global minimization subject to a basis in the physical space.
Thus, the method does not require any type of deterministic solver.
Moreover, it can be parallelized extremely well.

The structure of the paper is as follows:
In Section~\ref{sec:stoch-repr}, we elaborate on the representation of deterministic and stochastic PDEs in terms of stochastic differential equations.
Moreover, we recall the Euler method as a way to determine point-wise stochastic solutions numerically.
Based on a set of stochastic solutions in the physical domain, the reconstruction of a global approximation by means of a regression approach is described in Section~\ref{sec:regression}.
In addition to the presentation of the method, we also provide a global convergence analysis.
In Section~\ref{sec:conv-compl-analys}, we comment on the expected overall convergence and complexity properties of the method.
This should be considered as initial and not in-depth analysis which provides pointers to further required research.
The paper is concluded with numerical examples in Section~\ref{sec:examples} where the performance of our method is demonstrated with a very smooth and a less smooth field.
