\documentclass[a4paper,12pt,english]{amsart}
\usepackage{wiaspreprint}

\usepackage[leqno]{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{calc}
\usepackage{calc} % simple arithmetics in latex
\usepackage{txfonts}
\usepackage{enumitem}
\usepackage{ifpdf} % define \ifpdf
\usepackage{ifthen}
\usepackage{braket}
\usepackage{todonotes}
\usepackage[all]{xy}
\ifpdf
  \usepackage[pdftex,
  	bookmarks=true,
	unicode=true,
	pdfborder={0 0 0},
	pdfstartview={FitV},
	pdfpagelayout={TwoPageRight},
	pdftitle={Solving SPDEs with Monte Carlo methods based on stochastic
  representations},
	pdfauthor={Felix Anker, Christian Bayer, Martin Eigel, Marcel Ladkau, Johannes Neumann and John Schoenmakers}, 
	colorlinks=false,
	linktoc=all,
	plainpages=false,
	pdfpagelabels,
	pdffitwindow=true]{hyperref}
\else
  \usepackage[dvips]{hyperref}
\fi

% theoremstyles

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
% \newtheorem{algorithm}[theorem]{Algorithm} % or seperate counting for
                                % algorithms ?
\usepackage[ruled,norelsize]{algorithm2e}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{assumption}[theorem]{Assumption}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{question}{Question}

% mathematical commands

% symbols for important sets
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\dom}{\mathcal{D}}
\newcommand{\Rplus}{[0,\infty[}
\newcommand{\Ocal}{\mathcal{O}}

% abbreviations for often used latex commands
\newcommand{\f}[2]{\frac{#1}{#2}}
\newcommand{\pa}{\partial}
\newcommand{\half}{\f{1}{2}}
\newcommand{\pderiv}[1]{\f{\pa}{\pa #1}}

% often used variables
\newcommand{\dx}{\Delta x}
\newcommand{\dt}{\Delta t}
\newcommand{\dW}{\Delta W}
\newcommand{\epsd}{\epsilon_\textrm{disc}}

% often used variables with overline
\newcommand{\barX}{\overline{X}}
\newcommand{\barY}{\overline{Y}}
\newcommand{\barZ}{\overline{Z}}
\newcommand{\bartau}{\overline{\tau}}
\newcommand{\bartheta}{\overline{\theta}}
\newcommand{\barp}{\overline{p}}
\newcommand{\barrho}{\overline{\rho}}
\newcommand{\baru}{\overline{u}}
\newcommand{\barv}{\overline{v}}
\newcommand{\barx}{\overline{x}}
\newcommand{\barlambda}{\overline{\lambda}}
\newcommand{\bargamma}{\overline{\gamma}}
\newcommand{\barH}{\overline{H}}
\newcommand{\barkappa}{\overline{\kappa}}
\newcommand{\barXi}{\overline{\Xi}}
\newcommand{\barsigma}{\overline{\sigma}}
\newcommand{\barPhi}{\overline{\Phi}}

% often used variables with tilde
\newcommand{\tV}{\widetilde{V}}
\newcommand{\tv}{\widetilde{v}}
\newcommand{\tp}{\widetilde{p}}
\newcommand{\tm}{\widetilde{m}}
\newcommand{\tS}{\widetilde{S}}
\newcommand{\tlambda}{\widetilde{\lambda}}

%variables with hat
\newcommand{\hgamma}{\widehat{\gamma}}

% new composed latex constructs
\newcommand{\abs}[1]{\left\lvert#1\right\rvert} % absolute value
\newcommand{\norm}[1]{\left\lVert#1\right\rVert} % norm
\newcommand{\indic}[1]{\mathbf{1}_{#1}} % indicator function
\newcommand{\ip}[2]{\left\langle #1\,,#2\right\rangle} % inner product (< , >)
\newcommand{\rip}[2]{\left( #1\,,#2 \right)} % ip with round brackets ( ( , ) )
\newcommand{\floor}[1]{\left\lfloor#1\right\rfloor} % floor command
\newcommand{\ceil}[1]{\left\lceil#1\right\rceil} % ceiling command
\newcommand{\angbrack}[1]{\left\langle #1 \right\rangle} % angle brackets


% new mathematical operators

\DeclareMathOperator{\const}{const}
\DeclareMathOperator{\tol}{TOL}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator*{\argmin}{arg\, min}
\DeclareMathOperator{\sign}{sign}
\DeclareMathOperator{\id}{id}
\DeclareMathOperator{\cor}{cor}
\DeclareMathOperator{\var}{var}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\diag}{diag}

% A left hand side super- or subscript; usage: \Prefix^{h}{W} or
% \Prefix_{h}{W}
%\newcommand{\Prefix}[3]{\vphantom{#3}#1#2#3}

% \title{Solving SPDEs with Monte Carlo methods based on stochastic
%   representations}
\title{SDE based regression for random PDEs}

\date{December 21, 2015}

\author{Felix Anker, Christian Bayer, Martin Eigel, Marcel Ladkau, \\
Johannes Neumann, John Schoenmakers\nofnmark\footnote{Weierstrass
    Institute\\Mohrenstr. 39\\
10117 Berlin\\Germany\\ E-Mail: \{felix.anker, christian.bayer, martin.eigel, johannes.neumann, john.schoenmakers\}@wias-berlin.de}
}

%\author[F.~Anker]{Felix Anker}
%\author[C.~Bayer]{Christian Bayer}
%\author[M.~Eigel]{Martin Eigel}
%\author[M.~Ladkau]{Marcel Ladkau}
%\author[J.~Neumann]{Johannes Neumann}
%\author[J.~Schoenmakers]{John Schoenmakers}

\nopreprint{2192}  % Preprint-Nummer
\selectlanguage{english}                % hier nicht veraendern, wichtig fuer Datumsformat
\nopreyear{2015}

\subjclass[2010]{%
 35R60, % Partial differential equations with randomness, stochastic partial differential equations
 47B80, % Random operators
 60H35, % Computational methods for stochastic equations
 65C20, % Probabilistic methods, simulation and stochastic differential equations: Models, numerical methods
 65N12, % Stability and convergence of numerical methods
 65N22, % Solution of discretized equations
 65J10, % Equations with linear operators
 65C05} % Monte Carlo Methods
%
\keywords{partial differential equations with random coefficients, random PDE,
  uncertainty quantification, Feynman-Kac, stochastic differential equations,
  stochastic simulation, stochastic regression, Monte-Carlo, Euler-Maruyama}

\begin{document}

\begin{abstract}
  A simulation based method for the numerical solution of PDE with random
  coefficients is presented.  By the Feynman-Kac formula, the solution can
  be represented as conditional expectation of a functional of a
  corresponding stochastic differential equation driven by independent
  noise.  A time discretization of the SDE for a set of points in the domain
  and a subsequent Monte Carlo regression lead to an approximation of the
  global solution of the random PDE.  We provide an initial error and
  complexity analysis of the proposed method along with numerical examples
  illustrating its behaviour.
\end{abstract}

\maketitle

\section{Introduction}
\label{sec:introduction}

\input{introduction}

\section{Stochastic representations}
\label{sec:stoch-repr}

\input{stoch-repr}

\section{Regression based methods and their error analysis}
\label{sec:regression}

\input{regression}

\section{Convergence and complexity analysis}
\label{sec:conv-compl-analys}

\input{conv-compl-analys}

\section{Examples}
\label{sec:examples}

\input{examples}

\bibliographystyle{plain}
\bibliography{spde}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
