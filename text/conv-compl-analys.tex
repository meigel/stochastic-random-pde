This section is concerned with a first analysis of the convergence and complexity of the proposed numerical method.
We point out that these are only first results and a more thorough analysis
should be carried out in subsequent work.

%\subsection{Description of the simulation algorithm}

In order to clarify the proposed algorithm, we sketch a possible
implementation in pseudo code.
First, the Euler scheme for the simulation of Brownian motion is depicted in Algorithm~\ref{alg:euler}.
For the sake of simplicity, only uniform timestepping is included.
%
\begin{algorithm}[ht!]
\algo{
Field realization $\kappa$, initial value $x \in D$, time-increment $\dt$.
}{
Trajectory $\left( \barX_{t_0}, \ldots, \barX_{\bartau} \right)$ of the approximate path and approximate hitting time $\bartau$.
}{
$\barX_{t_0} \coloneqq x$, $i \coloneqq 0$\;
	
\While{$\barX_{t_i} \in D$}
{	
Simulate the increment of the Brownian motion $\dW_i \sim \mathcal{N}(0,\dt)$\;
  
$\barX_{t_{i+1}} \coloneqq \barX_{t_i} + \nabla \kappa(\barX_{t_i}) \dt + \sqrt{2\kappa(\barX_{t_i})} \dW_i$\;

$i \coloneqq i+1$\;
}
	
\Return $\left( \barX_{t_0}, \ldots, \barX_{\bartau} \right)$\;
}
\caption{Trajectory simulation of Brownian motion by Euler scheme.}
\label{alg:euler}
\end{algorithm}
%
Second, a sketch of the semi-stochastic regression algorithm is shown in
Algorithm~\ref{alg:regression} based on the Euler scheme. The deterministic regression algorithm has a very similar structure.
%
\begin{algorithm}[ht!]
\algo{
Number $N$ of samples to be computed, number $K$ of basis
  functions and basis functions $\psi_1, \ldots, \psi_K: D \to \R$, a
  probability measure $\mu$ on $D$, the matrix $\mathcal{G}$ defined
  in~\eqref{dkl}, time increment $\dt$ for use by the Euler method.}{
Solution approximation  $\barv(x) \coloneqq \sum_{k=1}^K \bargamma_k \psi_k(x)$ for any $x\in D$.
}{
Allocate an $N$-dimensional array $\mathcal{Y}$ and $N \times K$
  dimensional array $\mathcal{M}$\;
\For{
	$i = 1, \ldots, N$
}{
Sample $x_i \sim \mu$, sample realizations $\kappa_i$, $f_i$, $g_i$ from
  the distribution of the conductivity field, the source field and the
  boundary field, respectively.\;
  
Call \texttt{Euler}($\kappa_i$, $x_i$, $\dt$) Algorithm~\ref{alg:euler}, returning a pair of $\left(\barX_{t_0}, \ldots, \barX_{\bartau} \right)$ and $\bartau$.\;
    
$\Phi_i \coloneqq g_i\left( \barX_{\bartau} \right) + \int_0^{\bartau} f_i\left( \barX_s \right)\mathrm{d}s$ (with suitable quadrature)\;
  
$\mathcal{Y}_i \coloneqq \Phi_i$, $\mathcal{M}_{i,k} \coloneqq \psi_k(x_i)$, $k=1, \ldots, K$\;
  
$i \coloneqq i+1$\;
}
$\bargamma \coloneqq \f{1}{N} \mathcal{G}^{-1} \mathcal{M}^\top \mathcal{Y}$\;
\Return $\barv(\cdot) \coloneqq \sum_{k=1}^K \bargamma_k \psi_k(\cdot)$\;
}
\caption{Global regression algorithm to determine solution approximation based on point-wise samples.}
\label{alg:regression}
\end{algorithm}


Coming back to the analysis of the error,
let us first consider the case of a fixed point $x \in D \subset \R^d$, i.e.,
we want to approximate the solution $u(x)$ or $E[u(x)]$ for this particular
single point $x$.
Recall the elliptic model problem \eqref{eq:model} with stochastic coefficient $\kappa$, $\omega\in\Omega$.
Then we can naturally decompose the error into four parts:
\begin{enumerate}
\item[(i)] The error from approximating the stochastic fields $\kappa$, $g$ and
  $f$;
\item[(ii)] \label{item:disc-error} the error from the discretization of the
  SDE~\eqref{eq:SDEa} and the functional~\eqref{eq:SDEZ};
\item[(iii)] the truncation error in the regression, i.e., the error introduced by
  choosing a finite, non-dense set of basis functions;
\item[(iv)] the integration error in the regression, i.e., the error
  introduced by \emph{computing} an approximate projection of the true
  solution to the basis functions only.\label{item:int-error-reg}
\end{enumerate}

In this paper, we are mainly concerned with the second and fourth sources of
errors of the method. We believe this can be justified for pragmatic
reasons: while a quite general and convincing error and convergence analysis
can be given for all mentioned sources of errors, the influence of the first and third
error component is more difficult to describe without imposing very
specific assumptions on both the coefficients and the solution of the random
PDE. We just make the following remarks:
\begin{itemize}
\item We only consider stochastic fields with finite variance which can be represented by a Karhunen-Lo\`eve expansion.
These fields are described by a covariance function the regularity of which directly determines the smoothness of the realizations of the stochastic field.
Note that for the considered application, there always is some correlation between sufficiently close points in the domain (i.e. coloured noise).
The number of expansion terms required for an adequate approximation again directly depends on the regularity of the covariance.
If we assume an expansion~\eqref{eq:KL} of the form
\[
\kappa_\iota(x,\omega) = E[\kappa] + \sum_{m=1}^\iota \sqrt{\nu_m}\varphi_m(x)\xi_m(\omega)
\]
which is exact for $\iota=\infty$, then the truncation error for $M$ terms is determined by
\[
\norm{\kappa_\infty-\kappa_M}_{L^2(\Omega,L^2(D))}^2 \leq \sum_{m=M+1}^\infty \nu_m.
\]
The $\nu_m$ are the eigenvalues of the covariance integral operator and their decay behavior thus determines the truncation error with respect to the number of KL terms $M$.
In some cases, the decay behavior is known a priori for which we refer to~\cite{christakos2012random,MR2317004,lord2014introduction}.
Note that alternate techniques to generate field realizations can be employed equally well with our method.
For instance, turning band methods and circulant embedding are frequently used approaches, see~\cite{lord2014introduction}.
\item For a given set of basis functions $\psi_1, \ldots, \psi_K$, the
  truncation error of the regression method, denoted as \emph{bias} in
  Section~\ref{sec:regression}, is given by
  \begin{equation*}
    v(x) - v_K(x),
  \end{equation*}
  where $v_K$ denotes the projection of $v$ to $\operatorname{span}
  \set{\psi_1, \ldots, \psi_K}$,
  cf.~\eqref{eq:gamma-circ} and~\eqref{eq:v_K-projection}. Clearly, if $v \in
  L^2(D;\mu)$, then the error will converge to $0$ as $K \to \infty$, for any
  reasonable sequence $\psi_k$ of basis functions, for instance an
  orthonormal basis of $L^2(D;\mu)$. The \emph{speed} of convergence, however,
  depends on the regularity of the solution as well as the choice of
  the basis functions. For instance, if the solution is actually
  analytic, then the error decays exponentially, for good choices of basis
  functions. Algebraic convergence (with rate depending on the dimension) can
  be obtained when the solution is differentiable with square integrable
  derivatives up to a certain order. For more information we refer to
  Pinsker~\cite{pin80}.
\end{itemize}

In this paper, we assume that the true solution $v$ can be approximated by $v_K \in \operatorname{span}\set{\psi_1, \ldots, \psi_K}$ with an
error
\begin{equation*}
  \norm{v - v_K}_{L^2_\mu} \le e(K).
\end{equation*}
We remark that a similar analysis can be done for other norms, say
$\norm{\cdot}_{H^1(D)}$.
This would require a similar error analysis of the
regression method as shown for the $L^2$ norm.
We only assume that $\lim_{K\to\infty} e(K) = 0$ and that $e$ is invertible with inverse $e^{-1}$.

We first discuss the time-discretization error~(ii). By the empirically
well-established first order weak convergence of the adaptive Euler scheme,
see~\cite{dzo/moo/vSC/sze/tem05,bay/sze/tem10}, an error tolerance $\epsd$
will yield approximate solutions $\barX$, $\barY$, $\barZ$ and a corresponding
stopping time $\overline{\tau}$ such that the corresponding expected value
$v_{\text{disc}}(x) \coloneqq E\left[ g\left( \barX_{\overline{\tau}} \right)
  \barY_{\overline{\tau}} + \barZ_{\overline{\tau}} \right]$ satisfies
$\norm{v - v_{\text{disc}}}_{L^2_\mu} \le \epsd$ at a computational cost of
order $\epsd^{-1}$ \emph{on average} for computing one realization.  Notice
that the computational cost is a random variable as the hitting time at the
boundary is random.  However, uniform ellipticity and boundedness of the
domain $D$ (cf.~Assumption~\ref{ass:conditionons-FK-stoch} and
Lemma~\ref{lem:bound-hitting-times}) guarantee that the hitting time is square
integrable, both for the approximate process $\barX$ and the true solution
$X$.

We next consider the integration error in the regression, term (iv), first
concentrating on the semi-stochastic case. Recall that the outcome of the
semi-stochastic global regression $\barv = \barv_K = \sum_{k=1}^K \bargamma_k
\psi_k$ is obtained from $\bargamma = \f{1}{N} \mathcal{G}^{-1}
\mathcal{M}^\top \mathcal{Y}$, see~\eqref{gre3} and~\eqref{gre2}. By
Proposition~\ref{propv}, for instance assuming that $v = v_{\text{disc}}$ and
$\var \Phi^x$ are uniformly bounded in $x$ on $D$ (as guaranteed by
Assumption~\ref{ass:conditionons-FK-stoch}) and, for simplicity, that
$(\psi_k)_{k \ge 1}$ are orthonormal w.r.t.~$\mu$, the error is bounded by
\begin{equation}
  \label{eq:L2-stoch-reg}
  \norm{v_K - \barv_K}_{L^2\left(\Omega \times D; P \otimes \mu \right)}^2 \le
  \operatorname{const} \left( 1 + \f{2}{\sqrt{M}} + \f{1}{M} \right)
  \f{K}{N}.
\end{equation}
Clearly, $M = 1$ is the optimal choice, so we disregard other possible choices
henceforth.
\begin{remark}
  For this discussion, we note that the regression actually approximates
  $v_{\text{disc}}$, not the true solution $v$. Hence, the assumptions needed
  in Section~\ref{sec:regression} have to hold for the discretized solution
  $v_{\text{disc}}$ uniformly in $\epsd$.
\end{remark}
In total, we obtain the error decomposition
\begin{equation}
\begin{split}
  \label{eq:error-decomp}
  \norm{v - \barv_K}_{L^2\left(\Omega \times \tD; P \otimes \tmu \right)} &\le
  \norm{v - v_{\text{disc}}}_{L^2_\mu} + \norm{v_{\text{disc}} - v_K}_{L^2_\mu}
  + \norm{v_K - \barv_K}_{L^2\left(\Omega \times D; P \otimes \mu \right)} \eqqcolon \epsilon
\end{split}
\end{equation}
with overall error bound $\epsilon$.

For fixed $N$ and $K$, the computational cost of the regression part of the
algorithm is proportional to $NK$, if the cost of computing the $N$
realizations $\Phi_n^{U_n}$, $n=1, \ldots, N$, is neglected. For fixed $K$, we
need to choose $N$ proportional to $K \epsilon^{-2}$, so the computational
cost of the regression is proportional to $K^2 \epsilon^{-2}$. On the other
hand, we need to sample $N$ realizations of $\Phi^U$ at cost proportional to
$\epsilon^{-1}$ each, which amounts to cost proportional to $K
\epsilon^{-3}$. Finally, by assumption, we need to choose $K$ proportional to
$e^{-1}(\epsilon)$ to achieve a truncation error bounded by $\epsilon$.
To summarize, we obtain
\begin{proposition}
  \label{prop:complexity-sto}
  Given Assumption~\ref{ass:conditionons-FK-stoch} and a discretization error tolerance
  $\epsilon$ in
  $\norm{\cdot}_{L^2\left(\Omega \times D; P \otimes \mu \right)}$. Then the
  average computational cost $\mathcal{C}$ of the semi-stochastic global
  regression algorithm with adaptive time-discretization is bounded by
  \begin{equation*}
    \mathcal{C} \le C_1 e^{-1}(\epsilon)
    \epsilon^{-3} + C_2 e^{-2}(\epsilon) \epsilon^{-2}.
  \end{equation*}
\end{proposition}
\begin{remark}
  \label{rem:cost-stoch}
  Note that the computational cost is, superficially, independent of the
  dimension $d$. As discussed earlier, $e^{-1}(\epsilon)$ could be anything
  between, say, $\log \epsilon^{-1}$ and $\epsilon^{-d}$ (or even
  worse). Hence, it is not clear which of the two terms is dominant. One might
  expect the constant $C_1$ to be typically much bigger than $C_2$, as $C_2$
  essentially just entails a floating point multiplication, whereas $C_1$ is
  the entire computational cost of one step of the adaptive Euler scheme.
\end{remark}
\begin{remark}
  If we are only interested in the point-wise error, i.e., if we only want to
  compute $v(x)$ for one specific value $x \in D$, then we can replace the
  regression analysis by a simple Monte Carlo analysis. The computational cost
  of our method for computing $v(x)$ at tolerance $\epsilon$ in RMSE sense is
  then proportional to $\epsilon^{-3}$, independent of the dimension $d$.
\end{remark}

For the deterministic regression procedure analyzed in
  Proposition~\ref{propd} (variance) and~\eqref{eq:bias-det-reg} (bias), we
  need to replace the estimate~\eqref{eq:L2-stoch-reg} by
\begin{subequations}
\label{eq:L2-det-reg}
\begin{equation}
  \label{eq:L2-det-reg-tensor}
  \norm{v_K - \barv_K}^2_{L^2\left( \Omega \times D; P \otimes dx \right)}
  \le \const\left( \f{K}{NM} + \f{d^2}{N^{2/d}} \right),
\end{equation}
for a uniform, tensorized grid $x_1, \ldots, x_N$ and by
\begin{equation}
  \label{eq:L2-det-reg-qmc}
  \norm{v_K - \barv_K}^2_{L^2\left( \Omega \times D; P \otimes dx \right)}
  \le \const\left( \f{K}{NM} + \f{d^2}{N^{2}} \right),
\end{equation}
\end{subequations}
(ignoring logarithmic terms) for the low-discrepancy case,
where we use the special choice $\mu = dx|_{D}$.
The cost-optimal choice for $M$ will now depend on $K$ --- treated as fixed at
this stage ---, the error tolerance $\epsilon$ and the dimension $d$. The
computational cost $\mathcal{C}$ of performing the algorithm is
\begin{equation*}
  \mathcal{C} \le \const\left( M N \epsilon^{-1} + KN + K^2 \right)
\end{equation*}
on average, corresponding to the average cost of computing $MN$ samples at
accuracy $\epsilon$ by the adaptive Euler-Maruyama algorithm, the cost of
multiplying a $K\times N$-matrix with an $N$-dimensional vector and the cost
of solving a $K\times K$-linear system,
cf.~\eqref{eq:def-det-reg-coef}. Hence, one has to minimize the cost given
that the error~\eqref{eq:L2-det-reg} is bounded by $\epsilon^2$.

Let us first consider the case of a tensorized uniform grid.  We may assume
that both error contributions in~\eqref{eq:L2-det-reg-tensor} are of order
$\epsilon^2$, implying that
\begin{gather*}
  N = \const d^d \epsilon^{-d}\quad\text{ and }\quad
  M = \const \max\left( K d^{-d} \epsilon^{d-2}, 1\right).
\end{gather*}
Hence, for the computational cost it holds
\begin{equation*}
  \mathcal{C} \le \const \left(\max\left(K \epsilon^{-3}, d^d
      \epsilon^{-(d+1)}\right) + d^d K \epsilon^{-d} + K^2 \right).
\end{equation*}

No further calculation is needed for the case of $x_1, \ldots, x_N$ being
based on a low-discre\-pancy sequence, as this case essentially (up to
logarithmic terms) corresponds to the case $d=1$ in the uniform case.

\begin{proposition}
  \label{prop:complexity-det}
  Assume the conditions of Proposition~\ref{prop:complexity-sto} and an error
  tolerance $\epsilon$ in the sense of $\norm{\cdot}_{L^2\left(\Omega \times
      D; P \otimes dx \right)}$.\\
  a) If the grid $x_1, \ldots, x_N$ is a uniform, tensorized grid in
  dimension $d$,
  then the average computational cost
  $\mathcal{C}$ of the deterministic global regression algorithm with
  adaptive time-discretization is bounded by
  \begin{equation*}
    \mathcal{C} \le C_1 \max\left(e^{-1}(\epsilon) \epsilon^{-3}, d^d
      \epsilon^{-(d+1)}\right) + C_2 d^d e^{-1}(\epsilon) \epsilon^{-d} + C_3
    e^{-2}(\epsilon)
  \end{equation*}
  with constants $C_1,C_2,C_3>0$.\\
  b) Up to logarithmic terms, the above bound holds with $d=1$ regardless of the dimension of the space
  if the point set $x_1, \ldots, x_N$ has low discrepancy.
\end{proposition}
\begin{remark}
  Even in the analytic case ($e(K) \sim e^K$) we already see the curse of
  dimensionality in the deterministic regression case. It appears because of
  the inherent numerical approximation of integrals w.r.t.~$\mu$ based on the
  grid, i.e., the approximation error $\mu \approx \rho_N$, in the notation of
  Section~\ref{sec:regression}. Further, note that we have not considered
  stability restrictions ($N \gg K$) on the choice of $N$ and $K$ induced by
  the design matrix $\mathcal{N}^\top \mathcal{N}$.  Similarly to
  Remark~\ref{rem:cost-stoch}, we note that typically $C_1 \gg C_2, C_3$.
\end{remark}

\begin{table}[!htb]
  \centering
  \begin{tabular}{c|ccc}
    $e(K)$ & Semi-stochastic reg. & Det.~reg.~(tensor) & Det.~reg.~(low disc.) \\
    \hline\\[-0.35cm]
    $e^{-K}$ & $\epsilon^{-3}$ & $\max(\epsilon^{-(d+1)}, \epsilon^{-3})$ & $\epsilon^{-3}$ \\[0.1cm]
    $K^{-1/d}$ & $\epsilon^{-(3+d)}$ & $\epsilon^{-(3+d)}$ & $\epsilon^{-(3+d)}$\\[0.1cm]
  \end{tabular}
  \caption{Asymptotic computational costs for deterministic (tensor grid or low discrepancy sequence) and
    semi-stochastic regression for error tolerance $\epsilon$. Logarithmic
    terms are ignored, $C_2$ and $C_3$ are set to $0$.}
  \label{tab:comparison-complexity-1}
\end{table}

\begin{table}[!htb]
  \centering
  \begin{tabular}{c|ccc}
    $e(K)$ & Semi-stochastic reg. & Det.~reg.~(tensor) & Det.~reg.~(low disc.) \\
    \hline\\[-0.35cm]
    $e^{-K}$ & $\epsilon^{-3}$ & $\max(\epsilon^{-(d+1)}, \epsilon^{-3})$ & $\epsilon^{-3}$ \\[0.1cm]
    $K^{-1/d}$ & $\epsilon^{-(2+2d)}$ & $\epsilon^{-(3+d)} + \epsilon^{-2d}$ & $\epsilon^{-(3+d)} + \epsilon^{-2d}$ \\[0.1cm]
  \end{tabular}
  \caption{Asymptotic computational costs for deterministic (tensor grid or low discrepancy sequence) and
    semi-stochastic regression for error tolerance $\epsilon$. Logarithmic
    terms are ignored, $C_2$ and $C_3$ are kept.}
  \label{tab:comparison-complexity-2}
\end{table}


We end these theoretical complexity considerations by a sketchy asymptotic
comparison of deterministic and semi-stochastic regression techniques, see
Table~\ref{tab:comparison-complexity-1} and
Table~\ref{tab:comparison-complexity-2}. We compare the case of a very fast
decay of the error in terms of the number of basis functions $K$ with the case
of a slow small decay, i.e., the case of an analytic solution $v$ with a
merely square-integrable solution $v$. In
Table~\ref{tab:comparison-complexity-1}, we only consider the cost of
generating samples from the solution of the SDE. This is usually the dominant
contribution to the overall computational costs, as the cost of computing one
step in the stochastic Euler scheme is much larger than the cost of simple
floating point multiplications. In Table~\ref{tab:comparison-complexity-2}, we
treat all the contributions as equivalent, which is adequate for a true
asymptotic analysis. We see that the semi-stochastic regression is clearly
superior in the highly regular case, at least for $d > 2$ and it is never
worse than deterministic regression based on a uniform, tensorized grid in the realistic scenario with $C_2$ and
$C_3$ ignored. This is a consequence of the curse of
dimensionality. On the other hand, the deterministic regression algorithm
based on a low discrepancy point set $x_1, \ldots, x_N$ seems comparable to
the semi-stochastic algorithm. We should note, however,
that this very simple analysis does not take the constants into account.
Recall that all the presented algorithms are, overall, stochastic in nature.
A pure QMC version of the algorithm---i.e., an approach where the random
coefficients and the Brownian motion are replaced by their deterministic
counterpart---seems difficult due to the very high dimensionality.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "spde_paper"
%%% End:
