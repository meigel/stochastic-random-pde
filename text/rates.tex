\documentclass[a4paper]{article}

\usepackage{personal_paper}

\newcommand{\barZ}{\overline{Z}}
\newcommand{\barv}{\overline{v}}
\newcommand{\tv}{\widetilde{v}}

\begin{document}

\section{Stochastic representation}
\label{sec:stoch-repr}

Let us first consider the case of a fixed point $x \in D \subset \R^d$, i.e.,
we want to approximate the solution $u(x)$ or $E[u(x)]$ for this particular
single point $x$.

Once again, we consider the following PDE with stochastic coefficients:
\begin{subequations}
\label{eq:spde}
\begin{align}
  -\nabla \cdot \left(a(x) \nabla u(x) \right) &= g(x), \quad x \in D,\\
  u(x) &= f(x), \quad x \in \pa D.
\end{align}
\end{subequations}
Here, all the spatial functions $a, g, f$ are random variables taking values
in a suitable space of functions $D \to \R$ and $\pa D \to \R$,
respectively. The domain $D$ is assumed to be a ``nice'' open set in
$\R^d$. For notational clarity, these random variables are assumed to be
defined on a probability space $\left( \Omega_1, \mathcal{F}_1, P_1 \right)$,
and we denote the generic element of $\Omega_1$ by $\omega_1$, if
needed. Obviously, the PDE~(\ref{eq:spde}) can be re-formulated as
\begin{equation}
  \label{eq:spde-nondiv}
  a(x) \Delta u(x) + \nabla a(x) \cdot \nabla u(x) + g(x) = 0.  
\end{equation}

A stochastic representation of~(\ref{eq:spde}) will be given in terms of a
$d$-dimensional Brownian motion $W$ defined on a probability space
$\left(\Omega_2, \mathcal{F}_2, P_2 \right)$ (with $\omega_2$ as generic
element). Let
\begin{align}
  \label{eq:X}
  dX_t &= \nabla a(X_t) dt + \sqrt{2} \sqrt{a(X_t)} dW_t, \quad X_0 = x,\\
  \label{eq:Z}
  Z_t &= \int_0^t g(X_s) ds.
\end{align}
If we make the dependence on the two sources of randomness explicit, we obtain
\begin{equation*}
  dX_t(\omega) = \nabla a(X_t(\omega), \omega_1) dt + \sqrt{2}
  \sqrt{a(X_t(\omega), \omega_1)} dW_t(\omega_2),
\end{equation*}
where $\omega = (\omega_1, \omega_2)$. In fact, in order to make sense of this
SDE we understand the underlying probability space to be $\left(\Omega, \F, P
\right) = \left(\Omega_1, \F_1, P_1 \right) \otimes \left(\Omega_2, \F_2, P_2
\right)$, on which $W_t(\omega) = W_t(\omega_2)$ (for $\omega =
(\omega_1,\omega_2)$) is a standard Brownian motion independent of
$a(\cdot,\omega) = a(\cdot, \omega_1)$ and the other random functions
introduced above.

Further introducing $E_1[\cdot] = E[\cdot|\F_1]$ and $E_2[\cdot] =
E[\cdot|\F_2]$ and
\begin{equation}
  \label{eq:tau}
  \tau(\omega) = \inf\Set{t \ge 0 | X_t \notin D},
\end{equation}
we obtain the following stochastic representation for the solution:
\begin{gather}
  \label{eq:stochrep1}
  u(x) = E_1\left[ f(X_\tau) + Z_\tau \right],\\
  \label{eq:stochrep2}
  E[u(x)] = E\left[ f(X_\tau) + Z_\tau \right].
\end{gather}

\section{Regression}
\label{sec:regression}

Let us now discuss how to incorporate the regression into our setup. 

\section{Error decomposition and rates}
\label{sec:error-decomp-rates}

We assume that we want to compute $v(x) \coloneqq E[u(x)]$. Then we can
naturally decompose the error into three parts:
\begin{enumerate}
\item The error from approximating the stochastic fields $a$, $g$ and $f$;
\item the error from the discretization of the SDE~(\ref{eq:X}) and the
  functional~(\ref{eq:Z});
\item the error from the numerical integration, i.e., the Monte Carlo error.
\end{enumerate}

We first address the first error contribution coming from the finite
dimensional approximation of the random fields. Consider an expansion (for
instance, of Karhunen-Loeve type) of $a$, $g$ and $f$, respectively. We
truncate the infinite expansion at a level $K$ and obtain approximations
$a_k$, $g_k$ and $f_K$. Moreover, we denote the solution of the
PDE~(\ref{eq:spde}) with $a$ replaced by $a_K$, $g$ replaced by $g_K$ and $f$
replaced by $f_K$ by $u_K = u_K(x,\omega_1)$, $x \in D, \omega_1 \in
\Omega_1$. 

The idea of this step is that these functions $a_K(x,\omega_1)$ are elements
of a finite dimensional space of functions on $D$, i.e., can be represented in
the form
\begin{equation*}
  a_K(x,\omega_1) = \sum_{i=1}^K \xi_i(\omega_1) e_i(x), \quad x \in D
\end{equation*}
for some random variables $\xi_1, \ldots, \xi_K$. We do not discuss the
specifics of the expansions, but we assume convergence of $a_K$, $g_K$, $f_K$
to $a$, $g$, $f$ with a certain rate in $K$, respectively. Again, ignoring
specifics of the expansion, we assume that we are given $\alpha_1 > 0$ such
that for $v_K(x) \coloneqq E[u_K(x)]$, $x \in D$,
\begin{equation}
  \label{eq:KL-error}
  \abs{v(x) - v_K(x)} \le C_1 K^{-\alpha_1}.
\end{equation}

The second error contribution comes from the discretization of the SDE. Let us
assume a discretization controlled by some parameter $\dt$. This will
typically be the step-size of a uniform Euler scheme, but we do not rule out
the possibility of using more sophisticated schemes or adaptive, non-uniform
time-grids. Let $\barX_t = \barX^{(K,\dt)}_t$ denote the discretization of the
solution of~(\ref{eq:X}) for $a$ replaced by its approximation $a_K$ and
$\barZ_t = \barZ^{(K,\dt)}_t$ the corresponding approximation
of~(\ref{eq:Z}). Moreover, let $\bartau = \bartau^{(K,\dt)}$ denote the
corresponding approximation of the stopping time $\tau$. Note that typically
the discretization error will be dominated by the error for the approximation
of $X$, hence we can essentially neglect the contribution of $Z$. Let us now
define
\begin{equation*}
  \tv_{K,\dt}(x) \coloneqq E\left[ f\left( \barX_{\bartau} \right) +
    \barZ_{\bartau} \right].
\end{equation*}
Results from the numerical analysis of stochastic differential equations give
rates for the (weak) convergence, i.e., for the convergence of $\tv_{K,\dt}$
to $v_K$ as $\dt \to 0$ for \emph{fixed} $K$. However, note that the constants
typically depend on the regularity of the coefficients of the SDE, which in
our situation depends on $K$. For instance, for the classical Euler scheme,
the constant will depend on the Lipschitz constant of the driving vector
fields, i.e., of (the expected values of) the Lipschitz constants of
$a_K(\cdot)$ and $\nabla a_K(\cdot)$. For a Karhunen-Loeve expansion of a
non-differentiable stochastic field $a$, these Lipschitz constants will
explode with a certain rate in $K$, say $\alpha_2 \ge 0$ (to also allow smooth
random fields $a$). Let us, moreover, assume that the weak rate of convergence
of the discretization scheme for given $K$ is $\beta > 0$ in $\dt$. Then we
have
\begin{equation}
  \label{eq:disc-error}
  \abs{v_K(x) - \tv_{K,\dt}(x)} \le C_2 K^{\alpha_2} \dt^{\beta}.
\end{equation}
Note that for the standard Euler scheme we expect to have $\beta = \half$,
which can be improved to order $\beta = 1$ by random adaptive time-grids.

Finally, consider the error induced by the integration scheme, i.e., by the
approximation
\begin{equation*}
  \tv_{K,\dt}(x) \approx \barv_{K,\dt,M}(x) \coloneqq \f{1}{M} \sum_{i=1}^M
  f^{(i)}\left( \barX_{\bartau^{(i)}}^{(i)} \right) + \barZ_{\bartau^{(i)}}^{(i)},
\end{equation*}
where $f^{(i)}\left( \barX_{\bartau^{(i)}}^{(i)} \right) +
\barZ_{\bartau^{(i)}}^{(i)}$ is the $i$'th independent realization of the
random variable $f\left( \barX_{\bartau} \right) + \barZ_{\bartau}$. Note that
here we draw independent sample from the Brownian motion $W$ but also from the
random fields $a_K$, $g_K$, $f_K$ (hence the superscript appearing in $f$ as
well). Alternatively, we can also use carefully chosen deterministic
realizations from a low-discrepancy sequence, in which case $\barv$ would
denote the corresponding quasi Monte-Carlo estimator. We assume that the rate
of convergence is given by $\gamma$ in terms of $M$, where we expect $\gamma =
\half$ for the Monte-Carlo setting and $\gamma = 1$ for the quasi Monte-Carlo
setting. In the Monte-Carlo setting it makes sense to assume that the
corresponding constant factor for the error does not depend on either $K$ or
$\dt$. (Indeed, this is essentially saying the limiting objects have a finite
variance.) We (optimistically) make the same assumption in the QMC setting.
Then we get the error contribution
\begin{equation}
  \label{eq:MC-error}
  \abs{\tv_{K,\dt}(x) - \barv_{K,\dt,M}(x)} \le C_3 M^{-\gamma},
\end{equation}
which is understood in RMSE sense for the Monte-Carlo case.

Hence, the total error (in RMSE sense) can be bounded by
\begin{equation}
  \label{eq:total-error}
  \abs{v(x) - \barv_{K,\dt,M}(x)} \le C_1 K^{-\alpha_1} + C_2 K^{\alpha_2}
  \dt^{\beta} + C_3 M^{-\gamma}.
\end{equation}

\begin{remark}
  Let us note that the proposed scheme \emph{converges} to the true solution
  $v(x)$ under minimal requirements. Indeed, assume that
  \begin{itemize}
  \item (conditions guaranteeing that $v_K(x) \to v(x)$);
  \item for any $K$, we have that $a_K$ and $\nabla a_K$ are a.s.~Lipschitz
    continuous with Lipschitz constants in $L^p$ (which $p$?);
  \item for any $K$, $f_K$ and $g_K$ are a.s.~polynomially bounded with
    coefficients in $L^p$ (which $p$?).
  \end{itemize}
  Then for any $\epsilon > 0$ we can find $K$, $\dt$ and $M$ such that
  \begin{equation*}
    E\left[\abs{v(x) - \barv_{K,\dt,M}(x)}^2\right] \le \epsilon^2.
  \end{equation*}
  Thus, there is a curve $K(\epsilon)$, $\dt(\epsilon)$, $M(\epsilon)$, along
  which we have convergence of $\barv_{K,\dt,M}(x)$ to $v(x)$ in $L^2$.
\end{remark}

\begin{remark}
  It is quite straightforward to estimate the \emph{complexity} for given
  parameters $K$, $\dt$ and $M$. Assuming the cost for computing one
  trajectory $(\barX,\barZ)$ of the approximate solution to the SDE is
  proportional to $1/\dt$ (clearly true for Euler or higher order schemes
  based on uniform grids, not necessarily true for adaptive algorithms), then
  the total complexity can be bounded by
  \begin{equation}
    \label{eq:complexity}
    C(K, \dt, M) \le C_4 K \dt^{-1} M.
  \end{equation}
  (Strictly speaking, this is not correct, as the complexity is
  \emph{random}, since we have to solve the SDE up to the random stopping time
  $\bartau$. However, this estimate is true in expectation.)
\end{remark}

\begin{remark}
  Note that neither the rates of convergence nor the complexity estimates
  depend on the dimension $d$.
\end{remark}

\section{Examples of total rates}
\label{sec:examples-total-rates}

\subsubsection*{Standard Euler-Monte Carlo: the worst case scenario}

If we combine a standard uniform Euler scheme with classical Monte Carlo
simulation, then we have $\beta = \half$ and $\gamma = \half$. Moreover,
assume that the true random fields $a$, $f$, $g$ are log-normal with
covariance function 
\begin{equation*}
  C(x,y) = \exp\left( \abs{x-y} \right).
\end{equation*}
Then it is argued in the literature that the usually observed rate of
convergence for the solution based on the truncated Karhunen-Loeve expansion
is of the order $1$, whereas the rate of convergence of the Karhunen-Loeve
expansion itself (i.e., the rate of convergence of $a_K$ to $a$) is only
$\half$. Hence, we may suppose $\alpha_1 = 1$ (or, more conservatively,
$\alpha_1 = \half$). However, the Lipschitz constant of $\nabla a$ is easily
seen to be of order $K^2$, i.e., $\alpha_2 = 2$. Hence, the total error
estimate is
\begin{equation}
  \label{eq:total-error-worstcase}
  \abs{v(x) - \barv_{K,\dt,M}(x)} \le C_1 K^{-1/2} + C_2 K^{2}
  \dt^{1/2} + C_3 M^{-1/2}.
\end{equation}
Equating all three terms with a given error tolerance $\epsilon$, we find that
\begin{equation*}
  K \sim \epsilon^{-2}, \quad, \dt \sim \epsilon^5, \quad M \sim \epsilon^{-2},
\end{equation*}
and thus the total complexity would be of order
\begin{equation}
  \label{eq:complexity-worstcase}
  C \sim \epsilon^{-9}.
\end{equation}

\begin{remark}
  Maybe a fully adaptive algorithm, which also takes the variations and
  (almost) singularities in the fields $a$ and $\nabla a$ into account could
  reduce the overall complexity significantly even in this case. Moreover, we
  should, of course, couple the whole method with multi-level Monte Carlo.
\end{remark}

\begin{remark}
  Note that using the full random fields $a$, $f$, $g$ instead of their
  Karhunen-Loeve expansions would not only be impossible to implement on the
  computer in this case, but the stochastic representations would not even
  exist due to insufficient regularity. This is the reason why we have to
  choose ever finer discretization grids for the Euler scheme. Indeed, it is
  somewhat remarkable that the scheme converges at all!
\end{remark}

\subsubsection*{Standard Euler-Monte Carlo: the best case scenario}

Here we still use the standard Monte Carlo method, but we assume that
\begin{itemize}
\item the stochastic fields $a$, $g$ and $f$ are smooth, i.e., we can directly
  implement them without need of approximation ($\alpha_1 = \infty$) and the
  Lipschitz constant of $\nabla a$ is constant ($\alpha_2 = 0$);
\item we use an adaptive Euler scheme with weak convergence rate $\beta = 1$.
\end{itemize}

Then we are in a fully standard SDE discretization problem, and the overall
error is of order
\begin{equation}
  \label{eq:total-error-bestcase}
  \abs{v(x) - \barv_{K,\dt,M}(x)} \le C_2 \dt + C_3 M^{-1/2}.
\end{equation}
By choosing
\begin{equation*}
  \dt \sim \epsilon, \quad M \sim \epsilon^{-2}
\end{equation*}
we obtain the classical Euler-Monte Carlo complexity
\begin{equation}
  \label{eq:complexity-bestcase}
  C \sim \epsilon^{-3},
\end{equation}
which could immediately could be improved to $\mathcal{O}\left( \epsilon^{-2}
\right)$ by either using multi-level (up to a logarithmic term) or quasi Monte
Carlo. 


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
