% !TeX encoding = UTF-8
% !TeX spellcheck = en_US


\begin{figure}
	\centering
	\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/input/col1_res0_solution_surf_uni}
	\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/input/col1_res0_variance_surf_uni}
	\caption{The mean (left) and the variance (right) of the solution for the numerical example.}
	\label{fig:mean_var}
\end{figure}



This section is concerned with the illustration of the presented adaptive sampling method based on some numerical benchmark problems. The visualizations show properties of the unique feature of this approach, namely its proper separation of the spatial and stochastic domain. This allows to choose the number of samples locally based on the variance in any part of the spatial domain as well as to exploit sparsity with respect to the sampling locations. For comparison purposes we compute the solution for the experiments with a fixed number of samples for all the nodal points in the domain using uniform and adaptive meshes and we use different numbers of samples for the same meshes.


%It is a common modeling assumption that the random fields are Gaussian.
%They are then completely specified by the first two moments.
%In fact, any stochastic field $\kappa:\Omega\times D\to\mathbb{R}$ with finite variance can be represented by an expansion of the form
%\begin{equation}\label{eq:KL}
%\kappa(x,\omega) = E[\kappa] + \sum_{m=1}^\infty a_m(x) \xi_m(\omega),
%\end{equation}
%where the product in the sum separates the dependence on $\omega\in\Omega$ (a random event) and $x\in D$ (a point in the spatial domain) with \update{uncorrelated} random variables $\xi_m$, \update{which are also independent in the case of Gaussians}, and spatial functions $a_m$.
%A typical approach to obtain such a representation is the Karhunen-Lo\`eve expansion (KL) which will be used in the numerical examples with a finite number of terms (truncated KL)~\cite{ghanem2003stochastic,lord2014introduction}.
%In this case, the basis $a_m$ consists of eigenfunctions of the covariance integral operator weighted by the \update{square roots of the} eigenvalues of this operator.
%The smoothness of the $a_m$ is directly related to the smoothness of the covariance function used to model the respective stochastic field, see e.g.~\cite{christakos2012random,MR2317004,lord2014introduction}.

Consider the input data for the problem in \eqref{eq:model} with $f = \Delta u^*$ and
\begin{align*}
u^* = 5 x_1^2 (1 - x_1)^2 (e^{10 x_1^2} - 1) x_2^2 (1 - x_2)^2 (e^{10 x_2^2} - 1),
\end{align*} 
\update{together with homogeneous Dirichlet boundary data $u \equiv 0$ on $\partial D$.}

\update{A common representation of (Gaussian) random fields is the Karhunen-Loe\`eve expansion, see e.g.~\cite{ghanem2003stochastic,lord2014introduction}. In general, given the covariance function of a random field, it is an $L^2$ converging expansion in the eigenfunctions of the covariance integral operator weighted by the square roots of the eigenvalues of this operator. For the experiments, we assume an expansion of this type\footnote{Note that any other approach to generate random field realizations could be employed similarly.}, which then reads as}
\begin{align*}
\kappa(x,\varphi) := \frac{c_a}{\alpha_{\min}} \left( \sum_{m=1}^{t_a} a_m(x) \varphi_m + \alpha_{\min} \right) + \varepsilon_a.
\end{align*}
Here, the $\phi_m$ are \update{standard uniformly distributed uncorrelated} random variables \update{in [-1,1]}. The parameters $c_a, \varepsilon_a > 0$ and the truncation length $t_a\in\mathbb{N}$ control the properties of the random field. The coefficients $a_m$ are defined for $m = 1,2,...$ for the parameters $\sigma_\alpha > 1$ and $0 < A_\alpha < 1/\zeta(\sigma_\alpha)$ with the Riemann zeta function $\zeta$ by
\begin{align*}
\begin{split}
a_m(x) &= \alpha_m \cos(2\pi \beta_1(m) x_1)\cos(2\pi \beta_2(m) x_2),\quad
\alpha_m = A_\alpha m^{-\sigma_\alpha},\\
\beta_1(m) &= m - k(m) (k(m) + 1) /2,\quad
\beta_2(m) = k(m) - \beta_1(m),\\
k(m) &= \lfloor -1/2 + \sqrt{1/4 + 2m}\rfloor.
\end{split}
\end{align*}
In our experiment we choose the setting $\sigma_\alpha = 2, A_\alpha = 0.6, c_\kappa = 1, \varepsilon_\alpha = 0.5 \cdot 10^{-3}$ and $t_\alpha = 5$\update{, which ensures uniform ellipticity of the PDE}. Together with the prescribed right-hand side $f$ this results in strong oscillations in the corner $(1,1)$ of the unit square together with a high variance of the solution in the same region. The resulting mean $\m{E}{u}$ and variance $\m{Var}{u}$ are depicted in Figure~\ref{fig:mean_var} as computed by a standard Monte Carlo finite element method.

The algorithm automatically chooses the number of samples locally and applies mesh adaptivity at the same time. Plotting the numbers of samples for each vertex of the mesh as a $P_1$ projection thus gives a false impression of the distribution of the computational effort in the spatial domain. Hence, we define the additional quantity \update{$\mathcal C_T$ piecewise on each element $T \in \mathcal T$ as the mean over the elements three nodes as}
\begin{align}\label{eq:N4area}
\mathcal C_T = \frac{1}{3\abs{T}} \sum_{i=1}^3 N_i
\end{align}
which gives the average number of samples used for each element weighted by the size of that element as a measure for the computational effort per area.

In the experiments we set the minimum number of samples $N_{\min}$  for each vertex as 100. We continue the algorithm until our error heuristic drops below the prescribed value $\lnorm{\mathtt e^{\mathrm{MS}}}_{H^1(D)} = \lnorm{\m{E}{u} - \est{E}{MS}{\vec{M,N}}{u_h}}_{H^1(D)} \leq 2 \cdot 10^3$. We consider four experiments. The first uses a fixed number of samples for each level and uniform meshes, the second one applies adaptive meshes, the third experiment combines uniform meshes with locally chosen numbers of samples, and the fourths experiment uses both adaptive meshes and local sample adaptivity. 

\update{All computations have been carried out in parallel on fourteen core Intel Xeon E5-2697 v3 shared memory systems with 2.6 GHz clock frequencies running OpenSUSE using a combination of C++ programs for the solution of the SDE and Python with the FEniCS toolbox for the finite element related computations and the overall control.}

\begin{figure}[ht!]
	\centering
	\begin{tabular}{cc}
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_0_nSamp} &
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_1_nSamp}\\
		uniform mesh / uniform samples &
		uniform mesh / adaptive samples\\
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_2_nSamp} &
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_3_nSamp}\\
		adaptive mesh / uniform samples & 
		adaptive mesh / adaptive samples
	\end{tabular}
	\caption{The number of samples for each vertex for different methods.}
	\label{fig:sampels4vertex}
\end{figure}




The resulting numbers of samples are plotted in Figure~\ref{fig:sampels4vertex}. \update{The differences in the number of samples between two uniform samples approaches is due to the variance in the approximation of the error contributions in the heuristics. The weighting parameter $\delta$ in \eqref{eq:spdeDelta} has to be chosen such that this estimate is sufficiently stable.} Note that already a computational advantage is visible for the adaptive sampling variants whereas both uniform sampling experiments falsely appear to be similar in effort. This changes if one considers the plots of $\mathcal C_T$ shown in Figure~\ref{fig:compEff4area}. The adaptive mesh reduces the amount of vertices in the smooth regions of the solution whereas the local sample number reduces the amount of samples for vertices with a small variance. In the optimal case of the combined method, the adaptivity eliminates a lot of expensive vertices with a high number of samples and the local sampling reduces the amount of samples needed in areas with a highly oscillating mean.

The maximum computational effort is in the same order of magnitude for all four experiments but the computational effort $\mathcal C_T$ drops dramatically away from the $(1,1)$-corner in all but the first experiment. This effect is more pronounced for the two experiments with mesh adaptivity. The last experiment improves this even further as fewer samples are used for the small triangles which are closer to $(0.5,1)$ and $(1,0.5)$.

The above observations correspond with the convergence in the $H^1$ norm with respect to the measured processor time as presented in Figure~\ref{fig:convergence4timeH1}. The slowest method uses uniform meshes and applies a constant number of samples for all vertices in the domain. Local numbers of samples reduce the time almost nine-fold whereas the mesh adaptivity gives an improvement factor of 80. The combination of the two methods gives the fastest algorithm with an increase in speed of almost two orders of magnitude.

In Figure~\ref{fig:convergence4timeL2} the convergence of the error in the $L^2(D)$ norm is plotted with respect to the same computational effort. Here again, a uniform mesh with constant sample numbers results in the slowest method. Contrary to the last graph, all other methods perform very similarly to each other with an approximate improvement of 8-10 compared to the slowest approach.

\begin{figure}[p]
	\centering
	\begin{tabular}{cc}
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_0_n4area} &
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_1_n4area}\\
		uniform mesh / uniform samples &
		uniform mesh / adaptive samples\\
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_2_n4area} &
		\includegraphics[width=0.45\textwidth]{pics/ExponentialBuckle/compare/singlePlots/tc_3_n4area}\\
		adaptive mesh / uniform samples & 
		adaptive mesh / adaptive samples
	\end{tabular}
	\caption{The weighted computational effort $\mathcal C_T$ from \eqref{eq:N4area} for each element $T \in \mathcal T$ for different methods.}
	\label{fig:compEff4area}
\end{figure}


\begin{figure}[p]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/ExponentialBuckle/compare/compareH1}
	\caption{Convergence in the $H^1$ norm for the different methods. The abbreviations are \texttt{aFEM} for finite element adaptivity and \texttt{aV} for the localized adaptive number of samples.}
	\label{fig:convergence4timeH1}
\end{figure}
\begin{figure}[p]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/ExponentialBuckle/compare/compareL2}
	\caption{Convergence in the $L^2$ norm for the different methods. The abbreviations are \texttt{aFEM} for finite element adaptivity and \texttt{aV} for the localized adaptive number of samples.}
	\label{fig:convergence4timeL2}
\end{figure}

In the final example we consider the weight for the goal functional in \eqref{eq:qoi}
\begin{align*}
\update{q} = \begin{cases}
C_g \epsilon_g^{-2} \exp \bc{\frac{-1}{1- \lnorm{x_0 - x}^2 \epsilon_g^{-2}}} &\text{if} \lnorm{x_0 - x}_{L^2(D)} \leq \epsilon,\\
0 &\text{else.}
\end{cases}
\end{align*}
We choose the location $x_0 = \bc{0.6,0.8}$, radius $\epsilon_g = 0.1$, and weight $C_g = 2.14$.
In Figure~\ref{fig:convergence4goal} the effect of the different adaptive methods is shown. The greatest benefit stems from the goal-weighted adaptive finite element method by comparing uniform methods with global adaptivity as in the previous example and the goal-weighted adaptive methods. Compared to uniform refinements it is up to two orders of magnitude faster in achieving the same error level. Local adaptive variance and its goal-weighted variant add upon this improvement. With global finite element adaptivity no convergence with respect to the goal is observed. 

\begin{figure}[p]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/goal/compare/compareQ}\\
	\caption{Convergence for the goal error for the different methods. The abbreviations are \texttt{aFEM} for finite element adaptivity, \texttt{aFEMg} for goal-weighted finite element adaptivity, \texttt{aV} for the localized adaptive number of samples and \texttt{aVg} for the goal-weighted localized adaptive number of samples.}
	\label{fig:convergence4goal}
\end{figure}
