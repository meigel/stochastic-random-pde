To achieve convergence of the method presented in Section~\ref{sec:sampling} by means of an adaptive algorithm, all error components in the decomposition \eqref{eq:spdeInftyError} have to converge separately.
For optimal convergence, for all contributing parts the same rate should be achieved with respect to the computational effort that has to be invested to gain the error reduction.
The main idea of an adaptive algorithm is therefore to base the parameter choices in some way on the underlying mesh.
Hence, in the following it is the goal to define some optimal sequence of meshes $\mathcal{T}_0\subset\ldots\subset\mathcal{T}_L$ for the interpolation and then to choose appropriate values for the other discretization parameters.

We use the convenience notation $a\lesssim b$ (or $a\approx b$) to state that $a\leq Cb$ (or $C^{-1}b=a=Cb$) with a constant $C>0$ independent of the discretization parameters.

Our starting point is an initial quasi-uniform triangulation $\mathcal{T}_0$ of the spatial domain $D$.
The first step is to calculate a discrete solution $\est{E}{MS}{\vec{M_0,N_0}}{u_0}$.
The parameters $\vec{M_0}$ and $\vec{N_0}$ have to be guessed as not enough information is available on the initial level\footnote{the subscript denotes the refinement level for which the parameters or functions are considered}.
The next step involves the calculation of some finite element error indicator $\eta_0$ which we briefly describe.

One of the most common and simple a~posteriori FE error estimation techniques is given by the residual error estimator.
We define the local error indicator $\eta_{h,T}$ on some element $T\in\mathcal{T}_h$ of size $h_T$ subject to an approximation $u_h$ of the true solution $u$ by
\begin{align}
\label{eq:eta}
\eta_{h, T}^2 = h_T^2 \lnorm{f}^2_{L^2(T)} +  \sum_{E \in \mathcal E(T)} h_T \lnorm{\grad u_h \cdot n_E}^2_{L^2(E)}.
\end{align}
Recall that the set of edges of $\mathcal{T}_h$ is denoted $\mathcal{E}_h$ and the edges of some element $T$ are denoted $\mathcal{E}(T)$.
More details and a proper derivation can \eg be found in~\cite{cehl:12} and the references therein.

Once a local error indicator $\eta_{h,T}$ is readily available, a mesh refinement strategy can be chosen which selects a set of elements $\mathcal{M}_h\subset\mathcal{T}_h$ such that an error reduction is achieved for the approximate solution on the finer mesh.
A common choice is the so-called bulk or D\"orfler marking defined for some fraction parameter $0<\theta\leq 1$ by
\begin{align}
\label{eq:bulk}
\sum_{T \in \mathcal{M}_h} \m{E}{\eta_{h,T}}^2 \geq \theta \sum_{T \in \mathcal{T}_h} \m{E}{\eta_{h,T}}^2.
\end{align}
We also employ this marking and refer to~\cite{dorfler1996convergentAFEM} for further details, in particular with regard to error reduction properties.


As can be seen in \eqref{eq:spdeInftyError}, the estimator only covers the first term $\lnorm{\m{E}{u} - \m{E}{u_0}}_{L^2(T)}$ and thus the parameters $\vec{M_0}$ and $\vec{N_0}$ have to be chosen such that 
\begin{align*}
h_T \eta_{0,T} \gg \lnorm{\mathcal{I}_h\m{E}{u} - \m{E}{u_h^M}}_{L^2(T)} + \abs{T}\max_{K \in \mathcal{N}(T)}\bp{\m{Var}{u_h^M}^{1/2} N_K^{-1/2}}.
\end{align*}
The reason for this is that the finite element solution is bounded by the interpolator but the stochastic properties of the discrete solutions introduce oscillations of length $h$ and amplitudes as seen above.
These can be unbounded in principle and artificially increase the finite element error estimator.
As refinement would not reduce these oscillations, we have to limit them by means of the central limit theorem such that we can get reliable mesh refinements.
In fact, the stochastic error term from \eqref{eq:spdeInftyError} for each element has to be smaller than the smallest error estimator $\eta_{\ell,T}$ chosen for the refinement set $\mathcal{M}_\ell$ (on refinement levels $\ell=1,\ldots,L$) given by
\begin{align}\label{eq:sdpeEstMinSampError}
\min_{T \in \mathcal{M}_\ell} \eta_{0,T} > \abs{T}\max_{K \in \mathcal{N}_\ell(T)}\bp{\m{Var}{u_h^M}^{1/2} N_K^{-1/2}} + \lnorm{\mathcal{I}_h\m{E}{u} - \m{E}{u_h^M}}_{L^2(T)}.
\end{align}
This constraint also includes the error of the approximation in the Euler scheme but it is deterministic, smooth, and global with respect to the spatial domain $D$.
As a result, it alters the error estimator only to some minor extent.

The derived refinement $\mathcal{T}_1$ of the initial mesh $\mathcal{T}_0$ is the basis for the next level and the process is repeated.
Heuristics based on educated guesses of the error components allow to balance the parameters in actual computations.
This however is only possible if there are at least three meshes, that is $L \geq 3$.
Section~\ref{sec:spdeHeuristics} covers some approaches to this topic in more detail.


\subsection{A practical parameter selection strategy}
\label{sec:spdeHeuristics}

The error decomposition in \eqref{eq:spdeInftyError} results in three components that need to be balanced for guaranteed and optimal convergence.
We hence aim at finding good estimates for the convergence rates with respect to the relevant parameters and then extrapolate the error estimates to the next level.
With these, we can approximate parameters that fulfill the balancing requirements.
The first task is the approximation of the convergence rate of the interpolation error subject to the current mesh and controlled through the parameter $\alpha$.
Let $h$ be the minimal inradius over all the triangles of the triangulation $\mathcal T_h$ and suppose we have some $\alpha > 0$ such that
\begin{align*}
\lnorm{\m{E}{u} - \m{E}{\mathcal{I}_h u}}_{L^2(D)} \lesssim h^\alpha.
\end{align*}
Standard finite element theory gives
\[
\lnorm{\m{E}{u} - \m{E}{\mathcal{I}_h u}}_{L^2(D)} \sim \lnorm{\m{E}{u} - \m{E}{u_h}}_{L^2(D)}.
\]
Thus, with some efficient and reliable error estimate $h\eta_h \sim \lnorm{\m{E}{u} - \m{E}{u_h}}_{L^2(D)}$, we get $h\eta_h \sim h^\alpha$.
We exploit this property to gauge the parameter $\alpha$ as error estimators have smoothing properties and thus in practice exhibit a behavior closer to a monotonic convergence.
For the estimation $\alpha_L$ of $\alpha$, we carry out a linear regression over data points
$(\log(h_\ell), \log(h \eta_\ell))_{\ell = 1,\ldots,L}$.

Next, we estimate the expected spatial error on the next level $L+1$.
For this, the triangle inequality for $\ell = 1,\ldots,L$ yields
\begin{align*}
\lnorm{\m{E}{u} - \m{E}{u_\ell}}_{L^2(D)} \leq \lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)} + \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}.  
\end{align*}
The last term on the right-hand side is computable and it remains to estimate the error on level $L$.
We assume $\lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)} \ll \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}$ for the coarser levels $\ell < L$ \update{since, for a sufficiently high convergence rate $\alpha$ and a fine mesh $\mathcal T_L$, it holds $h^\alpha_\ell/h^\alpha_L \gg 1$ and we can conclude that}
\begin{align*}
\lnorm{\m{E}{u} - \m{E}{u_\ell}}_{L^2(D)} \approx \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}
\end{align*}
for each level $\ell = 1,\ldots,L-1$.

As we already have a good approximation of the expected convergence rate $\alpha_L$, \update{for brevity of notation denoted hereafter by $\alpha$}, we can now find approximations of the error $\lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)}$ with the help of the errors on the coarser levels by
\begin{align*}
\lnorm{\m{E}{u} - \m{E}{u_\ell}}_{L^2(D)} \sim h_\ell^\alpha \qquad \text{and} \qquad \lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)} \sim h_L^\alpha.
\end{align*}
\update{To improve the readability we write $\alpha$ instead of $\alpha_L$ for the numerical approximations hereafter.} For each $\ell = 1,\ldots,L-1$, this asymptotically yields the identity
\begin{align*}
\frac{\lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)}}{\lnorm{\m{E}{u} - \m{E}{u_\ell}}_{L^2(D)}} &= \frac{h_L^\alpha}{h_\ell^\alpha}.
\end{align*}
Hence, we can construct the approximation
\begin{align*}
\lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)} &\approx \frac{h_L^\alpha}{h_\ell^\alpha} \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}.
\end{align*}

We subsequently define the estimate $\tilde{e}_L$ for the error $\lnorm{\m{E}{u} - \m{E}{u_L}}_{L^2(D)}$ on level $L$ as the arithmetic mean of the different extrapolations from the coarser levels $\ell = 1,\ldots,L-1$, by
\begin{align}
\label{eq:tilde-e}
\tilde{e}_L \coloneq \frac{1}{L - 1} \sum_{\ell = 1}^{L - 1} \frac{h_L^\alpha}{h_\ell^\alpha} \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}.
\end{align}
The same technique is used to gauge the expected error on level $L+1$.
The finer mesh $\mathcal{T}_{L+1}$ on level $\ell=L+1$ is obtained from a local refinement~\eqref{eq:bulk}.
Thus, the parameter $h_{L+1}$ can be used to generate the extrapolation $\hat{e}_{L+1}$ with the same levels $L = 1,\ldots,L-1$,
\begin{align*}
\hat{e}_{L+1} \coloneq \frac{1}{L - 1} \sum_{\ell = 1}^{L - 1} \frac{h_{L+1}^\alpha}{h_\ell^\alpha} \lnorm{\m{E}{u_L} - \m{E}{u_\ell}}_{L^2(D)}.
\end{align*}

\update{
\begin{remark}
The derived estimate $\tilde{e}_L$ for the error in the $L^2(D)$ norm is also suitable as a stopping criterion $e_\text{TOL}$ to determine the convergence of the adaptive method. This estimate is with respect to the last level $L$ for which a numerical solution has been computed. If it is smaller than the criterion $e_\text{TOL}$, the computation is stopped.
\end{remark}
}

The next step is to balance the expected Monte Carlo error with the extrapolated spatial error.
For the refinement scheme~\eqref{eq:bulk}, it is crucial to keep the Monte Carlo error well below the spatial error as otherwise this error gets picked up by the estimator which results in wrong refinement and thus unstable behavior and suboptimal convergence or even in no convergence at all.
Hence, we introduce the balancing factor $\delta$ which describes the desired relation between the two errors in~\eqref{eq:spdeDecomp} by
\begin{align}\label{eq:spdeDelta}
\delta^2 = \frac{\bc{\m{E}{u} - \m{E}{u_h^M}}^2}{N^{-1} \m{Var}{u_h^M}}.
\end{align}
A choice of $\delta = 1$ would lead to equality.
For some constant $c_N^\ell$ for each level $\ell = 1,\ldots,L$, we set the numbers of samples in the vertices $\mathcal{N}_\ell = (\nu_\ell^i)_{i=1}^{\abs{\mathcal{N}_\ell}}$ as
\begin{align}\label{eq:spdeNumSamples}
N_\ell^i \coloneq c_N^\ell \m{Var}{u_\ell^M(\nu_\ell^i)} \qquad \text{for }i = 1,\ldots,\abs{\mathcal{N}_\ell}.
\end{align}
The next task is to choose $c_N^{L+1}$ appropriately so that~\eqref{eq:spdeDelta} will be fulfilled for $h = h_{L+1}$.
In fact, applying~\eqref{eq:spdeNumSamples} to~\eqref{eq:spdeDelta} results in
\begin{align*}
\m{E}{u} - \m{E}{u_L^M} = \delta \bc{c_N^{L} \m{Var}{u_{L}^M}}^{-1/2} \m{Var}{u_L^M}^{1/2}\update{ = \delta (c_N^L)^{-1/2}}.
\end{align*}
We assume, \update{ with $u^M = \lim_{h \to 0} u_h^M$, that} $\m{Var}{u_\ell^M} \approx \m{Var}{u^M}$ for $\ell = 1,\ldots,L$ is a sufficient approximation since only a rough estimate of the variance is needed.
Taking the $L^2$ norm of the last equation and applying the variance approximation yields
\begin{align*}
c_N^L = \update{\delta^2 \abs{D}^2} \lnorm{\m{E}{u_L^M} - \m{E}{u}}_{L^2(D)}^{-2}.
\end{align*}
With the extrapolated estimate $\hat{e}_{L+1} \approx \lnorm{\m{E}{u} - \m{E}{u_{L+1}^M}}$, we get an estimate for the constant $c_N^{L+1}$ as $\hat{c}_N^{L+1} = \frac{\abs{D}^2}{\delta^2} \hat{e}_{L+1}^{-2}$.
The numbers of samples for level $L+1$ are now set according to~\eqref{eq:spdeNumSamples} by
\begin{align}\label{eq:spdeNSampsExact}
N_{L+1}^i \coloneq \hat{c}_N^{L+1} \m{Var}{u_L^M(\nu_{L+1}^i)} \qquad \text{for }i = 1,\ldots,\abs{\mathcal{N}_{L+1}}.
\end{align}
\begin{remark}
It is imperative to ensure a minimum number of samples for each $N_L^i$ in~\eqref{eq:spdeNSampsExact} since on each level a sufficient approximation of the variance $\m{Var}{u_\ell^M}$ has to be available.
This is important since otherwise the algorithm might become unstable due to severe undersampling in single points.
This would result in a bad spatial error estimation in~\eqref{eq:sdpeEstMinSampError} and thus suboptimal mesh refinement with reduced convergence rate or even lack of convergence. 

To alleviate this issue, a simple solution is to choose some $N_{\min}$ independent of all parameters and especially independent of the level $\ell$.
The practical application requires some rough estimate of the variance which can be computed alongside the expected value and thus set the numbers of samples on level $L+1$ as
\begin{align}\label{eq:spdeNSampsApprox}
\hat{N}_{L+1}^i \coloneq \max\bp{\hat{c}_N^{L+1} \est{Var}{MC}{\vec{M,N}}{u_L^M(\nu_{L+1}^i)}, N_{\min}} \quad \text{for }i = 1,\ldots,\abs{\mathcal{N}_{L+1}}.
\end{align}
Finally, it remains to choose the parameter $\Delta t$ on each level.
The influence of this factor on the error is given in the last term of~\eqref{eq:spdeInftyError}.
Since we assume linear pointwise convergence with respect to $\Delta t$, we choose the relation $h \sim \Delta t$.
In Algorithm~\ref{alg:spdeAdapt}, the overall adaptive algorithm is sketched.
The computation of the employed pointwise solution estimator $\est{E}{MS}{\vec{M,N}}{u_\ell}$ is depicted in Algorithm~\ref{alg:spdePoint}.

Note that instead of the variance based adaptive local number of samples for each vertex, one could also choose a common number of samples based on the variance $\m{Var}{u^M_L}$.
\end{remark}

\begin{remark}\label{rem:spdeDeltaParam}
In the numerical calculations one has to impose \eqref{eq:sdpeEstMinSampError} well enough, such that the algorithm becomes stable. This can be achieved by choosing $\delta < 1$.
In the experiments in Section~\ref{sec:examples}, we choose $\delta = 0.2$. This allows for
%errors in the error estimation and variance approximation which results in
a stable algorithm and only imposes a slight impact on performance.
\end{remark}

\subsection{Adaptation to a goal functional}
\label{sec:goalHeuristics}

\update{
In addition to the computation of the global solution $u$, we also consider a derived \emph{quantity of interest} $Q$ depending on the solution of the form 
\begin{align}\label{eq:qoi}
Q(u) = \m{E}{\int_D \update{q} u \d{x}},
\end{align}
with a non-negative weight function $q\in L^2(D)$, i.e. $q\geq 0$ on $D$.
For such functionals, an adaptive procedure can result in different refinements than what one gets when solving for the global solution.
This is particularly true if $q$ has a smaller support than $D$.
In the case of goal-oriented adaptive FE algorithms~\cite{ainsoden:03}, an adaptive error indicator usually is based on the solution of some dual problem in order to identify the area of influence of the goal.
However, the localized nature of the presented approach allows to quantify the error contribution directly, i.e. without any global dual solutions.
Therefore, we quantify the error in the goal evaluation with the help of the weighted norm
\begin{align*}
\norm{v}_{q} := \left(\int_D (vq)^2 \d{x}\right)^{1/2}.
\end{align*}
%In the case when the support of $\update{q}$ is smaller than $D$ we can restrict the computations to that subdomain.
In this norm, we can perform the same residual error estimation approach as in the global case described in the preceding section.
With the local norms $\norm{v}_{q,T}^2 := \int_T (vq)^2 \d{x}$ on $T\in\mathcal T$ and $\norm{v}_{q,E}^2 := \int_E (vq)^2 \d{s}$ on $E\in\mathcal E$, we define the local error contributions estimator by
\begin{align*}
\eta_{h,t,g}^2 \coloneq  h_T^2 \lnorm{f}^2_{q,T} +  \sum_{E \in \mathcal E(T)} h_T \lnorm{\grad u_h \cdot n_E }^2_{q,E}
\end{align*}
for the local mesh refinement indicator.
A similar approach is used for the local number of samples
such that instead of $\hat{N}_{L+1}^i$ in \eqref{eq:spdeNSampsApprox} we apply
\begin{align*}
\hat{N}_{L+1}^i \coloneq \max\bp{\update{q}(x_i) \hat{\update{q}}^{-1} \hat{c}_N^{L+1} \est{Var}{MC}{\vec{M,N}}{u_L^M(\nu_{L+1}^i)}, N_{\min}} \quad \text{for }i = 1,\ldots,\abs{\mathcal{N}_{L+1}},
\end{align*}
where $\hat{\update{q}}$ is the maximum of $\update{q}$ in the domain $D$ and $x_i$ is the location of vertex $i$.
%
\begin{remark}
The proposed refinement with respect to $Q$ is merely a heuristical approach without a proper analytical derivation.
A common case for a goal functional is the point evaluation of the solution $u$.
In the finite element approach, this requires an approximation by some mollifier with a small support since evaluations on a null set are not well-defined for $H^1$ functions, see~\cite{ainsoden:03}.
With our approach, this can be computed directly by the solution of the SDE at the single point instead of the global evaluation in~\eqref{eq:qoi}.
\end{remark}
}

%\vspace*{\fill}
\begin{algofloat}[ht!]
\algo{
	$\mathcal{T}_0$, $\vec{N}_{init}$, $\Delta t_0$, $e_\text{TOL}$
}{
	solution $\est{E}{MS}{\vec{M,N}}{u_L}$
}{
	\For{
		$\ell = 1,2,\ldots$
	}{
		\eIf{
			$\ell \geq 2$
		}{
			compute $\vec{N}_\ell$ according to \eqref{eq:spdeNSampsApprox}\;
		}{
			set $\vec{N}_\ell$ to $\vec{N}_0$\;
		}
		set $\Delta t$ according to $h$\;
		compute $\est{E}{MS}{\vec{M,N_\ell}}{u_\ell}$ and $\est{Var}{MS}{\vec{M,N_\ell}}{u_\ell}$ at $x\in\mathcal{N}_\ell$ with Algorithm \ref{alg:spdePoint}\;
		\If{
			\update{$\tilde{e}_L \leq e_\text{TOL}$}
		}{	
			break\;
		}
		compute $\eta_\ell$\;
		refine $\mathcal T_\ell$ with $\eta_\ell$ to get $\mathcal T_{\ell + 1}$\;
	}
	\Return $\est{E}{MS}{\vec{M,N}}{u_L}$\;
}
\caption{Adaptive algorithm for the stochastic representation \update{with stopping criterion based on $\tilde{e}_L$ from~\eqref{eq:tilde-e}}.}
\label{alg:spdeAdapt}
\end{algofloat}
