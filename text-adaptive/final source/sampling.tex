In this section we provide a brief overview of the method presented
in~\cite{sde2015}. As a motivation to the approach, we want to construct an SDE
such that the solution $u(\omega,x)$ of the SPDE at some point $x\in D$ can be
expressed as conditional expectation of some functional of the solution of an
appropriate SDE. This forms the basis of the method which is then extended to
a fully adaptive scheme.

For the sake of concreteness, we present the method specifically in the
case of Darcy's law and refer once more to~\cite{sde2015} for the general
form. For a more comprehensive treatment of the well-known basic stochastic
theory, we refer the reader for instance to \cite{mils/tret04}.



\subsection{Stochastic representations of PDEs}
\label{sec:stoch-rep-det}

% We first consider the relation of a deterministic PDE to an appropriate stochastic differential equation (SDE).
The starting point is the following SDE
\begin{subequations}
\label{eq:SDE}
\begin{align}
  \label{eq:SDEab}
  \d{X_t} = \nabla \kappa(X_t) \d{t} + \sqrt{2\kappa(X_t)} \d{W_t},\qquad
  X_0 = x,
\end{align}
where $x \in D \subset \R^d$ is a deterministic point, $W$ is a $d$-dimensional standard
Brownian motion defined on some probability space $(\Omega, \F, P)$ and
$\kappa:\Omega \times D \to \R$ is the \emph{stochastic} field of conductivity
coefficients associated to the medium. Furthermore, it is important to require
the stochastic field $\kappa$ and the Brownian motion $W$ to be
independent.\footnote{This may require to enhance the original probability
  space $\Omega$, on which $\kappa$ is defined. See~\cite{sde2015} for details.}
% $b: \R^d
% \to \R^d$, $\sigma: \R^d \to \R^{d \times d}$ are assumed to be uniformly Lipschitz
% continuous functions. We could just as easily consider a Brownian motion with
% a different dimension. 
Additionally, we shall sometimes consider the derived process
\begin{gather}
  \label{eq:SDEYZ}
  %Y_t \coloneq \exp\left( \int_0^t c(X_s) \d{s} \right),\qquad Y = 1
  Z_t \coloneq \int_0^t f(X_s) \d{s},
\end{gather}
for the (again, possibly random, but independent from $W$) source term $f :
\Omega \times D \to \R$.
\end{subequations}
If we want to stress the dependence on the initial value, we write $X^x_t
\coloneq X_t$.  The process $Z$ depends on $x$ as well and we shall write
$Z_t^x$ if we want to stress this dependence. Notice that existence and
uniqueness of solutions to the SDE can be obtained simply by applying standard
theory after conditioning on the random fields $\kappa$ and $f$, provided that
the fields are regular enough. In particular, we need to require that $\nabla
\kappa$ exists and is Lipschitz continuous in $x$ a.s. For more details on
convenient regularity assumptions we again refer to \cite{sde2015}.


The (random) Dirichlet problem~\eqref{eq:model} now admits the following
representation in terms of the solutions of the above SDE: Let $\tau = \tau_x$
denote the first hitting time of the solution $X = X^x$ (started at $x \in \overline{D}$)
at the boundary $\partial D$. Then the random solution $u(x)$ and its
expectation $E[u(x)]$ satisfy the \emph{Feynman-Kac theorem}:
\begin{equation}
  \label{eq:elliptic}
  u(x) = E\left[ \left. g\left( X^x_{\tau_x} \right) + Z^x_{\tau_x} \, \right| \, \kappa,\, f
  \right], \qquad 
  E[u(x)] = E\left[ g\left( X^x_{\tau_x} \right) + Z^x_{\tau_x} \right].
\end{equation}
This means that the random solution $u(x)$ is obtained from the random
variable $g\left( X^x_{\tau_x} \right) + Z^x_{\tau_x}$ by taking expectations
only w.r.t.~the Brownian motion $W$. On the other hand, for
$E\left[u(x)\right]$, we simply take the total expectation as there is no need
for iterating the expectations.

  For a better understanding of~\eqref{eq:elliptic}, let us note
  that the result is classical\footnote{For instance, when $\kappa, \nabla
    \kappa, f$ are Lipschitz, $\kappa$ is strictly positive and $D$ is a
    convex polygon, but these conditions can certainly be relaxed.} in the
  special case of deterministic coefficients $\kappa$ and $f$, see, for instance,
  \cite{mils/tret04}. Of course, in that case the conditioning is trivial.
  As the coefficients $\kappa$ and $f$ are assumed to be independent from the
  Brownian motion $W$ driving the SDE~\eqref{eq:SDE}, the stochastic
  representation easily extends from the deterministic to the random case
  in the sense of~\eqref{eq:elliptic}, provided that the regularity conditions
  for the deterministic case hold ``uniformly in the randomness induced by
  $\kappa$, $f$''. Specifically, if we are interested in the solution $u =
  u(\omega)$ at a \emph{fixed} realization $\kappa(\cdot, \omega)$ and $f(\cdot, \omega)$
  of the random field, we can simply apply the Feynman-Kac formula for the
  case of deterministic coefficients $\kappa(\cdot, \omega)$ and $f(\cdot,
  \omega)$. Hence, \eqref{eq:elliptic} is an immediate consequence of the
  Feynman-Kac formula for deterministic coefficients.

Equation~\eqref{eq:elliptic} can also be used to estimate the variance of the
random solution $u$, since
\begin{equation*}
  \operatorname{Var}\left[ u(x) \right] \le \operatorname{Var}\left[ g\left(
      X^x_{\tau_x} \right) + Z^x_{\tau_x} \right].
\end{equation*}
This inequality is a simple consequence of the law of total variance. Note,
however, that the inequality is usually strict. Intuitively, the difference
of the right-hand side and the left-hand side corresponds to the variance
induced by the Brownian motion $W$.

Hence, for any $x\in D$ for which we want to obtain the solution $E[u(x)]$ of~\eqref{eq:model},
we have to compute a solution of the SDE~\eqref{eq:SDE}.
We thus have to solve two problems,
\begin{enumerate}
\item[(i)] Find an approximation $\barX_N$ of $X_t$, which is actually computable.
\item[(ii)] Given such an approximation, the sought solution $E\left[ g \left(
      \barX_{\overline{\tau}}\right) + \overline{Z}_{\overline{\tau}} \right]$ is
  computed by a (quasi) Monte Carlo method, where over-lined expressions
  denote computable approximations.
\end{enumerate}

\begin{remark}
    As indicated above for the variance, higher moments of the stochastic
    representation $g\left( X^x_{\tau_x} \right) + Z^x_{\tau_x}$ generally
    only give upper bounds for the corresponding moments of the solution
    $u$ as a simple consequence of Jensen's inequality. Hence, the method
    presented in this work can only be used to compute upper bounds of higher
    moments of $u$.
\end{remark}

\subsection{Discretization of the SDE}
\label{sec:SDE discretization}

Clearly the most popular approximation method for
SDEs is a straight-forward generalization of the Euler
scheme for ODEs. Indeed, let $0 = t_0 < \cdots < t_N = t$ be a time grid,
set
\[
\dt_i \coloneq t_i - t_{i-1},\quad \dW_i \coloneq W_{t_i} - W_{t_{i-1}},\quad \dt_{\max} \coloneq \max_i \dt_i,\quad \barX_0 \coloneq x,
\]
and iteratively define as a discretization of \eqref{eq:sde}
\begin{equation}
  \label{eq:Euler}
  \barX_{i} \coloneq \barX_{i-1} + b\left( \barX_{i-1} \right) \dt_i +
  \sigma\left( \barX_{i-1} \right) \dW_i, \quad i=1, \ldots, N.
\end{equation}
Under weak assumptions we have \emph{strong} convergence with rate
$1/2$, i.e.,
\begin{equation*}
  \m{E}{\abs{X_t - \barX_N}} \le C \sqrt{\dt_{\max}}
\end{equation*}
for some constant $C$ independent of $\dt_{\max}$.
More relevant in most applications is the concept of \emph{weak} approximation.
Fortunately, the Euler scheme typically exhibits first order weak convergence, i.e., for
any suitable test function $F: \R^d \to \R$, it holds that
\begin{equation}
  \label{eq:weak-error}
  \abs{\m{E}{F(X_t)} - \m{E}{F\left(\barX_N \right)}} \le C \dt_{\max}
\end{equation}
with a constant $C$ independent of $\dt_{\max}$ for fixed times $t$. However,
the stochastic representation~\eqref{eq:elliptic} involves the process stopped
at the first hitting time of the boundary $\partial D$ of the domain. This
stopping time is approximated by the first exit time $\bartau$ of the discrete
time process $\barX_i$, $i=0, \ldots, N$.


Note that there are two sources of errors in the approximation $\barX_{\bartau}$ of $X_\tau$:
\begin{enumerate}
\item[(i)] the error in the approximation of $X$ by $\barX$
\item[(ii)] the possibility that exit of $X$ occurs between two grid points $t_i$
  and $t_{i+1}$ while $\barX$ does not exit--or vice versa.
\end{enumerate}
Indeed, even if two paths $X$ and $\barX$ are close in, say, infinity norm,
their exit times can be substantially different. Hence, the second source of
error reduces the weak error rate, i.e., the approximation error for $E[u(x)]$
decreases to the rate $1/2$. For adaptive time-step refinements which can
recover the order of convergence to an observed order $1$ again, we refer to
the references given in~\cite{sde2015}.




\subsection{Monte Carlo approximation of the expectation}
\label{sec:MC E}

Another step in the discretization is the approximation of the expected value as in \eqref{eq:elliptic} with a Monte Carlo estimator. Multilevel Monte Carlo methods are also possible but require the construction of two related realizations of $\barX_M$ for each sample on the individual levels as described in \cite{giles2008pathMLMC}. Here, we restrict ourselves to the classical Monte Carlo estimator which is defined as
\begin{align}\label{eq:spdeEstPoint}
\est{E}{MS}{M,N}{u(x)} \coloneq \frac{1}{N} \sum_{i=1}^N g\big(\barX_{\bartau}^x(\omega_i)\big) + \barZ_{\bartau}^x(\omega_i),
\end{align}
where $(\omega_i)_{i=1}^N$ is a set of $N\in\mathbb N$ samples drawn from the
probability space $(\Omega, \mathcal F, P)$ and $M$ describes the number
of steps for the discrete diffusion process. Note that this random space
represents both the Brownian motion $W$ and the stochastic field $\kappa$ from the
initial Darcy problem~\eqref{eq:model}. Recall that
\begin{align*}
  \barZ_\tau^x = \int_0^\tau f(X_s) \d{s},
\end{align*}
which we approximate by a simple left-point rule $\barZ_{\bartau}$.
% Furthermore it holds $a(x) = 2 \kappa(x) \mathrm{I}_n$ and $b(x) = \grad \kappa(x)$ for the parameters from~\eqref{eq:L} where $\mathrm{I}_n$ is the $n$-dimensional identity matrix.
% Since this holds for the normal distribution $\mathcal{N}_{0,\Delta t} \sim
% \sqrt{\Delta t}\mathcal{N}_{0,1}$, for the time steps $t_0, t_1, \ldots$ and
% for $m = 0,\ldots,M$, we derive
Hence,
\begin{align}\label{eq:spdeDiscrete}
\begin{split}
\barX_0 &= x,\\
\barX_m &= \barX_{m-1} + \grad \kappa(\barX_{m-1})\Delta t_m + \sqrt{2\kappa(\barX_{m-1}) \Delta t_m}\ \Xi,
\end{split}
\end{align}
where $M$ is the last index with $\barX_m \in D$ for $m = 0, \ldots, M$,
$\barX_{M+1}\not\in D$, and $\Xi$ is a standard normal distributed random variable. Here, $\mathcal N_{0,1}$ denotes the standard normal
distribution in $d$ dimensions. The stopping position and the stopping time
$t_\tau$ of the diffusion process is now approximated by the projection
\begin{align}\label{eq:spdeProjBoundary}
\barX_{\bartau} \coloneq \argmin \set{\lnorm{x - \barX_M}}{x \in \partial D, x = \barX_M + s (\barX_{M+1} - \barX_M), s \geq 0}
\end{align}
of $\barX_M$ onto the boundary $\partial D$ in direction of
$\barX_{M+1}$. The stopping time $\tau$ is approximated by $\bartau
  \coloneq t_{M}$.
Equation~\eqref{eq:spdeEstPoint} provides the estimator with a simple one-point integration rule via
\begin{align*}
\est{E}{MS}{M,N}{u(x)} \coloneq \frac{1}{N} \sum_{i=1}^N \bc{g\big(\barX_{\bartau}(\omega_i)\big) + \sum_{\mathclap{m\in \bc{0,\ldots,M,\bartau}}} f\big(\barX_m(\omega_i)\big) \Delta t_m}.  
\end{align*}
Elementary time adaptivity is applied by choosing $\Delta t_j = \dist(\partial D, \barX_{j-1}) \Delta t_0$ where $\dist(\partial D, x) \coloneq \min\set{\lnorm{ x_d - x}}{x_d \in \partial D}$ is the Euclidean distance to the boundary of the domain. The entire process is depicted in Algorithm~\ref{alg:spdePoint} where a simple rectangle integration method is used.
Moreover, Figure~\ref{fig:spdePathExample} sketches the described process.

Alongside the approximate expectation we can also estimate the sample variance of this approach at the location $x \in D$, since
\begin{align*}
\est{Var}{MS}{M,N}{u(x)} \coloneq \frac{1}{N} \sum_{i = 1}^{N} \bc{g(\barX_{\bartau}(\omega_i)) + \sum_{\mathclap{m \in \bc{0,\ldots,M,\bartau}}} f\bc{\barX_m\bc{\omega_i}} \Delta t_m - \est{E}{MS}{M,N}{u(x)}}^2.
\end{align*}
Note that this estimated variance has contributions from both the random field of the PDE and the SDE.

%\begin{remark}\label{rem:sdeAssumptions}
%During the derivation above we made rather basic and restrictive assumptions, such as uniform ellipticity. In fact, most of the arguments hold with more relaxed conditions, as time-dependent coefficients.
%\end{remark}


\begin{figure}[ht!]
\begin{center}
 	\begin{tikzpicture}[ auto, scale=5]
		\tikzstyle{line} = [draw, thick, densely dotted]
		\tikzstyle{point} = [circle, outer sep=0.0, inner sep=1, fill=black]
		
		% Domain
		\draw [thick] (0,0) rectangle (1,1) {};
		\draw (0.9,0.9) node {$D$};
		
		% Nodes
		\node[point] (X0) at (0.3, 0.3) {};
		\node[point] (X1) at (0.4, 0.7) {};
		\node[point] (X2) at (0.7, 0.55) {};
		\node[point] (X3) at (0.6, 0.3) {};
		\node[point] (X4) at (0.7, 0.2) {};
		\node[point] (X5) at (0.8, 0.36) {};
		\node[point] (XJ) at (0.9, 0.22) {};
		\node[point] (XJ1) at (1.1, 0.12) {};
		\node[point] (XTau) at (1.0, 0.17) {};
		
		% Line
		\draw [line] (X0) -- (X1) -- (X2) -- (X3) -- (X4) -- (X5) -- (XJ) -- (XTau);
		\draw [line, dashed] (XTau) -- (XJ1);
		
		% Interruption
		\draw [postaction = {draw,
							 white,
							 line width = 1.5pt}, 
			   line width = 2.5pt] 
			   (0.6,0.2) -- (0.645,0.26) -- (0.655,0.24) -- (0.7,0.3) {};
		
		% Marker
		\node [below] at (X0) {$\barX_0$};
		\node [above] at (X1) {$\barX_1$};
		\node [right] at (X2) {$\barX_2$};
		\node [below] at (XJ) {$\barX_M$};
		\node [below right] at (XJ1) {$\barX_{M+1}$};
		\node [above right] at (XTau) {$\barX_\tau$};
		
		% t_i
		\node [label=left:$(\Delta t_0)$] (to) at ($(X0)!.5!(X1)$) {};
		\node [label=above right:$(\Delta t_1)$] (to) at ($(X1)!.5!(X2)$) {};
	\end{tikzpicture}
  	\caption[Example diffusion process]{Sketch of a discrete diffusion process realization with endpoint projection and indicated step width.}
  	\label{fig:spdePathExample}
\end{center}
\end{figure}

\begin{algofloat}[ht!]
\algo{
	point $x \in D$, number of samples $N$, initial time step $\Delta t_0$
}{
	$\est{E}{MS}{M,N}{u(x)}$, $\est{Var}{MS}{M,N}{u(x)}$
}{
	\For{
		$i = 1,\ldots, N$
	}{
	 	$\barX_0 = x$\;
	 	$F = 0$\;
	 	$m = 1$\;
	 	sample $\kappa^i = \kappa(\omega_i)$ with $\omega_i \in \Omega$\;
	 	\While{$\barX_m \in D$}{
	 		$F = F + f(\barX_{m-1})\Delta t_{m-1}$\;
	 		$\Delta t_m = \min\bp{\dist(\partial D, \barX), 1} \Delta t_0$\;
	 		sample $\Xi$ from $\mathcal{N}_{0,1}$\;
	 		$\barX_m = \barX_{m-1} + \grad \kappa^i(\barX_{m-1}) \Delta t_m + \sqrt{2\kappa^i(\barX_{m-1})\Delta t_m} \Xi$\;
	 		$m = m + 1$\;
	 	}
	 	compute $\barX_{\bartau}$ and $t_{\bartau}$ according to \eqref{eq:spdeProjBoundary}\;
	 	$F = F + f(\barX_{\bartau})\Delta t_{\bartau}$\;
	 	$u^i = g(\barX_{\bartau}) + F$\;
 	}
 	\Return $N^{-1} \sum_{i=1}^N u^i $ and $N^{-1} \sum_{i=1}^N \bc{u^i - N^{-1} \sum_{i=1}^N u^i}^2$\;
}
\caption{Point estimate algorithm to compute the estimators $\est{E}{MS}{M,N}{u(x)}$ and $\est{Var}{MS}{M,N}{u(x)}$ using the simple one-point rectangle method for integration.}
\label{alg:spdePoint}
\end{algofloat}




\subsection{Extension to the whole domain}\label{ssec:domainSDE}
In the following, the pointwise approximation of the solution at some $x\in D$ obtained by some Monte Carlo estimator is extended to the whole domain $D$ using interpolation techniques on a mesh. This allows applying finite element a~posteriori error control with respect to a global (in $D$) approximation of the solution which we define as follows: Consider some given mesh $\mathcal{T}_h$ as
a triangulation of the spatial domain $D$ with vertices $\mathcal{N}_h = (\nu_h^i)_{i=1}^{\abs{\mathcal{N}_h}}$ and edges $\mathcal{E}_h=\{E_i\}_{i=1}^{|\mathcal{E}_h|}$.
Note that the coefficients of a Courant $P_1$ function correspond to the function values at the vertices (nodes) of the mesh and that the nodal interpolation operator denoted by $\mathcal{I}_h$ is defined by these values.
Hence, let the discrete solution $\est{E}{MS}{\vec{M,N}}{u_h}$ with $\vec{M} = \bc{M_i}_{i=1}^{\abs{\mathcal{N}_h}}$ and $\vec{N} = \bc{N_i}_{i=1}^{\abs{\mathcal{N}_h}}$ be a Courant $P_1$ function on this mesh determined by setting the nodal values,
\begin{align*}
\est{E}{MS}{\vec{M,N}}{u_h}(\nu_h^i) &\coloneq \est{E}{MS}{M_i,N_i}{u(\nu_h^i)} &\text{for }i = 1,\ldots,\abs{\mathcal{N}_h}.
\end{align*}
This introduces three types of errors into the global approximation of $u$.
The first is the \emph{stochastic approximation error} originating from the Monte Carlo estimators. It can be controlled by means of the Central Limit Theorem (CLT) subject to the number of samples $N_i$ for each nodal point.
%Application of the inequality~\eqref{eq:combinedProb} is necessary to extend this control to the $P_1$ approximation.
The second error arises from the \emph{approximation of the diffusion process} and is controlled by the step widths $\Delta t_m$.
The third error contribution results from the \emph{$P_1$ interpolation} which is approximated based on the Monte Carlo estimators and determined by the interpolation mesh parameter $h$. 

% With the help of the Bramble-Hilbert-Lemma we get the following inequality with probability $\s{p}$ as
% \begin{align*}
% &\lnorm{\m{E}{u} - u_h^\s{S}}_\infty \leq \lnorm{\m{E}{u} - \mathcal{I}\m{E}{u}}_\infty + \lnorm{\mathcal{I} \m{E}{u} - u_h^\s{S}}_\infty\\
% &\qquad\leq h^{-1} \abs{u}_2 + \max\set{\m{Var}{u_h^\s{S}}^{1/2} N_i^{-1/2}\Phi^{-1}(\s{p}^*) + \epsilon_i^*(N_i,\s{p}^*)}{\nu \in \mathcal{N}_h}
% \end{align*}
% where $\s{p}^* \coloneq \abs{\mathcal{N}_h}^{-1}(\s{p} - 1) + 1$.

We shall consider two error representations. The first describes a \emph{decomposition of the mean square error} into three parts resulting from the interpolation error, the discretization of the ordinary differential equation \eqref{eq:spdeDiscrete}, and the sampling error in the Monte Carlo method.
For the stochastic solution $u_h^M$ of the discretized ordinary differential equation it holds $\m{E}{u_h^M} = \m{E}{\est{E}{MS}{\vec{M,N}}{u_h}}$, i.e., the estimator is unbiased.
Then, for the pointwise mean square error in the approximation, we derive
\begin{align}\label{eq:spdeDecomp}
\begin{split}
&\m{E}{\bc{\est{E}{MS}{\vec{M,N}}{u_h} - \m{E}{u}}^2} = \m{E}{\est{E}{MS}{\vec{M,N}}{u_h}^2} - 2 \m{E}{\bc{\est{E}{MS}{\vec{M,N}}{u_h}}} \m{E}{u} + \m{E}{u}^2\\
&\mSkip= \m{E}{\est{E}{MS}{\vec{M,N}}{u_h}^2} - \m{E}{\est{E}{MS}{\vec{M,N}}{u_h}}^2 + \bc{\m{E}{\est{E}{MS}{\vec{M,N}}{u_h}} - \m{E}{u}}^2\\
&\mSkip= \m{Var}{\est{E}{MS}{\vec{M,N}}{u_h}} + \bc{\m{E}{u_h^M} - \m{E}{u}}^2\\
&\mSkip= \frac{1}{N}\m{Var}{u_h^M}+ \bc{\m{E}{u_h^M} - \m{E}{u}}^2,
\end{split} 
\end{align}
since $\m{Var}{\est{E}{MS}{\vec{M,N}}{u_h}} = \m{Var}{\frac{1}{N} \sum_{i=1}^{N} u_h^M(\omega_i)} = \frac{1}{N} \m{Var}{u_h^M}$ for the uncorrelated samples of $u_h^M$.

The second decomposition seeks to represent the error \emph{locally in the $L^2$ norm}.
We assume some convex $D \subseteq \mathbb R^2$ for the sake of a simpler presentation and $H^2$ regularity of the solution $u$.
Higher dimensions are possible with different inequalities for the norms.
Consider the element $T \in \mathcal{T}_h$ and the pointwise $P_1$ interpolation operator $\mathcal{I}_h$ on the mesh $\mathcal{T}_h$.
By a triangle inequality and interpolation error estimates, for the approximation error it holds that
\begin{align*}\label{eq:spdeInftyError}
&\lnorm{\m{E}{u} - \est{E}{MS}{\vec{M,N}}{u_h}}_{L^2(T)} \leq \lnorm{\m{E}{u} - \mathcal{I}_h \m{E}{u}}_{L^2(T)} + \lnorm{\mathcal{I}_h\m{E}{u} - \est{E}{MS}{\vec{M,N}}{u_h}}_{L^2(T)}\\
&\mSkip \leq \lnorm{\m{E}{u} - \m{E}{u_h}}_{L^2(T)} + \lnorm{\m{E}{u_h} - \mathcal{I}_h \m{E}{u}}_{L^2(T)}\\
&\mSkip\mSkipSmall + \lnorm{\mathcal{I}_h\m{E}{u} -  \m{E}{u_h^M}}_{L^2(T)} + \lnorm{\m{E}{u_h^M} - \est{E}{MS}{\vec{M,N}}{u_h}}_{L^2(T)} \\
&\mSkip\lesssim h_T\eta_T + h_T^2 \norm{\Delta u}_{L^2(T)} + \abs{T} \Delta t_0 + \abs{T}\max_{K \in \mathcal{N}(T)}\bp{\m{Var}{u_h^M}^{1/2} N_K^{-1/2}}.\taghere
\end{align*}
The element-wise error indicator $\eta_T$ which controls the FE approximation error is described in Section~\ref{sec:adaptivity}.
While the first two terms are both governed by properties of the mesh $\mathcal{T}_h$ and can be controlled through refinement as well as the adaptive algorithms described in Section~\ref{sec:adaptivity},
the third term represents the Monte Carlo estimation error.
It solely depends on the number of samples used for each node in the element as the variance converges to the variance of the continuous solution $\m{Var}{u}$ for $M \rightarrow \infty$ and $h \rightarrow 0$.
The last term represents the error in the approximation of the diffusion process in the Euler scheme for the stochastic differential equation.
Numerical experiments show that $\Delta t_0 \simeq h_{\min}$ is a reasonable choice in the two-dimensional case.
The second term is of higher order with respect to $h$ and is therefore omitted in the further calculations as it vanishes with a higher rate of convergence for $h \to 0$.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "spde_paper2"
%%% End: 
