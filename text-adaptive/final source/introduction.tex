It becomes increasingly common that problems in the applied sciences, e.g. in engineering and computational biology, involve uncertainties of model parameters.
These can for instance be related to coefficients of random media, i.e. material properties, inexact domains and stochastic boundary data.
The uncertainties may result from heterogeneities and incomplete knowledge or inherent stochasticity of parameters.
With steadily increasing computing power, the research field of uncertainty quantification (UQ) has become a rapidly growing and vividly active area of research which covers many aspects of dealing with such uncertainties for problems of practical interest.

In this work, we derive a novel adaptive numerical approach for the solution of linear PDEs with stochastic data.
The proposed method is based on the presentation in~\cite{sde2015} where a similar idea was described in combination with a global regression and without adaptivity (except for the step width in the Euler scheme).
The important topic of adaptivity is picked up in this work where a \emph{unique feature of the method is exploited}, namely the completely decoupled and localized parametrization (and thus control) of approximation and stochastic errors.
This becomes feasible since
\begin{itemize}
\item the pointwise solution in the spatial domain is determined by an appropriate SDE and solved by an adaptive Euler scheme as in~\cite{sde2015},
\item the global solution on the spatial domain is reconstructed based on a triangulation and interpolation in between vertices (\ie on a discrete finite element space).
\end{itemize}
Consequently, the overall error is determined by
\begin{enumerate}
\item[(i)] the accuracy of the single SDE solutions dictated by the step width of the scheme,
\item[(ii)] the stochastic error for the pointwise expected value determined by the number of solutions computed on each vertex,
\item[(iii)] the number of solution points in the spatial domain determined by the refinement level of the employed mesh.
\end{enumerate}
Hence, the second item related to the stochastic error enables a localized adaptivity for the sampling while the first and third items are related to the approximation quality in the spatial domain.
In comparison to other common methods such as the Monte Carlo FEM for stochastic PDEs~\cite{MR3033013}, the proposed method provides means to {\em adjust the number of samples locally} while also reconstructing global solutions.
Moreover, it is also highly parallelizable and thus well suited for modern distributed multi-core architectures.
 
 
While the derivation of the method in the next section is rather general, a specific motivation is given by a model relevant in practical problems, namely the Darcy equation related to the modeling of groundwater flow.
It reads as
\begin{subequations}
\label{eq:model}
\begin{align}
  -\nabla \cdot \left( \kappa(x) \nabla u(x) \right) &= f(x), \quad x \in D,\\
  u(x) &= g(x), \quad x \in \partial D,
\end{align}
\end{subequations}
where the solution $u$ is the hydraulic head, $\kappa$ denotes the conductivity coefficient describing the porosity of the medium, $f$ is a source term and the Dirichlet boundary data is defined by $g$.
The computational domain in $d$ dimensions is denoted $D \subset \R^d$ and we suppose that $D$ is a convex polygon.
Moreover, all data are supposed to be sufficiently smooth such that the problem always exhibits a unique solution which then is also smooth.
%More specifically, the data is assumed uniformly Lipschitz, $f\in L^2(D)$ and $\kappa\in C^2(D)$.
A detailed regularity analysis is not in the scope of this paper.
In principle, although we restrict our investigations to a stochastic coefficient $\kappa$, any data of the PDE can be modeled as being stochastic.
The model~\eqref{eq:model} is quite popular for analytical and numerical examinations since it is one of the simplest models which reveals some major difficulties that also arise in more complex stochastic models. 
Moreover, the deterministic linear second order elliptic PDE is a well-studied model problem and it is of practical relevance, e.g. in the context of groundwater contamination. 


The random data used in the PDE model is a stochastic field given with an adequate representation.
This can for instance be based on actual measurements, expert-knowledge or simplifying assumptions regarding the statistics.
For numerical computations, the representation has to be amenable for the employed method.
However, the derived method does not rely on a specific representation of the random fields.
In particular, only the generation of realizations is required to carry out numerical computations.


A variety of numerical methods is available to obtain approximate solutions of the model problem \eqref{eq:model} with random data and we only refer to~\cite{lord2014introduction, smith2013uncertainty,le2010introduction} for an overview in the context of uncertainty quantification (UQ).
These methods often rely on the separation of the deterministic and the stochastic space and introduce separate discretizations~\cite{MR2805155}.
Common methods are based on sampling of the stochastic space, the projection onto an appropriate stochastic basis or a perturbation analysis.
The most popular sampling approach is the Monte Carlo (MC) method which is very robust and easy to implement.
Recent developments include the quite successful application of multilevel ideas for variance reduction and advances with structured point sequences (Quasi-MC), cf.~\cite{MR3033013,MR2835612,MR2783812,MR3024159,MR3029293}.
(Pseudo-)Spectral methods represent a popular class of projection techniques which can e.g. be based on interpolation (Stochastic Collocation)~\cite{MR2318799,MR2421041,MR2421037} or orthogonal projections with respect to the energy norm induced by the differential operator of the random PDE (Stochastic Galerkin FEM)~\cite{ghanem2003stochastic,MR2121216,MR2121215,MR2084236,MR2105161,MR3154028,emn2014mlmcBounds}.
These methods are more involved to analyze and implement but offer the benefit of possibly drastically improved convergence rates when compared to standard Monte Carlo sampling.
The deterministic discretization often relies on the finite element method (FEM) which also is employed with MC.


The aim of this paper is the description of a novel highly adaptive numerical
approach which is founded on the (classical) observation (see
  \cite{mils/tret04,sde2015}) that the random PDE~\eqref{eq:model} is
directly related to an SDE driven by a stochastic
process, namely
\begin{equation}\label{eq:sde}
\d{X_t} = b(X_t)\d t + \sigma(X_t)\d{W_t},
\end{equation}
with appropriate coefficients $b$ and $\sigma$, Brownian motion $W$ and additional boundary conditions.
For deterministic data $\kappa, f, g$, for any $x\in D$, the Feynman-Kac formula leads to a collection of random variables $\phi^x=\phi^x(\kappa,f,g)$ such that $u(x)=E[\phi^x]$, i.e. the deterministic solution at $x$ is equivalent to the expectation of the random variable.
When the data are stochastic, the solution $u(x)$ of the random PDE at $x\in D$ can be expressed as the {\em conditional expectation} of $u$ given data $\kappa,f,g$, i.e., $u(x)=E[\phi^x\,\vert\,\kappa,f,g]$, and the variance of $u(x)$ can be bounded by the variance of $\phi^x$.
To determine $\phi^x$ at points $x\in D$, a classical Euler method can be employed.
In order to recover a global solution in the spatial domain $D$, opposite to the previous work~\cite{sde2015} where global regression was utilized, we here rely on a mesh $\mathcal{T}$ which is a regular triangulation of $D$.
Sampled approximations of $\phi^x$ are then computed at the nodes of the mesh and the values are used for an interpolation in a discrete finite element space.
This yields the approximate expectation of the solution
$E[u(\omega,\cdot)]$ defined on the entire domain $D$.

A distinct advantage of this approach is the separation of all error components as mentioned above, which in the case of the discrete interpolation allows for the application of simple finite element (FE) a posteriori error estimates to refine the spatial mesh, \ie the location of sample points in the domain guided by the global approximation error.


One can regard the proposed method as a combination of sampling and
interpolation methods, that make use of classical stochastic solution
techniques pointwise and a global interpolation with FE basis functions.  When
compared to MC which samples a stochastic space $(\Omega,\mathcal{F},P)$ by
(typically) determining a FE solution at every point and subsequently
averaging the solutions, our method determines realizations of stochastic
solutions at points in the spatial domain $D$ and determines an approximation
of the expectation by a global interpolation in the physical space.  Thus, the
method does not require any type of global deterministic solver and can be
parallelized extremely well.

To the best of our knowledge, the regression/interpolation approach based on
the Feynman-Kac representation proposed in \cite{sde2015} and the present
paper is new for the field of PDEs with stochastic coefficients. Of course,
similar methods have a long history for deterministic PDEs, and we refer to
the monograph \cite{mils/tret04} for a thorough overview. There have also been
some works on numerics for SPDEs in the sense of time-dependent PDEs driven by
temporal (Brownian) noise. In particular, we refer to \cite{MT16} for
applications to stochastic Navier-Stokes equations. Due to the non-linearity,
the Feynman-Kac representation cannot be directly applied in that problem, but
a layer method based on linearized problems is constructed.

The structure of the paper is as follows:
In Section~\ref{sec:sampling}, we elaborate on the representation of deterministic and stochastic PDEs in terms of stochastic differential equations (SDEs).
Moreover, we recall the employed numerical methods to determine pointwise stochastic solutions, namely the Euler method and Monte Carlo sampling.
Additionally, based on a deterministically chosen set of stochastic solutions in the spatial domain, the reconstruction of a global approximation by means of an interpolation in a discrete FE basis is described.
This allows for a fully adaptive algorithm which is derived in Section~\ref{sec:adaptivity}.
There, the different error components are identified and a practical approach for the determination of the discretization parameters is explained.
The paper is concluded with several numerical examples in Section~\ref{sec:examples} where the performance of the new method is demonstrated.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "spde_paper2"
%%% End: 
