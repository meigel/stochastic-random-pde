\documentclass[a4paper]{amsart}

\usepackage[leqno]{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{todonotes}
%\usepackage[notref,notcite]{showkeys}

\usepackage[labelfont=bf]{caption}
\usepackage[algosection]{algorithm2e} %linesnumbered, noline, noend
\renewcommand{\algorithmautorefname}{Algorithm}
\usepackage{tocbasic}% So geht das auch mit einer Standardklasse. 
\providecommand*{\autodot}{}% So geht das auch mit einer Standardklasse.
%\DeclareNewTOC[% 
%  type=algofloat, 
%  name=Algorithm, 
%  counterwithin=section,
%  listname={List of Algorithms}, 
%  float, % definiere die Gleitumgebung algorithm
%  floatpos=tb, % Gleitparameter 
%  nonfloat % definiere die nicht gleitende Umgebung algorithm- 
%]{alg}
\usepackage{newfloat}
\DeclareFloatingEnvironment[fileext=alg,placement={!ht},name=Algorithm]{algofloat}
\setlength{\algomargin}{4em}
\newcommand{\algo}[3]{
	\begin{algorithm}[H]
		\DontPrintSemicolon
	  	\SetKwInOut{Input}{In}
		\SetKwInOut{Output}{Out}
		\SetAlgoVlined 
		\hspace{-2em}\Input{#1}
		\hspace{-2em}\Output{#2}
		\BlankLine
		\BlankLine
		#3
	\end{algorithm}
	}  

% ALWAYS LAST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[
	bookmarks=true,
	unicode=true,
	pdfborder={0 0 0},
	pdftoolbar=true,
	pdfmenubar=true,
	pdffitwindow=true,
	pdfstartview={FitH},%{FitH},{FitV},{FitB},{FitBH}
	pdfpagelayout={OneColumn},%SinglePage OneColumn TwoPageLeft TwoPageRight 
	pdftitle={},
	pdfauthor={Johannes Neumann}, 
	colorlinks=false,
	linktoc=all,
	]{hyperref}
	
\usepackage{tikz}
% \usetikzlibrary{external}
% \tikzexternalize[prefix=tikz/]
\usetikzlibrary{calc,positioning}
\usetikzlibrary{shapes,arrows,patterns}
\usetikzlibrary{decorations.shapes,decorations.pathreplacing,decorations.pathmorphing,decorations.markings}
\usetikzlibrary{fadings}
% \usepackage{enumerate}
\usepackage[inline]{enumitem}
	\setlist{noitemsep}
% \renewcommand{\labelenumi}{\Roman{enumi}}
\usepackage{units}
\usepackage{tikz-3dplot}
\newcommand{\tikzStyles}{
	\tikzstyle{meshline} = [draw, line width = 0.2]
	\tikzstyle{funline} = [draw, line width = 1.5]
	\tikzstyle{funlineCovered} = [funline, 
								  dotted,
								  line cap=round,
								  dash pattern=on 0.9\pgflinewidth 
								  			   off 2.3\pgflinewidth]
	\tikzstyle{pointer} = [decoration={markings,
										mark=at position 1 with
										{\arrow[scale=3,>=stealth]{>}}},
							postaction={decorate}]
}
\newcommand{\tikzPicFun}[2][3]{
	\tdplotsetmaincoords{70}{-38}
 	\begin{tikzpicture}[tdplot_main_coords, auto, scale=#1, line join=round]
 		\tikzStyles
		#2
	\end{tikzpicture}
}

\newcommand{\update}[1]{\textcolor{blue}{#1}}


%\usepackage{txfonts}
%\usepackage{enumitem}
%\usepackage{ifpdf} % define \ifpdf
%\usepackage{ifthen}
%\usepackage{braket}
%\usepackage[ruled,norelsize]{algorithm2e}
%\usepackage[all]{xy}
%\ifpdf
%  \usepackage[pdftex,
%  	bookmarks=true,
%	unicode=true,
%	pdfborder={0 0 0},
%	pdfstartview={FitV},
%	pdfpagelayout={TwoPageRight},
%	pdftitle={A Fully Adaptive Stochastic Sampling Method for PDE with Random Data},
%	pdfauthor={Felix Anker, Christian Bayer, Martin Eigel, Johannes Neumann and John Schoenmakers}, 
%	colorlinks=false,
%	linktoc=all,
%	plainpages=false,
%	pdfpagelabels,
%	pdffitwindow=true]{hyperref}
%\else
%  \usepackage[dvips]{hyperref}
%\fi


\input{macros}

\author[F.~Anker]{Felix Anker$^1$}
\thanks{$^1$Research of F.~Anker has been supported by grant Jo329/10-2 within the DFG priority programme 1679: Dynamic simulation of interconnected solids processes.}

\author[C.~Bayer]{Christian Bayer}

\author[M.~Eigel]{Martin Eigel}

\author[J.~Neumann]{Johannes Neumann$^2$}
\thanks{$^2$Research of J.~Neumann was funded in part by the DFG {\sc Matheon} project SE13.}

\author[J.~Schoenmakers]{John Schoenmakers}

\title[Adaptive Interpolated Stochastic Sampling for SPDEs]{A Fully Adaptive Interpolated Stochastic Sampling Method for \update{Linear} Random PDEs}

\begin{document}

\begin{abstract}
A numerical method for the fully adaptive sampling and interpolation of \update{linear} PDEs with random data is presented. It is based on the idea that the solution of the PDE with stochastic data can be represented as conditional expectation of a functional of a corresponding stochastic differential equation (SDE). The spatial domain is decomposed by a non-uniform grid and a classical Euler scheme is employed to approximately solve the SDE at grid vertices. Interpolation with a conforming finite element basis is employed to reconstruct a global solution of the problem. An a posteriori error estimator is introduced which provides a measure of the different error contributions. This facilitates the formulation of an adaptive algorithm to control the overall error by either reducing the stochastic error by locally evaluating more samples, or the approximation error by locally refining the underlying mesh. Numerical examples illustrate the performance of the presented novel method.
\end{abstract}

\keywords{random PDE, stochastic differential equation, Feynman-Kac, interpolation, finite element, a posteriori error estimator, adaptive method, Euler Maruyama}

\subjclass[2010]{%
 35R60, % Partial differential equations with randomness, stochastic partial differential equations
 47B80, % Random operators
 60H35, % Computational methods for stochastic equations
 65C20, % Probabilistic methods, simulation and stochastic differential equations: Models, numerical methods
 65N12, % Stability and convergence of numerical methods
 65N22, % Solution of discretized equations
 65J10,% Equations with linear operators
 65C05 % Monte Carlo Methods
}

\maketitle

\section{Introduction}
\label{sec:introduction}

\input{introduction}

\section{Stochastic Sampling and Interpolation}
\label{sec:sampling}

\input{sampling}

\section{Adaptivity}
\label{sec:adaptivity}

\input{adaptivity}

\section{Numerical Experiments}
\label{sec:examples}

\input{examples}

\bibliographystyle{plain}
\bibliography{spde}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
