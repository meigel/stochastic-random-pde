from __future__ import division
import numpy as np
import os
from dolfin import *
import scipy.stats as stats
import copy
#from joblib import Parallel, delayed
from matplotlib import pyplot as plt
import time
import euler as em


class dW( object ):
	'''Brownian motion class'''
	def __init__( self, dt, seed = None, cache_size = 0, cache_block_size = 100 ):
		# TODO: set seed and setup cache
		self.dt = np.sqrt( dt )
	def __call__( self, x ):
		return stats.norm.rvs( size = len( x ), scale = self.dt )


class Interpolator( object ):
	'''FEM interpolation class'''
	def __init__( self, mesh, inside_domain ):
		self.mesh = mesh
		self.inside_domain = inside_domain
		self.V = FunctionSpace( mesh, 'CG', 1 )
	
	def create_interpolation( self, f, runs = 1 ):
		# evaluate nodal values
		coords = self.mesh.coordinates()
		fx = [np.array( [f( x ) for x in coords] ) for _ in range( runs )]
		# compute point-wise expectation
		fx = sum( fx ) / runs
		# create interpolation function
		u = Function( self.V )
		try:
			# d2vm = dof_to_vertex_map(self.V)
			d2vm = vertex_to_dof_map( self.V )
		except:
			# old FEniCS version
			DM = self.V.dofmap()
			d2vm = DM.dof_to_vertex_map(self.mesh)
		u.vector()[d2vm] = fx
		return u
	
	def create_interpolationC( self, means ):
		u = Function( self.V )
		try:
			# d2vm = dof_to_vertex_map(self.V)
			d2vm = vertex_to_dof_map( self.V )
		except:
			# old FEniCS version
			DM = self.V.dofmap()
			d2vm = DM.dof_to_vertex_map(self.mesh)
		u.vector()[d2vm] = np.array(means[0:(self.mesh.coordinates().shape[0]),2])
		return u


def stochastic_euler( x0, pde_data, inside_domain, dw ):
	kappa, f, uD, g = pde_data["kappa"], pde_data["f"], pde_data["uD"], pde_data["g"]
	xi, T = copy.copy( x0 ), 0
	while inside_domain( xi ):
		xi += sqrt( 2 * kappa( xi ) ) * dw( xi )
		T += 1
	# project to boundary
	xT = min( max( xi[0], 0 ), 1 ), min( max( xi[1], 0 ), 1 )
	return uD( xT ) + T * dw.dt ** 2, T, xT # f = 1
	#     return uD( xT ), T, xT


def setup_interpolator( N ):
	def inside_domain( x ):
		return x[0] > 0 and x[0] < 1 and x[1] > 0 and x[1] < 1
	mesh = UnitSquareMesh( N, N )
	return Interpolator( mesh, inside_domain )


def solve_spde( pde_data, dt, interpolator, runs ):
	# initialise Brownian motion
	dw = dW( dt )
	# setup stochastic Euler solver
	ux = lambda x0: stochastic_euler( x0, pde_data, interpolator.inside_domain, dw )[0]
	Iu = interpolator.create_interpolation( ux, runs )
	return Iu

#----------------------- solves with multiple starting from mesh points,
#----------------------- returns an interpolated function
def solve_spdeC( pde_data, parallel_data, dt, interpolator, runs ):
	# setup stochastic Euler solver
	means = em.stochEulerC( parallel_data["numThreads"],
				parallel_data["seed"],
				runs,
				dt,
				parallel_data["adaptive"],
				parallel_data["intermediate"],
				interpolator.mesh.coordinates().flatten(),
				np.prod(parallel_data["dimGrid"]) )
	Iu = interpolator.create_interpolationC( means )
	return Iu

def solve_point ( pde_data, dt, interpolator, runs, x0 ):
	# initialise Brownian motion
	dw = dW( dt )
	ux = np.array( [stochastic_euler( x0, pde_data, interpolator.inside_domain, dw )[0]
		for _ in range( runs )] )
	
	return np.mean( ux )

#----------------------- solves with starting from a single point
#----------------------- returns mean
def solve_pointC ( pde_data, parallel_data, dt, interpolator, runs, x0 ):
	# pde_data will not yet be used, C-code does not use FEniCS-Expression so far
	points = em.stochEulerC(parallel_data["numThreads"],
				parallel_data["seed"],
				runs, dt,
				parallel_data["adaptive"],
				parallel_data["intermediate"],
				x0,
				np.prod(parallel_data["dimGrid"]) )
	Iu = interpolator.create_interpolationC( points )
	return Iu

def solve_fenics( pde_data, interpolator ):
	def boundary( x, on_boundary ):
		return on_boundary
	uD = pde_data["uD"]
	f = pde_data["f"]
	g = pde_data["g"]
	kappa = pde_data["kappa"]
	V = interpolator.V
	bc = DirichletBC( V, uD, boundary )
	u = TrialFunction( V )
	v = TestFunction( V )
	a = inner( kappa * grad( u ), grad( v ) ) * dx
	L = f * v * dx + g * v * ds
	u = Function( V )
	solve( a == L, u, bc )
	
	return u


def main():
	#------------------------------------------------------------- setup problem
	f = Constant( 1.0 ) # rhs
	kappa = Constant( 1.0 ) # diffusion coefficient
	### testing uD = 0 for variance
	# uD = Constant( 0.0 ) # Dirichlet bc
	uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" ) # Dirichlet bc
	#     uD = Constant( 1.0 ) # Dirichlet bc
	g = Constant( 0.0 ) # Neumann bc
	#-------------------------------------------- collect problem data and setup
	pde_data = {"kappa":kappa, "f":f, "uD":uD, "g":g}
	N = 100 # setup domain and interpolation space
	interpolator = setup_interpolator( N )
	dt = 0.1 # Time step
	x0 = [0.1, 0.1] # starting point / evaluation point
	#-------------------------------------------- setup parallel parameters
	intermediate = False
	numThreads = 50
	adaptive = False
	dimGrid = [N+1, N+1]
	seed = float('inf')
	prob_dim = len(dimGrid)
	parallel_data = {"numThreads":numThreads, "adaptive":adaptive, "intermediate":intermediate, "dimGrid":[1,1], "seed":seed, "prob_dim":prob_dim}
	#----------------------------------------------- solve stochastic random PDE
	#-------------------------------------------------------------- interpolated
	runsList = [2**i for i in range(0,21)]
	dtList = [2**-i for i in range(5,23)]
	
	set_log_level(ERROR)
	
	uF = solve_fenics( pde_data, interpolator )
	
# 	print "non-adaptive version"
# 	
# 	f = open("na_trajfixed_bound.txt", "a")
#    	
# 	runs = runsList[len(runsList)-1]
#    	
# 	f.write("# FEM Loesung in [0.1, 0.1] = %.13f\n\"Trajektorien = %d\"\n" % (uF(x0), runs))
#    	
# 	for dt in dtList:
# 		start = time.time()
# 		Iu = solve_pointC( pde_data, parallel_data, dt, interpolator, runs, x0 )
# 		end = time.time()
#    		
# 		usedTime = (end-start)*1000
#   		
# 		print "time for traj = ", runs, " , dt = ", dt, " : ", usedTime, " ms"
#    		
# 		mean = Iu[0]
# 		variance = Iu[1];
#    		
# 		#err = errornorm(uF, Iu, 'L2')
# 		err = abs(uF(x0)-mean)/uF(x0)
#    		
# 		print "relativ error for traj = ", runs, " , dt = ", dt, " : ", err
#    		
# 		f.write("%.13f\t\t%.13f\t\t%.13f\t\t%.13f\t\t%.13f\n" % (dt, mean, variance, err, usedTime))
# 		f.flush()
# 		os.fsync(f.fileno())
# 	f.write("\n\n")
# 	f.close()
#    	
# 	f = open("na_dtfixed_bound.txt", "a")
#   	
# 	dt = dtList[len(dtList)-1]
#    	
# 	f.write("# FEM Loesung in [0.1, 0.1] = %.13f\n\"Schrittweite = %.13f\"\n" % (uF(x0), dt))
#    	
# 	for runs in runsList:
# 		start = time.time()
# 		Iu = solve_pointC( pde_data, parallel_data, dt, interpolator, runs, x0 )
# 		end = time.time()
#    		
# 		usedTime = (end-start)*1000
#    		
# 		print "time for traj = ", runs, " , dt = ", dt, " : ", usedTime, " ms"
#    		
# 		mean = Iu[0]
# 		variance = Iu[1];
#    		
# 		#err = errornorm(uF, Iu, 'L2')
# 		err = abs(uF(x0)-mean)/uF(x0)
#    		
# 		print "relativ error for traj = ", runs, " , dt = ", dt, " : ", err
#    		
# 		f.write("%d\t\t%.13f\t\t%.13f\t\t%.13f\t\t%.13f\n" % (runs, mean, variance, err, usedTime))
# 		f.flush()
# 		os.fsync(f.fileno())
# 	f.write("\n\n")
# 	f.close()
	
	parallel_data["adaptive"] = 1

	print "adaptive version"
	
	f = open("ad_trajfixed_bound.txt", "a")

        runs = runsList[len(runsList)-1]

	f.write("# FEM Loesung in [0.1, 0.1] = %.13f\n\"Trajektorien = %d\"\n" % (uF(x0), runs))
		
	for dt in dtList:
		start = time.time()
		Iu = solve_pointC( pde_data, parallel_data, dt, interpolator, runs, x0 )
		end = time.time()
					
		usedTime = (end-start)*1000
		
		print "time for traj = ", runs, " , dt = ", dt, " : ", usedTime, " ms"
		
		mean = Iu[0]
		variance = Iu[1];
		
		#err = errornorm(uF, Iu, 'L2')
		err = abs(uF(x0)-mean)/uF(x0)
		
		print "relativ error for traj = ", runs, " , dt = ", dt, " : ", err
		
		f.write("%d\t\t%.13f\t\t%.13f\t\t%.13f\t\t%.13f\n" % (runs, mean, variance, err, usedTime))
		f.flush()
		os.fsync(f.fileno())
	f.write("\n\n")
	f.close()
	
	f = open("ad_dtfixed_bound.txt", "a")

        dt = dtList[len(dtList)-1]

	f.write("# FEM Loesung in [0.1, 0.1] = %.13f\n\"Schrittweite = %.13f\"\n" % (uF(x0), dt))
	
	for runs in runsList:
		start = time.time()
		Iu = solve_pointC( pde_data, parallel_data, dt, interpolator, runs, x0 )
		end = time.time()
		
		usedTime = (end-start)*1000
		
		print "time for traj = ", runs, " , dt = ", dt, " : ", usedTime, " ms"
		
		mean = Iu[0]
		variance = Iu[1];
		
		#err = errornorm(uF, Iu, 'L2')
		err = abs(uF(x0)-mean)/uF(x0)
		
		print "relativ error for traj = ", runs, " , dt = ", dt, " : ", err
		
		f.write("%d\t\t%.13f\t\t%.13f\t\t%.13f\t\t%.13f\n" % (runs, mean, variance, err, usedTime))
		f.flush()
		os.fsync(f.fileno())
	f.write("\n\n")
	f.close()
	
	plot( Iu, title = "Stochastic")
	plot( uF, title = "FEM")
	plot( abs((uF-Iu)/uF), title = "relative error" )
	plot( (uF-Iu), title = "absolute error: FEM - Stochastic" )
	
	interactive();
	
	set_log_level(INFO)

if __name__ == "__main__":
#     os.chdir( os.pardir )
    print "started"
    main()
