from dolfin import plot, interactive, UnitSquareMesh, Expression, FunctionSpace, interpolate, Function, Mesh, MeshFunctionSizet

class PlotProxy(object):
    def __init__(self, name):
        self.name = name
        self.handle = None
    def plot(self, f, *args, **kwargs):
        if self.handle is None:
            if not "title" in kwargs.keys():
                kwargs["title"] = self.name
            self.handle = plot(f, *args, **kwargs)
        else:
            self.handle.plot(f)

class PlotHelper(object):
    def __init__(self):
        self.handles = {}
    def __getitem__(self, name):
        try:
            ph = self.handles[name]
        except:
            ph = PlotProxy(name)
            self.handles[name] = ph
        return ph

def test_plotting():
    mesh = UnitSquareMesh(10,10)
    V = FunctionSpace(mesh, 'CG', 1)
    f1 = Expression("sin(A*(x[0]+x[1]))", A=1)
    f2 = Expression("cos(A*x[0]*x[1])", A=1)
    ph = PlotHelper()
    for A in range(5):
        f1.A = A+1
        f2.A = A+1
        g1 = interpolate(f1, V)
        g2 = interpolate(f2, V)
        ph["f1"].plot(g1)
        ph["f2"].plot(g2, interactive=True)


# taken from
# http://fenicsproject.org/qa/5795/plotting-dolfin-solutions-in-matplotlib

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as col

def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def plot_file(infile):
    file = open(str(infile), 'r')
    
    # ignore the # in front and split first line by spaces
    names = file.readline().split('\t')[1:]
    
    data = np.loadtxt(file)
    
    # assuming the first column is the x-axis for all following columns
    x = data[:,0]
    
    # clear current figure
    plt.clf()
    
    for i in range(1, data.shape[1]):
        plt.plot(x, data[:,i], label=names[i])
        
    plt.xscale('log', basex=10)
    plt.yscale('log', basey=10)
    ax = plt.gca()
    ax.set_xlabel('N (number of points)')
    ax.set_ylabel('errors')
    plt.legend(loc=3)
    plt.show()
#     plt.savefig()


def plot_mpl(obj):
    ''' plot FEniCS object (use with plt.figure() ... plt.show()) '''
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')
    elif isinstance(obj, MeshFunctionSizet):
        mesh, C = obj.mesh(), obj.array()
        vmin, vmax = min(C), max(C)
        plt.tripcolor(mesh2triang(mesh), facecolors=C, edgecolors=['none','k'][0], cmap=plt.cm.coolwarm, vmin=vmin, vmax=vmax)  # coolwarm, brg, seismic, winter, jet, hot, spectral, prism
    else:
        print "unsupported plotting type", type(obj)
        assert TypeError


if __name__ == '__main__': 
    test_plotting()
