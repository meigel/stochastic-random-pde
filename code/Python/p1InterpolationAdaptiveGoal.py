#!/usr/bin/env python
import os
import sys

from dolfin import *

import cPickle as pickle
from data_utils import saveFunction
import numpy as np
import p1InterpolationAdaptiveGoalAnalyse
from p1InterpolationAdaptiveTest import adaptiveSPDESolver
import time as tm


def MollifiedPoint2( mesh, x0, eps, refineSize = 50.0, C = None ):
    if C is None:
        epsBall = ball( x0, eps )
        while mesh.hmin() > eps / refineSize:
            DG = FunctionSpace( mesh, "DG", 0 )
            ballArr = project( epsBall, DG ).vector().array()
            cell_markers = MeshFunction( "bool", mesh, mesh.topology().dim() )
            for c in cells( mesh ):
                cell_markers[c] = ballArr[c.index()] > 0
            hminOld = mesh.hmin()
            mesh = refine( mesh, cell_markers )
            if hminOld == mesh.hmin():
                mesh = refine( mesh )

        space = spaceTools.getFineSpace( mesh, "CG",
                                         settings.intDegreeGoalMollifier )

        domainInt = Expression( """pow(x[0] - x0x,2) + pow(x[1] - x0y,2) < eps2 
                            ? 1.0/eps2 * exp(-1.0 /(1 - (pow(x[0] - x0x,2) + pow(x[1] - x0y,2))/eps2))  
                            : 0""",
                            x0x = x0[0],
                            x0y = x0[1],
                            eps2 = eps ** 2,
                            element = space.ufl_element() )
        domainInt = interpolate( domainInt, space )
        C = 1.0 / assemble( domainInt * dx )

        print "Computed C in Goal: {C}".format( C = C )
    goal = Expression( """pow(x[0] - x0x,2) + pow(x[1] - x0y,2) < eps2 
                        ? C/eps2 * exp(-1.0 /(1 - (pow(x[0] - x0x,2) + pow(x[1] - x0y,2))/eps2)) 
                        : 0""",
                        x0x = x0[0],
                        x0y = x0[1],
                        C = C,
                        eps = eps,
                        eps2 = eps ** 2,
                        degree = 10 )
    return goal

def run():

    #------------------------------------------------------------ define problem
#     problemName = "ExponentialBuckle"
    problemName = "SimpleSinSin"
    mesh = UnitSquareMesh( 5, 5 )

    #--------------------------------------------------------------- define goal
    x0 = [0.8, 0.8]
    eps = 0.01
    goal = MollifiedPoint2( mesh,
                            x0,
                            eps = eps,
                            C = 2.14356577539 )
    #----------------------------------------------------- define algo prameters
    minRuns = 10
    runs = 100
    maxDoF = 1000 # 1000
    dt = 1e-5 # 1e-4
    overEstConst = 2e0 # 2e1
    adaptiveFEM = True
    adaptiveDT = True # provided by euler module near boundary upto 10^2 diff
    adaptiveFEMDT = False # based on the diameters of triangles near the start point
    adaptiveDTConst = 10 # 10

    #--------------------------------------------------------------------- solve
    tStartP = tm.clock()
    tStartW = tm.time()
    solver = adaptiveSPDESolver( problemName,
                                 mesh,
                                 runs = runs,
                                 minRuns = minRuns,
                                 maxDoF = maxDoF,
                                 dt = dt,
                                 adaptiveFEM = adaptiveFEM,
                                 adaptiveDT = adaptiveDT,
                                 adaptiveFEMDT = adaptiveFEMDT,
                                 adaptiveDTConst = adaptiveDTConst,
                                 goal = goal )

    means, vars, nSamps, timeP, timeW = solver.solveAdaptive( mesh,
                                                              overEstConst = overEstConst ) # 20 fuer spde1
    tEndP = tm.clock()
    tEndW = tm.time()

    #---------------------------------------------------------------------- save
    dirName = "spdeGoal_" + tm.strftime( '%Y-%m-%d_%H-%M-%S' )
    dir = "out" + os.path.sep + dirName + os.path.sep
    meansData = [saveFunction( mean ) for mean in means]
    varsData = [saveFunction( varFun ) for varFun in vars]
    nSampsData = [saveFunction( nSampsFun ) for nSampsFun in nSamps]

    data = dict( [ ( name, eval( name ) ) for name in [ "problemName",
                                                        "meansData",
                                                        "varsData",
                                                        "nSampsData",
                                                        "tStartP",
                                                        "tStartW",
                                                        "tEndP",
                                                        "tEndW",
                                                        "dir",
                                                        "dt",
                                                        "maxDoF",
                                                        "minRuns",
                                                        "runs",
                                                        "adaptiveFEM",
                                                        "adaptiveDT",
                                                        "adaptiveFEMDT",
                                                        "timeP",
                                                        "timeW",
                                                        "x0",
                                                        "eps"
                                                      ] ] )

    try:
        os.makedirs( dir )
    except:
        pass


    filename = dir + "result.dat"

    with open( filename, 'wb' ) as outFile:
        pickle.dump( data, outFile,
                    protocol = pickle.HIGHEST_PROTOCOL )

    #---------------------------------------------------------------------- eval
    try:
        p1InterpolationAdaptiveGoalAnalyse.run( dirName )
    except Exception:
        traceback.print_exc()
        print "\nERROR: Could not run post processing.\n"

    return filename


if __name__ == "__main__":
    run()
    interactive()

    print "="*80
    print "finished"
