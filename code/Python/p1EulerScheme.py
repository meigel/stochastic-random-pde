#!/usr/bin/env python
from scipy.stats import norm as normal

import numpy as np
import itertools as it
from dolfin import *


def pairwise( iterable ):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    extIter = it.chain( iterable, [iterable[0]] )
    a, b = it.tee( extIter )
    next( b, None )
    return it.izip( a, b )

def insideDomain( boundary, x0 ):
    """
    Returns True if x0 is inside the boundary polygon. Uses cross product which
    always has to point down.
    """
    for ( a, b ) in pairwise( boundary ):
        if np.cross( b - a, x0 - a ) > 0:
            return False
    return True

def lineDist( A, B, P ):
    """
    compute the distance of point *P* from line *AB*
    """

    length2 = np.linalg.norm( A - B ) ** 2
    #---------------------------------------------------------- Case: A == B
    if length2 == 0.0:
        return np.linalg.norm( P - A )
    t = np.dot( P - A, B - A ) / length2
    #-------------------------------------------------------- Case: Beyond A
    if t < 0.0:
        return np.linalg.norm( P - A )
    #-------------------------------------------------------- Case: Beyond B
    elif t > 1.0:
        return np.linalg.norm( P - B )
    #--------------------------------------------------- Case: in the middle
    Z = A + t * ( B - A )
    return np.linalg.norm( P - Z )

def boundaryDist( boundary, x ):
    dist = float( "inf" )
    for ( b1, b2 ) in pairwise( boundary ):
        dist = min( dist, lineDist( b1, b2, x ) )

    return dist

def perp( a ) :
    b = np.empty_like( a )
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
# return
def segIntersect( a1, a2, b1, b2 ) :
    da = a2 - a1
    db = b2 - b1
    dp = a1 - b1
    dap = perp( da )
    denom = np.dot( dap, db )
    num = np.dot( dap, dp )
    fact = num / denom.astype( float )
    return fact * db + b1, fact

def boundaryIntersect( boundary, xIn, xOut ):
    bestPoint = None
    bestT = float( "inf" )
    for ( b1, b2 ) in pairwise( boundary ):
        point, t = segIntersect( b1, b2, xIn, xOut )
        if t >= 0 and t <= bestT:
            bestPoint, bestT = point, t

    return bestPoint, bestT

def gradient( u ):
    spaceU = u.function_space()
    mesh = spaceU.mesh()

    spaceGrad = VectorFunctionSpace( mesh, 'DG', 0 )
    uGrad = TrialFunction( spaceGrad )
    vGrad = TestFunction( spaceGrad )

    # This is the result we're after
    # gradUForm = nabla_grad( u )
    # v0 = project( gradUForm, spaceGrad )

    # The mass matrix is diagonal. Get the diagonal
    M = assemble( inner( uGrad, vGrad ) * dx )
    ones = Function( spaceGrad )
    ones.vector()[:] = 1.
    Md = M * ones.vector()

    gradUForm = assemble( inner( vGrad, grad( u ) ) * dx )

    gradU = Function( spaceGrad )
    gradU.vector().set_local( gradUForm.array() / Md.array() )

    return gradU


def singlePath( x0, boundary, dt, u_D, kappa, kappaGrad, f,
                adaptDT = True ):
    np.random.seed( seed = 0 )

    xi = x0
    intF = 0
    curDT = dt
    while True:
        if adaptDT:
            dist = boundaryDist( boundary, xi )
            curDT = dt * dist
        wiener = normal.rvs( loc = 0, scale = 1, size = 2 )
        step = kappaGrad( xi ) * curDT + np.sqrt( 2 * kappa( xi ) * curDT ) * wiener
        if insideDomain( boundary, xi + step ):
            xi += step
            intF += f( xi ) * np.linalg.norm( step ) # curDT #
        else:
            break
    # retreat one step back into domain
    xiOut = xi + step
    xiTau, t = boundaryIntersect( boundary, xi, xiOut )
    intF += f( xiTau ) * np.linalg.norm( xiTau - xi ) # * np.linalg.norm( xiTau - xi )

    return u_D( xiTau ) + intF

if __name__ == "__main__":
    from problems import spde1
    problem = spde1.problem()

    x0 = np.array( [0.5, 0.5] )
    boundary = np.array( [[0, 0], [0, 1], [1, 1], [1, 0]] )
    dt = 1e-3
    N = 1000

    samps = np.empty( N, dtype = float )
    for i in range( N ):
        print i
        kappa = problem.kappa.sample()
        single = singlePath( x0, boundary, dt,
                    u_D = problem.dirichletFunction,
                    kappa = kappa,
                    kappaGrad = gradient( kappa ),
                    f = problem.rhs )
        samps[i] = single

    print samps
    print np.mean( samps )

    print "done"
