#!/usr/bin/env python
import sys

class Logger( object ):
    """
    Create the logger and initialize path names based on time stamps.
    
    Parameters
    ----------
    logError : bool, optional
        if *True* stderr is saved as well.
    """
    def __init__( self, logDir, logError = True, openType = "w", alwaysFlush = True ):
        filepath = logDir + "log.txt"

        #--------------------------- save stdout and stderr and redirect to self
        self._terminal = sys.stdout
        self._error = sys.stderr

        self._log = open( filepath, openType )
        sys.stdout = self
        if logError:
            self._logError = True
            sys.stderr = self
        self.alwaysFlush = alwaysFlush

    def write( self, message ):
        """
        Write to the loggers files as well as to the console.
        
        Parameters
        ----------
        message : string
            message to output
        """
        message = message.encode( 'UTF-8' )
        if self._terminal is not None:
            self._terminal.write( message )
        if self._log is not None:
            self._log.write( message )

        if self.alwaysFlush:
            self.flush()

    def flush( self ):
        """
        Flush stdout, stderr and the logger.
        """
        self._terminal.flush()
        self._log.flush()
        if self._logError:
            self._error.flush()
