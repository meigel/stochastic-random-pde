#!/usr/bin/env python
import multiprocessing
import os

from dolfin import *

import ExponentialBuckle as em
import cPickle as pickle
from data_utils import loadFunction
import numpy as np
from p1InterpolationAdaptiveGoal import MollifiedPoint2
import time as tm


if True:
    import matplotlib as mpl
    mpl.use( 'Agg' )
    import matplotlib.pyplot as plt
    import plothelper

def run( dir = "spde_2015-05-07_19-50-14" ):
    #----------------------------------------------------------------- load data
    filename = "out" + os.path.sep + dir + os.path.sep + "result.dat"
    with open( filename, 'rb' ) as inFile:
        data = pickle.load( inFile )

    problemName = data.pop( "problemName", "SimpleSinSin" )
    print "Problem Name:", problemName
    for key in  data.keys():
        exec( key + " = data['" + key + "']" )

    means = [loadFunction( funData = meanData ) for meanData in meansData]
    vars = [loadFunction( funData = varData ) for varData in varsData]
    nSamps = [loadFunction( funData = sampData ) for sampData in nSampsData]

    goal = MollifiedPoint2( mesh,
                            x0,
                            eps = eps,
                            C = 2.14356577539 )

    #-------------------------------------------------------------- post process
    tDiffP = tEndP - tStartP
    tDiffW = tEndW - tStartW
    print "time taken (P):", tDiffP / 60.0, "min"
    print "time taken (W):", tDiffW / 60.0, "min"

    print "\nload reference solution ...",
    meanRef = loadFunction( problemName + "_mean.fun" )
    print "done"
    print "compute reference goal ...",
    goalRef = assemble( meanRef * goal * dx )
    print "done"

    print "\ncompute L2 and H1 errors ...",
    refSpace = meanRef.function_space()
    errorL2 = []
    errorH1 = []
    errorGoal = []
    for mean in means:
        e = interpolate( mean, refSpace )
        goalSol = assemble( e * goal * dx )
#         e.vector()[:] = e.vector().array() - meanRef.vector().array()
#         errorL2.append( norm( e, "L2" ) )
#         errorH1.append( norm( e, "H1" ) )
        errorGoal.append( abs( goalRef - goalSol ) )
    print "done"

#     print "L2 error:", errorL2
#     print "H1 error:", errorH1
    print "Goal error:", errorGoal

    #---------------------------------------------------------------------- plot
    ax = plt.subplot( 111 )
#     ax.loglog( [mean.function_space().dim() for mean in means ],
#                 errorL2, 'ko-', label = "L2" )
#     ax.loglog( [mean.function_space().dim() for mean in means ],
#                 errorH1, 'ks-', label = "H1" )
    ax.loglog( [mean.function_space().dim() for mean in means ],
                errorGoal, 'kd-', label = "Goal" )
    ax.set_position( [0.1, 0.1, 0.5, 0.6] )
    lgd = ax.legend( bbox_to_anchor = ( 1.05, 1 ), loc = 2, borderaxespad = 0. )
    plt.savefig( dir + "error.pdf",
                 bbox_extra_artists = ( lgd, ),
                 bbox_inches = 'tight' )
    for name in ["means", "vars", "nSamps"]:
        plt.figure()
        plothelper.plot_mpl( eval( name )[-1] )
        plt.colorbar()
        plt.title( name[:-1] )
        plt.savefig( dir + name[:-1] + ".pdf" )

    plot( means[-1], title = "mean" )
    plot( vars[-1], title = "var" )
    plot( nSamps[-1], title = "N" )
    plot( meanRef, title = "reference solution" )

    #----------------------------------------------------------- localised error
#     for mean in means:
#         mesh = mean.function_space().mesh()
#         Constants = FunctionSpace( mesh, "DG", 0 )
#         w = TestFunction( Constants )
#         lastMean = interpolate( mean, refSpace )
#         err2 = Function( refSpace )
#         err2.vector()[:] = ( meanRef.vector().array() - lastMean.vector().array() ) ** 2
#         localErrFun = Function( Constants )
#         localErrFun.vector()[:] = assemble( err2 * w * dx ).array() ** ( 1.0 / 2 )
#         plot( localErrFun, title = "local error" )



if __name__ == "__main__":
    run()
    interactive()

    print "="*80
    print "done"
