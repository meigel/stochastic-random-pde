from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('pdf')
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import time
from tictoc import TicToc
from dolfin import *

import OneDiscExample as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper

def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def solve_fenics( pde_data, interpolator ):
    def boundary( x, on_boundary ):
        return on_boundary
    uD = pde_data["uD"]
    f = pde_data["f"]
    g = pde_data["g"]
    kappa = pde_data["kappa"]
    V = interpolator.V
    bc = DirichletBC( V, uD, boundary )
    u = TrialFunction( V )
    v = TestFunction( V )
    a = inner( kappa * grad( u ), grad( v ) ) * dx
    L = f * v * dx + g * v * ds
    u = Function( V )
    solve( a == L, u, bc )
    return u

f = Expression("-6*x[0]*x[1]*(x[0]*x[0]+x[1]*x[1]-2)") # rhs
kappa = Constant( 1.0 ) # diffusion coefficient
uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" ) # Dirichlet bc
# uD = Constant(0.0)
g = Constant( 0.0 ) # Neumann bc
pde_data = {"kappa":kappa, "f":f, "uD":uD, "g":g}

#######################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
#######################################

### number of equidistant grid intervals
### => N+1 grid points
N = 99
refMesh = UnitSquareMesh(N, N)
intRef = setup_interpolator(mesh=refMesh)

degree = 7
loc_degree = 3

# uF = solve_fenics( pde_data, interpolator )

ph = PlotHelper()

### step size
dt = 1e-4

### evaluation point for single point version
x0 = [0.5, 0.5]

### number of threads
numThreads = 1

### collect intermediate points?
intermediate = -1

### switch adaptivity
adaptive = True

#dimGrid = [N+1, N+1]
seed = float('inf')
#prob_dim = len(dimGrid)

runs = 10

# meanRef = loadFunction( "spde1b_mean.fun" )  # SimpleSinSin_mean.fun
# meanRef = uF
# space = meanRef.function_space()
# meanRef = Expression("5*(exp(10*x[0]*x[0])-1)*(1-x[0])*(1-x[0])*x[0]*x[0]*(exp(10*x[1]*x[1])-1)*x[1]*x[1]*(1-x[1]*x[1])")
# x0_sol = meanRef( x0 )

# ph["rhs f"].plot(interpolate(f, space))

### single point version
with TicToc(key="stochEulerC", accumulate=False, do_print=True):
    means = problem.stochEulerC( numThreads,
                            seed,
                            runs,
                            dt,
                            adaptive,
                            intermediate,
                            x0,
                            1,
                            0,
                            1 );

# print "ref. solution :", x0_sol
print "means : ", means

# print "absolute error :", abs(x0_sol-means[0, 2])
# print "relative error :", abs(x0_sol-means[0, 2])/x0_sol

#runs = 100

# ### grid version
# with TicToc(key="stochEulerC", accumulate=False, do_print=True):
#     means = problem.stochEulerC( numThreads,
#                             seed,
#                             runs,
#                             dt,
#                             adaptive,
#                             intermediate,
#                             intRef.mesh.coordinates().flatten(),
#                             (N+1)*(N+1) );
#   
# u_mean = intRef.create_interpolationGLS( means, degree=degree)
# u_var = intRef.create_interpolationGLS( means[:,(0,1,3)], degree=degree)
# ph["GLS: plot mean, degree "+str(degree)].plot(u_mean)
# plt.savefig("plotmean.png")
# ph["GLS: plot variance, degree "+str(degree)].plot(u_var)
  
# u_R = intRef.create_interpolationMLS(means, neighbours=1000, degree=loc_degree, max_dist=4/N)
# ph["MLS: plot, degree "+str(loc_degree)].plot(u_R)

### plotting stochastic field

#kappa = problem.evalKappaC(x0, [1,1])

# coeffs = np.zeros(100)
# coeffs[0] = 1
#  
# print coeffs

# problem.setCoefficientsC(coeffs)

plt.clf()

refMesh = UnitSquareMesh(N, N)

XYvals = refMesh.coordinates()

kappa = problem.evalKappaC(XYvals.flatten(), (N+1)*(N+1))

Zvals = kappa[:,2]

print("converting mesh to triangles ... "),

trias = mesh2triang(refMesh)

fig = plt.figure(figsize=(20.36, 12.72))
ax = Axes3D(fig)
surf = ax.plot_trisurf(trias, Zvals, cmap=plt.cm.jet, linewidth=0.1, alpha=0.6)

print("done")

### grid version
try:
    Iu = intRef.create_interpolationC(kappa)
    plot(Iu, title="Stochastic field")
    ax.view_init( 25, -110) # was (75 , -120), (25, -110)
    
    ax.set_xlim3d(0, 1)
    ax.set_ylim3d(0, 1)
    ax.set_zlim3d(0, 2)
    
    plt.setp(ax.get_xticklabels(), fontsize=30)
    plt.setp(ax.get_yticklabels(), fontsize=30)
    plt.setp(ax.get_zticklabels(), fontsize=30)
    
#     print("saving figure ... "),
    
#     plt.savefig("kappaRough"+str(N+1)+".pdf")
#     plt.savefig("kappaRough"+str(N+1)+".png", dpi = 94.34)
    plt.show()
    
    print("done")
    
### single point
except:
    print kappa

interactive()
