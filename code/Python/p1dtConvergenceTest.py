#!/usr/bin/env python
import multiprocessing

from scipy import stats

import matplotlib.pyplot as plt
import numpy as np


if __name__ == "__main__":
    #------------------------------------------------------------------ settings
    problemName = "SimpleSinSin"
    maxI = 4

    seed = float( "inf" )
    runs = 1000
    adaptiveDT = True
    intermediate = float( "inf" )
    coord = [0.3, 0.3]
    numThreads = multiprocessing.cpu_count()

    #------------------------------------------------------------------- compute
    dts = [10 ** ( -( i + 1 ) ) for i in range( maxI )]
    exec( "import " + problemName + " as em" )

    means = []
    vars = []
    for dt in dts:
        retVals = em.stochEulerC( numThreads = numThreads,
                                  seed = seed,
                                  numTraj = runs,
                                  stepsize = dt,
                                  adapt = adaptiveDT,
                                  intermediate = intermediate,
                                  grid = coord,
                                  numGridPoints = 1,
                                  M = 2 )
        samps = retVals[:, 2]
        means.append( np.mean( samps ) )
        vars.append( np.var( samps ) )

    #-------------------------------------- post process with last  level as ref
    meanError = [np.abs( m - means[-1] ) for m in means[:-1]]
    varError = [np.abs( v - vars[-1] ) for v in vars[:-1]]

    print means
    print vars
    beta, c_M, _, _, _ = stats.linregress( np.log10( dts[:-1] ), np.log10( meanError ) )
    print beta, c_M
#     plt.loglog( dts[:-1], meanError, '.', dts[:-1], 10 ** c_M * dts[:-1] ** beta, 'r' )
    plt.show()
