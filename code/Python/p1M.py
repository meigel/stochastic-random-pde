#!/usr/bin/env python
from __future__ import division
import multiprocessing

from scipy import stats

import numpy as np
from data_utils import loadFunction
from p1InterpolationAdaptiveTest import _printDebugNumbers, loadProblem
import time as tm
import os
from dolfin import *

def computeLevel( backend, em, numThreads, numSamps, dt, adaptiveDT, coord, M, problem = None ):
    if backend == "eulermodule":
        retVals = em.stochEulerC( numThreads = numThreads,
                                      seed = float( "inf" ),
                                      numTraj = int( numSamps ),
                                      stepsize = dt,
                                      adapt = adaptiveDT,
                                      intermediate = float( "inf" ),
                                      grid = coord,
                                      numGridPoints = 1,
                                      M = 0 )
        samps = retVals[:, 2]
    elif backend == "p1EulerSchemeC" or backend == "p1EulerSchemeP":
        samps = em.multiPath( int( numSamps ),
                              dt,
                              adaptiveDT,
                              coord,
                              problem,
                              M = 0 )
    return samps

def run( problemName = "SimpleSinSin",
        nSamps = 100,
        initSamps = 10,
        dtTarget = 10 ** -3,
        adaptiveDT = True,
        coord = [0.5, 0.5],
        nLevel = 2,
        M = 4,
        numThreads = multiprocessing.cpu_count(),
        backend = "p1EulerSchemeP" ): # "eulermodule", "p1EulerSchemeC", "p1EulerSchemeP"
    #===========================================================================
    # settings
    #===========================================================================
    seed = float( "inf" )
    intermediate = float( "inf" )

    cSamps = nSamps / 10 ** ( nLevel - 1 )

    dt4level = [dtTarget * M ** i for i in reversed( range( nLevel ) )]

    problem = None
    if backend == "eulermodule":
        exec( "import " + problemName + " as em" )
    elif backend == "p1EulerSchemeC":
        import p1EulerSchemeC as em
        problem = loadProblem( problemName )
    elif backend == "p1EulerSchemeP":
        import p1EulerSchemeP as em
        problem = loadProblem( problemName )
    else:
        error( "Unknown Backend" )

   #============================================================================
   # estimate vars
   #============================================================================

    samps4lvl = []
    means4lvl = []
    vars4lvl = []

    for lev in range( nLevel ):
        if lev == 0:
            curM = 0
        else:
            curM = 1
        samps = computeLevel( backend, em, numThreads, initSamps, dt4level[lev],
                              adaptiveDT, coord, M = curM, problem = problem )
        samps4lvl.append( samps )
        means4lvl.append( np.mean( samps ) )
        vars4lvl.append( np.var( samps ) )
#     runs4level = np.ceil( [cSamps * 10 ** i for i in reversed( range( nLevel ) )] ) # not correct
#     print runs4level
    c = nSamps / np.sqrt( vars4lvl[0] * dt4level[0] )
    runs4level = np.ceil( [c * np.sqrt( var * dt ) for var, dt in zip( vars4lvl, dt4level )] )

    #===========================================================================
    # compute mean
    #===========================================================================
    for lev in range( nLevel ):
        if lev == 0:
            curM = 0
        else:
            curM = 1
        curNSamps = runs4level[lev] - initSamps
        if curNSamps > 0:
            samps = computeLevel( backend, em, numThreads, curNSamps,
                                  dt4level[lev], adaptiveDT, coord, M = curM,
                                  problem = problem )
            samps4lvl[lev] = np.concatenate( ( samps4lvl[lev], samps ) )
        means4lvl[lev] = np.mean( samps4lvl[lev] )
        vars4lvl[lev] = np.var( samps4lvl[lev] )
    print "Samps4lvl:", [len( samps ) for samps in samps4lvl]

    compEff = [len( samps ) / dt for samps, dt in zip( samps4lvl, dt4level )]
    print "CompEff:", compEff

    mean = np.sum( means4lvl )
    return mean, means4lvl, vars4lvl, np.sum( compEff )

if __name__ == "__main__":
    problemName = "SimpleSinSin"
    refSol = 0.86479131707
    coord = [0.5, 0.5]
    initSamps = 10
    nSamps = 100
    numLevels = 4
    minDT10 = 2
    maxDT10 = 4
    M = 4



    dts = [10 ** -i for i in range( minDT10, maxDT10 + 1 )]
    nLevels = [i for i in range( 1, numLevels + 1 )]
    results4level = []
    times4level = []
    compEffs4level = []
    for nLevel in nLevels:
        results4dts = []
        times4dts = []
        compEffs4dts = []
        for dt in dts:
            print "\nnLev: {nLevel}, dt: {dt}".format( nLevel = nLevel, dt = dt )
            tStart = tm.clock()
            mean, _, _, compEff = run( problemName = problemName,
                            nSamps = nSamps,
                            initSamps = initSamps,
                            dtTarget = dt,
                            coord = coord,
                            nLevel = nLevel,
                            M = M )
            tEnd = tm.clock()
            results4dts.append( mean )
            times4dts.append( tEnd - tStart )
            compEffs4dts.append( compEff )
        results4level.append( results4dts )
        times4level.append( times4dts )
        compEffs4level.append( compEffs4dts )

    from tools import plotTools

    hold = False
    for nLevel, results4dts, times4dts, compEffs4dts in zip( nLevels, results4level, times4level, compEffs4level ):
        meanError4dts = [abs( mean - refSol ) for mean in results4dts]
        print nLevel, "e", meanError4dts
        print nLevel, "t", times4dts
        print nLevel, "c", compEffs4dts
        plotTools.plot2D( compEffs4dts,
                     meanError4dts,
                     name = "Level {lev}".format( lev = nLevel ),
                     xlabel = "t",
                     ylabel = "e",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_right",
                     dots = True,
                     hold = hold
                     )
        hold = True
    filenameHeader = "p1M_"
    dirName = filenameHeader + tm.strftime( '%Y-%m-%d_%H-%M-%S' )
    dir = "out" + os.path.sep + dirName + os.path.sep
    try:
        os.makedirs( dir )
    except:
        pass

    filename = dir + "MLcompare"
    plotTools.savePlot( filename, show = True )

    print "done"

