from __future__ import division
import numpy as np
import os
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pylab
from matplotlib.patches import Rectangle
import time
from tictoc import TicToc
from dolfin import *

import euler as em
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats
from numpy import ndarray

# DEBUG ===
def TEST_reference_regression(means):
    Iu = np.zeros(72)
    for i in range(72):
        Iu[i]=np.array(means[0,i])
    u_R = Expression("""c0
    + c1 * x[0] + c2 * x[1]
    + c3 * x[0]*x[0] + c4 * x[0]*x[1] + c5 * x[1]*x[1]
    + c6 * x[0]*x[0]*x[0] + c7 * x[0]*x[0]*x[1] + c8 * x[0]*x[1]*x[1] + c9 * x[1]*x[1]*x[1]
    + c10 * x[0]*x[0]*x[0]*x[0] + c11 * x[0]*x[0]*x[0]*x[1] + c12 * x[0]*x[0]*x[1]*x[1] + c13 * x[0]*x[1]*x[1]*x[1] + c14 * x[1]*x[1]*x[1]*x[1]
    + c15 * x[0]*x[0]*x[0]*x[0]*x[0] + c16 * x[0]*x[0]*x[0]*x[0]*x[1] + c17 * x[0]*x[0]*x[0]*x[1]*x[1] + c18 * x[0]*x[0]*x[1]*x[1]*x[1] + c19 * x[0]*x[1]*x[1]*x[1]*x[1] + c20 * x[1]*x[1]*x[1]*x[1]*x[1] + c21 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c22 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c23 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c24 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c25 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c26 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c27 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]
    + c28 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c29 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c30 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c31 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c32 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c33 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c34 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c35 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c36 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c37 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c38 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c39 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c40 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c41 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c42 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c43 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c44 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]
    + c45 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c46 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c47 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c48 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c49 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c50 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c51 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c52 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c53 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c54 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]
    + c55 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c56 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c57 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c58 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c59 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c60 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c61 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c62 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c63 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c64 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c65 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]
    + c66 * sin(pi*x[0])*(x[0]-0.5)*(x[0]-0.5) + c67 *  sin(pi*x[1])*(x[1]-0.5)*(x[1]-0.5) + c68 *  sin(pi*x[0])*fabs(x[0]-0.5) + c69 *  sin(pi*x[1])*fabs(x[1]-0.5) + c70 *  sin(pi*x[0]) + c71 *  sin(pi*x[1])""",
                     c0 = Iu[0], c1 = Iu[1], c2 = Iu[2], c3 = Iu[3], c4 = Iu[4], c5 = Iu[5], c6 = Iu[6], c7 = Iu[7], c8 = Iu[8], c9 = Iu[9],
                     c10 = Iu[10], c11 = Iu[11], c12 = Iu[12], c13 = Iu[13], c14 = Iu[14], c15 = Iu[15], c16 = Iu[16], c17 = Iu[17], c18 = Iu[18], c19 = Iu[19],
                     c20 = Iu[20], c21 = Iu[21], c22 = Iu[22], c23 = Iu[23], c24 = Iu[24], c25 = Iu[25], c26 = Iu[26], c27 = Iu[27], c28 = Iu[28], c29 = Iu[29],
                     c30 = Iu[30], c31 = Iu[31], c32 = Iu[32], c33 = Iu[33], c34 = Iu[34], c35 = Iu[35], c36 = Iu[36], c37 = Iu[37], c38 = Iu[38], c39 = Iu[39],
                     c40 = Iu[40], c41 = Iu[41], c42 = Iu[42], c43 = Iu[43], c44 = Iu[44], c45 = Iu[45], c46 = Iu[46], c47 = Iu[47], c48 = Iu[48], c49 = Iu[49],
                     c50 = Iu[50], c51 = Iu[51], c52 = Iu[52], c53 = Iu[53], c54 = Iu[54], c55 = Iu[55], c56 = Iu[56], c57 = Iu[57], c58 = Iu[58], c59 = Iu[59],
                     c60 = Iu[60], c61 = Iu[61], c62 = Iu[62], c63 = Iu[63], c64 = Iu[64], c65 = Iu[65], c66 = Iu[66], c67 = Iu[67], c68 = Iu[68], c69 = Iu[69],
                     c70 = Iu[70], c71 = Iu[71])
    return u_R
# === DEBUG


#######################################################
### Variables for FEniCS only, which you can manipulate
### These have no effect in the C++ code
#######################################################

# # right hand side
# f = Constant(1.0)
#
# # permeability coefficient
# kappa = Constant(1.0)
#
# # Dirichlet boundary condition
# #uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" )
# uD = Constant(0.0)
#
# # Neumann boundary condition
# g = Constant(0.0)
#
# # collect everything in one object
# pde_data = {"kappa": kappa, "f": f, "uD": uD, "g": g}

###############################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
###############################################

# number of equidistant grid intervals
#    => N+1 grid points
N = 100

# step size
dt = 10**-4

# evaluation point for single point version
x0 = [0.5, 0.5]

# number of threads
numThreads = 40

# collect intermediate points?
intermediate = -1

# switch adaptivity
adaptive = True

# degree of polynomial for regression (10 is max currently)
degree = 7
loc_degree = 3

dimGrid = [N+1, N+1]
seed = float('inf')
prob_dim = len(dimGrid)
parallel_data = {"numThreads": numThreads, "adaptive": adaptive, "intermediate": intermediate, "dimGrid": [1,1], "seed": seed}

runs = 1
numPoints = 1000000

# the mesh is located in interpolator.mesh
interpolator = setup_interpolator(N)

# create some uniform random points in 2D
# =======================================
points = stats.uniform.rvs( loc = 0, scale = 1, size = (numPoints, prob_dim) )

# plt.plot(points[:,0], points[:,1], 'p')
# plt.title(str(numPoints)+' random 2D points')
# plt.show()
# plt.clf()

# run SDE simulations
# ===================

with TicToc(key="stochEulerC", accumulate=False, do_print=True):
    means = em.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            points.flatten(),
                            numPoints)

# evaluate local polynomial regression
# ====================================
# neighbour_scale = 30
# max_dist_scale = 10
# dist_weight = 0.5
# with TicToc(key="interpolationMLS", accumulate=False, do_print=True):
#    Iu = interpolator.create_interpolationMLS(means, neighbours=neighbour_scale*runs, max_dist=max_dist_scale*dt, dist_weight=dist_weight, degree=2)

#plot(Iu, mesh = interpolator.mesh, title="MLS of stoch. solution")

# load reference solution
# =======================
meanRef = loadFunction("spde1_mean.fun")
coords = interpolator.mesh.coordinates()
meanRefvec = meanRef.vector().array()
meanVals = np.empty(len(coords), dtype="float")
for i, coord in enumerate(coords):
    meanVals[i] = meanRef(coord)
coordsVal = np.column_stack((coords[:,0], coords[:,1], meanVals))

ph = PlotHelper()

# evaluate global polynomial regression
# =====================================

# regression on the stochastic solution
# StochReg means regression on stochastic solution
# ================================================

# stoch = em.regressionC(np.array(means).flatten(), (N+1)*(N+1), degree)
# u_StochReg = TEST_reference_regression(stoch)
u_StochRegLoc = interpolator.create_interpolationMLS(means, neighbours=10000, degree=loc_degree, max_dist=0.1)
u_StochRegGlo = interpolator.create_interpolationGLS(means, degree=degree)
ph["GLS regression of stoch. solution, degree "+str(degree)].plot(u_StochRegGlo)
ph["MLS regression of stoch. solution, degree "+str(loc_degree)].plot(u_StochRegLoc)

# regression on the reference FEM solution
# RefReg means regression on reference solution
# =============================================
# stoch = em.regressionC(np.array(coordsVal).flatten(), (N+1)*(N+1), degree)
# u_RefReg = TEST_reference_regression(stoch)
# u_RefReg = interpolator.create_interpolationMLS(coordsVal, neighbours=16, degree=degree, max_dist=1)
u_RefReg = interpolator.create_interpolationGLS(coordsVal, degree=degree)
ph["GLS regression of reference solution, degree "+str(degree)].plot(u_RefReg)



# calculating and plotting errors
# ===============================
errFGlob = Function(meanRef.function_space())
errFGlob.vector()[:] = meanRef.vector() - interpolate(u_StochRegGlo, meanRef.function_space()).vector()
errFLoc = Function(meanRef.function_space())
errFLoc.vector()[:] = meanRef.vector() - interpolate(u_StochRegLoc, meanRef.function_space()).vector()

# evaluate global errors
print("\n\nglobal regression errors: L2 = %s\tH1 = %s" % (norm(errFGlob, "L2"), norm(errFGlob, "H1")))
print("local regression errors: L2 = %s\tH1 = %s" % (norm(errFLoc, "L2"), norm(errFLoc, "H1")))

# ph["FEM reference solution"].plot(meanRef)
# ph["Error stoch. regression, degree "+str(degree)].plot(errFStoch)
# ph["Error ref. regression, degree "+str(degree)].plot(errFRef)
# ph["Regression difference"].plot(errFStoch - errFRef)

interactive()
