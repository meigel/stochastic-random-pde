from __future__ import division
import numpy as np
import os
from matplotlib import pyplot as plt
import time
from tictoc import TicToc
from dolfin import *

import ExponentialBuckle as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper

#######################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
#######################################

# number of equidistant grid intervals
#     => N+1 grid points
N = 30

# step size
dt = 1e-4

# evaluation point for single point version
x0 = [0.9, 0.9]

### number of threads
numThreads = 50

# collect intermediate points?
intermediate = 3/N
# intermediate = -1

# switch adaptivity
adaptive = True

# degree of polynomial for regression (10 is max currently)
glob_degree = 7

# degree of MLS (moving least squares) interpolation
loc_degree = 1

dimGrid = [N+1, N+1]
seed = float('inf')
prob_dim = len(dimGrid)
parallel_data = {"numThreads":numThreads, "adaptive":adaptive, "intermediate":intermediate, "dimGrid":[1,1], "seed":seed}

runs = (N+1)*(N+1)*10

interpolator = setup_interpolator( N )

mesh = interpolator.mesh

# load reference solution
meanRef = loadFunction("ExponentialBuckle_mean.fun")
coords = mesh.coordinates()
meanRefvec = meanRef.vector().array()
meanVals = np.empty(len(coords), dtype="float")
for i, coord in enumerate(coords):
    meanVals[i] = meanRef(coord)
coordsVal = np.column_stack((coords[:,0], coords[:,1], meanVals))

x0_sol = meanRef(x0)

ph = PlotHelper()

### single point version
with TicToc(key="stochEulerC", accumulate=False, do_print=True):
    means = problem.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            x0,
                            1 );

idx = means[:,3] > 0.
 
mean = np.mean(means[idx,2])
var = np.var(means[idx, 2])
 
print "absolute error :", abs(x0_sol-mean)
print "relative error :", abs(x0_sol-mean)/x0_sol
 
print "mean = ", mean
print " var = ", var
 
idx = means[:,3] != means[0,3]
 
mod_means = np.vstack((np.hstack((x0, mean, var)), means[idx]))
 
interpolator.refine()
 
with TicToc(key="create_interpolationMLS", accumulate=False, do_print=True):
    u_StochMLS1 = interpolator.create_interpolationMLS(mod_means,
                                                      neighbours=1000,
                                                      degree=loc_degree,
                                                      max_dist=5/N)
 
### grid version
runs = 10
 
with TicToc(key="stochEulerC", accumulate=False, do_print=True):
    means = problem.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            mesh.coordinates().flatten(),
                            (N+1)*(N+1));
 
# for max_dist = 0.5
# 42980 no
# 42981 yes
 
print np.array(means).shape
 
with TicToc(key="create_interpolationMLS", accumulate=False, do_print=True):
    u_StochMLS2 = interpolator.create_interpolationMLS(means,
                                                      neighbours=1000,
                                                      degree=loc_degree,
                                                      max_dist=4/N)
 
# calculating and plotting errors
# ===============================
errFSP = Function(meanRef.function_space())
errFSP.vector()[:] = meanRef.vector() - interpolate(u_StochMLS1, meanRef.function_space()).vector()
errFGrid = Function(meanRef.function_space())
errFGrid.vector()[:] = meanRef.vector() - interpolate(u_StochMLS2, meanRef.function_space()).vector()
 
ph["Error of single point solution, degree "+str(loc_degree)].plot(errFSP)
ph["Error of grid ("+str(N+1)+" x "+str(N+1)+") solution, degree "+str(loc_degree)].plot(errFGrid)
 
# evaluate global errors
print("\n\nsingle point errors: L2 = %s\tH1 = %s" % (norm(errFSP, "L2"), norm(errFSP, "H1")))
print(" grid points errors: L2 = %s\tH1 = %s" % (norm(errFGrid, "L2"), norm(errFGrid, "H1")))
 
 
interactive()
