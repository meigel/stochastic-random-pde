from scipy import spatial
import itertools as iter
import numpy as np
import ufl
#from dolfin import (FunctionSpace, Mesh, Expression, Function, UnitSquareMesh, interpolate, FiniteElement, refine,
#                       vertex_to_dof_map)
from dolfin import *
import math


class Interpolator( object ):
    """ FEM interpolation class with global/local regression.
        NOTE: argument means has the form [x y z] """

    def __init__(self, mesh, inside_domain, degree=1, globdegree = 16):
        self.mesh = mesh
        self.degree = degree
        self.inside_domain = inside_domain
        self.globdegree = globdegree
        self._setup()

    def _setup(self):
        self.V = FunctionSpace(self.mesh, 'CG', self.degree)
        try:
            # d2vm = dof_to_vertex_map(self.V)
            self.d2vm = vertex_to_dof_map(self.V)
        except:
            # FEniCS version < 1.4
            DM = self.V.dofmap()
            self.d2vm = DM.dof_to_vertex_map(self.mesh)
        
        basis = self._get_monom_basis(self.globdegree)
        self._integrate_monoms_exactly(basis)

    def refine(self):
        self.mesh = refine(self.mesh)
        self._setup()

    def _get_monom_basis(self, degree, totaldegree=True):
        basis = [b for b in iter.product(range(degree+1), range(degree+1)) if b[0]+b[1] <= degree or totaldegree]
        return basis

    def _get_monom_basis_set(self, max_degree):
        B = []
        for p in range(max_degree):
            B.append(self._get_monom_basis(p, totaldegree=False))
            B.append(self._get_monom_basis(p, totaldegree=True))
        return B

    def _leg(self, x, degree):
        n = degree
        return sum([(-1.)**k * math.factorial(2*n-2*k)/(math.factorial(n-k)*math.factorial(n-2*k)*math.factorial(k)*2.**n)*(x**(n-2*k)) for k in range(0, int(math.floor(n/2.))+1)])

    def _evaluate_legendre_basis(self, xy, degree, totaldegree=True):
         # get basis
        basis = self._get_monom_basis(degree, totaldegree)
        # evaluate at all points
        lxy = np.ndarray((xy.shape[0], len(basis)))
        
        for bi, expo in enumerate(basis):
            p = expo[0]
            q = expo[1]
            lxy[:, bi] = math.sqrt(2.*p+1.)*self._leg(2.*xy[:,0]-1, p) * math.sqrt(2.*q+1.)*self._leg(2.*xy[:,1]-1, q)
            
        return lxy, basis

    def _integrate_monoms_exactly(self, basis):
        self.G = np.ndarray((len(basis), len(basis)))
        
        for i, k in enumerate(basis):
            for j, kprime in enumerate(basis):
                p = k[0]
                q = k[1]
                pprime = kprime[0]
                qprime = kprime[1]
                self.G[i, j] = 1./((p+pprime+1.) * (q+qprime+1.))
                
    def _evaluate_monom_basis(self, xy, degree, totaldegree=True):
        # get basis
        basis = self._get_monom_basis(degree, totaldegree)
        # evaluate at all points
        bxy = np.ndarray((xy.shape[0], len(basis)))
        for bi, expo in enumerate(basis):
            bxy[:,bi] = np.power(xy[:,0], expo[0]) * np.power(xy[:,1], expo[1])
        return bxy, basis

    def create_interpolation(self, f, runs=1):
        # evaluate nodal values
        coords = self.mesh.coordinates()
        fx = [np.array([f(x) for x in coords]) for _ in range(runs)]
        # compute point-wise expectation
        fx = sum(fx) / runs
        # create interpolation function
        u = Function(self.V)
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map(self.V)
        except:
            # FEniCS version < 1.4
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        u.vector()[d2vm] = fx
        return u

    def create_interpolationC(self, means):
        """ create interpolation assuming given values are nodal dofs """
        u = Function(self.V)
        u.vector()[self.d2vm] = np.array(means[:, 2])
        return u

    def create_interpolationGLS(self, means, degree=5):
        """ global polynomial least squares regression """        
        xy, z = means[:,0:2], means[:,2]
        
#         bcoords, basis = self._evaluate_monom_basis(coords, degree)
        
        bxy, basis = self._evaluate_monom_basis(xy, degree)
        # determine lsq coefficients
#         coeffs = np.linalg.lstsq(bxy, z)[0]
        
#         z_tilde = np.dot(bxy.T, z)/len(z)
        coeffs = np.linalg.lstsq(bxy, z)[0]
        # construct expression
        bex = map(lambda s:[t if len(t) > 0 else "1" for t in s], [(["*".join(["x[0]"]*b[0]),"*".join(["x[1]"]*b[1])]) for b in basis])
        bex = "+".join(["*".join([str(c)]+bb) for c, bb in zip(coeffs, bex)]).replace("+-", "-")
        #print(bex)
        # interpolate onto grid
        Gfex = Expression(bex, element=FiniteElement('Lagrange', ufl.triangle, 1))
        Gf = interpolate(Gfex, self.V)
        return Gf

    def create_interpolationLegendreExact(self, means, degree=5):
        """ global polynomial least squares regression """        
        xy, z = means[:,0:2], means[:,2]
        
#         bcoords, basis = self._evaluate_monom_basis(coords, degree)
        
        lxy, basis = self._evaluate_legendre_basis(xy, degree)
        # determine lsq coefficients
#         coeffs = np.linalg.lstsq(bxy, z)[0]
        
        legcoeffs = np.dot(lxy.T, z)/len(z)
        
#         print legcoeffs
        
        # construct expression
        bex = [(["std::sqrt("+str(2.*n+1)+")*("+"+".join([str((-1.)**k * math.factorial(2*n-2*k)/(math.factorial(n-k)*math.factorial(n-2*k)*math.factorial(k)*(2.**n)))+" * std::pow(2.*x[0]-1., "+str(n-2*k)+")" for k in range(0, int(math.floor(n/2.))+1)]).replace("+-", "-")+")", "std::sqrt("+str(2.*m+1)+")*("+"+".join([str((-1.)**k * math.factorial(2*m-2*k)/(math.factorial(m-k)*math.factorial(m-2*k)*math.factorial(k)*(2.**m)))+" * std::pow(2.*x[1]-1., "+str(m-2*k)+")" for k in range(0, int(math.floor(m/2.))+1)]).replace("+-", "-")+")"]) for n, m in basis]
        bex = "+".join(["*".join([str(c)]+bb) for c, bb in zip(legcoeffs, bex)]).replace("+-", "-")
        #print(bex)
        # interpolate onto grid
        Gfex = Expression(bex, element=FiniteElement('Lagrange', ufl.triangle, 1))
        Gf = interpolate(Gfex, self.V)
        return Gf
    
    def create_interpolationLegendre(self, means, degree=5):
        """ global polynomial least squares regression """        
        xy, z = means[:,0:2], means[:,2]
        
#         bcoords, basis = self._evaluate_monom_basis(coords, degree)
        
        lxy, basis = self._evaluate_legendre_basis(xy, degree)
        # determine lsq coefficients
#         coeffs = np.linalg.lstsq(bxy, z)[0]
        
        legG = np.dot(lxy.T, lxy)/len(lxy)
        legz = np.dot(lxy.T, z)/len(z)
        
        legcoeffs = np.linalg.lstsq(legG, legz)[0]
        # construct expression
        bex = [(["std::sqrt("+str(2.*n+1)+")*("+"+".join([str((-1.)**k * math.factorial(2*n-2*k)/(math.factorial(n-k)*math.factorial(n-2*k)*math.factorial(k)*(2.**n)))+" * std::pow(2.*x[0]-1., "+str(n-2*k)+")" for k in range(0, int(math.floor(n/2.))+1)]).replace("+-", "-")+")", "std::sqrt("+str(2.*m+1)+")*("+"+".join([str((-1.)**k * math.factorial(2*m-2*k)/(math.factorial(m-k)*math.factorial(m-2*k)*math.factorial(k)*(2.**m)))+" * std::pow(2.*x[1]-1., "+str(m-2*k)+")" for k in range(0, int(math.floor(m/2.))+1)]).replace("+-", "-")+")"]) for n, m in basis]
        bex = "+".join(["*".join([str(c)]+bb) for c, bb in zip(legcoeffs, bex)]).replace("+-", "-")
        #print(bex)
        # interpolate onto grid
        Gfex = Expression(bex, element=FiniteElement('Lagrange', ufl.triangle, 1))
        Gf = interpolate(Gfex, self.V)
        return Gf

    def create_interpolationMLS(self, means, neighbours=10, degree=2, max_dist=1):
        """ local polynomial moving least squares regression """
        def W(d, scale=1):
            """ simple tri-cube weighting """
            w = np.ones(len(d)) - scale * d**3
            return w**3

        # setup spatial tree and iterate vertices of (fine) mesh
        tree = spatial.cKDTree([(c[0], c[1]) for c in means])
        # query neighbours for mesh nodes
        coords = self.mesh.coordinates()
        kd_d, kd_i = tree.query(coords, k=neighbours)

        # solve MLS problems
        L = coords.shape[0]
        basis_set = self._get_monom_basis_set(degree)
        max_dim = len(basis_set[-1])
        assert(neighbours >= max_dim)
        A = np.ndarray((neighbours, max_dim))
        xvec = np.ndarray(max_dim)
        values = np.ndarray(L)
        for l, current_x, current_d, current_i in zip(range(L), coords, kd_d, kd_i):
            # remove nodes too far away, e.g. close to boundary (>max distance)
            current_ind = np.where(current_d <= max_dist)[0]
            current_d, current_i = current_d[current_ind], current_i[current_ind]

            # reduce local basis degree if too few neighbours
            bi = len(basis_set) - 1
            while len(current_ind) < len(basis_set[bi]) and bi > 0:
                bi -= 1
                # print "\tXXX", bi, len(basis_set[bi])
            basis = basis_set[bi]
#             print "BASIS", len(basis) , "current_ind", len(current_ind), "max_dist", max_dist, means.shape
            assert len(current_ind) >= len(basis)

            # setup MLS system
            scale = 1 / max(current_d)**3
            if max_dist is not None:
                w = W(current_d, scale)
            else:
                w = 1
            B = w * means[current_i, 2]
            xloc = means[current_i, 0:2]
            for bi, expo in enumerate(basis):
                bcol = np.power(xloc[:,0], expo[0]) * np.power(xloc[:,1], expo[1])
                xvec[bi] = current_x[0]**expo[0] * current_x[1]**expo[1]
                A[:len(current_ind), bi] = w * bcol

            # determine MLS approximation and evaluate at point x
            y = np.linalg.lstsq(A[:len(current_ind), :], B)[0]
            val = np.dot(xvec, y)
            values[l] = val
        u = Function(self.V)
        u.vector()[self.d2vm] = np.array(values)
        return u

def setup_interpolator(N=100, mesh=None):
    def inside_domain(x):
        return x[0] > 0 and x[0] < 1 and x[1] > 0 and x[1] < 1
    if mesh is None:
        mesh = UnitSquareMesh(N, N)
    return Interpolator(mesh, inside_domain)
