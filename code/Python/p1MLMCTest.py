#!/usr/bin/env python
import collections
import multiprocessing
import os

from dolfin import *
from joblib import Parallel, delayed
from scipy.stats import uniform

import ExponentialBuckle as em
from data_utils import *
import numpy as np
from p1InterpolationAdaptiveTest import indicator, criterion
import time as tm


if True:
    import matplotlib as mpl
    mpl.use( 'Agg' )
    import matplotlib.pyplot as plt
    import plothelper

#===============================================================================
# Compute one kappa sample
#===============================================================================
def _sampleOnGrid( coords, Ncoords, dt, oldSamps = None ):
    if not isinstance( Ncoords, collections.Iterable ):
        Ncoords = [Ncoords for coord in coords]

    mean = np.empty( len( coords ) )
    var = np.empty( len( coords ) )
    samps = [None for coord in coords]
    for iCoord, coord in enumerate( coords ):
        if oldSamps is None:
            N = Ncoords[iCoord]
        else:
            N = max( Ncoords[iCoord] - len( oldSamps[iCoord] ), 0 )
        ret = em.stochEulerC( 1,
                            float( "inf" ),
                            N,
                            dt,
                            True,
                            float( "inf" ),
                            coord,
                            1 )
        if oldSamps is None:
            samps[iCoord] = ret[:, 2]
        else:
            samps[iCoord] = np.concatenate( ( ret[:, 2], oldSamps[iCoord] ) )
        mean[iCoord] = np.mean( samps[iCoord] )
        var[iCoord] = np.var( samps[iCoord] )

    return mean, var, samps

def _computeKappaSample( meshCoarseCoords,
                       meshCoarseCells,
                       meshFineCoords = None,
                       meshFineCells = None,
                       NCoarse = -1,
                       NFine = -1
                       ):
    startP = tm.clock()
    #--------------------------------------------------------------- unpack data
    meshCoarse = deserialize( meshCoarseCells, meshCoarseCoords )
    dtCoarse = meshCoarse.hmin() / 10.0
    if meshFineCells is not None:
        meshFine = deserialize( meshFineCells, meshFineCoords )
        dtFine = meshFine.hmin() / 10.0
    else:
        meshFine = None

    spaceCoarse = FunctionSpace( meshCoarse, "CG", 1 )
    try:
        d2vmCoarse = vertex_to_dof_map( spaceCoarse )
    except:
        # FEniCS version < 1.4
        DM = spaceCoarse.dofmap()
        d2vmCoarse = DM.dof_to_vertex_map( meshCoarse )

    if meshFine is not None:
        spaceFine = FunctionSpace( meshFine, "CG", 1 )
        try:
            d2vmFine = vertex_to_dof_map( spaceFine )
        except:
            # FEniCS version < 1.4
            DM = spaceCoarse.dofmap()
            d2vmFine = DM.dof_to_vertex_map( meshFine )



    #-------------------------------------------------------------- sample kappa
    np.random.seed()
    kappaVec = uniform.rvs( size = 5 )
    em.setCoefficientsC( kappaVec )

    #--------------------------------------------------------- compute solutions
    meanCoarse, varCoarse, sampsCoarse = _sampleOnGrid( meshCoarseCoords, NCoarse, dtCoarse )

    sol = Function( spaceCoarse )
    sol.vector()[d2vmCoarse] = meanCoarse

    if meshFine is not None:
        meanFine, varFine, sampsFine = _sampleOnGrid( meshFineCoords, NFine, dtFine )

        solFine = Function( spaceFine )
        solFine.vector()[d2vmFine] = meanFine

        sol = interpolate( sol, spaceFine )
        sol.vector()[:] = solFine.vector().array() - sol.vector().array()

    #--------------------------------------------- compute opt number of samples
    c = 8
    maxSol2 = np.max( sol.vector().array() ) ** 2
#     maxSol2 = norm( sol, "L2" ) ** 2
    NCoarse = np.ceil( ( 4 * c ** 2 * varCoarse ) / maxSol2 ).astype( int )

    if meshFine is not None:
        NFine = np.ceil( ( 4 * c ** 2 * varFine ) / maxSol2 ).astype( int )
        print "Max N: {maxNLcoarse}/{maxNLfine}\n".format( maxNLcoarse = np.max( NCoarse ),
                                                            maxNLfine = np.max( NFine ) ),

    #--------------------------------------------------- compute missing samples
    meanCoarse, varCoarse, sampsCoarse = _sampleOnGrid( meshCoarseCoords, NCoarse, dtCoarse, sampsCoarse )
    sol = Function( spaceCoarse )
    sol.vector()[d2vmCoarse] = meanCoarse

    if meshFine is not None:
        meanFine, varFine, sampsFine = _sampleOnGrid( meshFineCoords, NFine, dtFine, sampsFine )
        solFine.vector()[d2vmFine] = meanFine
        sol = interpolate( sol, spaceFine )
        sol.vector()[:] = solFine.vector().array() - sol.vector().array()

    return sol.vector().array(), tm.clock() - startP

#===============================================================================
# ML SPDE method class
#===============================================================================
class mlspde:
    def __init__( self,
                  meshes,
                  adaptiveFEM = True,
                  minDoF = 1000,
                  initN = 50,
                  targetN0 = 1000,
                  NDT = 20,
                  debugML = False,
                  verbose = 11,
                  n_jobs = multiprocessing.cpu_count() ):

        self.adaptiveFEM = adaptiveFEM
        self.minDoF = minDoF
        self.meshes = meshes
        self.initN = initN
        self.targetN0 = targetN0
        self.NDT = NDT

        self.N4lvl = [initN for mesh in meshes]
        self.samps4lev = [None for mesh in meshes]
        self.timeP4lev = [None for mesh in meshes]

        self.mean = None
        self.var4lvl = []
        self.timeP = 0.0

        self.verbose = verbose
        self.n_jobs = n_jobs

    def _addLevel( self ):
        self.N4lvl = np.append( self.N4lvl, self.initN )
        self.samps4lev.append( None )
        self.timeP4lev.append( None )
        if self.adaptiveFEM:
            ind = indicator( self.mean )
            marker = criterion( ind, self.meshes[-1] )
            self.meshes.append( refine( self.meshes[-1], marker ) )
        else:
            self.meshes.append( refine( self.meshes[-1] ) )


    def run( self ):
        while True:
            space = FunctionSpace( self.meshes[-1], "CG", 1 )
            print "="*80
            print "Current Level with {dof} dof.".format( dof = space.dim() )
            print "="*80
            self._sample()
            self._updateStatistics()
            self._sample()
            self._updateStatistics()
            if space.dim() > self.minDoF:
                break
            self._addLevel()

        return self.mean

    def _sample( self ):
        #----------------------------------------------------------- first level
        print "Level {level}: dt={dt}".format( level = 0, dt = self.meshes[0].hmin() / 10 )
        coarseCells, coarseCoords = serialize( self.meshes[0] )
        if self.samps4lev[0] is None:
            sampsToDo = self.initN
        else:
            sampsToDo = max( self.N4lvl[0] - len( self.samps4lev[0] ), 0 )
        if sampsToDo > 0:
            ret = np.array( 
                    Parallel( n_jobs = self.n_jobs,
                              verbose = self.verbose )( delayed( 
                              _computeKappaSample )( coarseCoords,
                                                     coarseCells,
                                                     NCoarse = self.NDT )
                              for i in range( sampsToDo ) ) )
            if self.samps4lev[0] is None:
                self.samps4lev[0] = ret[:, 0]
                self.timeP4lev[0] = ret[:, 1]
            else:
                self.samps4lev[0] = np.concatenate( ( self.samps4lev[0], ret[:, 0] ) )
                self.timeP4lev[0] = np.concatenate( ( self.timeP4lev[0], ret[:, 1] ) )

        #---------------------------------------------------------- other levels
        for level, ( meshCoarse, meshFine ) in enumerate( zip( self.meshes[0:-1], self.meshes[1:] ), start = 1 ):
            print "Level {level}: dt={dt}".format( level = level, dt = self.meshes[level].hmin() / 10 )
            coarseCells, coarseCoords = serialize( meshCoarse )
            fineCells, fineCoords = serialize( meshFine )
            if self.samps4lev[level] is None:
                sampsToDo = self.initN
            else:
                sampsToDo = max( self.N4lvl[level] - len( self.samps4lev[level] ), 0 )
            if sampsToDo > 0:
                ret = np.array( 
                    Parallel( n_jobs = self.n_jobs,
                              verbose = self.verbose )( delayed( 
                              _computeKappaSample )( coarseCoords,
                                                     coarseCells,
                                                     fineCoords,
                                                     fineCells,
                                                     NCoarse = self.NDT,
                                                     NFine = self.NDT )
                              for i in range( self.N4lvl[level] ) ) )
                if self.samps4lev[level] is None:
                    self.samps4lev[level] = ret[:, 0]
                    self.timeP4lev[level] = ret[:, 1]
                else:
                    self.samps4lev[level] = np.concatenate( ( self.samps4lev[level], ret[:, 0] ) )
                    self.timeP4lev[level] = np.concatenate( ( self.timeP4lev[level], ret[:, 1] ) )

    def _updateStatistics( self ):
        #---------------------------------------------------- mean and variances
        targetMesh = self.meshes[-1]
        targetSpace = FunctionSpace( targetMesh, "CG", 1 )
        self.mean = Function( targetSpace )
        self.var4lvl = []
        for mesh, samps in zip( self.meshes, self.samps4lev ):
            mean = np.mean( samps )
            curSpace = FunctionSpace( mesh, "CG", 1 )
            meanFun = Function( curSpace )
            meanFun.vector()[:] = mean
            self.mean.vector()[:] = self.mean.vector().array() \
                                    + interpolate( meanFun, targetSpace ).vector().array()
            var = np.var( samps )
            varFun = Function( curSpace )
            varFun.vector()[:] = var
            self.var4lvl.append( varFun )
            print "Var4lvl:", np.max( var )

        #------------------------------------- compute optimal number of samples
        relCompTime4samp4lvl = np.array( [np.mean( timeP ) for timeP in self.timeP4lev] )
#         varL2Norm4lvl = np.array( [norm( var, "L2" ) for var in self.var4lvl] )
        varL2Norm4lvl = np.array( [np.max( var.vector().array() ) for var in self.var4lvl] )

        lastSpace = FunctionSpace( self.meshes[-2], "CG", 1 )
        lastMeanFun = Function( targetSpace )
        lastMeanFun.vector()[:] = self.mean.vector().array() - mean
        lastMeanFun = interpolate( lastMeanFun, lastSpace )
#         diffL2 = norm( meanFun, "L2" ) ** 2 # meanFun is mean diff of last level
        diffL2 = np.max( meanFun.vector().array() ) ** 2
        indLast = indicator( lastMeanFun )
        ind = indicator( self.mean )

        sampConst = np.sum( indLast ) / np.sum( ind )\
                    * np.sum( np.sqrt( relCompTime4samp4lvl * varL2Norm4lvl ) ) / diffL2

#         sampConst = self.targetN0 * np.sqrt( relCompTime4samp4lvl[0] / varL2Norm4lvl[0] )
        print "CurN4lvl:", self.N4lvl
        self.N4lvl = sampConst * np.sqrt( varL2Norm4lvl / relCompTime4samp4lvl )
        self.N4lvl = np.ceil( self.N4lvl ).astype( int )
#         self.N4lvl = [np.ceil( sampConst * np.sqrt( varL2Norm / relCompTime4samp ) ).astype( int )
#                       for varL2Norm, relCompTime4samp
#                       in zip( varL2Norm4lvl, relCompTime4samp4lvl ) ]

        print "time4samp4lvl:", relCompTime4samp4lvl
        print "OptN4lvl:", self.N4lvl


def run():
    meshN = 2
    numMeshes = 2
    skipMeshes = 0
    initN = 100
    NDT = 40

    adaptiveFEM = False
    minDoF = 1000

    n_jobs = multiprocessing.cpu_count()
    verbose = 11


    meshes = [UnitSquareMesh( meshN, meshN )]
    for i in range( numMeshes - 1 ):
        meshes.append( refine( meshes[-1] ) )

    meshes = meshes[skipMeshes:]

    print "DOF:", [mesh.num_vertices() for mesh in meshes]

    ml = mlspde( meshes = meshes, minDoF = minDoF, adaptiveFEM = adaptiveFEM,
                 initN = initN, NDT = NDT, n_jobs = n_jobs, verbose = verbose )
    mean = ml.run()

    print "Load reference solution...",
    meanRef = loadFunction( "spde1_mean.fun" )
    print "done"

    print "Compute errors...",
    e = interpolate( mean, meanRef.function_space() )
    e.vector()[:] -= meanRef.vector()
    errorL2 = norm( e, "L2" )
    errorH1 = norm( e, "H1" )
    print "done\n"

    print "DOF:", [mesh.num_vertices() for mesh in meshes]
    print "L2-Norm: {L2: .1e}".format( L2 = errorL2 )
    print "H1-Norm: {H1: .1e}".format( H1 = errorH1 )

    #---------------------------------------------------------------------- plot
    timestamp = tm.strftime( '%Y-%m-%d_%H-%M-%S' )
    dir = "out" + os.path.sep + "MLSPDE_" + timestamp + os.path.sep
    try:
        os.makedirs( dir )
    except:
        pass


    plt.figure()
    plothelper.plot_mpl( mean )
    plt.colorbar()
    plt.title( "mean" )
    plt.savefig( dir + "mean" + ".pdf" )

    plot( mean, title = "mean" )

if __name__ == "__main__":
    tStartW = tm.time()
    tStartP = tm.clock()
    run()

    print "Finished in {timeW:0.1f}/{timeP:0.1f} min (W/P)".format( 
                                               timeW = ( tm.time() - tStartW ) / 60.0,
                                               timeP = ( tm.clock() - tStartP ) / 60.0 )

    interactive()

    print "="*80
    print "done"
    print "="*80
