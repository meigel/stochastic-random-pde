#!/usr/bin/env python
from __future__ import division
import multiprocessing

from scipy import stats

import matplotlib.pyplot as plt
import numpy as np
from data_utils import loadFunction
from p1InterpolationAdaptiveTest import _printDebugNumbers
import time as tm


if __name__ == "__main__":
    #===========================================================================
    # settings
    #===========================================================================
    problemName = "SimpleSinSin"
    refSol = 0.86479131707

    seed = float( "inf" )
    runs = 10000
    adaptiveDT = True
    intermediate = float( "inf" )
    coord = [0.5, 0.5]
    numThreads = multiprocessing.cpu_count()

    nLevel = 1
    M = 4
    dtTarget = 10 ** -4

    cSamps = runs / 10 ** ( nLevel - 1 )

    runs4level = np.ceil( [cSamps * 10 ** i for i in reversed( range( nLevel ) )] )
    dt4level = [dtTarget * M ** i for i in reversed( range( nLevel ) )]
    print "runs4level", runs4level
    print "dt4level", dt4level

    #===========================================================================
    # compute
    #===========================================================================
    tStart = tm.clock()
    exec( "import " + problemName + " as em" )

    means4lvl = []
    vars4lvl = []
    #------------------------------------------------------------------- level 0
    retVals = em.stochEulerC( numThreads = numThreads,
                                  seed = seed,
                                  numTraj = int( runs4level[0] ),
                                  stepsize = dt4level[0],
                                  adapt = adaptiveDT,
                                  intermediate = intermediate,
                                  grid = coord,
                                  numGridPoints = 1,
                                  M = 0 )
    samps = retVals[:, 2]
    means4lvl.append( np.mean( samps ) )
    vars4lvl.append( np.var( samps ) )

    #-------------------------------------------------------------- other levels
    for lev in range( 1, nLevel ):
        retVals = em.stochEulerC( numThreads = numThreads,
                                  seed = seed,
                                  numTraj = int( runs4level[lev] ),
                                  stepsize = dt4level[lev],
                                  adapt = adaptiveDT,
                                  intermediate = intermediate,
                                  grid = coord,
                                  numGridPoints = 1,
                                  M = M )
        samps = retVals[:, 2]
        means4lvl.append( np.mean( samps ) )
        vars4lvl.append( np.var( samps ) )

    #===========================================================================
    # post process with last  level as ref
    #===========================================================================
    mean = np.sum( means4lvl )
    t = tm.clock() - tStart
    e = np.abs( mean - refSol )

    _printDebugNumbers( "mean4lvl", means4lvl )
    _printDebugNumbers( "var4lvl", vars4lvl )
    _printDebugNumbers( "mean(x0)", mean )
    _printDebugNumbers( "Error", e )
    _printDebugNumbers( "time", e * t )
    _printDebugNumbers( "error*time", t )
    print "done"

