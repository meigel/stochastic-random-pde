import os

import cPickle as pickle


if __name__ == "__main__":
    dir = "2015-04-13_22-16-51"

    filename = "out" + os.path.sep + dir + os.path.sep + "result.dat"

    with open( filename, 'rb' ) as inFile:
        data = pickle.load( inFile )

        data["dt"] = 10e-5

    with open( filename, "wb" ) as outFile:
        pickle.dump( data, outFile,
                 protocol = pickle.HIGHEST_PROTOCOL )
