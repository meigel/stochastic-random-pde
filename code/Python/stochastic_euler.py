from __future__ import division
import numpy as np
import os
from dolfin import *
import scipy.stats as stats
import copy
from matplotlib import pyplot as plt
import itertools as iter
from scipy import spatial
import time
import euler as em
from tictoc import TicToc
# from pylab import figure, show
# from mpl_toolkits.mplot3d import axes3d

class dW( object ):
    '''Brownian motion class'''
    def __init__( self, dt, seed = None, cache_size = 0, cache_block_size = 100 ):
        # TODO: set seed and setup cache
        self.dt = np.sqrt( dt )
    def __call__( self, x ):
        return stats.norm.rvs( size = len( x ), scale = self.dt )


class Interpolator( object ):
    '''FEM interpolation class'''
    def __init__( self, mesh, inside_domain, mesh_coarse=None ):
        self.mesh = mesh
        self.inside_domain = inside_domain
        self.V = FunctionSpace( mesh, 'CG', 1 )
        if mesh_coarse is not None:
            self.V_coarse = FunctionSpace( mesh_coarse, 'CG', 1 )
            self.has_coarse = True
        else:
            self.has_coarse = False
    def create_interpolation( self, f, runs = 1 ):
        # evaluate nodal values
        coords = self.mesh.coordinates()
        fx = [np.array( [f( x ) for x in coords] ) for _ in range( runs )]
        # compute point-wise expectation
        fx = sum( fx ) / runs
        # create interpolation function
        u = Function( self.V )
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map( self.V )
        except:
            # old FEniCS version
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        u.vector()[d2vm] = fx
        return u
    def create_interpolationC( self, means ):
        u = Function( self.V )
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map( self.V )
        except:
            # old FEniCS version
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        u.vector()[d2vm] = np.array(means[0:(self.mesh.coordinates().shape[0]),2])
        return u
    def create_interpolationMLS( self, means, neighbours=1000, degree=2, dist_weight=0.5, max_dist=1 ):
        def W(d, scale=1):    # simple tri-cube weighting
            w = np.ones(len(d)) - scale * d**3
            return w**3
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map( self.V )
        except:
            # old FEniCS version
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        coords = self.mesh.coordinates()
        # setup spatial tree and iterate vertices of (fine) mesh
        tree = spatial.cKDTree([(c[0],c[1]) for c in means])
        # query neighbours
        kd_d, kd_i = tree.query(coords, k=neighbours)
        # solve MLS problems
        L = coords.shape[0]
        basis = [b for b in iter.product(range(degree+1),range(degree+1)) if b[0]+b[1]<=degree]
        assert(neighbours >= len(basis))
        A = np.ndarray((neighbours,len(basis)))
        xvec = np.ndarray(len(basis))
        values = np.ndarray(L)
        for l in range(L):
            current_x = coords[l,:]
            current_d, current_i = kd_d[l], kd_i[l]
            # filter wrt max distance
            current_ind = np.where(current_d <= max_dist)[0]
            current_d, current_i = current_d[current_ind], current_i[current_ind]
            # setup MLS system
            if dist_weight is not None:
                scale = dist_weight/max(current_d)**3
                w = W(current_d, scale)
            else:
                w = 1
            B = w * means[current_i,2]
            xloc = means[current_i,0:2]
            for bi, expo in enumerate(basis):
                bcol = np.power(xloc[:,0],expo[0]) * np.power(xloc[:,1],expo[1])
                xvec[bi] = current_x[0]**expo[0] * current_x[1]**expo[1]
                A[:len(current_ind),bi] = w * bcol
            # determine MLS approximation and evaluate at point x
            y = np.linalg.lstsq(A[:len(current_ind),:],B)[0]
            val = np.dot(xvec, y)
            values[l] = val     #test (current_x[0]-0.5)**4
        u = Function( self.V )
        u.vector()[d2vm] = np.array(values)
        return u


def stochastic_euler( x0, pde_data, inside_domain, dw ):
    kappa, f, uD, g = pde_data["kappa"], pde_data["f"], pde_data["uD"], pde_data["g"]
    xi, T = copy.copy( x0 ), 0
    while inside_domain( xi ):
        xi += sqrt( 2 * kappa( xi ) ) * dw( xi )
        T += 1
    # project to boundary
    xT = min( max( xi[0], 0 ), 1 ), min( max( xi[1], 0 ), 1 )
    return uD( xT ) + T * dw.dt ** 2, T, xT # f = 1


def setup_interpolator( N, refinements=0 ):
    def inside_domain( x ):
        return x[0] > 0 and x[0] < 1 and x[1] > 0 and x[1] < 1
    mesh = UnitSquareMesh( N, N )
    if refinements > 0:
        mesh_coarse = Mesh(mesh)
        for _ in range(refinements):
            mesh = refine(mesh)
    else:
        mesh_coarse = None
    return Interpolator( mesh, inside_domain, mesh_coarse=mesh_coarse )


def solve_spde( pde_data, dt, interpolator, runs ):
    # initialise Brownian motion
    dw = dW( dt )
    # setup stochastic Euler solver
    ux = lambda x0: stochastic_euler( x0, pde_data, interpolator.inside_domain, dw )[0]
    Iu = interpolator.create_interpolation( ux, runs )
    return Iu

#----------------------- solves with multiple starting from mesh points,
#----------------------- returns an interpolated function
def solve_spdeC( pde_data, parallel_data, dt, interpolator, runs ):
    # setup stochastic Euler solver
    with TicToc(key="stochEulerC", accumulate=False, do_print=True):
        means = em.stochEulerC( parallel_data["numThreads"],
                    parallel_data["seed"],
                    runs,
                    dt,
                    parallel_data["adaptive"],
                    parallel_data["intermediate"],
                    interpolator.mesh.coordinates().flatten(),
                    np.prod(parallel_data["dimGrid"]) )
    if parallel_data["intermediate"]:
        # # DEBUG===
        # fig = figure()
        # ax = fig.gca(projection='3d')
        # cdim = interpolator.V_coarse.dim()
        # ax.plot(means[:cdim,0],means[:cdim,1],means[:cdim,2],'o')
        # fig = figure()
        # ax = fig.gca(projection='3d')
        # ax.plot(means[:,0],means[:,1],means[:,2],'o')
        # show()
        # # ===DEBUG
        # TODO: parameters should be determined automatically!
        neighbour_scale = 30
        max_dist_scale = 10
        dist_weight = 0.5
        with TicToc(key="interpolationMLS", accumulate=False, do_print=True):
            Iu = interpolator.create_interpolationMLS( means, neighbours=neighbour_scale*runs, max_dist=max_dist_scale*dt, dist_weight=dist_weight, degree=2 )
    else:
        Iu = interpolator.create_interpolationC( means )
    return Iu


def solve_point ( pde_data, dt, interpolator, runs, x0 ):
    # initialise Brownian motion
    dw = dW( dt )
    ux = np.array( [stochastic_euler( x0, pde_data, interpolator.inside_domain, dw )[0]
          for _ in range( runs )] )
    return np.mean( ux )


#----------------------- solves with starting from a single point
#----------------------- returns mean
def solve_pointC ( pde_data, parallel_data, dt, interpolator, runs, x0 ):
	# pde_data will not yet be used, C-code does not use FEniCS-Expression so far
	points = em.stochEulerC( parallel_data["numThreads"],
				parallel_data["seed"],
				runs,
                dt,
				parallel_data["adaptive"],
				parallel_data["intermediate"],
				x0,
				np.prod(parallel_data["dimGrid"]) )
	Iu = interpolator.create_interpolationC( points )
	return Iu


def solve_fenics( pde_data, interpolator ):
    def boundary( x, on_boundary ):
        return on_boundary
    uD = pde_data["uD"]
    f = pde_data["f"]
    g = pde_data["g"]
    kappa = pde_data["kappa"]
    V = interpolator.V
    bc = DirichletBC( V, uD, boundary )
    u = TrialFunction( V )
    v = TestFunction( V )
    a = inner( kappa * grad( u ), grad( v ) ) * dx
    L = f * v * dx + g * v * ds
    u = Function( V )
    solve( a == L, u, bc )
    return u


def main():
    #------------------------------------------------------------- setup problem
    f = Constant( 1.0 ) # rhs
    kappa = Constant( 1.0 ) # diffusion coefficient
    uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" ) # Dirichlet bc
#     uD = Constant( 1.0 ) # Dirichlet bc
    g = Constant( 0.0 ) # Neumann bc
    #-------------------------------------------- collect problem data and setup
    pde_data = {"kappa":kappa, "f":f, "uD":uD, "g":g}
    dt = 0.02 # Time step
    x0 = [0.5, 0.5] # starting point / evaluation point
    #-------------------------------------------- setup parallel parameters and interpolator
    intermediate = False
    refinements = 2 if intermediate else 0
    N = 100 # setup domain and interpolation space
    interpolator = setup_interpolator( N, refinements=refinements )
    numThreads = 50
    adaptive = 1
    dimGrid = [N+1, N+1]
    seed = float('inf')
    prob_dim = len(dimGrid)
    parallel_data = {"numThreads":numThreads, "adaptive":adaptive, "intermediate":intermediate, "dimGrid":dimGrid, "seed":seed, "prob_dim":prob_dim}
    #----------------------------------------------- solve stochastic random PDE
    interpolated = True
    #-------------------------------------------------------------- interpolated
    if interpolated:
        runs = 100
        start = time.time()
        Iu = solve_spdeC( pde_data, parallel_data, dt, interpolator, runs )
        # Iu = solve_spde( pde_data, dt, interpolator, runs )
        end = time.time()

        #print "stochastic solution took ", (end-start)*1000, " ms"

        start = time.time()
        uF = solve_fenics( pde_data, interpolator )
        end = time.time()

        print "fem solution took ", (end-start)*1000, " ms"

        ## this is the solution of a regression made in R-code for a polynomial of degree 4 
        u_R = Expression("-0.006356321+3.234760673*x[0]-0.109775496*x[0]*x[0]-6.226939266*x[0]*x[0]*x[0]+3.101325473*x[0]*x[0]*x[0]*x[0]+3.241142127*x[1]-0.119173865*x[1]*x[1]-6.224537451*x[1]*x[1]*x[1]+3.097590087*x[1]*x[1]*x[1]*x[1]-18.115181827*x[0]*x[1]+18.092726734*x[0]*x[0]*x[1]+18.095606715*x[0]*x[1]*x[1]-18.150725020*x[0]*x[1]*x[0]*x[1]+0.035982048*x[0]*x[0]*x[0]*x[1]+0.046564916*x[0]*x[1]*x[1]*x[1]");

        #u0s = Iu( x0 )
        u0s = u_R( x0 )
        u0f = uF( x0 )

        print "u_s( ", x0, " )", u0s
        print "u_f( ", x0, " )", u0f
        print "relative error in ", x0," = ", abs(u0s - u0f)/u0f

        set_log_level(ERROR)
        #print "relativ error in L2-norm = ", errornorm(uF, Iu, 'L2')
        #print "relativ error in H1-norm = ", errornorm(uF, Iu, 'H1')


        #plot( Iu, title = "Stochastic")
        plot( u_R, interpolator.mesh, title = "Stochastic")
        plot( uF, title = "FEM")
        
	err_abs = (uF-uR)
        err_rel = err_abs/uF
        
        plot( err_rel, title = "relative error" )
        plot( err_abs, title = "absolute error: FEM - Stochastic" )
        set_log_level(INFO)
        interactive()
    #-------------------------------------------------------------- single point
    else:
        runs = 1000
        parallel_data["dimGrid"] = [1, 1]
        Iu = solve_pointC( pde_data, parallel_data, dt, interpolator, runs, x0 )
        u0s = Iu(x0)
        # u0s = solve_point( pde_data, dt, interpolator, runs, x0 )
        uF = solve_fenics( pde_data, interpolator )
        u0f = uF( x0 )
        print "u_s(x0)", u0s
        print "u_f(x0)", u0f
        print "relative error = ", abs(u0s - u0f)/u0f
        set_log_level(ERROR)
        print "relativ error in L2-norm = ", errornorm(uF, Iu, 'L2')

        plot( Iu, title = "Stochastic")
        plot( uF, title = "FEM")
        plot( abs((uF-Iu)/uF), title = "relative error" )
        set_log_level(INFO)
        interactive()


if __name__ == "__main__":
#     os.chdir( os.pardir )
    print "started"
    main()
