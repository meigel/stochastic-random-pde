from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

# number of equidistant grid intervals
#    => (N+1)**2 grid points
N = 3

# step size
dt = 1e-4

# number of threads
numThreads = 40

# collect intermediate points?
intermediate = -1

# switch adaptivity
adaptive = True

# degree of polynomial for regression
degree = 4

dimGrid = [N+1, N+1]
# seed = float('inf')
seed = 1
prob_dim = len(dimGrid)
parallel_data = {"numThreads": numThreads, "adaptive": adaptive, "intermediate": intermediate, "dimGrid": [1,1], "seed": seed}

runs = 1

# run SDE simulations
# ===================
meanRef = loadFunction("SimpleSinSin_mean.fun")

refMesh = meanRef.function_space().mesh()
intRef = setup_interpolator(mesh=refMesh)

dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S_detPoints' ) + os.path.sep

try:
    os.makedirs( dir )
except:
    pass

base = 2
n = 24

x_val = range(2,n+1)
expo = range(2,n+1)
errorL2 = np.zeros(n-1)
errorH1 = np.zeros(n-1)
errorRefReg = np.ones(n-1)
errorRefRegH1 = np.ones(n-1)
ix_val = np.zeros(n-1)
expect_05 = np.zeros(n-1)
times = np.zeros(n-1)

# ph = PlotHelper()

interpolator = setup_interpolator(int(sqrt(base**n)-0.5))
coords = interpolator.mesh.coordinates()

meanRefvec = meanRef.vector().array()
vals = np.empty(len(coords), dtype="float")
meanVals = np.empty(len(coords), dtype="float")
for i, coord in enumerate(coords):
    meanVals[i] = meanRef(coord)
coordsVal = np.column_stack((coords[:,0], coords[:,1], meanVals))

with TicToc(key="create_interpolationLegendre", accumulate=False, do_print=True):
    u_RefRegGlo = intRef.create_interpolationLegendre(coordsVal, degree=degree)   

errFRefReg = Function(meanRef.function_space())
errFRefReg.vector()[:] = meanRef.vector() - interpolate(u_RefRegGlo, meanRef.function_space()).vector()

errorRefReg = errorRefReg * norm(errFRefReg, "L2")
errorRefRegH1 = errorRefRegH1 * norm(errFRefReg, "H1")

for it in expo:
    
    N = sqrt(base**it)-0.5
    
    interpolator = setup_interpolator(int(N))
    
    plt.clf()
    
    coords = interpolator.mesh.coordinates()
    
    numPoints = coords.shape[0]
    
    x_val[it-2] = numPoints
    ix_val[it-2] = 1/x_val[it-2]
    expect_05[it-2] = 1/(x_val[it-2]**0.5)
    
    with TicToc(key="stochEulerC", accumulate=False, do_print=True):
        means = problem.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            coords.flatten(),
                            numPoints)    
     
    u_Glo = interpolator.create_interpolationLegendreExact(means, degree)
    
    # calculating and plotting errors
    # ===============================
    errFGlo = Function(meanRef.function_space())
    errFGlo.vector()[:] = meanRef.vector() - interpolate(u_Glo, meanRef.function_space()).vector()
     
    errorL2[it-2] = norm(errFGlo, "L2")
    errorH1[it-2] = norm(errFGlo, "H1")
    times[it-2] = TicToc.get("stochEulerC")
     
    # evaluate global errors
    print("iteration = %s\nglobal error: L2 = %s\tH1 = %s" % (str(it), errorL2[it-2], errorH1[it-2]))
 
    # prepare output
    data = None
    header = '\tN (number of points)'
    # write errors
    data = np.column_stack((x_val[0:it-1], errorL2[0:it-1], errorH1[0:it-1], errorRefReg[0:it-1], errorRefRegH1[0:it-1], times[0:it-1]))
    header += '\tL2 SPDE\tH1 SPDE\tL2 ref. reg.\tH1 ref. reg.\ttimes'
     
    np.savetxt(dir+"paper_detPoints_N.txt", data, delimiter='\t', header=header)
     
    plt.plot(x_val[0:it-1], errorL2[0:it-1], "b"+'s--', label="L2 SPDE" )
    plt.plot(x_val[0:it-1], errorH1[0:it-1], "b"+'d--', label="H1 SPDE" )
    
    plt.plot(x_val[0:it-1], errorRefReg[0:it-1], '-', label="L2 ref. reg." )
    plt.plot(x_val[0:it-1], errorRefRegH1[0:it-1], '--', label="H1 ref. reg-" )
    
    plt.xscale('log', basex=10)
    plt.yscale('log', basey=10)
    ax = plt.gca()
    ax.set_xlabel('N (number of points)')
    ax.set_ylabel('errors')
    plt.legend(loc=3)
    plt.savefig(dir+"paper_detPoints_N.png")

# ph["FEM reference solution"].plot(meanRef)
# ph["Error ref. regression, degree "+str(degree)].plot(errFGlo)
# ph["Regression difference"].plot(errFStoch - errFRef)

# interactive()
