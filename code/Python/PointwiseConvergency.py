from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

"""

This file computes the convergence in three points:

# middle point
x0 = [0.5, 0.5]

# near a corner
x1 = [0.1, 0.1]

# near an edge
x2 = [0.4, 0.1]

The error is measured in comparison to the reference solution.

"""

#######################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
#######################################

### step size
dt = 1e-4

### evaluation points for single point version
# xIn = np.array([0.5, 0.5, 0.1, 0.1, 0.4, 0.1]).reshape(3,2)
xIn = np.array([0.1, 0.1]).reshape(1,2)

### number of threads
numThreads = 80

### collect intermediate points?
intermediate = -1

### switch adaptivity
adaptive = True

dimGrid = [1, 1]

seed = 111111111 #seed = float('inf')

prob_dim = len(dimGrid)
parallel_data = {"numThreads":numThreads, "adaptive":adaptive, "intermediate":intermediate, "dimGrid":[1,1], "seed":seed}

base = 2
minn = 2
n = 24
base_dt = 2.

x_val = np.zeros(n-minn+1)
ix_val = np.zeros(n-minn+1)
expo = range(minn,n+1)
meanStoch = np.zeros(n-minn+1)
varStoch = np.zeros(n-minn+1)
expect_05 = np.zeros(n-minn+1)
abserr = np.zeros(n-minn+1)


meanRef = loadFunction( "SimpleSinSin_mean.fun" )

# dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S_PWC_3PointsDT' ) + os.path.sep
dir = "out" + os.path.sep +"2015-12-09_00-37-28_PWC_3PointsDT" + os.path.sep

try:
    os.makedirs( dir )
except:
    pass

referenceSolution = np.ones(n+1)

runs = 2*10**7

for j, x0 in enumerate(xIn):
    file = "PointwiseConvergency_dt_x1"
    
    x_sol = meanRef( x0 )
    
    refSolsArray = referenceSolution * x_sol
    
    print "Calculating in point", x0
    
    for i in expo: 
        plt.clf()
        
        dt = base_dt**(-i)
        
#         runs = 1./sqrt(dt)
        
        print "\tstep size", dt
        
        x_val[i-minn] = dt
        ix_val[i-minn] = dt
        expect_05[i-minn] = dt**0.5
        with TicToc(key="stochEulerC", accumulate=False, do_print=True):
            means = problem.stochEulerC( parallel_data["numThreads"],
                                    parallel_data["seed"],
                                    runs,
                                    dt,
                                    parallel_data["adaptive"],
                                    parallel_data["intermediate"],
                                    x0,
                                    1 );
        meanStoch[i-minn] = means[0,2]
        varStoch[i-minn] = means[0,3]
    
        abserr[i-minn] = abs(meanStoch[i-minn] - x_sol)
        
        # prepare output
        data = None
        header = '\tnumber of trajectories'
        # write errors
        data = np.column_stack((x_val[0:i-(minn-1)], abserr[0:i-(minn-1)], varStoch[0:i-(minn-1)], expect_05[0:i-(minn-1)], ix_val[0:i-(minn-1)]))
        header += '\tabsolute error\tvariance\torder 1/2\torder 1\t'
         
        np.savetxt(dir+file+".txt", data, delimiter='\t', header=header)
          
        plt.plot(x_val[0:i-(minn-1)], abserr[0:i-(minn-1)], label='absolute error')
        
        ################################################
        # the variance here is calculated like
        # \sum_{i = 0}^{runs} ( u_mean - u[i] )^2 / runs
        ################################################
        
        #plt.plot(x_val[0:i-1], varStoch[0:i-1], label='variance')
        plt.plot(x_val[0:i-(minn-1)], expect_05[0:i-(minn-1)], label='order 1/2')
        plt.plot(x_val[0:i-(minn-1)], ix_val[0:i-(minn-1)], label='order 1')
        plt.title('StochSol in [ ' + str(x0[0]) + ' , ' + str(x0[1]) +' ]')
        plt.xscale('log', basex=10)
        plt.yscale('log', basey=10)
        
        ax = plt.gca()
        ax.invert_xaxis()
        ax.set_xlabel('number of trajectories')
        ax.set_ylabel('errors')
        plt.legend(loc=3)
        plt.savefig(dir+file+".png")
          
    print "absolute error :", abs(meanStoch[n-minn] - x_sol)
    print "relative error :", abs(meanStoch[n-minn] - x_sol)/x_sol
    print abserr
    print varStoch

# runs = 10**5
# step = 1./base_dt
# 
# for i in expo:
#     step = step/base_dt
#     x_val[i] = 1./step
#     ix_val[i] = step
#     expect_05[i] = step**0.5
#     with TicToc(key="stochEulerC", accumulate=False, do_print=True):
#         means = problem.stochEulerC( parallel_data["numThreads"],
#                                 parallel_data["seed"],
#                                 runs,
#                                 step,
#                                 parallel_data["adaptive"],
#                                 parallel_data["intermediate"],
#                                 x0,
#                                 1 );
#     meanStoch[i] = means[0,2]
#     varStoch[i] = means[0,3]
#     abserr = np.zeros(n+1)
# abserr[:] = abs(meanStoch[:] - x0_sol)
#   
# plt.plot(x_val, abserr, label='absolute error')
# 
# ################################################
# # the variance here is calculated like
# # \sum_{i = 0}^{runs} ( u_mean - u[i] )^2 / runs
# ################################################
# 
# plt.plot(x_val, varStoch, label='variance')
# plt.plot(x_val, expect_05, label='order 1/2')
# plt.plot(x_val, ix_val, label='order 1')
# plt.title('StochSol in [ ' + str(x0[0]) + ' , ' + str(x0[1]) +' ]')
# plt.xscale('log', basex=base)
# plt.yscale('log', basey=base)
# plt.legend(loc=3)
# plt.savefig("loglog_convergency_stepsize.png")
# # plt.show()
#   
# print "absolute error :", abs(meanStoch[n] - x0_sol)
# print "relative error :", abs(meanStoch[n] - x0_sol)/x0_sol
# print abserr
# print varStoch
