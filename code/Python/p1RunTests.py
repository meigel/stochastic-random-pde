#!/usr/bin/env python

from pprint import pprint
import itertools
import os

from dolfin import *

from logger import Logger
import cPickle as pickle
import p1CompareResults
import p1InterpolationAdaptiveTest
import time as tm


if __name__ == "__main__":
    dirHeader = "tc_" + tm.strftime( '%Y-%m-%d_%H-%M-%S' ) + os.path.sep
    try:
        os.makedirs( "out" + os.path.sep + dirHeader )
    except:
        pass
    Logger( "out" + os.path.sep + dirHeader )

    #===========================================================================
    # define default data
    #===========================================================================
    defaultValues = {
         "problemName" : "ExponentialBuckle", # "SimpleSinSin", "ExponentialBuckle", "OneDiscExample", "OneDiscExampleSimple"
         "mesh" : UnitSquareMesh( 3, 3 ),
         "minRuns" : 100,
         "runs" : 1000, # 1000
         "maxDoF" : 1800, # 200, 1800, 1e5
         "targetError" : 5e-1,
         "dt" : 10 ** -2,
         "overEstConst" : 2e1,
         "errorNorm" : "H1",
         "adaptiveVAR" : True,
         "adaptiveVARConst" : False,
         "adaptiveVarDelta" : 0.2,
         "adaptiveVarGoal" : True,
         "adaptiveFEM" : True,
         "adaptiveFEMNorm" : "H1",
         "adpativeFEMTheta" : 0.7,
         "adaptiveFEMGoal" : True,
#          "adaptiveFEMAnisotropic" : False,
#          "adaptiveSmoother" : False,
#          "adaptiveEdgeFlip" : False,
         "adaptiveDT" : True,
         "adaptiveFEMDT" : True,
         "adaptiveOptDTRate" : True,
         "adaptiveDTConst" : 20,
         "adaptiveDTExp" : 1.0,
         "adaptiveDTBeta" : 1.0, # or "FEM" for est alpha
         }

    defaultValues["goal"] = "point2" # "None", "Exp", "point", "point2"

    problemSpecific = {
           "ExponentialBuckle" : {
                "overEstConst" : 1e6, # 1e6
                "targetError"  : 20, # 1e1
                },
           "SimpleSinSin" : {
                "overEstConst" : 2e1,
                "targetError"  : 3e-3
                },
           "OneDiscExample" : {
                "overEstConst" : 1e6,
                "targetError"  : 20
                },
           "OneDiscExampleSimple" : {
                "overEstConst" : 2.8e02,
                "targetError"  : 3e-3
                },
           }

    xAxis = "timeP"     # dof, timeP, timeW

    #===========================================================================
    # test over all combinations
    #===========================================================================
    #----------------------------------------------------- test for optimal beta
#     testVals = [0.6 + i * 0.1 for i in range( 4 )] + ["FEM", "DTInv"]

    #--------------------------------------------------- product space test case
    props = [ "adaptiveFEM", "adaptiveVAR"]
    testVals = [False, True]
    testCases = [dict( zip( props, sets ) )
                 for sets in itertools.product( testVals,
                                                repeat = len( props ) )]
#     testCases = [{}]

    #--------------------------------------------------------- test single cases
    if defaultValues["goal"] != "None":
        props = [ "adaptiveFEM", "adaptiveFEMGoal", "adaptiveVAR", "adaptiveVarGoal"]
        testCases = [
                    {"adaptiveFEM" : False,
                     "adaptiveVAR" : False,
                     "adaptiveFEMGoal" : False,
                     "adaptiveVarGoal" : False},
                    {"adaptiveFEM" : False,
                     "adaptiveVAR" : True,
                     "adaptiveFEMGoal" : False,
                     "adaptiveVarGoal" : False},
                    {"adaptiveFEM" : False,
                     "adaptiveVAR" : True,
                     "adaptiveFEMGoal" : False,
                     "adaptiveVarGoal" : True},
                    {"adaptiveFEM" : True,
                     "adaptiveVAR" : False,
                     "adaptiveFEMGoal" : True,
                     "adaptiveVarGoal" : False},
                    {"adaptiveFEM" : True,
                     "adaptiveVAR" : True,
                     "adaptiveFEMGoal" : True,
                     "adaptiveVarGoal" : False},
                    {"adaptiveFEM" : True,
                     "adaptiveVAR" : True,
                     "adaptiveFEMGoal" : False,
                     "adaptiveVarGoal" : True},
                    {"adaptiveFEM" : True,
                     "adaptiveVAR" : True,
                     "adaptiveFEMGoal" : True,
                     "adaptiveVarGoal" : True}
                     ]

    print "Test Cases"
    print 80 * "~"
    pprint( testCases )

    #===========================================================================
    # complete testCaseSettings
    #===========================================================================
    #------------------------------------------------------------------- general
    defaultValues.update( {"doAnalysis" : False} )

    if defaultValues["goal"] != "None": # if some goal -> no target error limit
        defaultValues["targetError"] = float( "-inf" )

    #---------------------------------------------------------- individual cases
    testCasesComplete = []
    for testCase in testCases:
        testCaseComplete = defaultValues.copy()
        testCaseComplete.update( testCase )
        curProbName = testCaseComplete["problemName"]
        curProbSpec = problemSpecific[curProbName]
        testCaseComplete.update( curProbSpec )
        testCasesComplete.append( testCaseComplete )

    pprint( defaultValues )
    print "="*80
    print ""
    print ""

    #===========================================================================
    # run test cases and save
    #===========================================================================
    dirNames = []
    for i, testCase in enumerate( testCasesComplete ):
        print ""
        print "="*80
        pprint( testCase )
        print "="*80
        testCase["filenameHeader"] = dirHeader + "tc{i}_".format( i = i )
        filename, dirName = p1InterpolationAdaptiveTest.run( **testCase )
        dirNames.append( str( dirName ) )

    saveData = {"props" : props,
                "dirNames" : dirNames}

    pprint( dirNames )

    filename = "out" + os.path.sep + dirHeader + "result.dat"

    with open( filename, 'wb' ) as outFile:
        pickle.dump( saveData, outFile,
                    protocol = pickle.HIGHEST_PROTOCOL )

    #===========================================================================
    # post process
    #===========================================================================
    p1CompareResults.compare( dataDirNames = dirHeader[:-1], xAxis = xAxis ) # dof, timeP, timeW
    print ""
    print "="*80
    print "done"
