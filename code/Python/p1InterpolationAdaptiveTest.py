#!/usr/bin/env python
from estimator import edgeJumpKappa
from methods import anisotrope
from tests import testEdgeFlip
from tools import functionTools, meshTools
import contextlib
import multiprocessing
import os
import sys
import traceback
import warnings

from dolfin import *
from scipy import stats

from data_utils import saveFunction
import cPickle as pickle
import numpy as np
import p1InterpolationAdaptiveAnalyse
import time as tm


def MollifiedPoint2( x0, eps, mesh = UnitSquareMesh( 3, 3 ),
                     refineSize = 50.0, C = None ):
    if C is None:
        epsBall = ball( x0, eps )
        while mesh.hmin() > eps / refineSize:
            DG = FunctionSpace( mesh, "DG", 0 )
            ballArr = project( epsBall, DG ).vector().array()
            cell_markers = MeshFunction( "bool", mesh, mesh.topology().dim() )
            for c in cells( mesh ):
                cell_markers[c] = ballArr[c.index()] > 0
            hminOld = mesh.hmin()
            mesh = refine( mesh, cell_markers )
            if hminOld == mesh.hmin():
                mesh = refine( mesh )

        space = spaceTools.getFineSpace( mesh, "CG",
                                         settings.intDegreeGoalMollifier )

        domainInt = Expression( """pow(x[0] - x0x,2) + pow(x[1] - x0y,2) < eps2 
                            ? 1.0/eps2 * exp(-1.0 /(1 - (pow(x[0] - x0x,2) + pow(x[1] - x0y,2))/eps2))  
                            : 0""",
                            x0x = x0[0],
                            x0y = x0[1],
                            eps2 = eps ** 2,
                            element = space.ufl_element() )
        domainInt = interpolate( domainInt, space )
        C = 1.0 / assemble( domainInt * dx )

        print "Computed C in Goal: {C}".format( C = C )
    goal = Expression( """pow(x[0] - x0x,2) + pow(x[1] - x0y,2) < eps2 
                        ? C/eps2 * exp(-1.0 /(1 - (pow(x[0] - x0x,2) + pow(x[1] - x0y,2))/eps2)) 
                        : 0""",
                        x0x = x0[0],
                        x0y = x0[1],
                        C = C,
                        eps = eps,
                        eps2 = eps ** 2,
                        degree = 10 )
    return goal

#===============================================================================
# constant functions for adaptive FEM
#===============================================================================
def indicator( solution, rhs = Constant( 1.0 ), weight = None, norm = "H1" ):
    """
    Compute a simple residual based jump error estimator and select cells for
    refinement.
    
    Parameters
    ----------
    
    solution : :class:`dolfin.Function`
        solution to estimate the error for
    
    Returns
    -------
    indicators : [int, ...]
        list of error markers for each cell
    """
    if weight is None:
        weight = Constant( 1.0 )

    space = solution.function_space()
    mesh = space.mesh()

    #------------------------------------------- Define cell and facet residuals

    R_T = -( rhs + div( grad( solution ) ) * weight )
    n = FacetNormal( mesh )
    R_dT = dot( grad( solution ), n ) * weight

    #-------------------- Will use space of constants to localize indicator form
    Constants = FunctionSpace( mesh, "DG", 0 )
    w = TestFunction( Constants )
    h = CellSize( mesh )

    #------------------------------------------------------------------- weights
    if norm == "H1":
        elemWeight = h ** 2
        sideWeight = avg( h )
    elif norm == "L2":
        elemWeight = h ** 4
        sideWeight = avg( h ** 3 )

    #------------------------------- Define form for assembling error indicators
    form = ( elemWeight * R_T ** 2 * w * dx
            + sideWeight * avg( R_dT ) ** 2 * 2 * avg( w ) * dS )
#            + h * R_dT ** 2 * w * ds)

    #------------------------------------------------- Assemble error indicators
    indicators = assemble( form )

    return indicators.array()

def criterion( indicators, mesh, thetaEstimator = 0.5 ):
    #-------------------------------------------------------- Bulk criterion
    estimate2 = sum( i for i in indicators )
    cell_markers = MeshFunction( "bool", mesh, mesh.topology().dim() )
    indicators_indexed = zip( range( len( indicators ) ), indicators )
    indicators_sorted = sorted( indicators_indexed, key = lambda ind:ind[1],
                               reverse = True )
    indices, values = zip( *indicators_sorted )
    indicators_cumsum = np.cumsum( values )

    indicator_min = next( val for val in indicators_cumsum
                         if val >= thetaEstimator * estimate2 )
    index_sorted_min = np.where( indicators_cumsum == indicator_min )[0]
    index_to_mark = indices[:index_sorted_min[0] + 1]

    for c in cells( mesh ):
        cell_markers[c] = c.index() in index_to_mark


    return cell_markers


#===============================================================================
# constant helper functions
#===============================================================================
def loadProblem( problemName ):
    nameTranslate = { "SimpleSinSin" : "spde1",
                  "ExponentialBuckle" : "spde2" }
    problemName = nameTranslate.get( problemName, problemName )
    exec( "import problems." + problemName )
    problemClass = eval( "problems." + problemName )
    return problemClass.problem()

@contextlib.contextmanager
def _printoptions( *args, **kwargs ):
    original = np.get_printoptions()
    np.set_printoptions( *args, **kwargs )
    yield
    np.set_printoptions( **original )

def _printDebugNumbers( name, numbers ):
    formatter = {'all':lambda x: "{: .1e}".format( x )}
    name = name.ljust( 15 )

    if not isinstance( numbers, np.ndarray ):
        if isinstance( numbers, list ):
            numbers = np.array( numbers )
        else:
            print name + ": " + formatter["all"]( numbers )
            return

    with _printoptions( suppress = False, formatter = formatter ):
        print "{name}: {numbers}".format( name = name, numbers = numbers )

def _getVertFunOnRefinedMesh( fun, newMesh ):
    oldMesh = fun.function_space().mesh()
    CG2 = FunctionSpace( oldMesh, "CG", 2 )
    fun2 = interpolate( fun, CG2 )

    dofmap = CG2.dofmap()
    for cell in cells( oldMesh ):
        localDoF = dofmap.cell_dofs( cell.index() )[3:]
        fun2.vector()[localDoF] = np.array( [0, 0, 0] )

    CG1fine = FunctionSpace( newMesh, "CG", 1 )
    funFine = interpolate( fun2, CG1fine )

    return funFine

def _formatTime( t, hLen = -1 ):
    """
    Format a time in seconds into a string.
    
    Parameters
    ----------
    time : int
        time in seconds
    hLen : int, optional
        number of digits for the hours, if *0* no hours are output, if *-1* the
        number of digits is derived from the value, else the desired number of 
        digits is reserved and outputted
        
    Returns
    -------
    timeStr : string
        formated time
    """
    t = int( round( t ) )
    seconds = t % 60
    hours = t / 3600
    if hLen == 0:
        hourStr = ""
        minutes = t / 60
    elif hLen == -1:
        hourStr = "{hours}:"
        minutes = t / 60 % 60
    else:
        hourStr = "{hours:" + str( hLen ) + "}:"
        minutes = t / 60 % 60
    timeStr = ( hourStr + "{minutes:02}:{seconds:02}" ).format( 
                                               hours = hours,
                                               minutes = minutes,
                                               seconds = seconds )
    return timeStr

class adaptiveSPDESolver:

    def __init__( self, **settings ):

        #------------------------------------------------------- problem setting
        self.problemName = settings.pop( "problemName", "simpleSinSin" )
        self.initMesh = settings.pop( "mesh", UnitSquareMesh( 5, 5 ) )
        self.rhs = settings.pop( "rhs", Constant( 1.0 ) )
        self.runs = settings.pop( "runs", 100 )
        self.minRuns = settings.pop( "minRuns", 10 )
        self.maxDoF = settings.pop( "maxDoF", 1000 )
        self.targetError = settings.pop( "targetError", float( "-inf" ) )
        self.dt = settings.pop( "dt", 10 ** -3 )
        self.goalString = settings.pop( "goalString", "None" )

        self.goal = eval( self.goalString )

        #------------------------------------------------------------ parameters
        self.spaceElem = settings.pop( "spaceElem", "CG" )
        self.spaceDim = settings.pop( "spaceDim", 1 )
        self.errorNorm = settings.pop( "errorNorm", "L2" )

        self.adaptiveVAR = settings.pop( "adaptiveVAR", True )
        self.adaptiveVARConst = settings.pop( "adaptiveVARConst", True )
        self.adaptiveVarDelta = settings.pop( "adaptiveVarDelta", 0.2 )
        self.adaptiveVarGoal = settings.pop( "adaptiveVarGoal", False )

        self.adaptiveFEM = settings.pop( "adaptiveFEM", True )
        self.adaptiveFEMNorm = settings.pop( "adaptiveFEMNorm", "H1" )
        self.adpativeFEMTheta = settings.pop( "adpativeFEMTheta", 0.5 )
        self.adaptiveFEMGoal = settings.pop( "adaptiveFEMGoal", False )
        self.adaptiveFEMAnisotropic = settings.pop( "adaptiveFEMAnisotropic",
                                                    False )
        self.adaptiveSmoother = settings.pop( "adaptiveSmoother", False )
        self.adaptiveEdgeFlip = settings.pop( "adaptiveEdgeFlip", False )
        self.adaptiveDT = settings.pop( "adaptiveDT", True ) # provided by euler module near boundary upto 10^2 diff
        self.adaptiveFEMDT = settings.pop( "adaptiveFEMDT", True ) # based on the diameters of triangles near the start point
        self.adaptiveOptDTRate = settings.pop( "adaptiveOptDTRate", True )
        self.adaptiveDTConst = settings.pop( "adaptiveDTConst", 10 )
        self.adaptiveDTExp = settings.pop( "adaptiveDTExp", 1.0 )
        self.adaptiveDTBeta = settings.pop( "adaptiveDTBeta", "FEM" ) # "FEM", "DTInv" or float
        self.fixedFEMRate = settings.pop( "fixedFEMRate", None )

        #---------------------------------------------------- technical settings
        self.seed = settings.pop( "seed", float( "inf" ) )
        self.numberOfThreads = settings.pop( "numberOfThreads",
                                             multiprocessing.cpu_count() )
        self.backend = settings.pop( "backend", "eulermodule" )

        #------------------------------------------- check for unknown arguments
        if len( settings ) > 0:
            raise ValueError( "Unknown argument(s): {settings}".format( 
                                                       settings = settings ) )

        #---------------------------------------------------- prepare other data
        self.intermediate = float( "inf" )

        self.means = []
        self.vars = []
        self.nSamps = []
        self.estimate = []
        self.meshes = []

        self.samples = {}
        self.minDT = []
        self.adaptiveDTExpOpt = [self.adaptiveDTExp]
        self.normErrEst = [float( "inf" )]
        self.mcErrEst = []

        self.c_N = []
        self.c_Nnext = None

        self.area = None


    def _solveOnGrid( self,
                      mesh,
                      eta2,
                      overEstConst = 1.0 ):
        #---------------------------------------------- create space and dof map
        if self.backend == "eulermodule":
            exec( "import " + self.problemName + " as em" )
        elif self.backend == "p1EulerSchemeC":
            import p1EulerSchemeC as em
            problem = loadProblem( self.problemName )
        else:
            error( "Unknown Backend" )
        space = FunctionSpace( mesh, self.spaceElem, self.spaceDim )
        try:
            d2vm = vertex_to_dof_map( space )
        except:
            # FEniCS version < 1.4
            DM = space.dofmap()
            d2vm = DM.dof_to_vertex_map( mesh )

        #---------------------------------------------- determine runs per coord
        if self.goal is not None and self.adaptiveVarGoal:
            weigth = interpolate( self.goal, space )
            weight4coords = np.abs( weigth.vector().array()[d2vm] / weigth.vector().max() )
        else:
            weight4coords = 1.0
        if len( self.vars ) == 0 or ( self.adaptiveVARConst and self.c_Nnext is None ):
            runs = np.empty( space.dim(), dtype = int )
            runs.fill( self.runs )
        elif eta2 is not None:
            oldVar = interpolate( self.vars[-1], space )
            var4coords = np.abs( oldVar.vector().array()[d2vm] )
#             c = np.sum( np.sqrt( var4coords ) ) / eta2 * overEstConst
            if not self.adaptiveVARConst:
                if len( self.means ) >= 2:
                    e = interpolate( self.means[-1], space )
                    e.vector()[:] -= interpolate( self.means[-2], space ).vector().array()
                    err2 = norm( e, "L2" ) ** 2
                    print "overConst {c: .1e}".format( c = eta2 / err2 )
    #                 overEstConst = eta2 / err2
                c = space.dim() / eta2 * overEstConst
                runs = c * np.sqrt( var4coords ) * weight4coords
            else:
                runs = self.c_Nnext * var4coords * weight4coords
            runs = runs.astype( int ) + self.minRuns
        else:
            oldVar = interpolate( self.vars[-1], space )
            var4coords = np.abs( oldVar.vector().array()[d2vm] )
            c = space.dim() * self.runs / np.sum( var4coords )
            runs = c * np.sqrt( var4coords ) * weight4coords
            runs = runs.astype( int ) + self.minRuns

        if not self.adaptiveVAR:
            runs[:] = np.max( runs )
        #-------------------------------------------------- solve for each coord
        nCoords = len( mesh.coordinates() )
        mean = np.empty( nCoords )
        var = np.empty( nCoords )
        samps = np.empty( nCoords )
        tStart = tm.time()
        noUpdateRuns = 0
        minDT = float( "inf" )
        maxDT = 0
        if self.adaptiveFEMDT:
#             h = CellSize( mesh )
#             CG1 = FunctionSpace( mesh, "CG", 1 )
#             h = project( h, CG1 )
            hmin = mesh.rmin() # old version
#             hmin = mesh.hmin()
            self.samples = {} # have to reset as different dt
        for iCoord, coord in enumerate( mesh.coordinates() ):
            samps4coords = self.samples.get( tuple( coord ), np.array( [] ) )
            runsLeft = max( runs[iCoord] - len( samps4coords ), 0 )

            #------------------------------------------------ information output
            tDiff = tm.time() - tStart
            time4Samp = tDiff * 1.0 / ( iCoord + 1 - noUpdateRuns )
            if runsLeft > 0:
                print ( "\r{cur:5.1f}% of DoF ({curDoF}/{totalDoF}) | T - {t} | N = {runsLeft} | x = ({coordx: .2f},{coordy: .2f})".format( 
                            cur = iCoord * 100.0 / nCoords,
                            curDoF = iCoord + 1,
                            totalDoF = nCoords,
                            t = _formatTime( time4Samp * ( nCoords - iCoord ) ),
                            runsLeft = runsLeft,
                            coordx = coord[0],
                            coordy = coord[1] ) ).ljust( 80 ),
                sys.stdout.flush()

            #------------------------------------------------ compute each coord
            if runsLeft > 0:
                if self.adaptiveFEMDT:
#                     dt = h( coord ) / 20
                    if self.adaptiveOptDTRate:
                        dt = hmin ** np.mean( self.adaptiveDTExpOpt ) / self.adaptiveDTConst
                    else:
                        dt = hmin ** self.adaptiveDTExp / self.adaptiveDTConst
                else:
                    dt = self.dt
                if self.backend == "eulermodule":
                    retVals = em.stochEulerC( self.numberOfThreads, # @UndefinedVariable
                            self.seed,
                            runsLeft,
                            dt,
                            self.adaptiveDT,
                            self.intermediate,
                            coord,
                            1,
                            M = 0 )
                    newSamps = retVals[:, 2]
                elif self.backend == "p1EulerSchemeC":
                    newSamps = em.multiPath( runsLeft, dt, self.adaptiveDT, coord, problem ) # @UndefinedVariable
                samps4coords = np.concatenate( ( samps4coords, newSamps ) )
                minDT = min( minDT, dt )
                maxDT = max( maxDT, dt )
            else:
                noUpdateRuns += 1

            #-------------------------------------------------------------- save
            mean[iCoord] = np.mean( samps4coords )
            var[iCoord] = np.var( samps4coords )
            samps[iCoord] = len( samps4coords )

            self.samples[tuple( coord )] = samps4coords

        self.minDT.append( minDT )
        print ( "\r{cur:5.1f}% of DoF ({curDoF}/{totalDoF}) | T + {t} | dt_max = {dtmax: .1e} | dt_min = {dtmin: .1e}".format( 
                            cur = 100.0,
                            curDoF = nCoords,
                            totalDoF = nCoords,
                            t = _formatTime( tm.time() - tStart ),
                            dtmax = maxDT,
                            dtmin = minDT ).ljust( 80 ) )
        _printDebugNumbers( "rec. dt", mesh.hmin() ** self.adaptiveDTExp / self.adaptiveDTConst )

        #------------------------------------------------------- build functions
        meanFun = Function( space )
        meanFun.vector()[d2vm] = mean
        varFun = Function( space )
        varFun.vector()[d2vm] = var
        sampFun = Function( space )
        sampFun.vector()[d2vm] = samps
#         plot( meanFun, title = "mean " + str( nCoords ) )
#         plot( varFun, title = "var " + str( nCoords ) )
#         plot( sampFun, title = "samps " + str( nCoords ) )
        return meanFun, varFun, sampFun


    def solveAdaptive( self, mesh, overEstConst = 1.0 ):
        # update area of domain in case it changed
        parameters['allow_extrapolation'] = True
        self.initMesh = mesh

        if self.goal is not None and self.adaptiveFEMGoal:
            indWeight = self.goal
        else:
            indWeight = Constant( 1.0 )

        self.area = assemble( indWeight * dx( mesh ) ) # TODO: or with indWeight

        space = FunctionSpace( mesh, self.spaceElem, self.spaceDim )
        mean = None
        eta2 = None
        timeP, timeW = [], []

        while True:
            #===================================================================
            # compute solution
            #===================================================================
            print "\nNew Level with {dof} DoF\n".format( dof = space.dim() ) + "~"*80
            timePStart = tm.clock()
            timeWStart = tm.time()
            mean, var, samps = self._solveOnGrid( mesh = mesh,
                                                  eta2 = eta2,
                                                  overEstConst = overEstConst )
            self.nSamps.append( samps )
            self.meshes.append( mesh )
            self.means.append( mean )
            self.vars.append( var )
            timeP.append( tm.clock() - timePStart )
            timeW.append( tm.time() - timeWStart )

            #===================================================================
            # break condition
            #===================================================================
            if space.dim() >= self.maxDoF:
                print "Stop for maximum degrees of freedom"
                break
            if self.normErrEst[-1] <= self.targetError:
                print "Stop for target Error"
                break

            #===================================================================
            # estimate and refine
            #===================================================================
            if self.adaptiveSmoother:
                boundary = FacetFunction( "size_t", mesh )
                boundary.set_all( 0 )
                DomainBoundary().mark( boundary, 1 )
                dbc = DirichletBC( mean.function_space(), Constant( 0 ), boundary, 1 )
                functionTools.smoothFunction( mean, [dbc] )
                functionTools.smoothFunction( var, [dbc] )
            if self.adaptiveEdgeFlip:
                testEdgeFlip.flipEdges( mean )

            indicators = indicator( mean, rhs = self.rhs, norm = self.adaptiveFEMNorm )
            if self.goal is not None and self.adaptiveFEMGoal:
                indicatorsRefine = indicator( mean, rhs = self.rhs, weight = indWeight, norm = self.adaptiveFEMNorm )
            else:
                indicatorsRefine = indicators

            eta2 = np.sum( indicators )
            self.estimate.append( np.sqrt( eta2 ) )
            _printDebugNumbers( "est", self.estimate )

            if self.adaptiveFEM:
                if not self.adaptiveFEMAnisotropic:
                    marker = criterion( indicatorsRefine, mesh, self.adpativeFEMTheta )
                    mesh = refine( mesh, marker )
                else:
                    indEdge = edgeJumpKappa.indicator( mean, self.rhs )
                    mesh = anisotrope.refine( mean, indEdge = indEdge )
            else:
                mesh = refine( mesh )
            space = FunctionSpace( mesh, self.spaceElem, self.spaceDim )

            #===================================================================
            # heuristics for parameters
            #===================================================================
            #------------------------------------ estimate MC error contribution
            mcErrFun = Function( self.vars[-1].function_space() )
            mcErrFun.vector()[:] = ( self.vars[-1].vector().array() / self.nSamps[-1].vector().array() ) ** ( 0.5 )
            self.mcErrEst.append( norm( mcErrFun, "L2" ) )
            _printDebugNumbers( "e_MC", self.mcErrEst )
            if len( self.estimate ) > 1:
                #--------------------------- compute fem rate based on estimates
                hmins = [m.rmin() for m in self.meshes]
                warnings.simplefilter( "ignore" )
                alpha, c_h, _, _, _ = stats.linregress( np.log10( hmins ), np.log10( self.estimate ) )
                warnings.simplefilter( "default" )
                _printDebugNumbers( "alpha", alpha )
                if self.fixedFEMRate is not None:
                    alpha = self.fixedFEMRate
                    _printDebugNumbers( "alpha fixed", alpha )

                if self.adaptiveFEMNorm == "H1":
                    alpha += 1 # so that alpha is rate in L2 norm

                #-------------------------------------------- estimate fem error
                refMean = self.means[-1]
                normErrs = []
                indWeightInt = interpolate( indWeight, refMean.function_space() )
                for m in self.means[:-1]:
                    errFun = Function( refMean.function_space() )
                    intM = interpolate( m, refMean.function_space() )
                    errFun.vector()[:] = ( refMean.vector().array() - intM.vector().array() ) * indWeightInt.vector().array()
                    normErrs.append( norm( errFun, self.errorNorm ) )
                normErrs.append( 0.0 )
                normErrs = np.array( normErrs )

                if len( normErrs ) > 2:
                    warnings.simplefilter( "ignore" )
                    alpha_Test, c_h_Test, _, _, _ = stats.linregress( np.log10( hmins[:-1] ), np.log10( normErrs[:-1] ) )
                    warnings.simplefilter( "default" )
                    _printDebugNumbers( "alpha Test", alpha_Test )

                if self.errorNorm == "H1": # slower rate in H1 than L2
                    hminAlphas = np.array( [m.function_space().mesh().rmin() ** ( alpha - 1 ) for m in self.means] )
                else: # L2 norm
                    hminAlphas = np.array( [m.function_space().mesh().rmin() ** alpha for m in self.means] )
                normErrsBias = np.mean( hminAlphas[-1] / hminAlphas[:-1] * normErrs[:-1] ) # estimate last level error based on old errors
                normErrNext = np.mean( mesh.rmin() ** alpha / hminAlphas[:-1] * normErrs[:-1] ) # estimate last level error based on old errors

                self.normErrEst = normErrs + normErrsBias
                _printDebugNumbers( "||e||_" + self.errorNorm + " bias", normErrsBias )
                _printDebugNumbers( "||e||_" + self.errorNorm + " est.", self.normErrEst )

                #-------------------------------- compute c_N and guess next c_N
                self.c_N = ( self.area / self.adaptiveVarDelta / self.normErrEst ) ** 2
                self.c_Nnext = ( self.area / self.adaptiveVarDelta / normErrNext ) ** 2
                _printDebugNumbers( "c_N", self.c_N )
                _printDebugNumbers( "c_Nnext", self.c_Nnext )

            if len( self.means ) > 2:
                #------------------------------- # estimate dt rate (not stable)
                refMean = self.means[-1]
                maxE = []
                normE = []
                for m in self.means[:-1]:
                    intM = interpolate( refMean, m.function_space() )
                    errs = np.abs( intM.vector().array() - m.vector().array() )
                    errs = errs[errs > 1e-10]
                    maxE.append( np.mean( errs ) )
                    errFun = Function( m.function_space() )
                    errFun.vector()[:] = intM.vector().array() - m.vector().array()
                    normE.append( norm( errFun, "L2" ) )
                warnings.simplefilter( "ignore" )
                betaMax, c_M_Max, _, _, _ = stats.linregress( np.log10( self.minDT[:-1] ), np.log10( maxE ) )
                beta, c_M, _, _, _ = stats.linregress( np.log10( self.minDT[:-1] ), np.log10( normE ) )
                warnings.simplefilter( "default" )
                _printDebugNumbers( "beta est_Max", betaMax )
                _printDebugNumbers( "beta est", beta )
                _printDebugNumbers( "beta inv", 1. / beta )
                if self.adaptiveDTBeta == "FEM":
                    beta = alpha
                elif self.adaptiveDTBeta == "DTInv":
                    beta = 1.0 / beta
                else:
                    beta = self.adaptiveDTBeta
#                 self.adaptiveDTExpOpt.append( alpha / beta ) # assumes beta = 1, true for simple domains
#                 print "OPT alpha/beta: {ab: .1e} (mean: {mOpt: .2e})".format(
#                                     ab = self.adaptiveDTExpOpt[-1],
#                                     mOpt = np.mean( self.adaptiveDTExpOpt ) )
                # new try for self.adaptiveDTExpOpt
                self.adaptiveDTExpOpt.append( alpha / 2 )
                _printDebugNumbers( "DTExpOpt", self.adaptiveDTExpOpt )

        return self.means, self.vars, self.nSamps, timeP, timeW


#===============================================================================
# main program to call for specific problem
#===============================================================================

def run( **settings ):

    #--------------------------------------------- clean up settings and prepare
    overEstConst = settings.pop( "overEstConst" )
    doAnalysis = settings.pop( "doAnalysis" )
    filenameHeader = settings.pop( "filenameHeader" )

    rhsDict = {
       "SimpleSinSin" : Constant( 1.0 ),
       "ExponentialBuckle" : Expression( """ 
            - (10 * ((exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) 
            * pow(x[0], 2) * (exp(10 * pow(x[1], 2)) - 1) *  pow(x[1] - 1, 2) 
            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1], 2)
            + 50 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) 
            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2) 
            * pow(x[1], 2) 
            + 50 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 2) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2) 
            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0], 2) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
            + 4 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2) 
            * (exp(10 * pow(x[1], 2)) - 1) * (x[1] - 1) *  x[1]
            + 4 * (exp(10 * pow(x[0], 2)) - 1) * (x[0] - 1) * x[0] 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2) 
            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2) 
            + 200 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) 
            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2) 
            * pow(x[1], 4) 
            + 40 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) 
            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * (x[1] - 1) * pow(x[1], 3) 
            + 200 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 4) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
            + 40 * exp(10 * pow(x[0], 2)) * (x[0] - 1) * pow(x[0], 3) 
            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)))
            """, degree = 8 ),
        "OneDiscExample" : Expression( """ 
            - (10 * ((exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
                * pow(x[0], 2) * (exp(10 * pow(x[1], 2)) - 1) *  pow(x[1] - 1, 2)
                + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1], 2)
                + 50 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
                * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
                * pow(x[1], 2)
                + 50 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 2)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
                + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0], 2)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
                + 4 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
                * (exp(10 * pow(x[1], 2)) - 1) * (x[1] - 1) *  x[1]
                + 4 * (exp(10 * pow(x[0], 2)) - 1) * (x[0] - 1) * x[0]
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
                + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
                + 200 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
                * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
                * pow(x[1], 4)
                + 40 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
                * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * (x[1] - 1) * pow(x[1], 3)
                + 200 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 4)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
                + 40 * exp(10 * pow(x[0], 2)) * (x[0] - 1) * pow(x[0], 3)
                * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)))
            - (10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2)
         - 40 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * (1 - x[1]) * x[1]
         - 400 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 3) * x[1]
         + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * x[1]*x[1]
         + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * pow(1 - x[1], 2) * x[1]*x[1]
         - 40 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * (1 - x[0]) * x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         - 400 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 3) * x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         + 500 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         + 500 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         + 2000 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 4) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
         + 2000 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[0], 4) * x[1]*x[1])
            """, degree = 8 ),
        "OneDiscExampleSimple" : Constant( 1.0 ),
        }
    settings["rhs"] = rhsDict[settings["problemName"]]

    goalDict = {
        "None" : "None",
        "Exp" : """Expression('''
            5 * pow(1 - x[0], 2) * pow(x[0], 2) 
              * (exp(10 * pow(1 - x[0], 2)) - 1)
              * pow(1 - x[1], 2) * pow(x[1], 2) 
              * (exp(10 * pow(1 - x[1], 2)) - 1)
            ''', degree = 8 )""",
        "point"  : """MollifiedPoint2( x0 = [0.2, 0.2], eps = 0.1, C = 2.14 )""",
        "point2" : """MollifiedPoint2( x0 = [0.6, 0.8], eps = 0.1, C = 2.14 )"""
        }
    settings["goalString"] = goalDict[settings.pop( "goal" )]

    #----------------------------------------------------------------------- run

    tStartP = tm.clock()
    tStartW = tm.time()

    solver = adaptiveSPDESolver( **settings )

    means, vars, nSamps, timeP, timeW = solver.solveAdaptive( 
                                 settings["mesh"],
                                 overEstConst = overEstConst )
    tEndP = tm.clock()
    tEndW = tm.time()

    #---------------------------------------------------------------------- save
    if filenameHeader is None:
        filenameHeader = "spde_"
    dirName = filenameHeader + tm.strftime( '%Y-%m-%d_%H-%M-%S' )
    settings["dir"] = "out" + os.path.sep + dirName + os.path.sep
    settings["meansData"] = [saveFunction( mean ) for mean in means]
    settings["varsData"] = [saveFunction( varFun ) for varFun in vars]
    settings["nSampsData"] = [saveFunction( nSampsFun ) for nSampsFun in nSamps]
    settings["mesh"] = meshTools.serialize( settings["mesh"] )
    settings.pop( "rhs" )
    settings["tStartP"] = tStartP
    settings["tStartW"] = tStartW
    settings["tEndP"] = tEndP
    settings["tEndW"] = tEndW
    settings["timeP"] = timeP
    settings["timeW"] = timeW

    try:
        os.makedirs( settings["dir"] )
    except:
        pass


    filename = settings["dir"] + "result.dat"

    with open( filename, 'wb' ) as outFile:
        pickle.dump( settings, outFile,
                    protocol = pickle.HIGHEST_PROTOCOL )

    #---------------------------------------------------------------------- eval
    if doAnalysis:
        try:
            p1InterpolationAdaptiveAnalyse.run( dirName , showPlots = showPlots )
        except Exception:
            traceback.print_exc()
            print "\nERROR: Could not run post processing.\n"

    return filename, dirName

if __name__ == "__main__":
    run()
    interactive()

    print "="*80
    print "done"
