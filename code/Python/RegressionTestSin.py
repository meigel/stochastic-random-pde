from __future__ import division
import numpy as np
import os
import matplotlib
from dolfin.functions.functionspace import FunctionSpace
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

###############################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
###############################################

# number of equidistant grid intervals
#    => N+1 grid points
N = 199

degree = 4

dimGrid = [N+1, N+1]
prob_dim = len(dimGrid)

base = 10
n = 10

x_val = range(1,n+1)
expo = range(1,n+1)
errorL2Ex = np.zeros(n)
errorH1Ex = np.zeros(n)
# errorL2Num = np.zeros(n)
# errorH1Num = np.zeros(n)
# errorDL2 = np.zeros(n)
# errorDH1 = np.zeros(n)
# errorMTL2 = np.zeros(n)
# errorMTH1 = np.zeros(n)
ix_val = np.zeros(n)
expect_05 = np.zeros(n)
times = np.zeros(n)

dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S' ) + "_RegressionSinSin_1e12" + os.path.sep
 
try:
    os.makedirs( dir )
except:
    pass

interpolator = setup_interpolator(N)
space = interpolator.V

rnorm = stats.norm.rvs

u2dTrue = Expression("sin(pi * x[0])* sin(pi * x[1])/exp(1)", element = space.ufl_element())

def disturbedSin(xy):
    return sin(pi * xy[0] + rnorm(0,1) ) * sin(pi * xy[1] + rnorm(0,1) )

def disturbedCorrSin(xy):
    # correlation coefficient
    rho = 0.2
    
    X1 = rnorm(0,1)
    X2 = rnorm(0,1)
    
    # correlated RV
    Y1 = rho * X1 + sqrt(1-rho**2)*X2
    
    return sin(pi * xy[0] + X1 ) * sin(pi * xy[1] + Y1 )

def disturbedMTSin(xy, rvs):
    return sin(pi * xy[0] + rvs[0] ) * sin(pi * xy[1] + rvs[1] )

# step size
dt = 10**-4

# evaluation point for single point version
x0 = [0.5, 0.5]

# number of threads
numThreads = 40

# collect intermediate points?
intermediate = dt**2

# switch adaptivity
adaptive = True

seed = float('inf')

runs = 1

numPoints = 10**12

np.random.seed(1)

print("sampling points ... "),

points_xy = stats.uniform.rvs( loc = 0, scale = 1, size = (numPoints, prob_dim) )

print("done")

calY = np.zeros(numPoints)
#    calYD = np.zeros(numPoints)
#    calYMT = np.zeros(numPoints)

#    means = problem.getIncrementsC( numThreads,
#                                 seed,
#                                 runs,
#                                 dt,
#                                 adaptive,
#                                 intermediate,
#                                 points_xy.flatten(),
#                                 numPoints)

#    rvsMT = means[:,0:2]

print("evaluate sin at each point ... "),

for i, point in enumerate(points_xy):
    calY[i] = disturbedSin(point)
#        calYD[i] = disturbedCorrSin(point)
#        calYMT[i] = disturbedMTSin(point, rvsMT[i,:])

#     means = np.column_stack((points_xy, calY))

print("done")

means = np.column_stack((points_xy, calY))

for it in expo:
#    numPoints = base**it

    print("degree = %s\n" % (str(it)))

    x_val[it-1] = it
#    ix_val[it-1] = 1/x_val[it-1]
#    expect_05[it-1] = 1/(x_val[it-1]**0.5)
    
    u_GloEx = interpolator.create_interpolationLegendreExact(means, it)
#    u_GloNum = interpolator.create_interpolationLegendre(means, it)
#    u_GloD = interpolator.create_interpolationLegendreExact(points_xy, calYD, degree)
#    u_GloMT = interpolator.create_interpolationLegendreExact(points_xy, calYMT, degree)
    
    # calculating and plotting errors
    # ===============================
    errFGloEx = Function(space)
    errFGloEx.vector()[:] = interpolate(u2dTrue, space).vector() - interpolate(u_GloEx, space).vector()
    
#    errFGloNum = Function(space)
#    errFGloNum.vector()[:] = interpolate(u2dTrue, space).vector() - interpolate(u_GloNum, space).vector()
    
#    errFGloD = Function(space)
#    errFGloD.vector()[:] = interpolate(u2dTrue, space).vector() - interpolate(u_GloD, space).vector()
    
#    errFGloMT = Function(space)
#    errFGloMT.vector()[:] = interpolate(u2dTrue, space).vector() - interpolate(u_GloMT, space).vector()
    
    errorL2Ex[it-1] = norm(errFGloEx, "L2")
    errorH1Ex[it-1] = norm(errFGloEx, "H1")

#    errorL2Num[it-1] = norm(errFGloNum, "L2")
#    errorH1Num[it-1] = norm(errFGloNum, "H1")
    
#    errorDL2[it-1] = norm(errFGloD, "L2")
#    errorDH1[it-1] = norm(errFGloD, "H1")
    
#    errorMTL2[it-1] = norm(errFGloMT, "L2")
#    errorMTH1[it-1] = norm(errFGloMT, "H1")
    
    # evaluate global errors
    print("global regression errors:\nL2 exact = %s\tH1 exact = %s" % (errorL2Ex[it-1], errorH1Ex[it-1]))

    plt.clf()

    # prepare output
    data = None
    header = '\tdegree of Legendre basis'
    # write errors
    data = np.column_stack((x_val[0:it], errorL2Ex[0:it], errorH1Ex[0:it]))
    header += '\t$L^2$ exact\t$H^1$ exact\t'
    
    names = header.split("\t")[1:]
    
    np.savetxt(dir+"SinusTest.txt", data, delimiter='\t', header=header)
    
    plt.title('number of points = '+str(numPoints))
    
    plt.plot(x_val[0:it], errorL2Ex[0:it], 'ks-', label=names[1] )
    plt.plot(x_val[0:it], errorH1Ex[0:it], 'ks:', label=names[2] )
    
#    plt.plot(x_val[0:it], errorL2Num[0:it], 'kd-', label=names[3] )
#    plt.plot(x_val[0:it], errorH1Num[0:it], 'kd:', label=names[4] )
       
#    plt.xscale('log', basex=10)
    plt.yscale('log', basey=10)
    ax = plt.gca()
    ax.set_xlabel(names[0])
    ax.set_ylabel('errors')
    plt.legend(loc=1)
    plt.savefig(dir+"SinusTest_1e12.png")
#   plt.show()
