from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('pdf')
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def mpl_plot(obj, fig):
    plt.gca().set_aspect('equal')
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        
        coords = mesh.coordinates()
        x = coords[:,0]
        y = coords[:,1]
        
        z = np.empty(len(coords), dtype="float")
        
        for i, coord in enumerate(coords):
            z[i] = obj(coord)
        
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            ax.plot_trisurf(x, y, z, mesh2triang(mesh), cmap=plt.cm.Spectral)
        else:
            C = obj.compute_vertex_values(mesh)
            ax.plot_trisurf(x, y, z, mesh2triang(mesh), cmap=plt.cm.Spectral)
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')

# step size
dt = 1e-4

# number of threads
numThreads = 40

# collect intermediate points?
intermediate = -1

# switch adaptivity
adaptive = True

# degree of polynomial for regression
degree = 5

# seed = float('inf')
seed = 1
np.random.seed(seed+5)

prob_dim = 2

runs = 1

numPoints = 10**4

# load reference solution
# ===================
# meanRef = loadFunction("SimpleSinSin_mean.fun")
# 
# refMesh = meanRef.function_space().mesh()
# intRef = setup_interpolator(mesh=refMesh)

refMesh = UnitSquareMesh(20, 20)
intRef = setup_interpolator(mesh=refMesh)

# sample starting points
# ===================
coords = stats.uniform.rvs( loc = 0, scale = 1, size = (numPoints, prob_dim) )

# run SDE simulations
# ===================
means = problem.stochEulerC( numThreads, seed, runs, dt, adaptive, intermediate, coords.flatten(), numPoints, 0, 1)

# regression
# ===================
u_RefRegGlo = intRef.create_interpolationLegendre(means, degree=degree)

XYref = refMesh.coordinates()

Zreg = np.empty(len(XYref), dtype="float")
for i, coord in enumerate(XYref):
    Zreg[i] = u_RefRegGlo(coord)

fig = plt.figure()

ax = Axes3D(fig)

surf = ax.plot_trisurf(mesh2triang(refMesh), Zreg, cmap=plt.cm.jet, linewidth=0.5, alpha=0.6)
ax.scatter3D(means[0:500, 0], means[0:500, 1], means[0:500, 2], 'bo')

ax.view_init( 25, -110) # was (75 , -120)

ax.set_xlim3d(0, 1)
ax.set_ylim3d(0, 1)
ax.set_zlim3d(0, 2)

plt.savefig("out/poster/regExample.pdf")
