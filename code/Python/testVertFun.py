from dolfin import *
import numpy as np

mesh = UnitSquareMesh( 3, 3 )

CG1 = FunctionSpace( mesh, "CG", 1 )
samps = Function( CG1 )
samps.vector()[:] = 10

CG2 = FunctionSpace( mesh, "CG", 2 )
samps2 = interpolate( samps, CG2 )

dofmap = CG2.dofmap()
for cell in cells( mesh ):
    localDoF = dofmap.cell_dofs( cell.index() )[3:]
    print cell.index(), localDoF
    samps2.vector()[localDoF] = np.array( [0, 0, 0] )

meshFine = refine( mesh )
CG1fine = FunctionSpace( meshFine, "CG", 1 )
sampsFine = interpolate( samps2, CG1fine )


plot( samps, title = "samps" )
plot( sampsFine, title = "sampsFine", interactive = True )
