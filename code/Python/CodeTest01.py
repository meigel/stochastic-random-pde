from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

###############################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
###############################################

# number of equidistant grid intervals
#    => N+1 grid points
N = 3

# step size
dt = 1e-4

x0 = [0.5, 0.5]

# number of threads
numThreads = 80

# collect intermediate points?
intermediate = -1

# switch adaptivity
adaptive = True

# degree of polynomial for regression
degree = 2

dimGrid = [N+1, N+1]
# seed = float('inf')
seed = 1
prob_dim = len(dimGrid)
parallel_data = {"numThreads": numThreads, "adaptive": adaptive, "intermediate": intermediate, "dimGrid": [1,1], "seed": seed}

runs = 1

# run SDE simulations
# ===================
meanRef = loadFunction("spde1b_mean.fun")

space = meanRef.function_space()
refMesh = space.mesh()

dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S_TestCase01' ) + os.path.sep

filename = "detVSsto_N"

try:
    os.makedirs( dir )
except:
    pass

base = 2
minn = 2
n = 26

x_val = range(minn,n+1)
expo = range(minn,n+1)
errorL2det = np.zeros(n-minn+1)
errorH1det = np.zeros(n-minn+1)
errorL2sto = np.zeros(n-minn+1)
errorH1sto = np.zeros(n-minn+1)
errorL2Reg = np.zeros(n-minn+1)
errorH1Reg = np.zeros(n-minn+1)
ix_val = np.zeros(n-minn+1)
expect_05 = np.zeros(n-minn+1)

# timings
longestTimeDet = np.zeros(n-minn+1)
longestTimeSto = np.zeros(n-minn+1)
meanTimesDet = np.zeros(n-minn+1)
meanTimesSto = np.zeros(n-minn+1)
timesRegDet = np.zeros(n-minn+1)
timesRegSto = np.zeros(n-minn+1)

# ph = PlotHelper()

intRef = setup_interpolator(mesh=refMesh)
coords = refMesh.coordinates()

meanRefvec = meanRef.vector().array()
vals = np.empty(len(coords), dtype="float")
meanVals = np.empty(len(coords), dtype="float")
for i, coord in enumerate(coords):
    meanVals[i] = meanRef(coord)
coordsVal = np.column_stack((coords[:,0], coords[:,1], meanVals))

with TicToc(key="create_interpolationLegendre", accumulate=False, do_print=True):
    u_RefRegGlo = intRef.create_interpolationLegendre(coordsVal, degree=degree)   

errFRefReg = Function(meanRef.function_space())
errFRefReg.vector()[:] = meanRef.vector() - interpolate(u_RefRegGlo, meanRef.function_space()).vector()
 
maxErrorL2 = norm(errFRefReg, "L2")
maxErrorH1 = norm(errFRefReg, "H1")

np.random.seed(seed+5)

for it in expo:
    
    N = sqrt(base**it)-0.5
    
    plt.clf()
    
    intDet = setup_interpolator(int(N))
    
    coordsDet = intDet.mesh.coordinates()
    numPoints = coordsDet.shape[0]
    coordsSto = stats.uniform.rvs( loc = 0, scale = 1, size = (numPoints, prob_dim) )
    
    print("iteration = %s:\tN = %s\tnumPoints = %s" % (str(it), str(N), str(numPoints)))
    
    x_val[it-minn] = numPoints
    ix_val[it-minn] = 1/x_val[it-minn]
    expect_05[it-minn] = 1/(x_val[it-minn]**0.5)
    
    startTime = tm.time()
    startClock = tm.clock()
    
    with TicToc(key="stochEulerC", accumulate=False, do_print=True):
        means = problem.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            coordsDet.flatten(),
                            numPoints)    
    
    longestTimeDet[it-minn] = tm.time()-startTime
    meanTimesDet[it-minn] = (tm.clock()-startClock)/numThreads
    
    startClock = tm.clock()
    
    u_GloDet = intDet.create_interpolationLegendreExact(means, degree)
    
    timesRegDet[it-minn] = tm.clock()- startClock
    
    startTime = tm.time()
    startClock = tm.clock()
    
    with TicToc(key="stochEulerC", accumulate=False, do_print=True):
        means = problem.stochEulerC( parallel_data["numThreads"],
                            parallel_data["seed"],
                            runs,
                            dt,
                            parallel_data["adaptive"],
                            parallel_data["intermediate"],
                            coordsSto.flatten(),
                            numPoints)
          
    longestTimeSto[it-minn] = tm.time()-startTime
    meanTimesSto[it-minn] = (tm.clock()-startClock)/numThreads
    
    startClock = tm.clock()
        
    u_GloSto = intDet.create_interpolationLegendreExact(means, degree)
    
    timesRegSto[it-minn] = tm.clock()- startClock
    
    # calculating and plotting errors
    # ===============================
    errFGloDet = Function(meanRef.function_space())
    errFGloDet.vector()[:] = meanRef.vector() - interpolate(u_GloDet, meanRef.function_space()).vector()
    
    errFGloSto = Function(meanRef.function_space())
    errFGloSto.vector()[:] = meanRef.vector() - interpolate(u_GloSto, meanRef.function_space()).vector()
     
    errorL2det[it-minn] = norm(errFGloDet, "L2")
    errorH1det[it-minn] = norm(errFGloDet, "H1")
    errorL2sto[it-minn] = norm(errFGloSto, "L2")
    errorH1sto[it-minn] = norm(errFGloSto, "H1")
    errorL2Reg[it-minn] = maxErrorL2
    errorH1Reg[it-minn] = maxErrorH1
 
    print("\tdet. error: L2 = %s , H1 = %s\n\trand. error: L2 = %s , H1 = %s" % (errorL2det[it-minn], errorH1det[it-minn],
                                                                                 errorL2sto[it-minn], errorH1sto[it-minn]))
 
    # prepare output
    data = None
    header = '\tNumber of trajectories'
    # write errors
    data = np.column_stack((x_val[0:it-(minn-1)],
                            errorL2det[0:it-(minn-1)], errorH1det[0:it-(minn-1)],
                            errorL2sto[0:it-(minn-1)], errorH1sto[0:it-(minn-1)],
                            errorL2Reg[0:it-(minn-1)], errorH1Reg[0:it-(minn-1)],
                            longestTimeDet[0:it-(minn-1)], longestTimeSto[0:it-(minn-1)],
                            meanTimesDet[0:it-(minn-1)], meanTimesSto[0:it-(minn-1)],
                            timesRegDet[0:it-(minn-1)], timesRegSto[0:it-(minn-1)]))
    header += '\t$L^2$ det.\t$H^1$ det.\t$L^2$ rand.\t$H^1$ rand.\t$L^2$ proj.\t$H^1$ proj.\tlongest time det.\tlongest time rand.\tmean time det.\tmean time rand.\tregression time det.\tregression time rand.\t'
    
    names = header.split('\t')[1:]
    
    np.savetxt(dir+filename+".txt", data, delimiter='\t', header=header)
    
   # plot SPDE errors
    # L2 det. grid
    plt.plot(x_val[0:it-1], data[0:it-1,1], 'ks-', label=names[1])
    # L2 rand. grid
    plt.plot(x_val[0:it-1], data[0:it-1,3], 'ko-', label=names[3])
    # L2 reference
    plt.plot(x_val[0:it-1], data[0:it-1,5], 'k-', label=names[5])
    
    # H1 det. grid
    plt.plot(x_val[0:it-1], data[0:it-1,2], 'ks:', label=names[2])
    # H1 rand. grid
    plt.plot(x_val[0:it-1], data[0:it-1,4], 'ko:', label=names[4])
    # H1 reference
    plt.plot(x_val[0:it-1], data[0:it-1,6], 'k:', label=names[6])
    
    plt.xscale('log', basex=10)
    plt.yscale('log', basey=10)
    ax = plt.gca()
    ax.set_xlabel('Number of trajectories')
    ax.set_ylabel('Errors')
    plt.legend(loc=1)
    plt.savefig(dir+filename+".png")
    plt.savefig(dir+filename+".pdf")

# ph["FEM reference solution"].plot(meanRef)
# ph["Error ref. regression, degree "+str(degree)].plot(errFGlo)
# ph["Regression difference"].plot(errFStoch - errFRef)

# interactive()
