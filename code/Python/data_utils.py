from dolfin import *
import cPickle as pickle
import numpy as np


def serialize( mesh ):
    """
    Serialize a :class:`dolfin.Mesh` for pickling.
    
    Parameters
    ----------
    mesh : :class:`dolfin.Mesh`
        mesh to serialize
    
    Returns
    -------
    meshCells : [[int, ...], ...]
        cells of the mesh
    meshCoords : [[float, ...], ...]
        coordinates of the meshes vertices
    """
    if mesh is None:
        raise TypeError( "Mesh can not be None." )

    meshCells = mesh.cells()
    meshCoords = mesh.coordinates()

    return meshCells, meshCoords

def deserialize( meshCells, meshCoords ):
    """
    Deserialize a mesh into a :class:`dolfin.Mesh`.

    Parameters
    ----------
    meshCells : [[int, ...], ...]
        cells of the mesh
    meshCoords : [[float, ...], ...]
        coordinates of the meshes vertices

    Returns
    -------
    mesh : :class:`dolfin.Mesh`
        mesh

    """
    mesh = Mesh()
    dim = meshCoords.shape[1]
    meshEditor = MeshEditor()
    meshEditor.open( mesh, dim, dim )
    meshEditor.init_vertices( meshCoords.shape[0] )
    meshEditor.init_cells( meshCells.shape[0] )
    for i, v in enumerate( meshCoords ):
        meshEditor.add_vertex( i, v[0], v[1] )
    for i, c in enumerate( meshCells ):
        meshEditor.add_cell( i, c[0], c[1], c[2] )
    meshEditor.close()

    return mesh

def saveFunction( function, name = None ):
    """
    Save a :class:`dolfin.Function` to the disk.
    
    Parameters
    ----------
    function : :class:`dolfin.Function`
        function to save to the disk
    name : string
        name of the file, to save in
    legacy : bool
        if set to True, the legacy format is used
    """
    space = function.function_space()
    mesh = space.mesh()
    meshCells, meshCoords = serialize( mesh )

    dofmap = space.dofmap()
    cell_dof = []
    for cell in cells( mesh ):
        cell_dof.append( dofmap.cell_dofs( cell.index() ) )

    vals = function.vector().array()
    val4dof = vals[ np.array( cell_dof ) ]

    funData = {'name': name,
              'val4dof': val4dof,
              'cells': meshCells,
              'coords': meshCoords,
              'element': space.ufl_element().family(),
              'dim': space.ufl_element().degree()}

    if name is not None:
        with open( name, 'wb' ) as output:
            pickle.dump( funData, output, pickle.HIGHEST_PROTOCOL )
    else:
        return funData

def loadFunction( name = None, funData = None ):
    """
    Load a :class:`dolfin.Function` from the disk.

    Parameters
    ----------
    name : string
        name of the file, to load from

    Returns
    -------
    fun : :class:`dolfin.Function`
        fun loaded from the disk
    """
    if funData is None:
        if name is None:
            raise TypeError( "Either name or funData need to be not None." )
        with open( name, 'rb' ) as inFile:
            funData = pickle.load( inFile )

    mesh = deserialize( funData['cells'], funData['coords'] )
    space = FunctionSpace( mesh, funData['element'], funData['dim'] )

    dofmap = space.dofmap()
    cell_dof = []
    for cell in cells( mesh ):
        cell_dof.append( dofmap.cell_dofs( cell.index() ) )

    val4dof = funData['val4dof']
    fun = Function( space )
    vals = fun.vector()
    for i in range( len( cell_dof ) ):
        vals[ cell_dof[ i ] ] = val4dof[ i ]

    return fun
