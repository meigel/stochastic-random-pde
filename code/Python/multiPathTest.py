from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper

#######################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
#######################################

### number of equidistant grid intervals
### => N+1 grid points
N = 40

### the mesh is located in interpolator.mesh
degree = 5

# uF = solve_fenics( pde_data, interpolator )

ph = PlotHelper()

### step size
dt = 1e-4

### evaluation point for single point version
x0 = [0.5, 0.5]

### number of threads
numThreads = 40

### collect intermediate points?
intermediate = -1

### switch adaptivity
adaptive = True

dimGrid = [N+1, N+1]
seed = 111111111
# seed = float('inf')
prob_dim = len(dimGrid)

runs = 1

meanRef = loadFunction( "spde1_mean.fun" )
# meanRef = uF
space = meanRef.function_space()
refMesh = space.mesh()

print "reference solution loaded"

x0_sol = meanRef( x0 )

base = 2
n = 20

dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S_multiPath' ) + os.path.sep

try:
    os.makedirs( dir )
except:
    pass

x_val = range(2,n+1)
expo = range(2,n+1)
errorsAbs = np.zeros((4, n-1))
stdAbs = np.zeros((4, n-1))
ix_val = np.zeros(n-1)
expect_05 = np.zeros(n-1)
times = np.zeros(n-1)

print "starting calculations"

for it in expo:

    runs = base**it
    
    plt.clf()
    
    x_val[it-2] = runs
    ix_val[it-2] = 1/x_val[it-2]
    expect_05[it-2] = 1/(x_val[it-2]**0.5)

    for exp in range(0, 4):
        
        print "exp = ", exp, ", it = ", it
        
        lastTraj = 10**exp
        
        ### single point version
        with TicToc(key="stochEulerC", accumulate=False, do_print=True):
            means = problem.stochEulerC( numThreads,
                                        seed,
                                        runs,
                                        dt,
                                        adaptive,
                                        intermediate,
                                        x0,
                                        1,
                                        M = 0,
                                        lastTraj = lastTraj );
    
        print "stochEuler done"
    
        errorsAbs[exp, it-2] = abs(x0_sol-means[0, 2])
        stdAbs[exp, it-2] = sqrt(means[0,3])
        
        print "absolute error :", errorsAbs[exp, it-2]
        print "relative error :", errorsAbs[exp, it-2]/x0_sol
        print "standard deviation :", stdAbs[exp, it-2]
        
        # prepare output
    data = None
    header = '\tnumber of trajectories'
    # write errors
    
    data = np.column_stack((x_val[0:it-1],
                            errorsAbs[0,0:it-1],
                            stdAbs[0,0:it-1],
                            errorsAbs[1,0:it-1],
                            stdAbs[1,0:it-1],
                            errorsAbs[2,0:it-1],
                            stdAbs[2,0:it-1],
                            errorsAbs[3,0:it-1],
                            stdAbs[3, 0:it-1],
                           ))
    header += '\terror 1\tstd 1\terror 10\tstd 10\terror 100\tstd 100\terror 1000\tstd 1000'
     
    np.savetxt(dir+"MP.txt", data, delimiter='\t', header=header)
     
    plt.plot(x_val[0:it-1], errorsAbs[0, 0:it-1], 'ks-', label="1" )
    plt.plot(x_val[0:it-1], errorsAbs[1, 0:it-1], 'bs--', label="10" )
    plt.plot(x_val[0:it-1], errorsAbs[2, 0:it-1], 'rs--', label="100" )
    plt.plot(x_val[0:it-1], errorsAbs[3, 0:it-1], 'gs--', label="1000" )
    
    plt.xscale('log', basex=10)
    plt.yscale('log', basey=10)
    ax = plt.gca()
    ax.set_xlabel('number of trajectories')
    ax.set_ylabel('absolute errors')
    plt.legend(loc=1)
    plt.savefig(dir+"MP.png")

