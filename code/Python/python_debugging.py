from __future__ import division
import numpy as np
import os
from dolfin import *
import scipy.stats as stats
import copy
#from joblib import Parallel, delayed
from matplotlib import pyplot as plt
import time
import euler as em

class Interpolator( object ):
    '''FEM interpolation class'''
    def __init__( self, mesh, inside_domain ):
        self.mesh = mesh
        self.inside_domain = inside_domain
        self.V = FunctionSpace( mesh, 'CG', 1 )
    def create_interpolation( self, f, runs = 1 ):
        # evaluate nodal values
        coords = self.mesh.coordinates()
        fx = [np.array( [f( x ) for x in coords] ) for _ in range( runs )]
        # compute point-wise expectation
        fx = sum( fx ) / runs
        # create interpolation function
        u = Function( self.V )
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map( self.V )
        except:
            # old FEniCS version
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        u.vector()[d2vm] = fx
        return u
    def create_interpolationC( self, means ):
        u = Function( self.V )
        try:
            # d2vm = dof_to_vertex_map(self.V)
            d2vm = vertex_to_dof_map( self.V )
        except:
            # old FEniCS version
            DM = self.V.dofmap()
            d2vm = DM.dof_to_vertex_map(self.mesh)
        u.vector()[d2vm] = np.array(means[:,2])
        return u

def setup_interpolator( N ):
    def inside_domain( x ):
        return x[0] > 0 and x[0] < 1 and x[1] > 0 and x[1] < 1
    mesh = UnitSquareMesh( N, N )
    return Interpolator( mesh, inside_domain )

def solve_fenics( pde_data, interpolator ):
    def boundary( x, on_boundary ):
        return on_boundary
    uD = pde_data["uD"]
    f = pde_data["f"]
    g = pde_data["g"]
    kappa = pde_data["kappa"]
    V = interpolator.V
    bc = DirichletBC( V, uD, boundary )
    u = TrialFunction( V )
    v = TestFunction( V )
    a = inner( kappa * grad( u ), grad( v ) ) * dx
    L = f * v * dx + g * v * ds
    u = Function( V )
    solve( a == L, u, bc )
    return u

def solve_pointC ( pde_data, parallel_data, dt, interpolator, runs, x0 ):
    # pde_data will not yet be used, C-code does not use FEniCS-Expression so far
    points = em.stochEulerC( parallel_data["numThreads"], parallel_data["numSamples"], parallel_data["seed"], runs, dt, parallel_data["adaptive"], parallel_data["intermediate"], x0, parallel_data["dimGrid"], parallel_data["prob_dim"] )
    Iu = interpolator.create_interpolationC( points )
    return Iu

def solve_spdeC( pde_data, parallel_data, dt, interpolator, runs ):
    # setup stochastic Euler solver
    means = em.stochEulerC( parallel_data["numThreads"], parallel_data["numSamples"], parallel_data["seed"], runs, dt, parallel_data["adaptive"], parallel_data["intermediate"], interpolator.mesh.coordinates().flatten(), parallel_data["dimGrid"], parallel_data["prob_dim"] )
    Iu = interpolator.create_interpolationC( means )
    return Iu

f = Constant( 1.0 )
kappa = Constant( 1.0 )
#uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" )
uD = Constant( 0.0 )
g = Constant( 0.0 )

pde_data = {"kappa":kappa, "f":f, "uD":uD, "g":g}
N = 100
interpolator = setup_interpolator( N )
dt = 2**-13
x0 = [0.5, 0.5]

numThreads = 2
numSamples = numThreads*4096*1000
intermediate = False
adaptive = False
dimGrid = [N+1, N+1]
seed = float('inf')
prob_dim = len(dimGrid)
parallel_data = {"numThreads":numThreads, "numSamples":numSamples, "adaptive":adaptive, "intermediate":intermediate, "dimGrid":[1,1], "seed":seed, "prob_dim":prob_dim}
runs = 2

#start = time.clock();
means = em.stochEulerC( parallel_data["numThreads"],parallel_data["numSamples"],parallel_data["seed"],runs, dt,parallel_data["adaptive"],parallel_data["intermediate"],x0,[1,1],parallel_data["prob_dim"] );
#end = time.clock();
#print "%.6fs" % (end-start)
