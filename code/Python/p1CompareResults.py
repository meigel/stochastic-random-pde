#!/usr/bin/env python
import glob
import os

from dolfin import *

from data_utils import loadFunction
import cPickle as pickle
import numpy as np
import p1InterpolationAdaptiveAnalyse
from p1InterpolationAdaptiveTest import MollifiedPoint2
import time as tm


def compare( dataDirNames = "tc_2016-11-10_12-03-48",
#             dataDirNames = ["spde_2015-07-09_11-28-23",
#                          "spde_2015-07-09_14-09-23",
#                          "spde_2015-07-09_11-33-43",
#                          "spde_2015-07-09_11-39-31",
#                          "spde_2015-07-09_13-04-51"],
            compareVals = [""],
            plotH1error = True,
            xAxis = "timeP",
            color = False,
            plotFunctions = True,
            maxTimeP = 1e6,
            customQLabels = True,
            plotQSplitColor = 3,
            debug = False ): # dof, timeP, timeW
    """
    Files for Thesis:
    =================
    
    SimpleSinSin:
    tc_2015-12-08_22-01-21
    
    ExponentialBuckle:
    tc_2015-12-08_09-46-09
    """
    #===========================================================================
    # settings
    #===========================================================================
    valNameShort = {
                    "problemName" : "prob",
                    "meansData" : "m",
                    "varsData" : "v",
                    "nSampsData" : "N",
                    "tStartP" : "tSp",
                    "tStartW" : "tSw",
                    "tEndP" : "tEp",
                    "tEndW" : "tEw",
                    "outDir" : "outDir",
                    "dt" : "dt",
                    "maxDoF" : "maxDoF",
                    "minRuns" : "minR",
                    "runs" : "runs",
                    "adaptiveVAR" : "aV",
                    "adaptiveVarGoal" : "aVg",
                    "adaptiveVARConst" : "aVC",
                    "adaptiveVarDelta" : "aVD",
                    "adaptiveFEM" : "aFEM",
                    "adaptiveFEMGoal" : "aFEMg",
                    "adaptiveDT" : "aDT",
                    "adaptiveFEMDT" : "aFEMDT",
                    "adaptiveOptDTRate" : "aODT",
                    "adaptiveDTConst" : "aDTC",
                    "adaptiveDTExp" : "aDTExp",
                    "adaptiveDTBeta" : "aDTB",
                    "timeP" : "tP",
                    "timeW" : "tW"
                    }

    if maxTimeP is not None:
        print u"\u203E" * 80
        print "WARNING: Max Time is set!"
        print "_" * 80 + "\n"

    #===========================================================================
    # load data
    #===========================================================================
    parameters['allow_extrapolation'] = True
    if isinstance( dataDirNames, basestring ): # case of testCase
        tcFileName = "out" + os.path.sep + dataDirNames + os.path.sep + "result.dat"
        with open( tcFileName, 'rb' ) as inFile:
            loadData = pickle.load( inFile )
            dataDirs = loadData["dirNames"]
            if debug:
                dataDirs = dataDirs[-4:-3]
            compareVals = loadData["props"]
        outDir = "out" + os.path.sep + dataDirNames + os.path.sep + "compare" + os.path.sep
    else:
        outDir = "out" + os.path.sep + "compare" + tm.strftime( '%Y-%m-%d_%H-%M-%S' ) + os.path.sep

    datas = []
    for dir in dataDirs:
        filename = "out" + os.path.sep + dir + os.path.sep + "result.dat"
        with open( filename, 'rb' ) as inFile:
            datas.append( pickle.load( inFile ) )

    problemName = datas[0]["problemName"]

    for data in datas:
        # backwards compatibility
        data.setdefault( "adaptiveVAR", True )

        data["means"] = [loadFunction( funData = meanData ) for meanData in data["meansData"]]
        data["vars"] = [loadFunction( funData = varData ) for varData in data["varsData"]]
        data["nSamps"] = [loadFunction( funData = nSampsData ) for nSampsData in data["nSampsData"]]
        if data["problemName"] != problemName:
            error( "Different Problems" )
#     plot( datas[1]["means"][-1].function_space().mesh(), interactive = True )
    # debug code
#     x0 = [0.2, 0.8]
#     x1 = [0.2, 0.6]
#     lastLevels = 5
#     for mean in datas[1]["means"][-lastLevels:]:
#         print "mean0", mean( x0 )
#         print "mean1", mean( x1 )
# #         plot( mean, title = "mean" )
#     for var in datas[1]["vars"][-lastLevels:]:
#         print "var0", var( x0 )
#         print "var1", var( x1 )
# #         plot( var, title = "var" )
#     for nSamps in datas[1]["nSamps"][-lastLevels:]:
#         print "nSamps0", nSamps( x0 )
#         print "nSamps1", nSamps( x1 )
# #         plot( nSamps, title = "nSamps" )
# #     interactive()
#     return

    print "ProblemName:", problemName

    print "load reference solution ...",
    if not debug:
        meanRef = loadFunction( problemName + "_mean.fun" )
    else:
        meanRef = loadFunction( "debug_mean.fun" )
    print "done"

    #============================================================================
    # post process
    #============================================================================
    # for other backend


    try:
        from tools import plotTools
        useMLMCPlot = True
    except:
        import matplotlib as mpl
        mpl.use( 'Agg' )
        import matplotlib.pyplot as plt
        useMLMCPlot = False

    try:
        os.makedirs( outDir )
    except:
        pass


    #---------------------------------------------------------------- individual
    print "\nCompute individual Errors\n"
    singleDirEnd = "singlePlots" + os.path.sep
    singleDir = outDir + singleDirEnd

    try:
        os.makedirs( singleDir )
    except:
        pass

    for i, ( dataDir, data ) in enumerate( zip( dataDirs, datas ) ):
        singleName = "tc_{i}_".format( i = i )
        print "\n", singleName
        errL2, errH1, errQ = p1InterpolationAdaptiveAnalyse.run( dataDir,
                                           showPlots = False,
                                           meanRef = meanRef,
                                           saveName = singleDir + singleName,
                                           color = color,
                                           plotFunctions = plotFunctions )

        if useMLMCPlot:
            print "combine plots for test case {i}".format( i = i ),
            pdfFiles = glob.glob( singleDir + singleName + "*.pdf" )
            pdfFiles = [pdfFile[len( outDir ):] for pdfFile in pdfFiles]
            pdfFiles.sort()
            plotTools.combinePlots( path = outDir,
                                    files = pdfFiles,
                                    n = 4,
                                    outname = singleName + "combined",
                                    addExtension = "" )
        data["errorL2"] = errL2
        data["errorH1"] = errH1
        data["errorQ"] = errQ
    print " done"

    #-------------------------------------------------------------- all together
    if useMLMCPlot:
        hold = False
        for data in datas:
            print "TimeP: {timeP}".format( timeP = data["tEndP"] - data["tStartP"] )
            if xAxis == "dof":
                xVals = [mean.function_space().dim() for mean in data["means"] ]
            elif xAxis == "timeP":
                xVals = data["timeP"]
            elif xAxis == "timeW":
                xVals == data["timeW"]

            if ( xAxis == "timeP" or xAxis == "timeW" ) and data["adaptiveFEM"] == True:
                xVals = np.cumsum( xVals )
            if compareVals != [""]:
                labelEnds = []
                for compVal in compareVals:
                    labelEnds.append( valNameShort.get( compVal, compVal ) + ": " + str( data[compVal] ) )
                labelEnd = ", ".join( labelEnds )
            else:
                labelEnd = " aV: {aV}, aF: {aFEM}, aD: {aDT}, aFD: {aFEMDT}, aOR: {aOptDTRate}".format( 
                                             aV = int( data["adaptiveVAR"] ),
                                             aFEM = int( data["adaptiveFEM"] ),
                                             aDT = int( data["adaptiveDT"] ),
                                             aFEMDT = int( data["adaptiveFEMDT"] ),
                                             aOptDTRate = int( data["adaptiveOptDTRate"] ) )

            plotTools.plot2D( xVals,
                     data["errorL2"],
                     name = labelEnd, # r"""$L^2$ """ + labelEnd,
                     xlabel = r"""\texttt{""" + xAxis + r"""}""",
                     ylabel = "error",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_right",
                     dots = True,
                     markerfacecolor = "k",
                     hold = hold
                     )
            hold = True
            if plotH1error:
                plotTools.plot2D.numCalls -= 1

                plotTools.plot2D( xVals,
                                  data["errorH1"],
                                  name = "", # "r"""$H^1$ """ + labelEnd,
                                  dots = True,
                                  markerfacecolor = "r",
                                  markeredgecolor = "r",
                                  color = "r"
                                  )

        #----------------------------------------------------------- mini legend
        import matplotlib.pyplot as plt
        dotPattern = r"""\hspace{1pt}\rule[0.5ex]{1.5pt}{1.2pt}\hspace{2pt}""" * 3 + r"""\hspace{-1pt}"""
        rulePattern = r"""\rule[0.5ex]{13.5pt}{1.2pt}""" + r"""\hspace{-1pt}"""
        bullet = r"""\raisebox{-0.2ex}{\scalebox{1.4}{$\bullet$}}\!"""
        circ = r"""\raisebox{-0.2ex}{\scalebox{1.4}{$\circ$}}\!"""
        plt.text( 0.77, 0.97,
                  r"""\textcolor{red}{"""
                  + rulePattern
                  + r""" $||\mathtt e^{\mathrm{MS}}||_{H^1}$}"""
                  + "\n"
                  + rulePattern
                  + r""" $||\mathtt e^{\mathrm{MS}}||_{L^2}$"""
                  ,
                  horizontalalignment = 'left',
                  verticalalignment = 'top',
                  transform = plt.gca().transAxes )

        plotTools.savePlot( filename = outDir + "compare", show = False )

        #------------------------------------------ separate plots for H1 and L2
        hold = False
        for data in datas:
            if xAxis == "dof":
                xVals = [mean.function_space().dim() for mean in data["means"] ]
            elif xAxis == "timeP":
                xVals = data["timeP"]
            elif xAxis == "timeW":
                xVals == data["timeW"]

            if ( xAxis == "timeP" or xAxis == "timeW" ) and data["adaptiveFEM"] == True:
                xVals = np.cumsum( xVals )
            if compareVals != [""]:
                labelEnds = []
                for compVal in compareVals:
                    labelEnds.append( valNameShort.get( compVal, compVal ) + ": " + str( data[compVal] ) )
                labelEnd = ", ".join( labelEnds )
            else:
                labelEnd = " aV: {aV}, aF: {aFEM}, aD: {aDT}, aFD: {aFEMDT}, aOR: {aOptDTRate}".format( 
                                             aV = int( data["adaptiveVAR"] ),
                                             aFEM = int( data["adaptiveFEM"] ),
                                             aDT = int( data["adaptiveDT"] ),
                                             aFEMDT = int( data["adaptiveFEMDT"] ),
                                             aOptDTRate = int( data["adaptiveOptDTRate"] ) )

            plotTools.plot2D( xVals,
                     data["errorL2"],
                     name = labelEnd, # r"""$L^2$ """ + labelEnd,
                     xlabel = r"""\texttt{""" + xAxis + r"""}""",
                     ylabel = r"""$||\mathtt e^{\mathrm{MS}}||_{L^2}$""",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_right",
                     dots = True,
                     markerfacecolor = "k",
                     hold = hold
                     )
            hold = True
        plotTools.savePlot( filename = outDir + "compareL2", show = False )

        if plotH1error:
            hold = False
            for data in datas:
                if xAxis == "dof":
                    xVals = [mean.function_space().dim() for mean in data["means"] ]
                elif xAxis == "timeP":
                    xVals = data["timeP"]
                elif xAxis == "timeW":
                    xVals == data["timeW"]

                if ( xAxis == "timeP" or xAxis == "timeW" ) and data["adaptiveFEM"] == True:
                    xVals = np.cumsum( xVals )
                if compareVals != [""]:
                    labelEnds = []
                    for compVal in compareVals:
                        labelEnds.append( valNameShort.get( compVal, compVal ) + ": " + str( data[compVal] ) )
                    labelEnd = ", ".join( labelEnds )
                else:
                    labelEnd = " aV: {aV}, aF: {aFEM}, aD: {aDT}, aFD: {aFEMDT}, aOR: {aOptDTRate}".format( 
                                                 aV = int( data["adaptiveVAR"] ),
                                                 aFEM = int( data["adaptiveFEM"] ),
                                                 aDT = int( data["adaptiveDT"] ),
                                                 aFEMDT = int( data["adaptiveFEMDT"] ),
                                                 aOptDTRate = int( data["adaptiveOptDTRate"] ) )
                plotTools.plot2D( xVals,
                     data["errorH1"],
                     name = labelEnd, # r"""$L^2$ """ + labelEnd,
                     xlabel = r"""\texttt{""" + xAxis + r"""}""",
                     ylabel = r"""$||\mathtt e^{\mathrm{MS}}||_{H^1}$""",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_right",
                     dots = True,
                     markerfacecolor = "k",
                     hold = hold
                     )
                hold = True
            plotTools.savePlot( filename = outDir + "compareH1", show = False )

        #------------------------------------------------------- goal error plot
        if errQ is not None:
            goal = eval( datas[0]["goalString"] )
            qRef = assemble( goal * meanRef * dx )
            hold = False
            for iData, data in enumerate( datas ):
                print "TimeP: {timeP}".format( timeP = data["tEndP"] - data["tStartP"] )
                if xAxis == "dof":
                    xVals = [mean.function_space().dim() for mean in data["means"] ]
                elif xAxis == "timeP":
                    xVals = data["timeP"]
                elif xAxis == "timeW":
                    xVals == data["timeW"]

                if ( xAxis == "timeP" or xAxis == "timeW" ) and data["adaptiveFEM"] == True:
                    xVals = np.cumsum( xVals )
                if compareVals != [""]:
                    labelEnds = []
                    for compVal in compareVals:
                        labelEnds.append( valNameShort.get( compVal, compVal ) + ": " + str( data[compVal] ) )
                    labelEnd = ", ".join( labelEnds )
                else:
                    labelEnd = " aV: {aV}, aF: {aFEM}, aD: {aDT}, aFD: {aFEMDT}, aOR: {aOptDTRate}".format( 
                                                 aV = int( data["adaptiveVAR"] ),
                                                 aFEM = int( data["adaptiveFEM"] ),
                                                 aDT = int( data["adaptiveDT"] ),
                                                 aFEMDT = int( data["adaptiveFEMDT"] ),
                                                 aOptDTRate = int( data["adaptiveOptDTRate"] ) )

                if customQLabels:
                    labelEnd = "aFEM: "
                    if data["adaptiveFEM"] and data["adaptiveFEMGoal"]:
                        labelEnd += "goal"
                    elif data["adaptiveFEM"]:
                        labelEnd += "global"
                    else:
                        labelEnd += "uniform"
                    labelEnd += ", aV: "
                    if data["adaptiveVAR"] and data["adaptiveVarGoal"]:
                        labelEnd += "goal"
                    elif data["adaptiveVAR"]:
                        labelEnd += "global"
                    else:
                        labelEnd += "uniform"

                markerfacecolor = "k"
                if iData >= plotQSplitColor:
                    markerfacecolor = "w"

                if iData == plotQSplitColor:
                    plotTools.plot2D.numCalls = -1

                yVals = np.array( data["errorQ"] ) / abs( qRef )
                if xAxis == "timeP":
                    yVals = [yVal for yVal, xVal in zip( yVals, xVals ) if xVal <= maxTimeP]
                    xVals = [xVal for xVal in xVals if xVal <= maxTimeP]
                plotTools.plot2D( xVals,
                     yVals,
                     name = labelEnd, # r"""$L^2$ """ + labelEnd,
                     xlabel = r"""\texttt{""" + xAxis + r"""}""",
                     ylabel = r"""$|Q(u) - Q(u_\ell)| / |Q(u)|$""",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_lower", # "outside_right",
                     dots = True,
                     markerfacecolor = markerfacecolor,
                     hold = hold
                     )
                hold = True
            plotTools.savePlot( filename = outDir + "compareQ", show = False )
    else:
        ax = plt.subplot( 111 )
        tikStyles = ["s", "o", "*", "d", "p", "^", "v", ">", "<", "h", "x", "+"]
        for data, tik in zip( datas, tikStyles ):
            print "TimeP: {timeP}".format( timeP = data["tEndP"] - data["tStartP"] )
            if xAxis == "dof":
                xVals = [mean.function_space().dim() for mean in data["means"] ]
            elif xAxis == "timeP":
                xVals = data["timeP"]
            elif xAxis == "timeW":
                xVals == data["timeW"]

            if ( xAxis == "timeP" or xAxis == "timeW" ) and data["adaptiveFEM"] == True:
                xVals = np.cumsum( xVals )
            if compareVals != [""]:
                labelEnds = []
                for compVal in compareVals:
                    labelEnds.append( valNameShort.get( compVal, compVal ) + ": " + str( data[compVal] ) )
                labelEnd = ", ".join( labelEnds )
            else:
                labelEnd = "aF: {aFEM}, aD: {aDT}, aFD: {aFEMDT}".format( aFEM = int( data["adaptiveFEM"] ),
                                                                               aDT = int( data["adaptiveDT"] ),
                                                                               aFEMDT = int( data["adaptiveFEMDT"] ) )
            p = ax.loglog( xVals,
                    data["errorL2"], 'k' + tik + '-', label = "L2 " + labelEnd )
            plt.setp( p, "markerfacecolor", "w" )
            if plotH1error:
                p = ax.loglog( xVals,
                        data["errorH1"], 'k' + tik + '--', label = "H1 " + labelEnd )
                plt.setp( p, "markerfacecolor", "k" )

        plt.xlabel( xAxis )
        ax.set_position( [0.1, 0.1, 0.5, 0.6] )
        lgd = ax.legend( bbox_to_anchor = ( 1.05, 1 ), loc = 2, borderaxespad = 0. )
        plt.savefig( outDir + "compare.pdf",
                     bbox_extra_artists = ( lgd, ),
                     bbox_inches = 'tight' )

        plt.close( 'all' )

if __name__ == "__main__":
    compare()

    print "="*80
