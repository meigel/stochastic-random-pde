from __future__ import division, print_function
import numpy as np
import ufl
from dolfin import Expression, FiniteElement, interactive, Function, norm
from interpolator import setup_interpolator
from plothelper import PlotHelper

# problem parameters
# ------------------

# function type
Ftype = 4       # HINT: try switching to type 6 while keeping the following parameters at 500, 7, 4, 50, 50

# sample size
M = 500

# polynomial regression degrees
globalp = 7
localp = 4

# local regression neighbours
local_neighbours = 50

# interpolator grid size
N = 50

# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------

# define test function
ref_elem= FiniteElement('Lagrange', ufl.triangle, 1)
Fstr = ["x[0]", "x[0]*x[1]", "x[0]*x[0] + x[1]*x[1]", "sin(2*pi*x[0])", "sin(2*pi*x[0])*cos(2*pi*x[1])", "sin(5*pi*x[0])",
        "sin(5*pi*x[0])*cos(5*pi*x[1])"]
f = Expression(Fstr[Ftype], element=ref_elem)

# compute samples
xy = np.random.rand(M,2)
# add boundary nodes
NB = np.floor(np.sqrt(M))
dx = 1/(NB-1)
x1 = np.vstack((np.linspace(0,1,NB), np.zeros(NB)))
x2 = np.vstack((np.linspace(0,1,NB), np.ones(NB)))
y1 = np.vstack((np.zeros(NB-2), np.linspace(dx,1-dx,NB-2)))
y2 = np.vstack((np.ones(NB-2), np.linspace(dx,1-dx,NB-2)))
print(xy.shape, x1.shape, x2.shape, y1.shape, y2.shape)
xy = np.concatenate((xy,x1.T,x2.T,y1.T,y2.T))
# evaluate function
fxy = np.array([f(p) for p in xy])
print(xy.shape, len(fxy))

# plot samples
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(xy[:,0], xy[:,1], fxy, c='r', marker='o')
plt.ion()
plt.show()

# setup iterpolator and evaluate interpolation/regression
Interpolator = setup_interpolator(N)
If = Interpolator.create_interpolation(f)
Gf = Interpolator.create_interpolationGLS(np.hstack((xy, fxy[:,np.newaxis])), degree=globalp)
max_dist = 10*Interpolator.mesh.hmax()
Lf = Interpolator.create_interpolationMLS(np.hstack((xy, fxy[:,np.newaxis])), neighbours=local_neighbours, degree=localp, max_dist=max_dist)

# setup error function
Ef = Function(Interpolator.V)
Ef.vector()[:] = If.vector() - Gf.vector()
Ef2 = Function(Interpolator.V)
Ef2.vector()[:] = If.vector() - Lf.vector()

# evaluate global errors
print("\n\nglobal regression errors: L2 = %s\tH1 = %s" % (norm(Ef, "L2"), norm(Ef, "H1")))
print("local regression errors: L2 = %s\tH1 = %s" % (norm(Ef2, "L2"), norm(Ef2, "H1")))

# plot
ph = PlotHelper()
ph["If (interpolation)"].plot(If)
ph["Gf (global regression)"].plot(Gf)
ph["Lf (global regression)"].plot(Lf)
ph["Ef (error global regression)"].plot(Ef)
ph["Ef (error local regression)"].plot(Ef2)

interactive()
