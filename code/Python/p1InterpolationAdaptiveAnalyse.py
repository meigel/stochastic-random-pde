#!/usr/bin/env python
import multiprocessing
import os

from dolfin import *

from data_utils import loadFunction
from p1InterpolationAdaptiveTest import MollifiedPoint2
import ExponentialBuckle as em
import cPickle as pickle
import numpy as np
import time as tm


def run( dir = "spde_2015-07-10_18-30-38",
         showPlots = True,
         meanRef = None,
         saveName = None,
         color = False,
         plotFunctions = True ):

    set_log_level( ERROR )
    parameters['allow_extrapolation'] = True
    #----------------------------------------------------------------- load data
    filename = "out" + os.path.sep + dir + os.path.sep + "result.dat"
    with open( filename, 'rb' ) as inFile:
        data = pickle.load( inFile )

    problemName = data.pop( "problemName", "SimpleSinSin" )
    print "\nProblem Name:", problemName
    for key in  data.keys():
        exec( key + " = data['" + key + "']" )

    # backwards compatibility
    data.setdefault( "adaptiveVAR", True )


    means = [loadFunction( funData = meanData ) for meanData in meansData]
    vars = [loadFunction( funData = varData ) for varData in varsData]
    nSamps = [loadFunction( funData = sampData ) for sampData in nSampsData]
    goal = eval( goalString )

    # samples/area (needs time/area)
    n4areas = []
    for nSamp in nSamps:
        mesh = nSamp.function_space().mesh()
        DG0 = FunctionSpace( mesh, "DG", 0 )
        area = CellVolume( mesh )
        n4areas.append( project( nSamp / area, DG0 ) )

    if saveName is None:
        saveName = dir

    #-------------------------------------------------------------- post process
    tDiffP = tEndP - tStartP
    tDiffW = tEndW - tStartW
    print "time taken (P):", tDiffP / 60.0, "min"
    print "time taken (W):", tDiffW / 60.0, "min"

    if meanRef is None:
        print "load reference solution ...",
        meanRef = loadFunction( problemName + "_mean.fun" )
        print "done"

    print "compute L2 and H1 errors ...",
    refSpace = meanRef.function_space()
    errorL2 = []
    errorH1 = []
    for mean in means:
        e = interpolate( mean, refSpace )
        e.vector()[:] = e.vector().array() - meanRef.vector().array()
        errorL2.append( norm( e, "L2" ) )
        errorH1.append( norm( e, "H1" ) )
    print "done"

    print "L2 error:", errorL2
    print "H1 error:", errorH1

    if goal is not None:
        print "compute goal errors ...",
        goalError = []
        goalRef = assemble( goal * meanRef * dx )
        for mean in means:
            goalVal = assemble( goal * mean * dx )
            goalError.append( abs( goalRef - goalVal ) )
        print "done"
        print "Goal error:", goalError
    else:
        goalError = None

    #---------------------------------------------------------------------- plot
    try:
        from tools import plotTools
        useMLMCPlot = True
    except:
        import matplotlib as mpl
        mpl.use( 'Agg' )
        import matplotlib.pyplot as plt
        import plothelper

        useMLMCPlot = False

    if useMLMCPlot:
        print "Using MLMC plotting"
        plotTools.plot2D( [mean.function_space().dim() for mean in means ],
                     errorL2,
                     name = r"""$L^2$""",
                     xlabel = r"""\texttt{ndof}""",
                     ylabel = "",
                     logX = True,
                     logY = True,
                     legend_loc = "outside_right",
                     dots = True,
                     hold = False
                     )
        plotTools.plot2D.numCalls = -1

        plotTools.plot2D( [mean.function_space().dim() for mean in means ],
                              errorH1,
                              name = r"""$H^1$""",
                              dots = True,
                              lineStyles = [":"],
                              markerfacecolor = "k"
                              )
        plotTools.savePlot( filename = saveName + "error", show = False )

        if plotFunctions:
            for name in ["means", "vars", "nSamps", "n4areas"]:
                print "plotting", name
                plotTools.plotFunction2DSurf( f = eval( name )[-1],
                                              filename = saveName + name[:-1],
                                              color = color,
                                              useOffset = False )
    else:
        print "Using SPDE plotting"
        plt.figure()
        ax = plt.subplot( 111 )
        ax.loglog( [mean.function_space().dim() for mean in means ],
                    errorL2, 'ko-', label = "L2" )
        ax.loglog( [mean.function_space().dim() for mean in means ],
                    errorH1, 'ks-', label = "H1" )
        ax.set_position( [0.1, 0.1, 0.5, 0.6] )
        lgd = ax.legend( bbox_to_anchor = ( 1.05, 1 ), loc = 2, borderaxespad = 0. )
        plt.savefig( saveName + "error.pdf",
                     bbox_extra_artists = ( lgd, ),
                     bbox_inches = 'tight' )

        for name in ["means", "vars", "nSamps"]:
            plt.figure()
            plothelper.plot_mpl( eval( name )[-1] )
            plt.colorbar()
            plt.title( name[:-1] )
            plt.savefig( saveName + name[:-1] + ".pdf" )
            plt.close()

        if showPlots:
            plot( means[-1], title = "mean" )
            plot( vars[-1], title = "var" )
            plot( nSamps[-1], title = "N" )
            plot( meanRef, title = "reference solution" )

    #----------------------------------------------------------- localised error
#     for mean in means:
#         mesh = mean.function_space().mesh()
#         Constants = FunctionSpace( mesh, "DG", 0 )
#         w = TestFunction( Constants )
#         lastMean = interpolate( mean, refSpace )
#         err2 = Function( refSpace )
#         err2.vector()[:] = ( meanRef.vector().array() - lastMean.vector().array() ) ** 2
#         localErrFun = Function( Constants )
#         localErrFun.vector()[:] = assemble( err2 * w * dx ).array() ** ( 1.0 / 2 )
#         plot( localErrFun, title = "local error" )

        plt.close( 'all' )

    return errorL2, errorH1, goalError



if __name__ == "__main__":
    run()
    interactive()

    print "="*80
    print "done"
