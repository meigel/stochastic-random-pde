from __future__ import division
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time as tm

from tictoc import TicToc
from dolfin import *

import SimpleSinSin as problem
from interpolator import setup_interpolator
from data_utils import deserialize, loadFunction
from plothelper import PlotHelper
from scipy.special.orthogonal import u_roots
import scipy.stats as stats

# DEBUG ===
def TEST_reference_regression(means):
    Iu = np.zeros(72)
    for i in range(72):
        Iu[i]=np.array(means[0,i])
    u_R = Expression("c0 + c1 * x[0] + c2 * x[1] + c3 * x[0]*x[0] + c4 * x[0]*x[1] + c5 * x[1]*x[1] + c6 * x[0]*x[0]*x[0] + c7 * x[0]*x[0]*x[1] + c8 * x[0]*x[1]*x[1] + c9 * x[1]*x[1]*x[1] + c10 * x[0]*x[0]*x[0]*x[0] + c11 * x[0]*x[0]*x[0]*x[1] + c12 * x[0]*x[0]*x[1]*x[1] + c13 * x[0]*x[1]*x[1]*x[1] + c14 * x[1]*x[1]*x[1]*x[1] + c15 * x[0]*x[0]*x[0]*x[0]*x[0] + c16 * x[0]*x[0]*x[0]*x[0]*x[1] + c17 * x[0]*x[0]*x[0]*x[1]*x[1] + c18 * x[0]*x[0]*x[1]*x[1]*x[1] + c19 * x[0]*x[1]*x[1]*x[1]*x[1] + c20 * x[1]*x[1]*x[1]*x[1]*x[1] + c21 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c22 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c23 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c24 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c25 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c26 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c27 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c28 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c29 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c30 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c31 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c32 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c33 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c34 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c35 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c36 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c37 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c38 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c39 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c40 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c41 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c42 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c43 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c44 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c45 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c46 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c47 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c48 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c49 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c50 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c51 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c52 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c53 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c54 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c55 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0] + c56 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1] + c57 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1] + c58 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1] + c59 * x[0]*x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1] + c60 * x[0]*x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1] + c61 * x[0]*x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c62 * x[0]*x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c63 * x[0]*x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c64 * x[0]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c65 * x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1]*x[1] + c66 *   sin(pi*x[0])*(x[0]-0.5)*(x[0]-0.5) + c67 *  sin(pi*x[1])*(x[1]-0.5)*(x[1]-0.5) + c68 *  sin(pi*x[0])*fabs(x[0]-0.5) + c69 *  sin(pi*x[1])*fabs(x[1]-0.5) + c70 *  sin(pi*x[0]) + c71 *  sin(pi*x[1])", c0 = Iu[0], c1 = Iu[1], c2 = Iu[2], c3 = Iu[3], c4 = Iu[4], c5 = Iu[5], c6 = Iu[6], c7 = Iu[7], c8 = Iu[8], c9 = Iu[9], c10 = Iu[10], c11 = Iu[11], c12 = Iu[12], c13 = Iu[13], c14 = Iu[14], c15 = Iu[15], c16 = Iu[16], c17 = Iu[17], c18 = Iu[18], c19 = Iu[19], c20 = Iu[20], c21 = Iu[21], c22 = Iu[22], c23 = Iu[23], c24 = Iu[24], c25 = Iu[25], c26 = Iu[26], c27 = Iu[27], c28 = Iu[28], c29 = Iu[29], c30 = Iu[30], c31 = Iu[31], c32 = Iu[32], c33 = Iu[33], c34 = Iu[34], c35 = Iu[35], c36 = Iu[36], c37 = Iu[37], c38 = Iu[38], c39 = Iu[39], c40 = Iu[40], c41 = Iu[41], c42 = Iu[42], c43 = Iu[43], c44 = Iu[44], c45 = Iu[45], c46 = Iu[46], c47 = Iu[47], c48 = Iu[48], c49 = Iu[49], c50 = Iu[50], c51 = Iu[51], c52 = Iu[52], c53 = Iu[53], c54 = Iu[54], c55 = Iu[55], c56 = Iu[56], c57 = Iu[57], c58 = Iu[58], c59 = Iu[59], c60 = Iu[60], c61 = Iu[61], c62 = Iu[62], c63 = Iu[63], c64 = Iu[64], c65 = Iu[65], c66 = Iu[66], c67 = Iu[67], c68 = Iu[68], c69 = Iu[69], c70 = Iu[70], c71 = Iu[71])
    return u_R
# === DEBUG


#######################################################
### Variables for FEniCS only, which you can manipulate
### These have no effect in the C++ code
#######################################################

# # right hand side
# f = Constant(1.0)
#
# # permeability coefficient
# kappa = Constant(1.0)
#
# # Dirichlet boundary condition
# #uD = Expression( "sin(pi*x[0]) + sin(pi*x[1])" )
# uD = Constant(0.0)
#
# # Neumann boundary condition
# g = Constant(0.0)
#
# # collect everything in one object
# pde_data = {"kappa": kappa, "f": f, "uD": uD, "g": g}

###############################################
### General variables, which you can manipulate
### Effect in C++ and FEniCS
###############################################

# number of equidistant grid intervals
#    => N+1 grid points
N = 785

# step size
dt = 1e-4

x0 = [0.5, 0.5]

# number of threads
numThreads = 40

# collect intermediate points?
intermediate = -1

# switch adaptivity
adaptive = True

dimGrid = [N+1, N+1]
seed = 1 #float('inf')
prob_dim = len(dimGrid)
parallel_data = {"numThreads": numThreads, "adaptive": adaptive, "intermediate": intermediate, "dimGrid": [1,1], "seed": seed}

runs = 1

# load reference solution
print("load reference solution")
uRef = loadFunction("SimpleSinSin_mean.fun")
space = uRef.function_space()
refMesh = space.mesh()

maxNumPoints = (N+1)*(N+1)

monom_degree = 15
legendre_degree = 5

intRef = setup_interpolator(mesh = refMesh)
intN = setup_interpolator(N = N)

# dir = "out" + os.path.sep + tm.strftime( '%Y-%m-%d_%H-%M-%S' ) + os.path.sep
# 
# try:
#     os.makedirs( dir )
# except:
#     pass

ph = PlotHelper()
np.random.seed(seed+5)

# coordinates from reference mesh
coordsRefP = refMesh.coordinates()
print("coordinates from reference mesh, %s" % str(coordsRefP.shape))

# coordinates from uniform distribution
coordsRanP = stats.uniform.rvs( loc = 0, scale = 1, size = (maxNumPoints, prob_dim) )
print("coordinates from uniform distribution, %s" % str(coordsRanP.shape))

# coordinates from equidistant grid
coordsDetP = intN.mesh.coordinates()
print("coordinates from equidistant grid, %s" % str(coordsDetP.shape))

# calculate multiple interpolations for reference mesh
# ====================================================
print("1) calculate multiple interpolations for reference mesh")
z_vals = np.empty(len(coordsRefP), dtype="float")
for i, coord in enumerate(coordsRefP):
    z_vals[i] = uRef(coord)
xyz_vals = np.column_stack((coordsRefP[:,0], coordsRefP[:,1], z_vals))

print("\t1a) P1 interpolation")
u_RefReg_C = intRef.create_interpolationC(xyz_vals)
print("\t1b) global least squares interpolation")
u_RefReg_GLS = intRef.create_interpolationGLS(xyz_vals, degree=monom_degree)
print("\t1c) Legendre interpolation")
u_RefReg_L = intRef.create_interpolationLegendre(xyz_vals, degree=legendre_degree)
print("\t1d) Legendre interpolation exact")
u_RefReg_LE = intRef.create_interpolationLegendreExact(xyz_vals, degree=legendre_degree)

# calculate interpolation errors
err_RefReg_C = Function( space )
err_RefReg_C.vector()[:] = uRef.vector() - interpolate( u_RefReg_C, space ).vector()

print("L2 error, P1 interpolation: %s" % (norm(err_RefReg_C, "L2")) )

err_RefReg_GLS = Function( space )
err_RefReg_GLS.vector()[:] = uRef.vector() - interpolate( u_RefReg_GLS, space ).vector()

print("L2 error, GLS interpolation: %s" % (norm(err_RefReg_GLS, "L2")) )

err_RefReg_L = Function( space )
err_RefReg_L.vector()[:] = uRef.vector() - interpolate( u_RefReg_L, space ).vector()

print("L2 error, Legendre interpolation: %s" % (norm(err_RefReg_L, "L2")) )

err_RefReg_LE = Function( space )
err_RefReg_LE.vector()[:] = uRef.vector() - interpolate( u_RefReg_LE, space ).vector()

print("L2 error, Legendre interpolation (exact): %s" % (norm(err_RefReg_LE, "L2")) )

# ====================================================

# calculate multiple interpolations for sampled points, interpolate on equidistant grid
# =====================================================================================
print("2) calculate multiple interpolations for sampled points, interpolate on equidistant grid")
z_vals = np.empty(len(coordsRanP), dtype="float")
for i, coord in enumerate(coordsRanP):
    z_vals[i] = uRef(coord)
xyz_vals = np.column_stack((coordsRanP[:,0], coordsRanP[:,1], z_vals))

print("\t2a) P1 interpolation skipped")
# u_RanReg_C_N = intN.create_interpolationC(xyz_vals)
print("\t2b) global least squares interpolation")
u_RanReg_GLS_N = intN.create_interpolationGLS(xyz_vals, degree=monom_degree)
print("\t2c) Legendre interpolation")
u_RanReg_L_N = intN.create_interpolationLegendre(xyz_vals, degree=legendre_degree)
print("\t2d) Legendre interpolation exact")
u_RanReg_LE_N = intN.create_interpolationLegendreExact(xyz_vals, degree=legendre_degree)    

# calculate interpolation errors
# err_RanReg_C = Function( space )
# err_RanReg_C.vector()[:] = uRef.vector() - interpolate( u_RanReg_C_N, space ).vector()
# 
# print("L2 error, P1 interpolation: %s" % (norm(err_RanReg_C_N, "L2")) )

err_RanReg_GLS = Function( space )
err_RanReg_GLS.vector()[:] = uRef.vector() - interpolate( u_RanReg_GLS_N, space ).vector()

print("L2 error, GLS interpolation: %s" % (norm(err_RanReg_GLS, "L2")) )

err_RanReg_L = Function( space )
err_RanReg_L.vector()[:] = uRef.vector() - interpolate( u_RanReg_L_N, space ).vector()

print("L2 error, Legendre interpolation: %s" % (norm(err_RanReg_L, "L2")) )

err_RanReg_LE = Function( space )
err_RanReg_LE.vector()[:] = uRef.vector() - interpolate( u_RanReg_LE_N, space ).vector()

print("L2 error, Legendre interpolation (exact): %s" % (norm(err_RanReg_LE, "L2")) )

# =====================================================================================

# calculate multiple interpolations for sampled points, interpolate on reference mesh
# ===================================================================================
print("3) calculate multiple interpolations for sampled points, interpolate on reference mesh")

print("\t3a) P1 interpolation skipped")
# u_RanReg_C_Ref = intN.create_interpolationC(xyz_vals)
print("\t3b) global least squares interpolation")
u_RanReg_GLS_Ref = intRef.create_interpolationGLS(xyz_vals, degree=monom_degree)
print("\t3c) Legendre interpolation")
u_RanReg_L_Ref = intRef.create_interpolationLegendre(xyz_vals, degree=legendre_degree)
print("\t3d) Legendre interpolation exact")
u_RanReg_LE_Ref = intRef.create_interpolationLegendreExact(xyz_vals, degree=legendre_degree)    

# calculate interpolation errors
# err_RanReg_C = Function( space )
# err_RanReg_C.vector()[:] = uRef.vector() - interpolate( u_RanReg_C_Ref, space ).vector()
# 
# print("L2 error, P1 interpolation: %s" % (norm(err_RanReg_C_Ref, "L2")) )

err_RanReg_GLS = Function( space )
err_RanReg_GLS.vector()[:] = uRef.vector() - interpolate( u_RanReg_GLS_Ref, space ).vector()

print("L2 error, GLS interpolation: %s" % (norm(err_RanReg_GLS, "L2")) )

err_RanReg_L = Function( space )
err_RanReg_L.vector()[:] = uRef.vector() - interpolate( u_RanReg_L_Ref, space ).vector()

print("L2 error, Legendre interpolation: %s" % (norm(err_RanReg_L, "L2")) )

err_RanReg_LE = Function( space )
err_RanReg_LE.vector()[:] = uRef.vector() - interpolate( u_RanReg_LE_Ref, space ).vector()

print("L2 error, Legendre interpolation (exact): %s" % (norm(err_RanReg_LE, "L2")) )

# ===================================================================================

# calculate multiple interpolations for equidistant points
# ========================================================
print("4) calculate multiple interpolations for equidistant points")
z_vals = np.empty(len(coordsDetP), dtype="float")
for i, coord in enumerate(coordsDetP):
    z_vals[i] = uRef(coord)
xyz_vals = np.column_stack((coordsDetP[:,0], coordsDetP[:,1], z_vals))

print("\t4a) P1 interpolation")
u_DetReg_C = intN.create_interpolationC(xyz_vals)
print("\t4b) global least squares interpolation")
u_DetReg_GLS = intN.create_interpolationGLS(xyz_vals, degree=monom_degree)
print("\t4c) Legendre interpolation")
u_DetReg_L = intN.create_interpolationLegendre(xyz_vals, degree=legendre_degree)
print("\t4d) Legendre interpolation exact")
u_DetReg_LE = intN.create_interpolationLegendreExact(xyz_vals, degree=legendre_degree)    

# calculate interpolation errors
err_DetReg_C = Function( space )
err_DetReg_C.vector()[:] = uRef.vector() - interpolate( u_DetReg_C, space ).vector()

print("L2 error, P1 interpolation: %s" % (norm(err_DetReg_C, "L2")) )

err_DetReg_GLS = Function( space )
err_DetReg_GLS.vector()[:] = uRef.vector() - interpolate( u_DetReg_GLS, space ).vector()

print("L2 error, GLS interpolation: %s" % (norm(err_DetReg_GLS, "L2")) )

err_DetReg_L = Function( space )
err_DetReg_L.vector()[:] = uRef.vector() - interpolate( u_DetReg_L, space ).vector()

print("L2 error, Legendre interpolation: %s" % (norm(err_DetReg_L, "L2")) )

err_DetReg_LE = Function( space )
err_DetReg_LE.vector()[:] = uRef.vector() - interpolate( u_DetReg_LE, space ).vector()

print("L2 error, Legendre interpolation (exact): %s" % (norm(err_DetReg_LE, "L2")) )

# ========================================================


# plot everthing
#===============
print("plot everthing")

# reference mesh
ph["reference points, P1"].plot( u_RefReg_C )
ph["reference points, GLS"].plot( u_RefReg_GLS )
ph["reference points, Legendre"].plot( u_RefReg_L )
ph["reference points, Legendre exact"].plot( u_RefReg_LE )

# sampled points, equidistant mesh
# ph["sampled points, P1, equidistant mesh"].plot( u_RanReg_C_N )
ph["sampled points, GLS, equidistant mesh"].plot( u_RanReg_GLS_N )
ph["sampled points, Legendre, equidistant mesh"].plot( u_RanReg_L_N )
ph["sampled points, Legendre exact, equidistant mesh"].plot( u_RanReg_LE_N )

# sampled points, reference grid
# ph["sampled points, P1, reference mesh"].plot( u_RanReg_C_Ref )
ph["sampled points, GLS, reference mesh"].plot( u_RanReg_GLS_Ref )
ph["sampled points, Legendre, reference mesh"].plot( u_RanReg_L_Ref )
ph["sampled points, Legendre (exact), reference mesh"].plot( u_RanReg_LE_Ref )

# equidistant points
ph["equidistant points, P1"].plot( u_DetReg_C )
ph["equidistant points, GLS"].plot( u_DetReg_GLS )
ph["equidistant points, Legendre"].plot( u_DetReg_L )
ph["equidistant points, Legendre exact"].plot( u_DetReg_LE )


interactive()

# for it in range(0, n_refine):
#      
#     plt.clf()
#      
#     coords = interpolator.mesh.coordinates()
#      
#     ix_val[it] = 1/x_val[it]
#     expect_05[it] = 1/(x_val[it]**0.5)
#      
#     uRefvec = uRef.vector().array()
#     vals = np.empty(len(coords), dtype="float")
#     meanVals = np.empty(len(coords), dtype="float")
#     for i, coord in enumerate(coords):
#         meanVals[i] = uRef(coord)
#     coordsVal = np.column_stack((coords[:,0], coords[:,1], meanVals))
#   
#      
#     # evaluate global polynomial regression
#     # =====================================
#      
#     # regression on the stochastic solution
#     # StochReg means regression on stochastic solution
#     # ================================================
#      
#     # stoch = problem.regressionC(np.array(means).flatten(), (N+1)*(N+1), degree)
#     # u_StochReg = TEST_reference_regression(stoch)
#     # u_StochReg = interpolator.create_interpolationMLS(means, neighbours=1000, degree=1, max_dist=4/N)
#     # u_StochReg = interpolator.create_interpolationGLS(means, degree=degree)
#     # ph["GLS regression of stoch. solution, degree "+str(degree)].plot(u_StochReg)
#      
#     # regression on the reference FEM solution
#     # RefReg means regression on reference solution
#     # =============================================
#     # stoch = problem.regressionC(np.array(coordsVal).flatten(), (N+1)*(N+1), degree)
#     # u_RefReg = TEST_reference_regression(stoch)
# #     with TicToc(key="create_interpolationMLS", accumulate=False, do_print=True):
# #         u_RefRegMLS = interpolator.create_interpolationMLS(coordsVal, neighbours=1000, degree=loc_degree, max_dist=(4./3.)/sqrt(x_val[it])+0.01)
#     # ph["GLS regression of reference solution, degree "+str(degree)].plot(u_RefReg)
#      
#     with TicToc(key="create_interpolationGLS", accumulate=False, do_print=True):
#         u_RefRegGlo = interpolator.create_interpolationLegendreExact(coordsVal, degree=max_degree)    
#      
#     # calculating and plotting errors
#     # ===============================
# #     errFMLS = Function(uRef.function_space())
# #     errFMLS.vector()[:] = uRef.vector() - interpolate(u_RefRegMLS, uRef.function_space()).vector()
#     errFGlo = Function(uRef.function_space())
#     errFGlo.vector()[:] = uRef.vector() - interpolate(u_RefRegGlo, uRef.function_space()).vector()
#      
# #     L2MLS[it] = norm(errFMLS, "L2")
# #     H1MLS[it] = norm(errFMLS, "H1")
#      
#     L2Glo[it] = norm(errFGlo, "L2")
#     H1Glo[it] = norm(errFGlo, "H1")
#      
#     # evaluate global errors
#     print("refinement = %s\nglobal regression errors: L2 = %s\tH1 = %s" % (str(it), norm(errFGlo, "L2"), norm(errFGlo, "H1")))
# #     print("refinement = %s\nlocal regression errors: L2 = %s\tH1 = %s" % (str(it), norm(errFMLS, "L2"), norm(errFMLS, "H1")))
#  
#     interpolator.refine()
#      
#     # prepare output
#     data = None
#     header = '\tN (number of points)'
#     # write errors
#     data = np.column_stack((x_val[0:it+1], L2Glo[0:it+1], H1Glo[0:it+1]))
#     header += '\tL2 glob\tH1 glob'
#      
#     np.savetxt(dir+"ReferenceRegression_SimpleSinSin.txt", data, delimiter='\t', header=header)
#      
#     plt.plot(x_val[0:it+1], L2Glo[0:it+1], colors[colcount%7]+'s--', label="L2 reg" )
#     plt.plot(x_val[0:it+1], H1Glo[0:it+1], colors[colcount%7]+'d--', label="H1 reg" )
#      
#      
#     # plt.plot(x_val, L2MLS, colors[colcount%7]+'s--', label="L2 loc" )
#     # plt.plot(x_val, H1MLS, colors[colcount%7]+'d--', label="H1 loc" )
#      
#     colcount+=1
#     plt.xscale('log', basex=10)
#     plt.yscale('log', basey=10)
#     ax = plt.gca()
#     ax.set_xlabel('N (number of points)')
#     ax.set_ylabel('errors')
#     plt.legend(loc=3)
#     plt.savefig(dir+"ReferenceRegression_SimpleSinSin.png")
# plt.show()
