
stochEuler <- function(numThreads = 20, numSamp = 4096*numThreads*1000,
		       seed = Inf, numTraj = 100, stepsize = 1e-4,
		       adapt = TRUE, intermediate = TRUE,
		       grid = c(0.5, 0.5), grid_dim = c(1,1)){
	dyn.load("RInterface.so")
	
	if(!is.matrix(grid)){
		if(!is.vector(grid)){
			grid <- as.matrix(grid)
		}
		else{
			grid <- t(as.matrix(grid))
		}
	}
	
	samples <-  .Call("stochEulerC",
		    as.integer(numThreads),
		    as.integer(numSamp),
		    as.double(seed),
		    as.integer(numTraj),
		    as.double(stepsize),
		    as.integer(adapt),
		    as.integer(intermediate),
		    as.double(t(grid)),	 # grid
		    as.integer(grid_dim),		 # dimensions of the grid
		    as.integer(dim(grid)[2]),		 # dimension of the problem
		    DUP=FALSE)
	
	file <- "traject.bin"
	size <- file.info("traject.bin")$size
	
	dd <- readBin(file, what="numeric", size=8, n=size/8)
	
	system("rm -f traject.bin")
	
	dim(dd) <- c(3, length(dd)/3)
	
	list(dd=dd, samples=samples)
	
	dd
}

regNplot <- function(points, degree = 4, grid_disc = 0.01){
	
	library(rgl)
	
	xCoords <- as.matrix(points[1,])
	yCoords <- as.matrix(points[2,])
	data <- as.vector(points[3,])
	
	b <- seq(0,1,grid_disc)
	grid <- expand.grid(b,b)
	
	z <- 0
	
	if(degree == 1){
		reg <- lm(data ~ 1+xCoords+yCoords)
		cat("regression done\n")
		
		M <- matrix(data=0, nrow = dim(grid)[1], ncol=3)
		M[,1] <- 1
		M[,2] <- grid[,1]
		M[,3] <- grid[,2]
		
		z <- M %*% reg$coeff
		dim(z) <- c(length(b), length(b))
	} else if(degree == 2){
		x2Coords <- xCoords^2
		y2Coords <- yCoords^2
		
		xyCoords <- xCoords*yCoords
		
		reg <- lm(data ~ 1+xCoords+x2Coords+yCoords+y2Coords+xyCoords)
		cat("regression done\n")
		
		M <- matrix(data=0, nrow = dim(grid)[1], ncol=6)
		M[,1] <- 1
		M[,2] <- grid[,1]
		M[,3] <- grid[,1]^2
		M[,4] <- grid[,2]
		M[,5] <- grid[,2]^2
		M[,6] <- grid[,1]*grid[,2]
		
		z <- M %*% reg$coeff
		dim(z) <- c(length(b), length(b))
	} else if(degree == 3){
		x2Coords <- xCoords^2
		y2Coords <- yCoords^2
		
		x3Coords <- xCoords^3
		y3Coords <- yCoords^3
		
		xyCoords <- xCoords*yCoords
		
		x2yCoords <- x2Coords*yCoords
		xy2Coords <- xCoords*y2Coords
		
		reg <- lm(data ~ 1+xCoords+x2Coords+x3Coords+yCoords+y2Coords+y3Coords+xyCoords+x2yCoords+xy2Coords)
		cat("regression done\n")
		
		M <- matrix(data=0, nrow = dim(grid)[1], ncol=10)
		M[, 1] <- 1
		M[, 2] <- grid[,1]
		M[, 3] <- grid[,1]^2
		M[, 4] <- grid[,1]^3
		M[, 5] <- grid[,2]
		M[, 6] <- grid[,2]^2
		M[, 7] <- grid[,2]^3
		M[, 8] <- grid[,1]*grid[,2]
		M[, 9] <- grid[,1]^2 * grid[,2]
		M[,10] <- grid[,1] * grid[,2]^2
		
		z <- M %*% reg$coeff
		dim(z) <- c(length(b), length(b))
	} else if(degree == 4){
		x2Coords <- xCoords^2
		y2Coords <- yCoords^2
		
		x3Coords <- xCoords^3
		y3Coords <- yCoords^3
		
		x4Coords <- xCoords^4
		y4Coords <- yCoords^4
		
		xyCoords <- xCoords*yCoords
		
		x2yCoords <- x2Coords*yCoords
		xy2Coords <- xCoords*y2Coords
		x2y2Coords <- x2Coords*y2Coords
		
		x3yCoords <- x3Coords*yCoords
		xy3Coords <- xCoords*y3Coords
		
		reg <- lm(data ~ 1+xCoords+x2Coords+x3Coords+x4Coords+ yCoords+y2Coords+y3Coords+y4Coords+xyCoords+x2yCoords+xy2Coords+x2y2Coords+x3yCoords+xy3Coords)
		cat("regression done\n")
		
		M <- matrix(data=0, nrow = dim(grid)[1], ncol=15)
		M[, 1] <- 1
		M[, 2] <- grid[,1]
		M[, 3] <- grid[,1]^2
		M[, 4] <- grid[,1]^3
		M[, 5] <- grid[,1]^4
		M[, 6] <- grid[,2]
		M[, 7] <- grid[,2]^2
		M[, 8] <- grid[,2]^3
		M[, 9] <- grid[,2]^4
		M[,10] <- grid[,1]*grid[,2]
		M[,11] <- grid[,1]^2 * grid[,2]
		M[,12] <- grid[,1] * grid[,2]^2
		M[,13] <- grid[,1]^2 * grid[,2]^2
		M[,14] <- grid[,1]^3 * grid[,2]
		M[,15] <- grid[,1] * grid[,2]^3
		
		z <- M %*% reg$coeff
		dim(z) <- c(length(b), length(b))
	}
	
	open3d( windowRect=c(0,0,640,400) )
	
	zlim <- range(z)
	zlen <- (zlim[2] - zlim[1])/grid_disc
	
	colorlut <- rev(rainbow(zlen, start=0, end=4/6))
	
	col <- colorlut[(z-zlim[1])/grid_disc]
	
	persp3d(b, b, z, color=col, back="lines", aspect=c(1,1,0.25))
# 	grid3d(c("x", "y+", "z"), n =10)
	axes3d(nticks=10)
	
	return(reg$coeff)
}
