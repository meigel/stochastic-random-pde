
#include <R.h>
#include <Rinternals.h>
#include <sys/time.h>
#include "ParaRNG.h"
#include "Euler.h"

extern "C"{
	SEXP stochEulerC(SEXP ui_numT, SEXP ui_numS, SEXP ui_seed,
			 SEXP ui_numTraj,
			 SEXP ui_stepsize, SEXP ui_adapt, SEXP ui_intermediate,
			 SEXP ui_grid, SEXP ui_dimGrid,
			 SEXP ui_dimProb
			)
	{
		SEXP trajectories;

		// maximum number of threads is 52, due to memory limitation
		/// @WARNING Every number greater then 52 WILL produce segfaults.
		/// @TODO fix this!
		int numThreads = *INTEGER(ui_numT);
		if(numThreads > 52) numThreads = 52;
		if(numThreads <= 0) numThreads =  1;
		
		// number of samples
		unsigned int numSamples = *INTEGER(ui_numS);
		
		/// numSamples MUST be a multiple of MT_RNG_COUNT
		///      and SHOULD be a multiple of numThreads
		
		numSamples = ( (numSamples + numThreads * MT_RNG_COUNT -1)/(numThreads * MT_RNG_COUNT) ) * numThreads * MT_RNG_COUNT;
		
		// number of trajectories
		unsigned int numPaths = *INTEGER(ui_numTraj);
		
		/// set numPaths to the smallest multiple of numThreads to have optimal performance
// 		numPaths = ( (numPaths + numThreads -1)/numThreads ) * numThreads;
		
		double stepsize = *REAL(ui_stepsize);
		
		bool adaptive = (*INTEGER(ui_adapt) != 0);
		bool intermediate = (*INTEGER(ui_intermediate) != 0);
		
		int prob_dim = *INTEGER(ui_dimProb);
		
		std::vector<int> grid_dim(prob_dim);
		
		int numGridPoints = 1;
		
		for(int i = 0; i < prob_dim; ++i){
			grid_dim[i] = INTEGER(ui_dimGrid)[i];
			numGridPoints *= grid_dim[i];
		}
		
		// starting points
		std::vector<double> grid(numGridPoints*prob_dim);
		
		for(int i = 0;  i < numGridPoints*prob_dim; ++i){
			grid[i] = REAL(ui_grid)[i];
		}
		
		double d_seed = *REAL(ui_seed);
		unsigned int seed = UINT_MAX;
		
		if(R_finite(d_seed))
			seed = (unsigned int)d_seed;
		
		/***************************************************/
		Rprintf("Starting computation with %d threads over %d trajectories in %d points.\n", numThreads, numPaths, numGridPoints);
		
		ParaRNG rng(numSamples, numThreads);
		EulerScheme stochEuler(numPaths, numThreads, 1, true, "traject.bin");
		
// 		int nProtected = 0;
		
		// solve with stochastic Euler scheme
		
		// which type to use?
		bool singlePoint = (numGridPoints == 1);
		
		if(singlePoint){
			stochEuler.solveSP(grid, stepsize, adaptive, intermediate, seed);
		} else {
			stochEuler.solveGrid(grid, grid_dim, stepsize, adaptive, intermediate, seed);
		}
		
// 		fprintf(stderr, "left function solveSP\n");
		
// 		unsigned long int length = stochEuler.getLength();
		
		/*
		Rprintf("length = %lu\n", length);
		
		PROTECT(trajectories = allocVector(REALSXP, length));
		++nProtected;
		
		for (unsigned long int i = 0; i < length; ++i)
		{
			REAL(trajectories)[i] = vals[i];
		}
		
		UNPROTECT(nProtected);
		*/
		
		return trajectories;
	}
} // extern C end
