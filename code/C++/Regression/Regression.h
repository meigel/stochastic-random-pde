/*
 * Regression.h
 *
 *  Created on: Feb 9, 2015
 *      Author: Marcel Ladkau
 */

#ifndef REGRESSION_H_
#define REGRESSION_H_
//-----------------------------------------------------------------------------------------------//
#include "Arr2Dmc.h"

class Regression
{
	public:
		Regression();
		~Regression();

		Arr2D_mc* regEuler;
		std::vector<double> coeff;
		std::vector<std::vector<double> > grid;

		Arr2D_mc* RegressionEuler(Arr2D_mc* regression, int basis,
				int numOfGridPoints, std::vector<double> &grid1,
				std::vector<double> &grid2);

		/**
		 * Returns the Laguerre polynomial of degree counter evaluated at x, y.
		 */
		double laguerreBasis(double x, double y, int counter);

		Arr2D_mc* getBasisEuler(int index, int basis,
				std::vector<double> &grid1, std::vector<double> &grid2);

		Arr2D_mc* solveForward(Arr2D_mc* matrix, Arr2D_mc* vector);
		Arr2D_mc* solveBackward(Arr2D_mc* matrix, Arr2D_mc* vector);

	private:

		/// terminal time
		double T;

};
#endif /* REGRESSION_H_ */
