/*
 * Arr2Dmc.h
 *
 *  Created on: Feb 9, 2015
 *      Author: Marcel Ladkau
 */

#ifndef ARR2DMC_H_
#define ARR2DMC_H_

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <vector>
#include "string.h"
#include "stdlib.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_sf_zeta.h>

class Arr2D_mc
{
	private:

		/// two dimensional array of pointers without memory allocation
		long double **array;

		/// variable for the dimension of rows
		int dimr;

        /// variable for the dimension of columns
		int dimc;

	public:
		// constructers
		Arr2D_mc(int n, int m);

		Arr2D_mc* matrixAlloc(int rows, int cols);

		/// pointer to double-value, given position (row, column) in the array
		long double* getEntry_mc(int Row, int Column) const;

		/// handover of val, to position (row, column)
		void setEntry_mc(long double val, int Row, int Column);

		/// get amount of rows
		int getDimRow();

		//get amount of columns
		int getDimColumn();
		void Print();
		Arr2D_mc* Transpose();
		Arr2D_mc* matrixProduct(Arr2D_mc* a1, Arr2D_mc* a2);
		long double Det();
		Arr2D_mc* Minor(const int row, const int col) const;
		Arr2D_mc* Inv();
		Arr2D_mc* Cholesky();
		Arr2D_mc* Diag(int rows);
		void Swap(long double &a, long double &b);
		void TestPrint();
		void TestMinor();
//-------------------------------------------------------------
		static gsl_matrix* getPinv(const gsl_matrix* A);
		static void solveLeastSquare(const gsl_matrix* A, const gsl_vector* b,
				gsl_vector* regCoeff);

		//destructor
		virtual ~Arr2D_mc();
};


#endif /* ARR2DMC_H_ */
