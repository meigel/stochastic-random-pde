/*
 * Arr2Dmc.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: Marcel Ladkau
 */

#include "Arr2Dmc.h"

gsl_matrix* Arr2D_mc::getPinv(const gsl_matrix* A)
{

	int numMCPaths = (int) A->size1;
	int kappa = (int) A->size2;

	// first create an SVD of the input matix A
	gsl_matrix* U = gsl_matrix_alloc(numMCPaths, kappa);
	gsl_matrix_memcpy(U, A);
	gsl_vector* S = gsl_vector_alloc(kappa); // contains the singular values
	gsl_matrix* V = gsl_matrix_alloc(kappa, kappa);

	// this is the SVD. Note that U is numMCPaths x kappa.
	// This is already the reduced SVD.
	gsl_linalg_SV_decomp_jacobi(U, V, S);

	// invert the diagonal entries of the singular value vector and rescale V
	// to obtain V*Sigma^(-1), which is overwritten to V
	gsl_vector* dummy = gsl_vector_alloc(kappa);
	for (int k = 0; k < kappa; k++)
	{
		long double sVal = gsl_vector_get(S, k);
		//sVal = sVal>1e-20? 1.0/sVal : sVal;
		//sVal = sVal>0. ? 1.0/sVal : sVal;
		/*sVal = sVal>1e-09 ? 1.0/sVal : sVal;*/
		sVal = sVal > 1e-11 ? 1.0 / sVal : sVal;

		//if(sval>0) {
		//	gsl_vector_set(S,k,1.0/sval);
		//}

		gsl_matrix_get_col(dummy, V, k);
		gsl_vector_scale(dummy, sVal);
		gsl_matrix_set_col(V, k, dummy);
	}
	gsl_vector_free(dummy);

	// now the pseudoinverse: comes by the matrix product pinv=V*U^T
	gsl_matrix* pinv = gsl_matrix_calloc(kappa, numMCPaths);
	//gsl_matrix_set_zero(pinv);

	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, V, U, 0.0, pinv);	//pinv=V*U

	// free memory
	gsl_matrix_free(U);
	gsl_vector_free(S);
	gsl_matrix_free(V);

	return pinv;
}

//--------------------------------------------------------------
void Arr2D_mc::solveLeastSquare(const gsl_matrix* A, const gsl_vector* b,
		gsl_vector* regCoeff)
{// solves AregCoeff=b in the least square sense: min ||AregCoeff-b||, x = A*regCoeff

	/*

	 //int MCpaths = A->size1;
	 gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc(A->size1,A->size2);
	 gsl_matrix* cov = gsl_matrix_alloc(A->size2,A->size2);
	 double chisq;
	 gsl_multifit_linear(A,b,regCoeff,cov,&chisq,work);
	 gsl_blas_dgemv(CblasNoTrans,1.0,A,regCoeff,0.0,x);

	 gsl_matrix_free(cov);
	 gsl_multifit_linear_free(work);
	 */

	// ---------------------------- -------------------
	// using simulation based regression and pinv
	// ---------------------------- -------------------
	int MCpaths = A->size1;
	long double one_over_sqrt_L = 1.0 / sqrt((long double) MCpaths);
	gsl_matrix* dummy = gsl_matrix_alloc(A->size1, A->size2);
	gsl_matrix_memcpy(dummy, A);
	gsl_matrix_scale(dummy, one_over_sqrt_L);
	gsl_matrix* pinv = getPinv(dummy);

	gsl_blas_dgemv(CblasNoTrans, one_over_sqrt_L, pinv, b, 0.0, regCoeff);

	gsl_matrix_free(dummy);
	gsl_matrix_free(pinv);

}

Arr2D_mc::Arr2D_mc(int Row, int Column) :
		dimr(Row), dimc(Column)	//Klassenmethode, int d uebergeben, Zuweisung zur Variablen dim
{
	array = new long double*[dimr];
	for (int i = 0; i < dimr; i++)
	{
		array[i] = new long double[dimc];
	}
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::matrixAlloc(int rows, int cols)
{
	Arr2D_mc* res = new Arr2D_mc(rows, cols);
	return res;
}

//-----------------------------------------------------------------------------------------------//
long double* Arr2D_mc::getEntry_mc(int Row, int Column) const
{
	if (Row >= dimr)
	{
		std::cerr << "No Entry available (Row)!" << std::endl;
	}
	if (Column >= dimc)
	{
		std::cerr << "No Entry available (Column)!" << std::endl;
	}
	return &array[Row][Column];
}
//-----------------------------------------------------------------------------------------------//
void Arr2D_mc::setEntry_mc(long double val, int Row, int Column)//ueberschreibt den Wert an Position Zeile/Spalte mit val
{
	if (Row >= dimr)
	{
		std::cerr << "Matrix exceeded (Row)!" << std::endl;
	}
	if (Column >= dimc)
	{
		std::cerr << "Matrix exceeded (Column)!" << std::endl;
	}
	long double * x = getEntry_mc(Row, Column);
	*x = val;
}
//-----------------------------------------------------------------------------------------------//
int Arr2D_mc::getDimRow()
{
	return dimr;
}
//-----------------------------------------------------------------------------------------------//
int Arr2D_mc::getDimColumn()
{
	return dimc;
}
//-----------------------------------------------------------------------------------------------//
void Arr2D_mc::Print()
{
	std::cout << "The matrix reads:\n";
	std::cout << dimr << "\t" << dimc << std::endl;
	for (int i = 0; i < dimr; i++)
	{
		for (int j = 0; j < dimc; j++)
		{
			std::cout << *getEntry_mc(i, j) << "\t";
		}
		std::cout << "\n";
	}
	std::cout << "This is the end of matrix output!\n";
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::Transpose()
{
	Arr2D_mc* res = new Arr2D_mc(dimc, dimr);
	for (int i = 0; i < dimc; i++)
	{
		for (int j = 0; j < dimr; j++)
			res->setEntry_mc(*getEntry_mc(j, i), i, j);
	}
	return res;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::matrixProduct(Arr2D_mc* a1, Arr2D_mc* a2)
{
	Arr2D_mc* res = matrixAlloc(a1->getDimRow(), a2->getDimColumn());
	if (a1->getDimColumn() == a2->getDimRow())
	{
		for (int i = 0; i < a1->getDimRow(); i++)
		{
			for (int j = 0; j < a2->getDimColumn(); j++)
			{
				long double help = 0.0;
				for (int d = 0; d < a1->getDimColumn(); d++)
				{
					help += (*a1->getEntry_mc(i, d)) * (*a2->getEntry_mc(d, j));
				}
				res->setEntry_mc(help, i, j);
			}
		}
	}
	else
	{
		throw "Matrix dimensions do not match!";
	}
	return res;
}
//-----------------------------------------------------------------------------------------------//
long double Arr2D_mc::Det()
{
	long double d = 0;    // value of the determinant
	int rows = dimr;
	int cols = dimc;

	if (rows == cols)
	{
		// this is a square matrix
		if (rows == 1)
		{
			// this is a 1 x 1 matrix
			d = *getEntry_mc(0, 0);
		}
		else if (rows == 2)
		{
			// this is a 2 x 2 matrix
			// the determinant of [a11,a12;a21,a22] is det = a11*a22-a21*a12
			d = *getEntry_mc(0, 0) * *getEntry_mc(1, 1)
					- *getEntry_mc(1, 0) * *getEntry_mc(0, 1);
		}
		else
		{
			// this is a matrix of 3 x 3 or larger
			for (int c = 0; c < cols; c++)
			{
				//d += pow(-1, 1+c) * a(1, c) * Det(M);
				d += ((c + 1) % 2 + (c + 1) % 2 - 1) * *getEntry_mc(0, c)
						* Minor(0, c)->Det(); // faster than with pow()
			}
			//cout << "d " << d << endl;
		}
		//cout << "d " << d << endl;
	}
	else
	{
		throw "Matrix must be square!";
	}
	return d;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::Minor(const int row, const int col) const
{
	Arr2D_mc* res = new Arr2D_mc(dimr - 1, dimc - 1);
	if (row >= 0 && row < dimr && col >= 0 && col < dimc)
	{
		// copy the content of the matrix to the minor, except the selected
		long double dummyr = 0.;
		long double dummyc = 0.;
		bool boolr = true;
		bool boolc;
		for (int r = 0; r < dimr - 1; r++)
		{
			boolc = true;
			dummyc = 0;
			for (int c = 0; c < dimc - 1; c++)
			{
				if (boolr && r == row)
				{
					boolr = false;
					dummyr += 1;
				}
				if (boolc && c == col)
				{
					boolc = false;
					dummyc += 1;
				}
				res->setEntry_mc(*getEntry_mc(r + dummyr, c + dummyc), r, c);
			}
		}
	}
	else
	{
		throw "Index for minor out of range";
	}
	return res;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::Inv()
{
	Arr2D_mc* res = new Arr2D_mc(dimr, dimc);
	long double d = 0;    // value of the determinant
	int rows = dimr;
	int cols = dimc;
	d = Det();

	if (rows == cols && d != 0)
	{
		// this is a square matrix
		if (rows == 1)
		{
			// this is a 1 x 1 matrix
			res->setEntry_mc(1 / *getEntry_mc(0, 0), 0, 0);
		}
		else if (rows == 2)
		{
			// this is a 2 x 2 matrix
			res->setEntry_mc(*getEntry_mc(0, 0), 1, 1);
			res->setEntry_mc(-*getEntry_mc(0, 1), 0, 1);
			res->setEntry_mc(-*getEntry_mc(1, 0), 1, 0);
			res->setEntry_mc(*getEntry_mc(1, 1), 0, 0);
			for (int r = 0; r < dimr; r++)
			{
				for (int c = 0; c < dimc; c++)
				{
					res->setEntry_mc(*res->getEntry_mc(r, c) / d, r, c);
				}
			}
		}
		else
		{
			// this is a matrix of 3 x 3 or larger
			// calculate inverse using gauss-jordan elimination
			//   http://mathworld.wolfram.com/MatrixInverse.html
			//   http://math.uww.edu/~mcfarlat/inverse.htm
			res = Diag(rows); // a diagonal matrix with ones at the diagonal
			Arr2D_mc* ai = new Arr2D_mc(dimr, dimc);
			for (int r = 0; r < dimr; r++)
			{
				for (int c = 0; c < dimc; c++)
				{
					ai->setEntry_mc(*getEntry_mc(r, c), r, c);
				}
			}

			for (int c = 0; c < cols; c++)
			{
				// element (c, c) should be non zero. if not, swap content
				// of lower rows
				int r;
				for (r = c; r < rows; r++)
				{
					if (*ai->getEntry_mc(c, c) == 0)
					{
						if (r != c)
						{
							// swap rows
							for (int s = 0; s < cols; s++)
							{
								Swap(*ai->getEntry_mc(c, s),
										*ai->getEntry_mc(r, s));
								Swap(*res->getEntry_mc(c, s),
										*res->getEntry_mc(r, s));
							}
						}
					}
				}

				// eliminate non-zero values on the other rows at column c
				for (int r = 0; r < rows; r++)
				{
					if (r != c)
					{
						// eleminate value at column c and row r
						if (*ai->getEntry_mc(r, c) != 0)
						{
							long double f = -*ai->getEntry_mc(r, c)
									/ *ai->getEntry_mc(c, c);

							// add (f * row c) to row r to eleminate the value
							// at column c
							for (int s = 0; s < cols; s++)
							{
								*ai->getEntry_mc(r, s) += f
										* *ai->getEntry_mc(c, s);
								*res->getEntry_mc(r, s) += f
										* *res->getEntry_mc(c, s);
							}
						}
					}
					else
					{
						// make value at (c, c) one,
						// divide each value on row r with the value at ai(c,c)
						long double f = *ai->getEntry_mc(c, c);
						for (int s = 0; s < cols; s++)
						{
							*ai->getEntry_mc(r, s) /= f;
							*res->getEntry_mc(r, s) /= f;
						}
					}
				}
			}
		}
	}
	else
	{
		if (rows == cols)
		{
			Print();
			throw "Determinant of matrix is zero";
		}
		else
		{
			throw "Matrix must be square";
		}
	}
	return res;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::Cholesky()
{
	Arr2D_mc* res = new Arr2D_mc(dimr, dimc);
	int rows = dimr;
	int cols = dimc;

	for (int r = 0; r < rows; r++)
	{
		for (int c = 0; c < cols; c++)
		{
			if (r == c)
			{
				long double dummy = 0.;
				for (int k = 0; k < r; k++)
				{
					dummy += *res->getEntry_mc(r, k) * *res->getEntry_mc(r, k);
				}
				res->setEntry_mc(sqrt(*getEntry_mc(r, c) - dummy), r, c);
			}
			if (r > c)
			{
				long double dummy = 0.;
				for (int k = 0; k < c; k++)
				{
					dummy += *res->getEntry_mc(r, k) * *res->getEntry_mc(c, k);
				}
				res->setEntry_mc(
						1. / *res->getEntry_mc(c, c)
								* (*getEntry_mc(r, c) - dummy), r, c);
			}
			if (r < c)
			{
				res->setEntry_mc(0., r, c);
			}
		}
	}

	return res;
}

//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Arr2D_mc::Diag(int rows)
{
	Arr2D_mc* res = new Arr2D_mc(rows, rows);
	for (int r = 0; r < rows; r++)
	{
		for (int c = 0; c < rows; c++)
		{
			if (r == c)
			{
				res->setEntry_mc(1, r, c);
			}
			else
			{
				res->setEntry_mc(0, r, c);
			}
		}
	}
	return res;
}
//-----------------------------------------------------------------------------------------------//
void Arr2D_mc::Swap(long double &a, long double &b)
{
	long double help = a;
	a = b;
	b = help;
}
//-----------------------------------------------------------------------------------------------//
void Arr2D_mc::TestPrint()
{
	Print();
}
//-----------------------------------------------------------------------------------------------//
void Arr2D_mc::TestMinor()
{
	std::cout << "For the following matrix:\n";
	Print();
	for (int i = 0; i < getDimRow(); i++)
	{
		for (int j = 0; j < getDimColumn(); j++)
		{
			std::cout << "The Minor where row " << i + 1 << " and column "
					<< j + 1 << " is removed reads\n";
			Minor(i, j)->Print();
		}
	}
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc::~Arr2D_mc()                        //Destruktor
{
	delete[] array;
}

