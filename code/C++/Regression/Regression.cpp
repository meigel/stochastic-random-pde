/*
 * Regression.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: Marcel Ladkau
 */

#include "Regression.h"

//-----------------------------------------------------------------------------------------------//
Regression::Regression()
{
	T = 0;
	regEuler = NULL;
}
//-----------------------------------------------------------------------------------------------//
Regression::~Regression()
{
	/// clean up grid
	grid.clear();
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Regression::RegressionEuler(Arr2D_mc* regression, int basis,
		int numOfGridPoints, std::vector<double> &grid1,
		std::vector<double> &grid2)
{
	if (false)
	{
		Arr2D_mc* d = new Arr2D_mc(numOfGridPoints, basis);
		Arr2D_mc* res = new Arr2D_mc(basis, 1);
		Arr2D_mc* res1 = new Arr2D_mc(basis, 1);
		Arr2D_mc* mTransposeM = new Arr2D_mc(basis, basis);
		Arr2D_mc* cholesky = new Arr2D_mc(basis, basis);
		Arr2D_mc* u = new Arr2D_mc(numOfGridPoints, 1);

		for (int j = 0; j < numOfGridPoints; j++)
		{
			u->setEntry_mc(
					*regression->getEntry_mc(0,
							j) /** sqrt(1./numOfGridPoints)*/, j, 0);
			for (int m = 0; m < basis; m++)
			{
				d->setEntry_mc(
						laguerreBasis(grid1[j], grid2[j],
								m) /** sqrt(1./numOfGridPoints)*/, j, m);
			}
		}

		mTransposeM = d->matrixProduct(d->Transpose(), d);
		cholesky = mTransposeM->Cholesky();

		res1 = solveForward(cholesky, d->matrixProduct(d->Transpose(), u));
		res = solveBackward(cholesky->Transpose(), res1);

		//res = matrixProduct(matrixProduct(matrixProduct(d->Transpose(), d)->Inv(), d->Transpose()), u);//u is missing (Y on all trajectories)

		delete d;
		delete res1;
		delete mTransposeM;
		delete cholesky;
		delete u;

		return res;
	}
	else
	{
		gsl_matrix* d = gsl_matrix_alloc(numOfGridPoints, basis);
		gsl_vector* res = gsl_vector_alloc(basis);
		gsl_vector* u = gsl_vector_alloc(numOfGridPoints);
		Arr2D_mc* res1 = new Arr2D_mc(basis, 1);
		for (int j = 0; j < numOfGridPoints; j++)
		{
			gsl_vector_set(u, j,
					*regression->getEntry_mc(0, j)
							* sqrt(1. / numOfGridPoints));   //  * sqrt(1./mc));
			for (int m = 0; m < basis; m++)
			{
				gsl_matrix_set(d, j, m,
						laguerreBasis(grid1[j], grid2[j], m)
								* sqrt(1. / numOfGridPoints)); //  * sqrt(1./mc));//d->setEntry_mc(basisFunction(SDE,time,j,m) * sqrt(1./mc), j, m);
			}
		}

		Arr2D_mc::solveLeastSquare(d, u, res);

		for (int j = 0; j < basis; j++)
		{
			res1->setEntry_mc(gsl_vector_get(res, j), j, 0);
		}

		gsl_matrix_free(d);
		gsl_vector_free(u);
		gsl_vector_free(res);

		return res1;
	}
}
//-----------------------------------------------------------------------------------------------//
double Regression::laguerreBasis(double x, double y, int counter)
{
	double help;
	switch (counter)
	{
		case 0:
			help = 1.;
			break;
		case 1:
			help = x;
			break;
		case 2:
			help = y;
			break;
		case 3:
			help = x * x;
			break;
		case 4:
			help = x * y;
			break;
		case 5:
			help = y * y;
			break;
		case 6:
			help = x * x * x;
			break;
		case 7:
			help = x * x * y;
			break;
		case 8:
			help = x * y * y;
			break;
		case 9:
			help = y * y * y;
			break;
		case 10:
			help = x * x * x * x;
			break;
		case 11:
			help = x * x * x * y;
			break;
		case 12:
			help = x * x * y * y;
			break;
		case 13:
			help = x * y * y * y;
			break;
		case 14:
			help = y * y * y * y;
			break;
		case 15:
			help = x * x * x * x * x;
			break;
		case 16:
			help = x * x * x * x * y;
			break;
		case 17:
			help = x * x * x * y * y;
			break;
		case 18:
			help = x * x * y * y * y;
			break;
		case 19:
			help = x * y * y * y * y;
			break;
		case 20:
			help = y * y * y * y * y;
			break;
		case 21:
			help = x * x * x * x * x * x;
			break;
		case 22:
			help = x * x * x * x * x * y;
			break;
		case 23:
			help = x * x * x * x * y * y;
			break;
		case 24:
			help = x * x * x * y * y * y;
			break;
		case 25:
			help = x * x * y * y * y * y;
			break;
		case 26:
			help = x * y * y * y * y * y;
			break;
		case 27:
			help = y * y * y * y * y * y;
			break;
		case 28:
			help = x * x * x * x * x * x * x;
			break;
		case 29:
			help = x * x * x * x * x * x * y;
			break;
		case 30:
			help = x * x * x * x * x * y * y;
			break;
		case 31:
			help = x * x * x * x * y * y * y;
			break;
		case 32:
			help = x * x * x * y * y * y * y;
			break;
		case 33:
			help = x * x * y * y * y * y * y;
			break;
		case 34:
			help = x * y * y * y * y * y * y;
			break;
		case 35:
			help = y * y * y * y * y * y * y;
			break;
		case 36:
			help = x * x * x * x * x * x * x * x;
			break;
		case 37:
			help = x * x * x * x * x * x * x * y;
			break;
		case 38:
			help = x * x * x * x * x * x * y * y;
			break;
		case 39:
			help = x * x * x * x * x * y * y * y;
			break;
		case 40:
			help = x * x * x * x * y * y * y * y;
			break;
		case 41:
			help = x * x * x * y * y * y * y * y;
			break;
		case 42:
			help = x * x * y * y * y * y * y * y;
			break;
		case 43:
			help = x * y * y * y * y * y * y * y;
			break;
		case 44:
			help = y * y * y * y * y * y * y * y;
			break;
		case 45:
			help = x * x * x * x * x * x * x * x * x;
			break;
		case 46:
			help = x * x * x * x * x * x * x * x * y;
			break;
		case 47:
			help = x * x * x * x * x * x * x * y * y;
			break;
		case 48:
			help = x * x * x * x * x * x * y * y * y;
			break;
		case 49:
			help = x * x * x * x * x * y * y * y * y;
			break;
		case 50:
			help = x * x * x * x * y * y * y * y * y;
			break;
		case 51:
			help = x * x * x * y * y * y * y * y * y;
			break;
		case 52:
			help = x * x * y * y * y * y * y * y * y;
			break;
		case 53:
			help = x * y * y * y * y * y * y * y * y;
			break;
		case 54:
			help = y * y * y * y * y * y * y * y * y;
			break;
		case 55:
			help = x * x * x * x * x * x * x * x * x * x;
			break;
		case 56:
			help = x * x * x * x * x * x * x * x * x * y;
			break;
		case 57:
			help = x * x * x * x * x * x * x * x * y * y;
			break;
		case 58:
			help = x * x * x * x * x * x * x * y * y * y;
			break;
		case 59:
			help = x * x * x * x * x * x * y * y * y * y;
			break;
		case 60:
			help = x * x * x * x * x * y * y * y * y * y;
			break;
		case 61:
			help = x * x * x * x * y * y * y * y * y * y;
			break;
		case 62:
			help = x * x * x * y * y * y * y * y * y * y;
			break;
		case 63:
			help = x * x * y * y * y * y * y * y * y * y;
			break;
		case 64:
			help = x * y * y * y * y * y * y * y * y * y;
			break;
		case 65:
			help = y * y * y * y * y * y * y * y * y * y;
			break;
		case 66:
			help = sin(M_PI * x) * (x - 0.5) * (x - 0.5);
			break;
		case 67:
			help = sin(M_PI * y) * (y - 0.5) * (y - 0.5);
			break;
		case 68:
			help = sin(M_PI * x) * fabs(x - 0.5);
			break;
		case 69:
			help = sin(M_PI * y) * fabs(y - 0.5);
			break;
		case 70:
			help = sin(M_PI * x);
			break;
		case 71:
			help = sin(M_PI * y);
			break;
		case 215:
			help = x * x * x * x * x;
			break;
		case 216:
			help = x * x * x * x * y;
			break;
		case 217:
			help = x * x * x * y * y;
			break;
		case 218:
			help = x * x * y * y * y;
			break;
		case 219:
			help = x * y * y * y * y;
			break;
		case 220:
			help = y * y * y * y * y;
			break;
		case 115:
			help = cos(M_PI * x);
			break;
		case 116:
			help = sin(M_PI * x);
			break;
		case 117:
			help = sin(M_PI * y);
			break;
		case 118:
			help = cos(M_PI * y);
			break;
		case 119:
			help = sin(M_PI * (x + y));
			break;
		case 120:
			help = cos(M_PI * (x + y));
			break;
		case 121:
			help = cos(M_PI * x + M_PI);
			break;
		case 122:
			help = cos(M_PI * y + M_PI);
			break;
		case 123:
			help = sin(M_PI * x + M_PI);
			break;
		case 124:
			help = sin(M_PI * y + M_PI);
			break;
		default:
			std::cerr << counter << "hell\t"
					<< "Laguerre polynomials are only provided up to L_5!\n";
	}

	return help;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Regression::getBasisEuler(int index, int basis,
		std::vector<double> &grid1, std::vector<double> &grid2)
{
	Arr2D_mc* res = new Arr2D_mc(basis, 1);

	for (int m = 0; m < basis; m++)
	{
		res->setEntry_mc(laguerreBasis(grid1[index], grid2[index], m), m, 0);
	}

	return res;
}

//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Regression::solveForward(Arr2D_mc* matrix, Arr2D_mc* vector)
{        ////////////////////////
	int d = matrix->getDimRow();
	Arr2D_mc* res = new Arr2D_mc(d, 1);
	for (int i = 0; i < d; i++)
	{
		long double dummy = 0.;

		for (int j = 0; j < i; j++)
		{
			dummy += *matrix->getEntry_mc(i, j) * *res->getEntry_mc(j, 0)
					/ *matrix->getEntry_mc(i, i);
		}

		res->setEntry_mc(
				(*vector->getEntry_mc(i, 0) / *matrix->getEntry_mc(i, i) - dummy),
				i, 0);
	}

	return res;
}
//-----------------------------------------------------------------------------------------------//
Arr2D_mc* Regression::solveBackward(Arr2D_mc* matrix, Arr2D_mc* vector)
{
	int d = matrix->getDimRow();
	Arr2D_mc* res = new Arr2D_mc(d, 1);
	for (int i = d - 1; i >= 0; i--)
	{
		long double dummy = 0.;

		for (int j = d - 1; j > i; j--)
		{
			dummy += *matrix->getEntry_mc(i, j) * *res->getEntry_mc(j, 0)
					/ *matrix->getEntry_mc(i, i);
		}

		res->setEntry_mc(
				(*vector->getEntry_mc(i, 0) / *matrix->getEntry_mc(i, i) - dummy),
				i, 0);
	}
	return res;
}
