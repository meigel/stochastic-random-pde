#include "FourierModes.h"

FourierModes::FourierModes()
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

	// the Mersenne Twister from C++ standard library
	std::mt19937 MTS(seed);

//	this->gamma = 0.999;
//	this->sigma = 1.1;
//
//	this->A = 0.0009;
//
//	this->M = 100;
//	this->skipM = 1000;

	this->gamma = 0.9;
	this->sigma = 2.;

	this->A = 0.6;

	this->M = 5;
	this->skipM = 0;

	this->coeff.resize(M);

	this->alpha.resize(M);
	this->alpha_sum = 0.;

	for (int m = skipM + 1; m <= M + skipM; m++)
	{
		alpha[m - skipM - 1] = A * std::pow(m, -sigma);
		alpha_sum += alpha[m - skipM - 1];
	}

	fixedCoeff = false;

	// left interval border
	p1 = 0.;
	// right interval border
	p2 = 1.;
	
	idist = 0; // uniform
	
	// getting coefficients for stochatic basis (uniform)
	std::uniform_real_distribution<double> dist(p1, p2);

	for (int i = 0; i < M; i++)
		this->coeff[i] = dist(MTS);
}

FourierModes::FourierModes(double ui_gamma, double ui_A, double ui_sigma, int ui_M,
		int ui_skipM, double ui_p1 /* = 0. */, double ui_p2 /* = 1. */,
		int ui_idist /* = 0*/) :
		gamma(ui_gamma), A(ui_A), sigma(ui_sigma), M(ui_M), skipM(ui_skipM)
{
	// this->A = 0.0009;
	this->A = 0.6;

	this->coeff.resize(M);

	this->alpha.resize(M);
	this->alpha_sum = 0.;

	for (int m = this->skipM + 1; m <= M + skipM; m++)
	{
		this->alpha[m - skipM - 1] = A * std::pow(m, -sigma);
		this->alpha_sum += alpha[m - skipM - 1];
	}

	this->fixedCoeff = false;

	idist = ui_idist;
	p1 = ui_p1;
	p2 = ui_p2;
	
	newCoefficients();
}

StochasticField* FourierModes::create() const
{
	StochasticField* ret = new FourierModes();

	return ret;
}

StochasticField* FourierModes::clone() const
{
	// use copy-ctor
	StochasticField* ret = new FourierModes(*this);

	return ret;
}

/**
 * @note Maybe we have to do an own implementation of the Riemann zeta function here, if Python can't handle the boost-library.
 * @todo Gradient calculation should be done automatically for higher dimensions.
 */
std::vector<double> FourierModes::eval(const std::vector<double>& x)
{
	unsigned prob_dim = x.size();

	// return value
	std::vector<double> a((prob_dim + 1) * this->M, 2.);

//	double riemann_zeta = boost::math::zeta(this->sigma);
//	double riemann_zeta = gsl_sf_zeta(sigma);

	// additional functions
	double k, *beta, *cos;

	// keep the pow() and cos() evaluations, maybe it safes some time
	beta = new double[prob_dim];
	cos = new double[prob_dim];

	int index = 0;
	double c = 1;
	double eps = 0.0005;

//	double a_bar = 0.;

	for (int m = this->skipM + 1; m <= M + skipM; m++)
	{
		k = std::floor(-0.5 + std::sqrt(0.25 + 2 * m));

		beta[0] = m - k * (k + 1) / 2.;
		beta[1] = k - beta[0];

		for (unsigned i = 0; i < prob_dim; ++i)
		{
			cos[i] = std::cos(2 * M_PI * beta[i] * x[i]);
		}

		a[index] = this->alpha[m - skipM - 1] * cos[0] * cos[1];
//		a_bar += fabs(a[index])/gamma;
		index++;

		// here comes the gradient
		for (unsigned i = 0; i < prob_dim; ++i)
		{
			a[index] = -2 * M_PI * this->alpha[m - skipM - 1] * beta[i]
					* std::sin(2 * M_PI * beta[i] * x[i])
					* cos[prob_dim - 1 - i];
			index++;
		}
	}

	delete[] beta;
	delete[] cos;

	// returned vector (gradient has size prob_dim, kappa is just one value)
	std::vector<double> kappa_x(prob_dim + 1, 0.);

	for (int m = 1; m <= M; m++)
	{
		kappa_x[0] += coeff[m - 1] * a[(prob_dim + 1) * (m - 1)];

		for (unsigned int i = 1; i <= prob_dim; i++)
			kappa_x[i] += coeff[m - 1] * a[(prob_dim + 1) * (m - 1) + i];
	}

	kappa_x[0] *= c/this->alpha_sum;
	kappa_x[0] += eps + c;

	// gradients
	for (unsigned int i = 1; i <= prob_dim; i++)
		kappa_x[i] *= c/this->alpha_sum;

	return kappa_x;
}

void FourierModes::setCoefficients(const std::vector<double>& coeff)
{
	this->coeff = coeff;
}

void FourierModes::setCoefficientsFixed(const std::vector<double>& coeff)
{
	this->coeff = coeff;
	this->fixedCoeff = true;
}

void FourierModes::newCoefficients()
{
	if (!fixedCoeff)
	{
		unsigned seed =
				std::chrono::system_clock::now().time_since_epoch().count();

		// the Mersenne Twister from C++ standard library
		std::mt19937 MTS(seed);

		switch(idist)
		{
			case 0: // uniform (real)
			{
				// getting coefficients for stochatic basis (uniform)
				using dist_t = std::uniform_real_distribution<double>;
				using param_t = dist_t::param_type;
				
				param_t p{p1, p2};
				
				dist_t dist;
				dist.param(p);

				for (int i = 0; i < M; i++)
				{
					this->coeff[i] = dist(MTS);
				}
				
				break;
			}
			case 1: // normal
			{
				// getting coefficients for stochatic basis (normal)
				using dist_t = std::normal_distribution<double>;
				using param_t = dist_t::param_type;
				
				param_t p{p1, p2};
				
				dist_t dist;
				dist.param(p);

				for (int i = 0; i < M; i++)
				{
					this->coeff[i] = dist(MTS);
				}
				
				break;
			}
			case 2: // lognormal
			{
				// getting coefficients for stochatic basis (lognormal)
				using dist_t = std::lognormal_distribution<double>;
				using param_t = dist_t::param_type;
				
				param_t p{p1, p2};
				
				dist_t dist;
				dist.param(p);

				for (int i = 0; i < M; i++)
				{
					this->coeff[i] = dist(MTS);
				}
				
				break;
			}
			default:
			{
				// set everything to 0.
				for (int i = 0; i < M; i++)
				{
					this->coeff[i] = 0.;
				}
				
				break;
			}
		}
	}
}

void FourierModes::setDistribution(int idist)
{
	this->idist = idist;
}

void FourierModes::setDistParams(double p1, double p2)
{
	this->p1 = p1;
	this->p2 = p2;
}

void FourierModes::setDecayRatio(double A)
{
	this->A = A;
}
