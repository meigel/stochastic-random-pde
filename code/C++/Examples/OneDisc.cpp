/*
 * OneDisc.cpp
 *
 *  Created on: May 24, 2016
 *      Author: anker
 */

#include "OneDisc.h"

OneDisc::OneDisc()
{
	this->coeff.resize(1);

	this->mean = 0.;
	this->sd = 1.;

	this->outerValue = 1e-3;

	unsigned seed =
			std::chrono::system_clock::now().time_since_epoch().count();

	// the Mersenne Twister from C++ standard library
	std::mt19937 MTS(seed);

	// getting coefficients for stochatic field (lognormal)
	std::lognormal_distribution<double> dist(mean, sd);

	this->coeff[0] = dist(MTS);

	this->radius = 0.1;

	// middle point of unit square
	this->position.push_back(0.2);
	this->position.push_back(0.2);
}

OneDisc::OneDisc(double ui_radius, std::vector<double> &ui_position, double ui_outerValue,
				 double ui_mean, double ui_sd) :
		radius(ui_radius), position(ui_position), outerValue(ui_outerValue),
		mean(ui_mean), sd(ui_sd)
{
	this->coeff.resize(1);

	unsigned seed =
			std::chrono::system_clock::now().time_since_epoch().count();

	// the Mersenne Twister from C++ standard library
	std::mt19937 MTS(seed);

	// getting coefficients for stochatic field (lognormal)
	std::lognormal_distribution<double> dist(mean, sd);

	this->coeff[0] = dist(MTS);
}

StochasticField* OneDisc::create() const
{
	StochasticField* ret = new OneDisc();

	return ret;
}

StochasticField* OneDisc::clone() const
{
	// use copy-ctor
	StochasticField* ret = new OneDisc(*this);

	return ret;
}

/**
 * @todo Gradient calculation should be done automatically for higher dimensions.
 */
std::vector<double> OneDisc::eval(const std::vector<double>& x)
{
	unsigned prob_dim = x.size();

	// returned vector (gradient has size prob_dim, kappa is just one value)
	std::vector<double> kappa_x(prob_dim + 1, 0.);

	// check if x is in the disc
	double x_distance = ( x[0] - position[0] ) * ( x[0] - position[0] );
	double y_distance = ( x[1] - position[1] ) * ( x[1] - position[1] );

	kappa_x[0] = outerValue * x[0] * x[1] + outerValue / 100.;

	if(radius * radius > x_distance + y_distance)
	{
		kappa_x[0] += coeff[0];
	}

	kappa_x[1] = outerValue * x[1];
	kappa_x[2] = outerValue * x[0];

	return kappa_x;
}

void OneDisc::setCoefficients(const std::vector<double>& coeff)
{
	this->coeff = coeff;
}

void OneDisc::setCoefficientsFixed(const std::vector<double>& coeff)
{
	this->coeff = coeff;
	this->fixedCoeff = true;
}

void OneDisc::newCoefficients()
{
	if (!fixedCoeff)
	{
		unsigned seed =
				std::chrono::system_clock::now().time_since_epoch().count();

		// the Mersenne Twister from C++ standard library
		std::mt19937 MTS(seed);

		// getting coefficients for stochatic field (lognormal)
		std::lognormal_distribution<double> dist(mean, sd);

		this->coeff[0] = dist(MTS);
	}
}

void OneDisc::setDistribution(int idist)
{
	this->idist = idist;
}

void OneDisc::setDistParams(double p1, double p2)
{
	this->p1 = p1;
	this->p2 = p2;
}
