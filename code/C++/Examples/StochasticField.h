#ifndef STOCHASTICFIELD_H_
#define STOCHASTICFIELD_H_

#include <memory>
#include <vector>
#include <random>

/**
 * This abstract class implements the base for all stochastic fields.
 *
 * Functions that need to be implemented in the derived classes are:
 *
 * @eval(const std::vector<double>& x)
 * @setCoefficients(const std::vector<double>& coeff)
 * @setCoefficientsFixed(const std::vector<double>& coeff)
 * @newCoefficients()
 */

class StochasticField
{
	public:

		/**
		 * Pure virtual default constructor.
		 * Override this in derived classes and use the override keyword.
		 */
		virtual StochasticField* create() const = 0;

		/**
		 * Pure virtual copy constructor.
		 * Override this in derived classes and use the override keyword.
		 */
		virtual StochasticField* clone() const = 0;

		/**
		 * @brief evaluate stochastic field
		 * This function will evaluate the stochastic field and its derivatives described by
		 * the stochastic basis and the coefficients at point \f$ x \f$.
		 *
		 * @param x evaluation point
		 * @return @return vector containing \f$ \left[ \kappa(x) , \frac{\partial \kappa}{\partial x_1}(x), \frac{\partial \kappa}{\partial x_2}(x), \ldots, \frac{\partial \kappa}{\partial x_n}(x) \right] \f$
		 */
		virtual std::vector<double> eval(const std::vector<double>& x) = 0;

		/**
		 * Returns the used coefficients for the stochastic basis.
		 * The size is @a M.
		 *
		 * @return coefficients of stochastic basis.
		 */
		std::vector<double> getCoefficients() const
		{
			return this->coeff;
		}

		/**
		 * Set the coefficients of the stochastic basis.
		 *
		 * @param coeff coefficients
		 */
		virtual void setCoefficients(const std::vector<double>& coeff) = 0;

		/**
		 * Set the coefficients of the stochastic basis from Python.
		 * After this function call, the coefficients are fixed and can not be
		 * altered with @newCoefficients().
		 *
		 * @param coeff coefficients
		 */
		virtual void setCoefficientsFixed(const std::vector<double>& coeff) = 0;

		/**
		 * Generates new random coefficients, if @fixedCoeff == false.
		 */
		virtual void newCoefficients() = 0;

		/**
		 * Set the distribution of the coefficients.
		 */
		virtual void setDistribution(int idist) = 0;
		
		/**
		 * Set the distribution parameters.
		 * @param p1 first parameter ( e.g. mean )
		 * @param p2 second parameter ( e.g. standard deviation)
		 */
		virtual void setDistParams(double p1, double p2) = 0;
		
		/**
		 * Virtual destructor to release
		 */
		virtual ~StochasticField() {};

	protected:

		/// Are there coefficients set fix?
		bool fixedCoeff;
		
		/// parameters for the distributions
		/// for uniform: p1 and p2 are the interval boundaries
		/// for (log)normal: p1 is the mean, p2 is the standard deviation
		/// default values are always p1 = 0. , p2 = 1.
		double p1, p2;
		
		/// simple way to define the
		/// distribution of the coefficients
		/// idist = 0  <=>  uniform (real)
		/// idist = 1  <=>  normal
		/// idist = 2  <=>  lognormal
		int idist;
		
		/// coefficients of the stochastic basis
		std::vector<double> coeff;
};

#endif /* STOCHASTICBASE_H_ */
