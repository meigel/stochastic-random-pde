#ifndef ONEDISC_H_
#define ONEDISC_H_

#include <chrono>
#include <random>
#include <memory>
#include <Python.h>
#include <vector>
#include <math.h>

#include "StochasticField.h"

class OneDisc : public StochasticField
{
	private:

		/// radius of the disc
		double radius;

		/// position of the disc
		std::vector<double> position;

		/// constant value, if out of disc
		double outerValue;

		/// mean and standard deviation for lognormal distribution
		double mean, sd;

	public:

		/**
		 * Default constructor.
		 *
		 * Sets
		 * @a radius = 0.1
		 * @a position = ( 0.2, 0.2 )
		 * @a outerValue = 1e-3
		 * @a mean = 0.
		 * @a sd = 1.
		 */
		OneDisc();

		/**
		 * Constructor to set member variables.
		 *
		 * @param ui_radius radius of the disc
		 * @param ui_position position of the disc
		 * @param ui_outerValue value to return, if x lays not in the disc
		 * @param ui_mean mean of the lognormal distribution
		 * @param ui_sd standard deviation of the lognormal distribution
		 */
		OneDisc(double ui_radius, std::vector<double> &ui_position, double ui_outerValue,
				double ui_mean, double ui_sd);

		/**
		 * default destructor.
		 */
		~OneDisc() = default;

		/// Implements the virtual default constructor from the base class StochasticField.
		StochasticField* create() const override;

		/// Implements the virtual copy constructor from the base class StochasticField.
		StochasticField* clone() const override;

		/**
		 * @brief evaluate stochastic field
		 * This function will evaluate the stochastic field and its derivatives described by
		 * the stochastic basis and the coefficients at point \f$ x \f$.
		 *
		 * @param x evaluation point
		 * @return @return vector containing \f$ \left[ \kappa(x) , \frac{\partial \kappa}{\partial x_1}(x), \frac{\partial \kappa}{\partial x_2}(x), \ldots, \frac{\partial \kappa}{\partial x_n}(x) \right] \f$
		 */
		std::vector<double> eval(const std::vector<double>& x) override;

		/**
		 * Set the coefficients of the stochastic basis.
		 *
		 * @param coeff coefficients
		 */
		void setCoefficients(const std::vector<double>& coeff) override;

		/**
		 * Set the coefficients of the stochastic basis from Python.
		 * After this function call, the coefficients are fixed and can not be
		 * altered with @newCoefficients().
		 *
		 * @param coeff coefficients
		 */
		void setCoefficientsFixed(const std::vector<double>& coeff) override;

		/**
		 * Generates new random coefficients, if @fixedCoeff == false.
		 */
		void newCoefficients() override;
		
				/**
		 * Set the distribution of the coefficients.
		 * 
		 * @param idist distribution of the coefficients
		 * idist = 0  <=>  uniform (real)
		 * idist = 1  <=>  normal
		 * idist = 2  <=>  lognormal
		 */
		void setDistribution(int idist) override;
		
		/**
		 * Set the distribution parameters.
		 * @param p1 first parameter ( e.g. mean )
		 * @param p2 second parameter ( e.g. standard deviation)
		 */
		void setDistParams(double p1, double p2) override;
};

#endif /* ONEDISC_H_ */
