#ifndef FOURIERMODES_H_
#define FOURIERMODES_H_

#include <chrono>
#include <random>
#include <memory>
#include <Python.h>
#include <math.h>

#include "StochasticField.h"

// this implements the zeta-function used for the coefficients
//#include <gsl/gsl_sf_zeta.h>

class FourierModes : public StochasticField
{
	private:
		/*
		 * gamma and sigma should be chosen according to Martins paper.
		 */
		double gamma; // = 0.9;

		/// factor for decay
		double A; // = 0.6;

		/// decay rate
		double sigma; // = 2.; or = 4.;

		/// this is the number of functions in the sequence approximation of the stochastic field
		int M; // = 5;

		/// how much modes should be skipped
		int skipM; // = 0;

		/// basis functions, size is @M
		std::vector<double> alpha;

		/// sum of alphas
		double alpha_sum;

	public:

		/**
		 * Default constructor.
		 *
		 * Sets
		 * @a gamma = 0.9
		 * @a A = 0.6
		 * @a sigma = 2
		 * @a M = 5
		 * @a skipM = 0
		 * @a fixedCoeff = false
		 * @a p1 = 0.
		 * @a p2 = 1.
		 * @a idist = 0, uniform (real)
		 */
		FourierModes();

		/**
		 * Constructor to set member variables.
		 *
		 * @param ui_gamma scaling factor between Riesz isomorphism and Riemann zeta function
		 * @param ui_A decay ratio
		 * @param ui_sigma decay rate
		 * @param ui_M number of basis functions
		 * @param ui_skipM number of skipped basis functions
		 * @param ui_p1,ui_p2 parameters of the distribution, default 0. and 1., resp.
		 * @param ui_idist distribution for coefficients, default is 0, uniform (real)
		 * 
		 */
		FourierModes(double ui_gamma, double ui_A, double ui_sigma, int ui_M,
				int ui_skipM, double ui_p1 = 0., double ui_p2 = 1., int ui_idist = 0);

		/**
		 * default destructor frees FourierModes::alpha .
		 */
		~FourierModes() = default;

		/// Implements the virtual default constructor from the base class StochasticField.
		StochasticField* create() const override;

		/// Implements the virtual copy constructor from the base class StochasticField.
		StochasticField* clone() const override;

		/**
		 * @brief evaluate stochastic field
		 * This function will evaluate the stochastic field and its derivatives described by
		 * the stochastic basis and the coefficients at point \f$ x \f$.
		 *
		 * @param x evaluation point
		 * @return @return vector containing \f$ \left[ \kappa(x) , \frac{\partial \kappa}{\partial x_1}(x), \frac{\partial \kappa}{\partial x_2}(x), \ldots, \frac{\partial \kappa}{\partial x_n}(x) \right] \f$
		 */
		std::vector<double> eval(const std::vector<double>& x) override;

		/**
		 * Set the coefficients of the stochastic basis.
		 *
		 * @param coeff coefficients
		 */
		void setCoefficients(const std::vector<double>& coeff) override;

		/**
		 * Set the coefficients of the stochastic basis from Python.
		 * After this function call, the coefficients are fixed and can not be
		 * altered with @newCoefficients().
		 *
		 * @param coeff coefficients
		 */
		void setCoefficientsFixed(const std::vector<double>& coeff) override;

		/**
		 * Generates new random coefficients, if @fixedCoeff == false.
		 */
		void newCoefficients() override;
		
		/**
		 * Set the distribution of the coefficients.
		 * 
		 * @param idist distribution of the coefficients
		 * idist = 0  <=>  uniform (real)
		 * idist = 1  <=>  normal
		 * idist = 2  <=>  lognormal
		 */
		void setDistribution(int idist) override;
		
		/**
		 * Set the distribution parameters.
		 * @param p1 first parameter ( e.g. mean )
		 * @param p2 second parameter ( e.g. standard deviation)
		 */
		void setDistParams(double p1, double p2) override;

		/**
		 * Sets decay ratio StochasticBasis::A to a new value.
		 * Default is 0.6.
		 *
		 * @param A decay ratio of Fourier Modes
		 */
		void setDecayRatio(double A);
};

#endif /* FOURIERMODES_H_ */
