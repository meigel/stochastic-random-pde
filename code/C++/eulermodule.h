
#define CAT_I(a,b) a##b
#define CAT(a,b) CAT_I(a, b)

#define xstr(s) str(s)
#define str(s) #s

#define INIT_MODULE(filename) \
PyMODINIT_FUNC CAT(init, filename)(void)\
{\
    (void) Py_InitModule(xstr(filename), EulerMethods);\
}

extern "C"
{
  #include <Python.h>
}

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "StochasticField.h"
#include "Euler.h"
#include "Regression.h"
#include <iostream> // For debug

/**
 * Using the same stochastic basis for each code part.
 * A static variable might not be the best way to do so...
 */
static StochasticField* sf;

double f(std::vector<double> &x);
double uD(std::vector<double> &x);
//double uN(std::vector<double> &x);

static void setSF();

extern "C"
{
/**
 * Work in progress.
 * @param self
 * @param args
 * @return
 */
static PyObject* euler_regressionC(PyObject *self, PyObject *args)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// degree of the Laguerre polynomial basis
	int degree;

	// number of points in the grid
	int numGridPoints = 1;

	// grid from Python
	PyObject* ui_grid;

	// dimension of the grid

	/*
	 * Parse the arguments from Python. This has to be done in the same order as they are given
	 * in the Python call. The string gives the expected type of the variables.
	 * "O" is for PythonObject
	 * "i" is for int
	 * This function returns false, if any of these types doesn't match their corresponding variable.
	 */
	if (!PyArg_ParseTuple(args, "Oii", &ui_grid, &numGridPoints, &degree))
		return NULL;

	// assign sequence arguments
	ui_grid = PySequence_Fast(ui_grid, "argument must be iterable");

	if (!ui_grid)
		return NULL;

	/*
	 * grid and dimGrid need special treatment
	 * convert them to std::vector
	 */
	// length of the sequences
	int ui_grid_len = PySequence_Fast_GET_SIZE(ui_grid);

	// dimension of domain D
	int prob_dim = ui_grid_len/numGridPoints;

	// starting points in the grid
	std::vector<double> grid(ui_grid_len);

	/*
	 * In this loop we convert the sequences from Python to standard C types.
	 * This has to be done with each element of each sequence.
	 */
	for (int i = 0; i < ui_grid_len; ++i)
	{
		PyObject *fitem;

		// get i-th item
		PyObject *item = PySequence_Fast_GET_ITEM(ui_grid, i);
		if (!item)
		{
			Py_DECREF(ui_grid);
			return NULL;
		}

		fitem = PyNumber_Float(item);

		if (!fitem)
		{
			Py_DECREF(ui_grid);
			PyErr_SetString(PyExc_TypeError, "all grid items must be numbers");
			fprintf(stderr, "item %d of %d\n", i, ui_grid_len);
			return NULL;
		}

		grid[i] = PyFloat_AS_DOUBLE(fitem);
		Py_DECREF(fitem);
	}

	// output numpy array and its dimensions
	PyArrayObject *vecout;

	int numberOfBasisFunctions = degree*(degree+3)/2;

	double* gridhelp = new double[2];
	std::vector<double> grid1(numGridPoints);
	std::vector<double> grid2(numGridPoints);

	Arr2D_mc* reg = new Arr2D_mc(numberOfBasisFunctions, 1); //std::vector<double> regEuler(numOfGridPoints);
	Arr2D_mc* reg1 = new Arr2D_mc(72, 1);

	for (int i = 0; i < 72; ++i)
	{
		reg1->setEntry_mc(0, i, 0);
	}

	Regression* func = new Regression();
	func->regEuler = new Arr2D_mc(1, numGridPoints);

	//
	for (int i = 0; i < numGridPoints; ++i)
	{
		gridhelp[0] = grid[(prob_dim + 1) * i];
		gridhelp[1] = grid[(prob_dim + 1) * i+ 1];
		func->regEuler->setEntry_mc(grid[(prob_dim + 1) * i + 2], 0, i);
		grid1[i] = gridhelp[0];
		grid2[i] = gridhelp[1];
	}

	reg = func->RegressionEuler(func->regEuler, numberOfBasisFunctions,
			numGridPoints, grid1, grid2);

	// convert to numpy-array
	for (int i = 0; i < numberOfBasisFunctions; i++)
	{
		reg1->setEntry_mc(*reg->getEntry_mc(i, 0), i, 0);
	}

	int dimshelp[2] = {1, 72};

	vecout = (PyArrayObject*) PyArray_FromDims(2, dimshelp, NPY_DOUBLE);

	double *arr = (double *) PyArray_DATA(vecout);

	for (int i = 0; i < 72; ++i)
	{
		arr[i] = (double) *reg1->getEntry_mc(i, 0);
	}

	Py_DECREF(ui_grid);

	// return numpy-array
	return PyArray_Return(vecout);
}

static PyObject* euler_setCoefficientsC(PyObject *self, PyObject *args)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// coefficients set from Python
	PyObject* ui_coeff;

	/*
	 * Parse the arguments from Python. This has to be done in the same order as they are given
	 * in the Python call. The string gives the expected type of the variables.
	 * "O" is for PythonObject
	 * This function returns false, if any of these types doesn't match their corresponding variable.
	 */
	if (!PyArg_ParseTuple(args, "O", &ui_coeff))
		return NULL;

	// assign sequence arguments
	ui_coeff = PySequence_Fast(ui_coeff, "argument must be iterable");
	if (!ui_coeff)
		return NULL;

	// number of coefficients
	int ui_coeff_len = PySequence_Fast_GET_SIZE(ui_coeff);

	std::vector<double> coeff(ui_coeff_len);

	/*
	 * In these two loops we convert the sequences from Python to standard C types.
	 * This has to be done with each element of each sequence.
	 */
	for (int i = 0; i < ui_coeff_len; ++i)
	{
		PyObject *fitem;

		// get i-th item
		PyObject *item = PySequence_Fast_GET_ITEM(ui_coeff, i);
		if (!item)
		{
			Py_DECREF(ui_coeff);
			return NULL;
		}

		fitem = PyNumber_Float(item);

		if (!fitem)
		{
			Py_DECREF(ui_coeff);
			PyErr_SetString(PyExc_TypeError,
					"all coefficients must be numbers");
			return NULL;
		}

		coeff[i] = PyFloat_AS_DOUBLE(fitem);
		Py_DECREF(fitem);
	}

	// These three should be passed from Python.
	//	double gamma = 0.999;
	//	double sigma = 1.1;
	//	int skipBasis = 1000;
	//	int dimBasis = 5;

	if (sf == NULL)
	{
		setSF();
	}

	sf->setCoefficientsFixed(coeff);

	return ui_coeff;
}

static PyObject* euler_getCoefficientsC(PyObject *self, PyObject *args)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// These four should be passed from Python.
	//	double gamma = 0.9;
	//	double sigma = 2.;
	//	int dimBasis = 5;
	//	int skipBasis = 0;


	if (sf == NULL)
	{
		setSF();
	}

	// output numpy array and its dimensions
	PyArrayObject *vecout;

	// this is the output array from the evaluation code
	std::vector<double> coeff = sf->getCoefficients();

	int dims[] = {(int)coeff.size()};

	// convert to numpy-array
	vecout = (PyArrayObject*) PyArray_FromDims(1, dims, NPY_DOUBLE);

	double *arr = (double *) PyArray_DATA(vecout);

	for (int i = 0; i < dims[0]; ++i)
	{
		arr[i] = coeff[i];
	}

	// return numpy-array
	return PyArray_Return(vecout);
}

static PyObject* euler_getIncrementsC(PyObject *self, PyObject *args)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// maximum number of threads is 52, due to memory limitation
	int numThreads;

	// number of trajectories
	unsigned long numTraj;

	unsigned int lastTraj = 1;

	// well... the step size
	double stepsize;

	// adaptive step size or equidistant?
	int ui_adapt;

	// give intermediate points?
	double intermediate;

	// number of points in the grid
	int numGridPoints = 1;

	// the grid is a sequence ...
	PyObject* ui_grid;
	// ... so is the dimension information of the grid
//	PyObject* ui_dimGrid;

	// the seed value
	double d_seed;

	// set as UINT_MAX initially
	unsigned int seed = std::numeric_limits<unsigned int>::max();

	/*
	 * Parse the arguments from Python. This has to be done in the same order as they are given
	 * in the Python call. The string gives the expected type of the variables.
	 * "i" is for int
	 * "I" is for unsigned int
	 * "k" is for unsigned long
	 * "d" is for double
	 * "O" is for PythonObject
	 * This function returns false, if any of these types doesn't match their corresponding variable.
	 */

	if (!PyArg_ParseTuple(args, "idkdidOi", &numThreads, &d_seed, &numTraj,
			&stepsize, &ui_adapt, &intermediate, &ui_grid, &numGridPoints))
		return NULL;

	// assign sequence arguments
	ui_grid = PySequence_Fast(ui_grid, "argument must be iterable");
	if (!ui_grid)
		return NULL;

	/*
	 * grid and dimGrid need special treatment
	 * convert them to std::vector
	 */
	// length of the sequences
	int ui_grid_len = PySequence_Fast_GET_SIZE(ui_grid);

	// dimension of domain D
	int prob_dim = ui_grid_len/numGridPoints;

	// starting points in the grid
	std::vector<double> grid(ui_grid_len);

	// dimension of the grid
	std::vector<int> grid_dim(prob_dim);

	/*
	 * In this loop we convert the sequences from Python to standard C types.
	 * This has to be done with each element of each sequence.
	 */
	for (int i = 0; i < ui_grid_len; ++i)
	{
		PyObject *fitem;

		// get i-th item
		PyObject *item = PySequence_Fast_GET_ITEM(ui_grid, i);
		if (!item)
		{
			Py_DECREF(ui_grid);
			return NULL;
		}

		fitem = PyNumber_Float(item);

		if (!fitem)
		{
			Py_DECREF(ui_grid);
			PyErr_SetString(PyExc_TypeError, "all grid items must be numbers");
			fprintf(stderr, "item %d of %d\n", i, ui_grid_len);
			return NULL;
		}

		grid[i] = PyFloat_AS_DOUBLE(fitem);
		Py_DECREF(fitem);
	}

	// determine whether we use the single point or the grid version of the code
	bool singlepoint = (numGridPoints == 1);

	/*
	 * Check for invalid user input. OpenMP should check this automatically, but who knows...
	 */
	if (numThreads <= 0)
		numThreads = 1;
	/*
	 * Don't use more threads then there are problems.
	 * This would result in copy overhead.
	 */
	if (singlepoint)
	{
		// we parallelize over trajectories
		if ((unsigned long) numThreads > numTraj)
			numThreads = (int) numTraj;
	}
	else
	{
		// we parallelize over grid points
		if (numThreads > numGridPoints)
			numThreads = numGridPoints;
	}

	if (std::isfinite(d_seed))
		seed = (unsigned int) d_seed;

	// These four should be passed from Python.
	//	double gamma = 0.999;
	//	double sigma = 1.1;
	//  int dimBasis = 20;
	//	int skipBasis = 1000;

	if (sf == nullptr)
	{
		setSF();
	}

	// Creating an object of the Euler-Maruyama solver.
	EulerScheme stochEuler(numTraj, lastTraj, numThreads, sf, false, "");

	// set the functions for our problem
	stochEuler.setFunctions(f, uD);

	// output numpy array and its dimensions
	PyArrayObject *vecout;
	int dims[2] = {1, prob_dim};

	// this is the output array from the Euler-Maruyama code
	double* incs;

	incs = stochEuler.getIncrements(prob_dim, stepsize, seed);

	unsigned long length = 2000000;
	dims[0] = length / prob_dim;

	// convert to numpy-array
	vecout = (PyArrayObject*) PyArray_FromDims(2, dims, NPY_DOUBLE);

	double *arr = (double *) PyArray_DATA(vecout);

	for (int i = 0; i < dims[0] * dims[1]; ++i)
	{
		arr[i] = incs[i];
	}

	Py_DECREF(ui_grid);

	// return numpy-array
	return PyArray_Return(vecout);
}

/**
 * This method evaluates the stochastic field at given points.
 * If a StochasticBasis was already created by a call to \a stochEulerC,
 * it will be used here, too.
 *
 * It is a standard Python interface to call C/C++ functions.
 * The args parameter collects everything which is passed to the Python call.
 *
 * @param self a pointer to the object itself
 * @param args collection of arguments from Python
 */
static PyObject* euler_evalKappaC(PyObject *self, PyObject *args)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// number of points in the grid
	int numGridPoints = 1;

	// the grid is a sequence ...
	PyObject* ui_grid;
	// ... so is the dimension information of the grid
//	PyObject* ui_dimGrid;

	/*
	 * Parse the arguments from Python. This has to be done in the same order as they are given
	 * in the Python call. The string gives the expected type of the variables.
	 * "O" is for PythonObject
	 * This function returns false, if any of these types doesn't match their corresponding variable.
	 */
	if (!PyArg_ParseTuple(args, "Oi", &ui_grid, &numGridPoints))
		return NULL;

	// assign sequence arguments
	ui_grid = PySequence_Fast(ui_grid, "argument must be iterable");
	if (!ui_grid)
		return NULL;

	/*
	 * grid and dimGrid need special treatment
	 * convert them to std::vector
	 */
	// length of the sequences
	int ui_grid_len = PySequence_Fast_GET_SIZE(ui_grid);

	// dimension of domain D
	int prob_dim = ui_grid_len/numGridPoints;

	// starting points in the grid
	std::vector<double> grid(ui_grid_len);

	// dimension of the grid
	std::vector<int> grid_dim(prob_dim);

	/*
	 * In this loop we convert the sequences from Python to standard C types.
	 * This has to be done with each element of each sequence.
	 */
	for (int i = 0; i < ui_grid_len; ++i)
	{
		PyObject *fitem;

		// get i-th item
		PyObject *item = PySequence_Fast_GET_ITEM(ui_grid, i);
		if (!item)
		{
			Py_DECREF(ui_grid);
			return NULL;
		}

		fitem = PyNumber_Float(item);

		if (!fitem)
		{
			Py_DECREF(ui_grid);
			PyErr_SetString(PyExc_TypeError, "all grid items must be numbers");
			fprintf(stderr, "item %d of %d\n", i, ui_grid_len);
			return NULL;
		}

		grid[i] = PyFloat_AS_DOUBLE(fitem);
		Py_DECREF(fitem);
	}

	// These four should be passed from Python.
	//	double gamma = 0.999;
	//	double sigma = 1.1;
	//  int dimBasis = 20;
	//	int skipBasis = 1000;

	if (sf == NULL)
	{
		setSF();
	}

	// output numpy array and its dimensions
	PyArrayObject *vecout;
	int dims[] = {numGridPoints, prob_dim + 1};

	// this is the output array from the evaluation code
	std::vector<double> kappa_x(numGridPoints * (prob_dim + 1));

	for (int i = 0; i < numGridPoints; ++i)
	{
		std::vector<double> x(prob_dim);

		for (int j = 0; j < prob_dim; ++j)
		{
			x[j] = grid[i * prob_dim + j];

			// 0, 1, 3, 4, 6, 7, ... = 0, 1, 2, 3, 4, 5, ...
			kappa_x[i * (prob_dim + 1) + j] = grid[i * prob_dim + j];
		}

		std::vector<double> ev = sf->eval(x);

		// 2, 5, 8, 11, 14, ...
		kappa_x[i * (prob_dim + 1) + prob_dim] = ev[0];
	}

	// convert to numpy-array
	vecout = (PyArrayObject*) PyArray_FromDims(2, dims, NPY_DOUBLE);

	double *arr = (double *) PyArray_DATA(vecout);

	for (int i = 0; i < dims[0] * dims[1]; ++i)
	{
		arr[i] = kappa_x[i];
	}

	Py_DECREF(ui_grid);

	// return numpy-array
	return PyArray_Return(vecout);
}

/**
 * This method calls the Euler-Maruyama scheme from Python.
 * If a StochasticBasis was already created by a call to \a evalKappaC,
 * it will be used here, too.
 *
 * It is a standard Python interface to call C/C++ functions.
 * The args parameter collects everything which is passed to the Python call.
 *
 * @param self a pointer to the object itself
 * @param args collection of arguments from Python
 */
static PyObject* euler_stochEulerC(PyObject *self, PyObject *args, PyObject *keywds)
{
	if (_import_array() != 0)
	{
		std::cerr << "Something is wrong with the Python array import."
				<< std::endl;
	}

	// number of threads
	int numThreads;

	// number of trajectories
	unsigned long numTraj;

	// initial the step size
	double stepsize;

	// adaptive or equidistant step size?
	int ui_adapt;

	// give intermediate points?
	double intermediate;

	// number of points in the grid
	int numGridPoints = 1;

	// the grid is a sequence
	PyObject* ui_grid;

	// the seed value
	double d_seed;

	// set as UINT_MAX initially
	unsigned int seed = UINT_MAX;

	/* KEYWORDS */
	/*
	 * Optional arguments must be initialized.
	 * If not specified in Python, they remain untouched.
	 */
	// number of Monte-Carlo multi levels
	unsigned int M = 0;

	// number of simulations for the last step in each trajectory
	unsigned int lastTraj = 1;

	static char kw[][16] = {"numThreads", "seed", "numTraj", "stepsize",
							"adapt", "intermediate", "grid", "numGridPoints",
							"M", "lastTraj"};
	static char *kwlist[] = {kw[0], kw[1], kw[2], kw[3],
							 kw[4], kw[5], kw[6], kw[7],
							 kw[8], kw[9], NULL}; // NULL terminated list of strings

	/*
	 * Parse the arguments from Python. This has to be done in the same order as they are given
	 * in the Python call. The string gives the expected type of the variables.
	 * "i" is for int
	 * "I" is for unsigned int
	 * "k" is for unsigned long
	 * "d" is for double
	 * "O" is for PythonObject
	 * "|" is a separator for optional arguments: any optional argument must be right of |
	 * This function returns false, if any of these types doesn't match their corresponding variable.
	 */
	if (!PyArg_ParseTupleAndKeywords(args, keywds, "idkdidOi|II", kwlist,
			&numThreads, &d_seed, &numTraj, &stepsize, &ui_adapt, &intermediate,
			&ui_grid, &numGridPoints, &M, &lastTraj))
		return NULL;

	// assign sequence arguments
	ui_grid = PySequence_Fast(ui_grid, "argument must be iterable");
	if (!ui_grid)
		return NULL;

	/*
	 * grid and dimGrid need special treatment
	 * convert them to std::vector
	 */
	// length of the sequences
	int ui_grid_len = PySequence_Fast_GET_SIZE(ui_grid);

	// dimension of domain D
	int prob_dim = ui_grid_len/numGridPoints;

	// starting points in the grid
	std::vector<double> grid(ui_grid_len);

	// dimension of the grid
	std::vector<int> grid_dim(prob_dim);

	/*
	 * In this loop we convert the sequences from Python to standard C types.
	 * This has to be done with each element of each sequence.
	 */
	for (int i = 0; i < ui_grid_len; ++i)
	{
		PyObject *fitem;

		// get i-th item
		PyObject *item = PySequence_Fast_GET_ITEM(ui_grid, i);
		if (!item)
		{
			Py_DECREF(ui_grid);
			return NULL;
		}

		fitem = PyNumber_Float(item);

		if (!fitem)
		{
			Py_DECREF(ui_grid);
			PyErr_SetString(PyExc_TypeError, "all grid items must be numbers");
			fprintf(stderr, "item %d of %d\n", i, ui_grid_len);
			return NULL;
		}

		grid[i] = PyFloat_AS_DOUBLE(fitem);
		Py_DECREF(fitem);
	}

	// determine whether we use the singel point or the grid version of the code
	bool singlepoint = (numGridPoints == 1);

	/*
	 * Check for invalid user input. OpenMP should check this automatically, but who knows...
	 */
	if (numThreads <= 0)
		numThreads = 1;
	/*
	 * Don't use more threads then there are problems.
	 * This would result in copy overhead.
	 */
	if (singlepoint)
	{
		// we parallelize over trajectories
		if ((unsigned long) numThreads > numTraj)
			numThreads = (int) numTraj;
	}
	else
	{
		// we parallelize over grid points
		if (numThreads > numGridPoints)
			numThreads = numGridPoints;
	}

	/*
	 * The boolean variables for adaptivity and intermediate points are parsed
	 * as integers for simplicity.
	 */
	bool adaptive = (ui_adapt == 1);

	if (std::isfinite(d_seed))
		seed = (unsigned int) d_seed;

	// Summarize the input.
//	printf(
//			"Starting computation with %d thread%s over %lu trajector%s in %d point%s.\n",
//			numThreads, numThreads == 1 ? "" : "s", numTraj,
//			numTraj == 1 ? "y" : "ies", numGridPoints, singlepoint ? "" : "s");

	// These four should be passed from Python.
	//	double gamma = 0.999;
	//	double sigma = 1.1;
	//  int dimBasis = 20;
	//	int skipBasis = 1000;

	if (sf == NULL)
	{
		setSF();
	}

	// Creating an object of the Euler-Maruyama solver.
	EulerScheme stochEuler(numTraj, lastTraj, numThreads, sf, false, "");

	// set the functions for our problem
	stochEuler.setFunctions(f, uD);

	// output numpy array and its dimensions
	PyArrayObject *vecout;
	int dims[2] = {1, prob_dim + 2};

	// this is the output array from the Euler-Maruyama code
	double* means;

	// solve with Euler-Maruyama scheme
	if (singlepoint)
	{
		means = stochEuler.solveSPML(grid, stepsize, adaptive, intermediate,
				seed, M);
		
	}
	else
	{
		means = stochEuler.solveGrid(grid, numGridPoints, prob_dim, stepsize, adaptive,
						intermediate, seed);
	}

	unsigned long length = stochEuler.getLength();
	dims[0] = length / (prob_dim + 2);

	// convert to numpy-array
	vecout = (PyArrayObject*) PyArray_FromDims(2, dims, NPY_DOUBLE);

	double *arr = (double *) PyArray_DATA(vecout);

	for (int i = 0; i < dims[0] * dims[1]; ++i)
	{
		arr[i] = means[i];
	}

	delete[] means;

	Py_DECREF(ui_grid);

	// return numpy-array
	return PyArray_Return(vecout);
}
} // extern "C" end

static PyMethodDef EulerMethods[] = {
		{
				"regressionC",
				euler_regressionC,
				METH_VARARGS,
				"Calculate global regression."
		},{
				"setCoefficientsC",
				euler_setCoefficientsC,
				METH_VARARGS,
				"Set the coefficients of the stochastic field."
		}, {
				"getCoefficientsC",
				euler_getCoefficientsC,
				METH_VARARGS,
				"Get the coefficients of the stochastic field."
		}, {
				"getIncrementsC",
				euler_getIncrementsC,
				METH_VARARGS,
				"Get the normal random values from the C++ Mersenne Twister."
		}, {
				"evalKappaC",
				(PyCFunction) euler_evalKappaC,
				METH_VARARGS | METH_KEYWORDS,
				"Evaluate the stochastic field at given points."
		}, {
				"stochEulerC",
				(PyCFunction) euler_stochEulerC,
				METH_VARARGS | METH_KEYWORDS,
				"Run stochastic Euler scheme."
		}, {NULL,NULL, 0, NULL} /* Sentinel */
};
