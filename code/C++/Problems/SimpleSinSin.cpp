#include "eulermodule.h"
#include "FourierModes.h"

#define FILENAME SimpleSinSin

/**
 * Very simple problem with rhs f = 1 and sinosoidal Dirichlet boundary conditions.
 */

/**
 * Stochastic basis with Fourier Modes
 *
 * \f$ \gamma = 0.9 \f$
 * \f$ \sigma = 2 \f$
 * \f$ m = 1, \ldots, 5 \f$
 */
void setSF()
{
	double gamma = 0.9;
	double A = 0.6;
	double sigma = 2.;
	int dimBasis = 5;
	int skipBasis = 0;

	sf = new FourierModes(gamma, A, sigma, dimBasis, skipBasis);
}

/**
 * \f$ f = 1 \f$
 */
double f(std::vector<double> &x)
{
	return 1.;
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = \sin(x \pi) + \sin(y \pi) \f$
 */
double uD(std::vector<double> &x)
{
	return std::sin(x[0] * M_PI) + std::sin(x[1] * M_PI);
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
