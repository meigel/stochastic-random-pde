/*
 * OneDiscExample.cpp
 *
 *  Created on: May 24, 2016
 *      Author: anker
 */

#include "eulermodule.h"
#include "OneDisc.h"

#define FILENAME OneDiscExample

/**
 * Very difficult problem with a buckle at around (0.9, 0.9) and zero Dirichlet boundary conditions.
 */

/**
 * Stochastic basis with a disc in ( 0.2, 0.2 ) and radius 0.1
 *
 * \f$ r = 0.1 \f$
 * \f$ M = (0.2, 0.2) \f$
 */
void setSF()
{
	double radius = 0.1;

	std::vector<double> position;
	position.push_back(0.2);
	position.push_back(0.2);

	double outerValue = 0.001;
	double mean = 0.;
	double sd = 1.;

	sf = new OneDisc(radius, position, outerValue, mean, sd);
}

double grad_v(std::vector<double> &x)
{
	using namespace std;

	return 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2)
		 - 40 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * (1 - x[1]) * x[1]
		 - 400 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 3) * x[1]
		 + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * x[1]*x[1]
		 + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * pow(1 - x[1], 2) * x[1]*x[1]
		 - 40 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * (1 - x[0]) * x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 - 400 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 3) * x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 + 10 * (-1 + exp(10 * pow(1 - x[0], 2))) * (-1 + exp(10 * pow(1 - x[1], 2))) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 + 500 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 + 500 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 + 2000 * exp(10 * pow(1 - x[0], 2)) * (-1 + exp(10 * pow(1 - x[1], 2))) * pow(1 - x[0], 4) * x[0]*x[0] * pow(1 - x[1], 2) * x[1]*x[1]
		 + 2000 * exp(10 * pow(1 - x[1], 2)) * (-1 + exp(10 * pow(1 - x[0], 2))) * pow(1 - x[0], 2) * x[0]*x[0] * pow(1 - x[0], 4) * x[1]*x[1];
}

/**
 * \f[ u(x, y) = 5 \left(e^(10 x^2)-1\right) \left(1-x\right)^2 x^2 \left(e^(10 y^2)-1\right) y^2 \left(1-y\right)^2
 *
 * \Rightarrow -\Delta u = f
 *
 *  f = -10 (5 (e^(10 x^2)-1) (x-1)^2 x^2 (e^(10 y^2)-1) y^2
 *  +(e^(10 x^2)-1) (x-1)^2 x^2 (e^(10 y^2)-1) (y^2-1)
 *  +50 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^2 (y^2-1)
 *  +50 e^(10 x^2) (x-1)^2 x^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +(e^(10 x^2)-1) x^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +4 (e^(10 x^2)-1) (x-1) x (e^(10 y^2)-1) y^2 (y^2-1)
 *  +(e^(10 x^2)-1) (x-1)^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +40 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^4
 *  +200 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^4 (y^2-1)
 *  +200 e^(10 x^2) (x-1)^2 x^4 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +40 e^(10 x^2) (x-1) x^3 (e^(10 y^2)-1) y^2 (y^2-1)) \f]
 */
double f(std::vector<double> &x)
{
	using namespace std;

	return - (10 * ((exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * (exp(10 * pow(x[1], 2)) - 1) *  pow(x[1] - 1, 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1], 2)
	            + 50 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
	            * pow(x[1], 2)
	            + 50 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 4 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * (x[1] - 1) *  x[1]
	            + 4 * (exp(10 * pow(x[0], 2)) - 1) * (x[0] - 1) * x[0]
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 200 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
	            * pow(x[1], 4)
	            + 40 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * (x[1] - 1) * pow(x[1], 3)
	            + 200 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 4)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 40 * exp(10 * pow(x[0], 2)) * (x[0] - 1) * pow(x[0], 3)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2))) - grad_v(x);
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = 0 \f$
 */
double uD(std::vector<double> &x)
{
	return 0;
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
