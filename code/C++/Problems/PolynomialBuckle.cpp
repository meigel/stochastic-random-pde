#include "eulermodule.h"
#include "FourierModes.h"

#define FILENAME PolynomialBuckle

/**
 * Polynomial problem with a buckle at around (0.9, 0.9) and zero Dirichlet boundary conditions.
 */

/**
 * Stochastic basis with Fourier Modes
 *
 * \f$ \gamma = 0.9 \f$
 * \f$ \sigma = 2 \f$
 * \f$ m = 1, \ldots, 5
 */
void setSF()
{
	double gamma = 0.9;
	double A = 0.6;
	double sigma = 2.;
	int dimBasis = 5;
	int skipBasis = 0;

	sf = new FourierModes(gamma, A, sigma, dimBasis, skipBasis);
}

/**
 * \f$ f = -6 x y (x^2 + y^2 - 2)\f$
 */
double f(std::vector<double> &x)
{
	return -6*x[0]*x[1]*(x[0]*x[0]+x[1]*x[1]-2);
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = 0 \f$
 */
double uD(std::vector<double> &x)
{
	return 0.;
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
