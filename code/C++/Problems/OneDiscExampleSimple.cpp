/*
 * OneDiscExample.cpp
 *
 *  Created on: May 24, 2016
 *      Author: anker
 */

#include "eulermodule.h"
#include "OneDisc.h"

#define FILENAME OneDiscExampleSimple

/**
 * Very difficult problem with a buckle at around (0.9, 0.9) and zero Dirichlet boundary conditions.
 */

/**
 * Stochastic basis with a disc in ( 0.2, 0.2 ) and radius 0.1
 *
 * \f$ r = 0.1 \f$
 * \f$ M = (0.2, 0.2) \f$
 */
void setSF()
{
	double radius = 0.1;

	std::vector<double> position;
	position.push_back(0.5);
	position.push_back(0.5);

	double outerValue = 0.001;
	double mean = 0.;
	double sd = 2.0;

	sf = new OneDisc(radius, position, outerValue, mean, sd);
}

/**
 * \f[ u(x, y) = 5 \left(e^(10 x^2)-1\right) \left(1-x\right)^2 x^2 \left(e^(10 y^2)-1\right) y^2 \left(1-y\right)^2
 *
 * \Rightarrow -\Delta u = f
 *
 */
double f(std::vector<double> &x)
{
	return 1.0;
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = 0 \f$
 */
double uD(std::vector<double> &x)
{
	return 0;
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
