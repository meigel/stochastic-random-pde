#include "eulermodule.h"
#include "FourierModes.h"

#define FILENAME RoughSinSin

/**
 * rhs f = 1 and sinosoidal Dirichlet boundary conditions.
 * The underlying stochastic field is quiet rough.
 */

/**
 * Stochastic basis with Fourier Modes
 *
 * \f$ \gamma = 0.9 \f$
 * \f$ \sigma = 1.1 \f$
 * \f$ m = 1001, \ldots, 1020
 */
void setSF()
{
	double gamma = 0.9;
	double A = 0.0009;
	double sigma = 1.1;
	int dimBasis = 20;
	int skipBasis = 1000;

	sf = new FourierModes(gamma, A, sigma, dimBasis, skipBasis);
}

/**
 * \f$ f = 1 \f$
 */
double f(std::vector<double> &x)
{
	return 1.;
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = \sin(x \pi) + \sin(y \pi) \f$
 */
double uD(std::vector<double> &x)
{
	return std::sin(x[0] * M_PI) + std::sin(x[1] * M_PI);
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
