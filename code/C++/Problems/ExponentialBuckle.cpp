#include "eulermodule.h"
#include "FourierModes.h"

#define FILENAME ExponentialBuckle

/**
 * Very difficult problem with a buckle at around (0.9, 0.9) and zero Dirichlet boundary conditions.
 */

/**
 * Stochastic basis with Fourier Modes
 *
 * \f$ \gamma = 0.9 \f$
 * \f$ \sigma = 2 \f$
 * \f$ m = 1, \ldots, 5
 */
void setSF()
{
	double gamma = 0.9;
	double A = 0.6;
	double sigma = 2.;
	int dimBasis = 5;
	int skipBasis = 0;
	
	// features of the distribution of the coefficients
	// which distribution? // 0 = uniform, 1 = normal, 2 = lognormal
	int distribution = 1;
	
	// parameters of the distribution
	double p1 = 0.;
	double p2 = 0.002;

	sf = new FourierModes(gamma, A, sigma, dimBasis, skipBasis, p1, p2, distribution);
}

/**
 * \f[ u(x, y) = 5 \left(e^(10 x^2)-1\right) \left(1-x\right)^2 x^2 \left(e^(10 y^2)-1\right) y^2 \left(1-y^2\right)
 *
 * \Rightarrow -\Delta u = f
 *
 *  f = -10 (5 (e^(10 x^2)-1) (x-1)^2 x^2 (e^(10 y^2)-1) y^2
 *  +(e^(10 x^2)-1) (x-1)^2 x^2 (e^(10 y^2)-1) (y^2-1)
 *  +50 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^2 (y^2-1)
 *  +50 e^(10 x^2) (x-1)^2 x^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +(e^(10 x^2)-1) x^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +4 (e^(10 x^2)-1) (x-1) x (e^(10 y^2)-1) y^2 (y^2-1)
 *  +(e^(10 x^2)-1) (x-1)^2 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +40 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^4
 *  +200 (e^(10 x^2)-1) (x-1)^2 x^2 e^(10 y^2) y^4 (y^2-1)
 *  +200 e^(10 x^2) (x-1)^2 x^4 (e^(10 y^2)-1) y^2 (y^2-1)
 *  +40 e^(10 x^2) (x-1) x^3 (e^(10 y^2)-1) y^2 (y^2-1)) \f]
 */
double f(std::vector<double> &x)
{
	using namespace std;

	return - (10 * ((exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * (exp(10 * pow(x[1], 2)) - 1) *  pow(x[1] - 1, 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1], 2)
	            + 50 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
	            * pow(x[1], 2)
	            + 50 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 4 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2) * pow(x[0], 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * (x[1] - 1) *  x[1]
	            + 4 * (exp(10 * pow(x[0], 2)) - 1) * (x[0] - 1) * x[0]
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 200 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * pow(x[1] - 1, 2)
	            * pow(x[1], 4)
	            + 40 * (exp(10 * pow(x[0], 2)) - 1) * pow(x[0] - 1, 2)
	            * pow(x[0], 2) * exp(10 * pow(x[1], 2)) * (x[1] - 1) * pow(x[1], 3)
	            + 200 * exp(10 * pow(x[0], 2)) * pow(x[0] - 1, 2) * pow(x[0], 4)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)
	            + 40 * exp(10 * pow(x[0], 2)) * (x[0] - 1) * pow(x[0], 3)
	            * (exp(10 * pow(x[1], 2)) - 1) * pow(x[1] - 1, 2) * pow(x[1], 2)));
}

/**
 * Dirichlet boundary conditions on every boundary
 *
 * \f$ u_D = 0 \f$
 */
double uD(std::vector<double> &x)
{
	return 0.;
}

extern "C"
{
	INIT_MODULE(FILENAME)
} // extern C end
