#ifndef EULER_H_
#define EULER_H_

#include <algorithm>
#include <omp.h>
#include <functional>
#include <limits>
#include <iostream>
#include <memory>
#include "StochasticField.h"

/**
 * This class implements the Euler-Maruyama scheme to solve a stochastic
 * differential equation. The scheme is parallelized with OpenMP and supports
 * elementary adaptivity as well as two different solving methods for single
 * points or collection of points.
 *
 */
class EulerScheme
{
	private:
		/// number of trajectories
		unsigned long numTraj;

		/// number of trajectories for the last step in each trajectory
		unsigned int lastTraj;

		/// number of threads
		int numThreads;

		/**
		 * @brief The domain where to solve the problem, for example Unit Square.
		 */
		double* domain;

		/**
		 * Mid point of the domain, which is currently needed for
		 * the adaptivity. As soon as better adaptive methods are
		 * implemented, this can be dropped.
		 */
		double* mp;

		/**
		 * right hand side
		 */
		std::function<double(std::vector<double> &x)> f;

		/**
		 * Neumann boundary conditions
		 */
		std::function<double(std::vector<double> &x)> g;

		/**
		 * Dirichlet boundary conditions
		 */
		std::function<double(std::vector<double> &x)> u_D;

		/**
		 * @brief Things that need to be done in every constructor can be done here.
		 * E.g. setting f, g, u_D, domain etc.
		 */
		void init();

		/**
		 * Determine whether to write a file with the results.
		 *
		 * @note This is the current way to interface R, because of some allocation error.
		 * @todo fix the allocation error
		 */
		bool write;

		/**
		 * the file name (relative path)
		 */
		const char* filename;

		/**
		 * File object related to EulerScheme::filename.
		 *
		 * @see write
		 * @see filename
		 */
		FILE *file;

		/**
		 * Adaptive step size calculation depending on difference to mid point.
		 *
		 * @param xi reference point \f$ x_i \f$
		 *
		 * @return multiplicative coefficient for step size in \f$ [0,1] \f$
		 */
		double adapt(std::vector<double> &xi, std::vector<double> &kappa);

		/**
		 * Projects the end point of the trajectory onto the boundary.
		 * The intersection point of the last line segment of the trajectory
		 * \f$ \left[ U_{\tau-1}, U_{\tau} \right] \f$ and the boundary is calculated and returned.
		 *
		 * @param start last point in the domain 
		 * @param end first point outside of domain
		 * @return point on the boundary
		 *
		 */
		std::vector<double> project(std::vector<double> &start, std::vector<double> &end);

		/**
		 * Integrates the right hand side EulerScheme::f between a and b with interval length dt.
		 *
		 * @param a begin of integration interval
		 * @param b end of integration interval
		 * @param dt length of integration interval
		 * @return integral value
		 */
		double integrate(std::vector<double> &a, std::vector<double> &b, double dt);

		/**
		 * Upon leaving the domain in EulerScheme::solveGrid, EulerScheme::solveSP and EulerScheme::solveSPML with a trajectory,
		 * we stop and re-do the last step several times to obtain a smoothed result at the boundary.
		 *
		 * @param last_xi the last point in the domain
		 * @param stepsize initial step size
		 * @param adaptive enable/disable adaptivity
		 * @param increments the increments for the brownian motion
		 * @param sbs stochastic bases for simulation
		 * @param tid thread id
		 * @param p_count pointer to the count variable
		 * @param ndist normal distribution to create new increments if necessary
		 *
		 * @return average [ path integral length , boundary evaluation ]
		 */
		double* last_steps(std::vector<double> &last_xi_in, double stepsize,
				bool adaptive, std::vector<double> &increments, StochasticField &sfs, int tid, unsigned* p_count,
				std::normal_distribution<double> &ndist);

		/// Size of the result vector.
		unsigned long overallSize;

		/// Standard Mersenne-Twister for each thread
		std::mt19937_64* MTS;

		/// Member object to hold the configuration of the stochastic basis functions.
		StochasticField* sf;

		/// Number of basis functions for the stochastic expansion
		int dimBasis;

		/// How many basis functions should be skipped
		int skipBasis;

	public:
		/**
		 * Initialize the Euler-Maruyama scheme.
		 *
		 * If using R as interfaced language, choose EulerScheme::write = true and provide EulerScheme::filename.
		 * If using Python as interfaced language, choose EulerScheme::write = false.
		 *
		 * @param numTraj number of trajectories
		 * @param lastTraj number of trajectories for the last step in each trajectory
		 * @param numThreads number of threads
		 * @param write enable/disable file writing
		 * @param filename specify file
		 */
		EulerScheme(unsigned long numTraj, unsigned int lastTraj, int numThreads, bool write,
				const char *filename);

		/**
		 * Initialize the Euler-Maruyama scheme with a previously defined StochasticBasis object.
		 *
		 * If using R as interfaced language, choose EulerScheme::write = true and provide EulerScheme::filename.
		 * If using Python as interfaced language, choose EulerScheme::write = false.
		 *
		 * @param numTraj number of trajectories
		 * @param lastTraj number of trajectories for the last step in each trajectory
		 * @param numThreads number of threads
		 * @param sb stochastic basis
		 * @param write enable/disable file writing
		 * @param filename specify file
		 */
		EulerScheme(unsigned long numTraj, unsigned int lastTraj, int numThreads, StochasticField* sf,
				    bool write, const char *filename);

		/**
		 * Returns the total size of the result vector.
		 *
		 * @return overall size of result vector
		 */
		unsigned long getLength();

		/**
		 * @deprecated coefficients are now generated in the StochasticField EulerScheme::sb
		 *
		 * Returns the used coefficients for the stochastic basis.
		 * The size is EulerScheme::dimBasis.
		 * 
		 * @return coefficients of stochastic basis
		 */
		std::vector<double> getCoefficients();

		/**
		 * Get the brownian increments.
		 *
		 * @param prob_dim
		 * @param stepsize
		 * @param seed
		 * @return
		 */
		double* getIncrements(int prob_dim, double stepsize, unsigned int seed);

		/**
		 * Set the right hand side and the Dirichlet boundary condition.
		 *
		 * @param f right hand side
		 * @param uD Dirichlet boundary condition
		 */
		void setFunctions(std::function<double(std::vector<double> &x)> f, std::function<double(std::vector<double> &x)> uD);

		/**
		 * Solves the specified problem at a single given point.
		 *
		 * @param x0 point where to solve the problem
		 * @param stepsize initial step size
		 * @param adaptive enable/disable adaptivity
		 * @param intermediate enable/disable saving of intermediate steps of the Euler-Maruyama scheme
		 * @param seed set a specific seed value for random number generator, if seed is UINT_MAX a
		 *		  random seed will be set
		 */
		double* solveSP(std::vector<double> &x0, double stepsize, bool adaptive,
				double intermediate, unsigned int seed);

		/**
		 * Solves the specified problem at a single given point for one
		 * multilevel level.
		 *
		 * @param x0 point where to solve the problem
		 * @param stepsize initial step size
		 * @param adaptive enable/disable adaptivity
		 * @param intermediate enable/disable saving of intermediate steps of the Euler-Maruyama scheme
		 * @param seed set a specific seed value for random number generator, if seed is UINT_MAX a
		 *        random seed will be set
		 * @param M number of steps to sum for the coarse level
		 */
		double* solveSPML(std::vector<double> &x0, double stepsize, bool adaptive,
						double intermediate, unsigned int seed, unsigned int M);

		/**
		 * Solves the specified problem at all given grid points.
		 *
		 * @param grid grid of points where to solve the problem
		 * @param numGridPoints number of grid points
		 * @param prob_dim spatial dimension of the problem
		 * @param stepsize initial step size
		 * @param adaptive enable/disable adaptivity
		 * @param intermediate enable/disable saving of intermediate steps of the Euler-Maruyama scheme
		 * @param seed set a specific seed value for random number generator, if seed is UINT_MAX a
		 random seed will be set
		 */
		double* solveGrid(std::vector<double> &grid, int numGridPoints, int prob_dim,
				double stepsize, bool adaptive, double intermediate,
				unsigned int seed);
//		double* solveGrid(std::vector<double> &grid, std::vector<int> gridDim,
//				double stepsize, bool adaptive, bool intermediate,
//				unsigned int seed);

		/**
		 * default destructor
		 */
		~EulerScheme();

};

#endif /*EULER_H_*/
