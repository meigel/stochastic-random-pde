#include "Euler.h"

/*
 * This include is only here to make the constructor without StochasticField
 * possible. It initializes the underlying StochasticField to FourierModes.
 */
#include "FourierModes.h"

EulerScheme::EulerScheme(unsigned long numTraj, unsigned int lastTraj, int numThreads,
		bool write, const char *filename) :
		numTraj(numTraj), lastTraj(lastTraj), numThreads(numThreads), write(write), filename(filename)
{
	init();

	this->sf = new FourierModes(0.9, 0.6, 2., dimBasis, skipBasis);
}

EulerScheme::EulerScheme(unsigned long numTraj, unsigned int lastTraj, int numThreads,
		StochasticField* sf, bool write, const char *filename) :
		numTraj(numTraj), lastTraj(lastTraj), numThreads(numThreads), write(
				write), filename(filename), sf(sf)
{
	init();
}

void EulerScheme::setFunctions(std::function<double(std::vector<double> &x)> f, std::function<double(std::vector<double> &x)> uD)
{
	this->f = f;
	this->u_D = uD;
}

/**
 * Look into this function if you want to change anything like
 * boundary conditions or the right hand side.
 */
void EulerScheme::init()
{
	omp_set_num_threads(this->numThreads);

	this->overallSize = 0;

	// the Mersenne Twister from C++ standard library
	this->MTS = new std::mt19937_64[this->numThreads];

	this->dimBasis = 5;

	this->skipBasis = 0;

	if (this->write)
	{
		this->file = fopen(filename, "wb");
	}
	else
	{
		this->file = NULL;
	}

	// Neumann boundary g = 0;
	this->g = [](const std::vector<double> &x)
	{
		return 0.;
	};

	// domain: Unit Square
	this->domain = new double[4] { 0., 1., 0., 1. };

	// mid-point of domain
	this->mp = new double[2] { 0.5, 0.5 };
}

/**
 * Allocated arrays for domain, mid point etc. are released, omp_num_threads is reset to 1.
 */
EulerScheme::~EulerScheme()
{
	delete[] domain;
	delete[] mp;

	delete[] MTS;

	omp_set_num_threads(1);
}

// double distnorm(std::vector<double> &kappa, std::vector<double> &last_kappa)
// {
// 	double sum = 0.;
// 
// 	for (unsigned i = 1; i < kappa.size(); ++i)
// 	{
// 		sum += ( kappa[i] - last_kappa[i] ) * ( kappa[i] - last_kappa[i] );
// 	}
// 
// 	return std::sqrt(sum);
// }

/**
 * Minimum distance to boundary relative to mid-point gives factor for step size.
 */
double EulerScheme::adapt(std::vector<double> &xi, std::vector<double> &kappa)
{
	// boundary adaptive
	double relmin = fmin(
			fmin(fabs(xi[0] - domain[0]), fabs(xi[0] - domain[1])) / mp[0],
			fmin(fabs(xi[1] - domain[2]), fabs(xi[1] - domain[3])) / mp[1]);

	// kappa adaptive
	// relmin /= sqrt(kappa[0]);

	return relmin;
}

double distance(std::vector<double> &x, std::vector<double> &y)
{
	double sum = 0.;

	for (unsigned i = 0; i < x.size(); i++)
	{
		sum += (x[i]-y[i])*(x[i]-y[i]);
	}

	return std::sqrt(sum);
}

int get_line_intersection(double p0_x, double p0_y, double p1_x, double p1_y,
    double p2_x, double p2_y, double p3_x, double p3_y, double *i_x, double *i_y)
{
    double s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    double s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }

    return 0; // No collision
}

std::vector<double> EulerScheme::project(std::vector<double> &start, std::vector<double> &end)
{

	/*
	 * This is a very simple version.
	 * It just detects the shortest distance to a boundary
	 * and puts it onto this boundary.
	 * No further calculation done.
	 */
	/*unsigned prob_dim = end.size();

	std::vector<double> xT(prob_dim);

	for (unsigned j = 0; j < prob_dim; ++j)
			xT[j] = fmin(
						  fmax( end[j], domain[j * prob_dim] ),
						  domain[j * prob_dim + 1]
						);

	return xT;*/
	
	/*
	 * This version calculates the intersecting point of the boundaries
	 * and the line between the last two points of the trajectory (@p start, @p end).
	 */
	
	std::vector<double> temp_end(end);
	
	/*
	 * If we started on the boundary or somewhere outside, 
	 * create an artificial line from start point to mid point.
	 */
	if (start[0] == end[0] && start[1] == end[1])
	{
		temp_end[0] = mp[0];
		temp_end[1] = mp[1];
	}
		
	std::vector<std::vector<double>> intersections;
	std::vector<std::vector<double>> edges;

	std::vector<double> temp{0.,0.,1.,0.};
	edges.push_back(temp);

	temp[0] = 1.; temp[1] = 0.; temp[2] = 1.; temp[3] = 1.;
	edges.push_back(temp);

	temp[0] = 1.; temp[1] = 1.; temp[2] = 0.; temp[3] = 1.;
	edges.push_back(temp);

	temp[0] = 0.; temp[1] = 1.; temp[2] = 0.; temp[3] = 0.;
	edges.push_back(temp);

	for (int i = 0; i < 4; i++)
	{
		double i_x = 0.;
		double i_y = 0.;
		if (get_line_intersection(start[0], start[1], temp_end[0], temp_end[1], edges[i][0], edges[i][1], edges[i][2], edges[i][3], &i_x, &i_y))
		{
			std::vector<double> inter(2); inter[0] = i_x; inter[1] = i_y;
			intersections.push_back(inter);
		}
	}


	return intersections[0];
}



/**
 * Here the trapezoidal rule is used to approximate the integral.
 */
double EulerScheme::integrate(std::vector<double> &a, std::vector<double> &b, double dt)
{
	return dt * ( f(a) + f(b) )/2.;
}

/**
 * This is very akin to EulerScheme::solveSPML but without the parallelization.
 */
double* EulerScheme::last_steps(std::vector<double> &last_xi_in, double stepsize,
		bool adaptive, std::vector<double> &increments, StochasticField &sfs, int tid, unsigned* p_count,
		std::normal_distribution<double> &ndist)
{
	// dimension of problem
	int prob_dim = last_xi_in.size();

	double meanInt = 0.;
	double meanu_D = 0.;

	// increments per thread: how many brownian increments each thread generates for himself at once
	unsigned ipt = 64. / stepsize; // 2^4

	// the location where a trajectory hits the boundary of D ( \partial D )
	std::vector<double> xT(prob_dim);

	// do an Euler-Maruyama scheme for each trajectory
	for (unsigned long i = 0; i < this->lastTraj; ++i)
	{

		// get new coefficients for the stochastic field
		sfs.newCoefficients();

		std::vector<double> xi(last_xi_in);

		// the integral value, with f = 1 it's just the end time T
		double integral = 0.;

		// the stopping time \tau
		double T = 0.;

		// this is x_{i-1}
		std::vector<double> last_xi(xi);

		double dt = stepsize;

		std::vector<double> kappa = sfs.eval(xi);
// 		std::vector<double> last_kappa(kappa);
		
		// we do the first step outside of the loop
		// as we need x_i and x_{i-1} for the integral calculation
		if (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
			 && xi[1] < domain[3])
		{
			if (adaptive)
				dt *= adapt(xi, kappa);
			
			// the sqrt term from the Euler-Maruyama scheme
			double sqrtTerm = sqrt(2 * kappa[0] * dt);
		
			for (int j = 0; j < prob_dim; ++j)
			{
				xi[j] += kappa[j + 1] * dt
						+ sqrtTerm * increments[(*p_count)];
	// 				xi[j] += sqrt(2 * dt) * increments[(*p_count)];
				(*p_count)++;
			}
		}

		// Are we still inside of the domain?
		while (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
				&& xi[1] < domain[3])
		{
			// if count comes to the end of increments ...
			if ( (*p_count) >= ipt)
			{
				// ... create some more increments ...
				for (unsigned dd = 0; dd < ipt; dd++)
					increments[dd] = ndist(MTS[tid]);

				// ... and don't forget to reset count!
				(*p_count) = 0;
			}

			dt = stepsize;

// 			last_kappa = kappa;
			kappa = sfs.eval(xi);

			// if enabled, calculate the new adaptive step size, ...
			if (adaptive)
				dt *= adapt(xi, kappa);

			// the sqrt term from the Euler-Maruyama scheme
			double sqrtTerm = sqrt(2 * kappa[0] * dt);
		
			// increase stopping time
			T += dt;

			integral += f(xi) * dt;

			last_xi = xi;

			// This is the actual Euler-Maruyama step.
			for (int j = 0; j < prob_dim; ++j)
			{
				xi[j] += kappa[j + 1] * dt
						+ sqrt(2 * kappa[0] * dt) * increments[(*p_count)];
// 				xi[j] += sqrt(2 * dt) * increments[(*p_count)];
				(*p_count)++;
			}
		}
		// We left the domain, so T is our stopping time \tau from Feynman-Kac.

		// project onto boundary
		if (xi[0] != last_xi[0] || xi[1] != last_xi[1]) {
			xT =  project(last_xi, xi);
			integral += f(xT) * dt * distance(xT, last_xi)/distance(xi, last_xi);
		} else {
			xT = xi;
		}

		// sum up all integral values, ...
		meanInt += integral;

		// ... sum up all boundary values ...
		meanu_D += u_D(xT);

	} // end for

	// ... and divide by total number of trajectories
	meanInt /= this->lastTraj;
	meanu_D /= this->lastTraj;

	double *results = new double[2]{meanInt, meanu_D};

	return results;
}

/**
 * This method solves (numTraj) stochastic differential equation with the Euler-Maruyama scheme and
 * then uses the results to obtain the solution \f$ u \f$ of the PDE
 * \f[
 *   \Delta u = f \quad , u(\boldsymbol{x}) \vert_{\partial D} = u_D(\boldsymbol{x})
 * \f]
 * with a Monte-Carlo approach using the Feynman-Kac connection.
 *
 * By Feynman-Kac formula it holds that the solution of a PDE
 * \f[
 *   \Delta u = f \quad , u(\boldsymbol{x}) \vert_{\partial D} = u_D(\boldsymbol{x})
 * \f]
 * can be written as
 * \f[
 *   u(\boldsymbol{x}) = \mathbb{E}\left[ u_D \left( U_{\tau}(\boldsymbol{x}) \right) -\int_0^{\tau} \! f \left( U_s \right) \, \mathrm{d}s \right],
 * \f] where \f$ U_t \f$ is the solution of the SDE
 * \f[
 *   \mathrm{d}U_t = a \left( t, U_t \right) \mathrm{d}t + b \left( t, U_t \right) \mathrm{d}W_t
 * \f] and \f$ \tau \f$ is the stopping time for leaving \f$ D \f$.
 * For \f$ f = 1 \f$ this gives the Euler-Maruyama scheme used here.
 * 
 * The needed random numbers (brownian increments) for the Euler-Maruyama are computed on the fly by a 
 * Mersenne-Twister algorithm using the C++ standard library.
 *
 * The code for the Euler-Maruyama scheme is parallelized.
 */
double* EulerScheme::solveSP(std::vector<double> &x0, double stepsize,
		bool adaptive, double intermediate, unsigned int seed) {
	return solveSPML(x0, stepsize, adaptive, intermediate, seed, 0);
}

double* EulerScheme::solveSPML(std::vector<double> &x0, double stepsize,
		bool adaptive, double intermediate, unsigned int seed,
		unsigned int M)
{
	// the intermediate points of the i-th trajectory will be stored here as
	// { (x0, y0, u(x0, y0), var ), (x1, y1, u(x1, y1), var ), ... }
	std::vector<double> points[this->numTraj];

	// initialize trajectory
	for (unsigned long i = 0; i < this->numTraj && intermediate >= 0.; ++i)
	{
		points[i].reserve(1000);
	}

	// here the results of all trajectories will be saved
	double* results = new double[this->numTraj];

	// increments per thread: how many brownian increments each thread generates for himself at once
	unsigned ipt = 64. / stepsize; // 2^4

	// dimension of problem
	int prob_dim = x0.size();

	// counter to avoid reusing the same increments
	unsigned count = 0;//rand()/ ( (float) RAND_MAX ) * ipt;

	// each thread collects the sums of his trajectories by himself to avoid race conditions
	double* sums = new double[this->numThreads];

	// the location where a trajectory hits the boundary of D ( \partial D )
	std::vector<double> xT;
	std::vector<double> xTCoarse;

	// each thread uses his own set of brownian increments ...
	std::vector<double> increments[this->numThreads];

	// ... and stochastic field ...
	std::vector< std::unique_ptr<StochasticField> > sfs(this->numThreads);

	// ... as well as distribution
	std::normal_distribution<double>* ndist = new std::normal_distribution<
			double>[this->numThreads];

	std::random_device rdev;

	// initialize sums and brownian increments for each thread
#pragma omp parallel for shared(increments, ipt, sums, ndist, sfs)
	for (int s = 0; s < this->numThreads; ++s)
	{
		std::normal_distribution<double> temp_ndist(0, 1.);
		ndist[s] = temp_ndist;

		std::mt19937_64 temp_mts;

		if (seed == std::numeric_limits<unsigned int>::max())
		{
			std::vector<int> seed_data(2 * std::mt19937_64::state_size);
			std::generate_n(seed_data.data(), seed_data.size(), std::ref(rdev));
			std::seed_seq seq(std::begin(seed_data), std::end(seed_data));

			temp_mts.seed(seq);
		}
		else
		{
			temp_mts.seed(seed + s);
		}

		this->MTS[s] = temp_mts;
		sums[s] = 0.f;

		std::unique_ptr<StochasticField> temp_sf(this->sf->clone());

		sfs[s] = std::move(temp_sf);

		increments[s].resize(ipt);

		for (unsigned dd = 0; dd < ipt; dd++)
		{
			increments[s][dd] = ndist[s](MTS[s]);
		}
	}

#pragma omp parallel for shared(x0, increments, results, prob_dim, ipt, sums, points, adaptive, stepsize, ndist, sfs) firstprivate(xT, count)
	// do an Euler-Maruyama scheme for each trajectory
	for (unsigned long i = 0; i < this->numTraj; ++i)
	{
		std::vector<double> xi(x0);
		std::vector<double> lastXICoarse(x0);

		// the integral value, with f = 1 it's just the end time T
		double integral = 0.;
		double integralCoarse = 0.;

		// the stopping time \tau
		double T = 0.;

		// current thread index
		int tid = omp_get_thread_num();

		// get new coefficients for the stochastic field
		sfs[tid]->newCoefficients();

		if (intermediate >= 0.)
		{
			// start the trajectory with the initial point x0
			points[i].push_back(xi[0]);
			points[i].push_back(xi[1]);

			/* The last value represent the solution u at x_i which in case of
			 * the right hand side f = 1 is just the current time.
			 */
			points[i].push_back(-integral);

			// variance of an intermediate point is 0 as there is only one value for that point
			points[i].push_back(0.);
		}

		// this is x_{i-1}
		std::vector<double> last_xi(xi);

		double dt = stepsize;

		std::vector<double> kappa = sfs[tid]->eval(xi);
// 		std::vector<double> last_kappa(kappa);

		// we do the first step outside of the loop
		// as we need x_i and x_{i-1} for the integral calculation

		// checking domain
		if (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
			 && xi[1] < domain[3])
		{			
			if (adaptive)
				dt *= adapt(xi, kappa);
			
			// the sqrt term from the Euler-Maruyama scheme
			double sqrtTerm = sqrt(2 * kappa[0] * dt);
		
			for (int j = 0; j < prob_dim; ++j)
			{
				xi[j] += kappa[j + 1] * dt
						+ sqrtTerm * increments[tid][count];
	// 				xi[j] += sqrt(2 * dt) * increments[tid][count];
				count++;
			}
		}

		// Are we still inside of the domain?
		int mlRotCount = 0;
		double dtCoarse = 0;

		while (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
				&& xi[1] < domain[3])
		{
			mlRotCount++;
			// if count comes to the end of increments ...
			if (count >= ipt)
			{
				// ... create some more increments ...
				for (unsigned dd = 0; dd < ipt; dd++)
					increments[tid][dd] = ndist[tid](MTS[tid]);

				// ... and don't forget to reset count!
				count = 0;
			}

			dt = stepsize;

// 			last_kappa = kappa;
			kappa = sfs[tid]->eval(xi);

			// if enabled, calculate the new adaptive step size
			if (adaptive)
				dt *= adapt(xi, kappa);

			dtCoarse += dt;

			// the sqrt term from the Euler-Maruyama scheme
			double sqrtTerm = sqrt(2 * kappa[0] * dt);
		
			// increase stopping time
			T += dt;

			integral += f(xi) * dt;
			if (M != 0 && mlRotCount % M == 0) {
				integralCoarse += f(xi) * dtCoarse; // Probably wrong dt
				dtCoarse = 0;
				lastXICoarse = xi;
			}

			last_xi = xi;

			// This is the actual Euler-Maruyama step.
			for (int j = 0; j < prob_dim; ++j)
			{
				xi[j] += kappa[j + 1] * dt
						+ sqrtTerm * increments[tid][count];
// 				xi[j] += sqrt(2 * dt) * increments[tid][count];
				count++;
			}

			/* Here the output for intermediate points is written.
			 * A point is only added if its spatial distance to the last point is greater or equal to @intermediate
			 */
			if (intermediate >= 0. && intermediate < std::numeric_limits<double>::infinity())
			{
				if ((points[i][points[i].size() - 4] - xi[0])
						* (points[i][points[i].size() - 4] - xi[0])
						+ (points[i][points[i].size() - 3] - xi[1])
								* (points[i][points[i].size() - 3] - xi[1])
						>= intermediate * intermediate)
				{
					// if we hit the maximum capacity of the current trajectory, we need more space
					if (points[i].size() + 4 > points[i].capacity())
					{
						//#pragma omp critical
						{
							/* Experiments have shown, that increasing the capacity by the
							 * Golden Ratio is a good tradeoff between allocating too much
							 * memory and allocating not enough memory.
							 */
							points[i].reserve(
									(int) (1.618034 * points[i].capacity()));
						}
					}

					// Write intermediate results into output
					points[i].push_back(xi[0]);
					points[i].push_back(xi[1]);
					points[i].push_back(-integral);
					points[i].push_back(0.);
				}
			}
		}
		// We left the domain, so T is our stopping time \tau from Feynman-Kac.

		double u_D_xT;

		// project onto boundary
		if (xi[0] != last_xi[0] || xi[1] != last_xi[1]) {

			// check count ones more before last_steps(...)
			// you could mess up a lot by forgetting this
			if (count >= ipt)
			{
				// create some more increments ...
				for (unsigned dd = 0; dd < ipt; dd++)
					increments[tid][dd] = ndist[tid](MTS[tid]);

				// ... and don't forget to reset count!
				count = 0;
			}

			xT =  project(last_xi, xi);
//			integral += f(xT) * dt * distance(xT, last_xi)/distance(xi, last_xi);
//			u_D_xT = u_D(xT);

			double *average = last_steps(last_xi, stepsize, adaptive, increments[tid], *sfs[tid], tid, &count, ndist[tid]);

			integral += average[0];
			u_D_xT = average[1];

			// count was increased in last_steps(...)
			// so check it again
			if (count >= ipt)
			{
				for (unsigned dd = 0; dd < ipt; dd++)
					increments[tid][dd] = ndist[tid](MTS[tid]);

				count = 0;
			}

		} else {
			xT = xi;
			u_D_xT = u_D(xT);
		}

		if (M != 0 && dtCoarse != 0){
//			xTCoarse = project(lastXICoarse, xi); // raises exception
			xTCoarse = xT;
			integralCoarse += f(xTCoarse) * dtCoarse;
		}

		// the last point in the saved trajectory is not projected to boundary and therefore outside
		// we fix this here
		if (intermediate >= 0.)
		{
			if (points[i].size() > (unsigned long int) (prob_dim + 2))
			{
				points[i][points[i].size() - 4] = xT[0];
				points[i][points[i].size() - 3] = xT[1];
				points[i][points[i].size() - 2] = -integral;
			}
		}

		/* Use the boundary condition u_D
		 * and the integral \int_0^{\tau} f(x_i) \mathrm{d}s .
		 */
		results[i] = u_D_xT + integral - integralCoarse;

		if (M != 0) {
//			results[i] -= u_D(xTCoarse);
			results[i] -= u_D_xT;
		}

		if (intermediate >= 0.)
		{
			for (unsigned int j = prob_dim; j < points[i].size();
					j += (prob_dim + 2))
			{
				// here every intermediate points gets corrected for the integral value
				points[i][j] += results[i];
			}
		}

		// sum up all results in each thread
		sums[tid] += results[i];
		
	} // end parallel for

	// calculate mean - this is the Monte-Carlo part to approximate the mean value of u_D - \int f
	double mean = 0.f;

	// sum up the sums of all threads ...
	for (int i = 0; i < numThreads; ++i)
	{
		mean += sums[i];
	}

	// ... and divide by total number of trajectories
	mean /= this->numTraj;

	// for convergency measurements we calculate the variance
	double variance = 0.;

	for (unsigned long i = 0; i < this->numTraj; ++i)
	{
		variance += (results[i] - mean) * (results[i] - mean);
	}

	variance /= this->numTraj;

	delete[] results;
	delete[] sums;

	// shrink each trajectory to required size to get the over all size which is required for the results
	if (intermediate >= 0.)
	{
		for (unsigned long i = 0; i < this->numTraj; ++i)
		{
			points[i][prob_dim + 1] = variance;

			points[i].shrink_to_fit();
			this->overallSize += points[i].size();
		}
	}
	else
	{
		this->overallSize = prob_dim + 2;
	}

	// allocate room for {x0, x1, u(x), variance} or intermediate points

	double* ret = new double[this->overallSize];

	if (intermediate < 0.)
	{
		for (int i = 0; i < prob_dim; ++i)
		{
			ret[i] = x0[i];
		}

		ret[prob_dim] = mean;
		ret[prob_dim + 1] = variance;
	}
	else
	{
		unsigned count = 0;
		for (unsigned long i = 0; i < this->numTraj; ++i)
		{
			for (unsigned int j = 0; j < points[i].size(); ++j)
			{
				ret[count] = points[i][j];
				count++;
			}
		}
	}

	// here the file for the R output is written
	if (write && file != NULL)
	{
		fwrite(ret, sizeof(double), this->overallSize, file);
		fclose(file);
		printf("Trajectories written to file.\n");
	}

	delete[] ndist;

	return ret;
}

/**
 * Essentially this is the same method as solveSP(), but the parallelization here it not applied to the
 * Euler-Maruyama scheme, but the grid of points.
 *
 * Only the differences between this function an solveSP() will be commented.
 *
 * @see solveSP()
 */
double* EulerScheme::solveGrid(std::vector<double> &grid, int numGridPoints,
		int prob_dim, double stepsize, bool adaptive, double intermediate,
		unsigned int seed)
{
	double* results = new double[numGridPoints * this->numTraj];

	// increments per thread
	unsigned ipt = 64. / stepsize; // 2^4

	std::vector<double> increments[numThreads];

	std::vector< std::unique_ptr<StochasticField> > sfs(this->numThreads);

	std::normal_distribution<double> *ndist = new std::normal_distribution<
			double>[this->numThreads];

	std::random_device rdev;

	double* means = new double[numGridPoints];
	double* variance = new double[numGridPoints];

	// initialize sums and brownian increments for each thread
#pragma omp parallel for shared(increments, ipt, ndist, sfs)
	for (int s = 0; s < numThreads; ++s)
	{
		std::normal_distribution<double> temp_ndist(0., 1.);
		ndist[s] = temp_ndist;

		std::mt19937_64 temp_mts;

		if (seed == std::numeric_limits<unsigned int>::max())
		{
			std::vector<int> seed_data(2 * std::mt19937_64::state_size);
			std::generate_n(seed_data.data(), seed_data.size(), std::ref(rdev));
			std::seed_seq seq(std::begin(seed_data), std::end(seed_data));

			temp_mts.seed(seq);
		}
		else
		{
			temp_mts.seed(seed + s);
		}

		MTS[s] = temp_mts;

		std::unique_ptr<StochasticField> temp_sf(this->sf->clone());

		sfs[s] = std::move(temp_sf);

		increments[s].resize(ipt);

		for (unsigned dd = 0; dd < ipt; dd++)
			increments[s][dd] = ndist[s](MTS[s]);
	}

	std::vector<double> points[numGridPoints * this->numTraj];
	unsigned count = 0;

#pragma omp parallel for shared(grid, increments, results, numGridPoints, prob_dim, ipt, means, variance, points, adaptive, stepsize, ndist, sfs) firstprivate(count)
	// iteration over the grid points
	for (int it = 0; it < numGridPoints; ++it)
	{

		std::vector<double> xT(prob_dim);
		means[it] = 0.;
		variance[it] = 0.;

		std::vector<double> x0(prob_dim);

		for (int i = 0; i < prob_dim; ++i)
		{
			x0[i] = grid[it * prob_dim + i];
		}

		int tid = omp_get_thread_num();

		// iteration over trajectories
		for (unsigned long i = 0; i < this->numTraj; ++i)
		{
			// point index
			unsigned long int pid = i + it * this->numTraj;

			sfs[tid]->newCoefficients();

			std::vector<double> xi(x0);

			double integral = 0.;
			double T = 0.;

			if (intermediate >= 0.)
			{
				points[pid].push_back(xi[0]);
				points[pid].push_back(xi[1]);
				points[pid].push_back(-integral);
				points[pid].push_back(0.);
			}

			std::vector<double> last_xi(xi);

			double dt = stepsize;

			std::vector<double> kappa = sfs[tid]->eval(xi);
// 			std::vector<double> last_kappa(kappa);

			if (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
				 && xi[1] < domain[3])
			{

				if (adaptive)
					dt *= adapt(xi, kappa);

				// the sqrt term from the Euler-Maruyama scheme
				double sqrtTerm = sqrt(2 * kappa[0] * dt);
			
				for (int j = 0; j < prob_dim; ++j)
				{
					xi[j] += kappa[j + 1] * dt
							+ sqrtTerm * increments[tid][count];
		// 				xi[j] += sqrt(2 * dt) * increments[tid][count];
					count++;
				}
			}

			while (xi[0] > domain[0] && xi[0] < domain[1] && xi[1] > domain[2]
					&& xi[1] < domain[3])
			{
				if (count >= ipt)
				{
					for (unsigned dd = 0; dd < ipt; dd++)
						increments[tid][dd] = ndist[tid](MTS[tid]);

					count = 0;
				}

				dt = stepsize;

// 				last_kappa = kappa;
				kappa = sfs[tid]->eval(xi);

				if (adaptive)
					dt *= adapt(xi, kappa );

				// the sqrt term from the Euler-Maruyama scheme
				double sqrtTerm = sqrt(2 * kappa[0] * dt);
			
				integral += f(xi) * dt;

				T += dt;

				last_xi = xi;

				for (int j = 0; j < prob_dim; ++j)
				{
					xi[j] += kappa[j + 1] * dt
							+ sqrtTerm * increments[tid][count];
//					xi[j] += sqrt(2 * dt) * increments[tid][count];
					count++;
				}

				if (intermediate >= 0.)
				{
					if ((points[pid][points[pid].size() - 4] - xi[0])
							* (points[pid][points[pid].size() - 4] - xi[0])
							+ (points[pid][points[pid].size() - 3] - xi[1])
									* (points[pid][points[pid].size() - 3]
											- xi[1])
							>= intermediate * intermediate)
					{
						if (points[pid].size() + 4 > points[pid].capacity())
						{
							{
								points[pid].reserve(
										(int) (1.618034 * points[pid].capacity()));
							}
						}

						points[pid].push_back(xi[0]);
						points[pid].push_back(xi[1]);
						points[pid].push_back(-integral);
						points[pid].push_back(0.);
					}
				}
			}

			if (xi[0] != last_xi[0] || xi[1] != last_xi[1])
			{
				xT =  project(last_xi, xi);
				integral += f(xT) * dt * distance(xT, last_xi)/distance(xi, last_xi);
			}
			else
			{
				xT = xi;
			}

			if (intermediate >= 0.)
			{
				if (points[pid].size() > (unsigned long int) (prob_dim + 2))
				{
					points[pid][points[pid].size() - 4] = xT[0];
					points[pid][points[pid].size() - 3] = xT[1];
					points[pid][points[pid].size() - 2] = -integral;
				}
			}

			results[pid] = u_D(xT) + integral;

			if (intermediate >= 0.)
			{
				for (unsigned int j = prob_dim; j < points[pid].size();
						j += (prob_dim + 2))
				{
					points[pid][j] += results[pid];
				}

			}

			/* In the single point version we needed to calculate the sums for each thread.
			 * Here the threads have another scope, so we don't need to worry about that.
			 */
			means[it] += results[pid];

		} // end for numTraj

		means[it] /= this->numTraj;

		for (unsigned long i = 0; i < this->numTraj; ++i)
		{
			unsigned long int pid = i + it * this->numTraj;
			variance[it] += (results[pid] - means[it])
						  * (results[pid] - means[it]);
		}

		variance[it] /= this->numTraj;

	} // end for grid iteration

	if (intermediate >= 0.)
	{
		for (long it = 0; it < numGridPoints; ++it)
		{
			for (unsigned long i = 0; i < this->numTraj; ++i)
			{
				unsigned long pid = i + it * this->numTraj;
				points[pid][prob_dim + 1] = variance[it];

				points[pid].shrink_to_fit();
				this->overallSize += points[pid].size();
			}
		}
	}
	else
	{
		this->overallSize = (prob_dim + 2) * numGridPoints;
	}

	double* ret = new double[this->overallSize];

	if (intermediate < 0.)
	{
		for (int it = 0; it < numGridPoints; it++)
		{
			for (int i = 0; i < prob_dim; ++i)
			{
				// 0, 1, 4, 5, 8, 9, ...	   // 0, 1, 2, 3, 4, 5, ...
				ret[i + it * (prob_dim + 2)] = grid[i + it * prob_dim];
			}

			// 2, 6, 10, ...
			ret[prob_dim + it * (prob_dim + 2)] = means[it];
			// 3, 7, 11, ...
			ret[prob_dim + 1 + it * (prob_dim + 2)] = variance[it];
		}
	}
	else
	{
		unsigned count = 0;
		for (int it = 0; it < numGridPoints; it++)
		{
			for (unsigned long i = 0; i < this->numTraj; ++i)
			{
				unsigned long pid = i + it * this->numTraj;
				for (unsigned int j = 0; j < points[pid].size(); ++j, ++count)
				{
					ret[count] = points[pid][j];
				}
			}
		}
	}

	// here the file for the R output is written
	if (write && file != NULL)
	{
		fwrite(ret, sizeof(double), this->overallSize, file);
		fclose(file);
		printf("Trajectories written to file.\n");
	}

	// clean up
	delete[] ndist;
	delete[] means;
	delete[] variance;

	return ret;
}

double* EulerScheme::getIncrements(int prob_dim, double stepsize, unsigned int seed)
{
	// increments per thread
	unsigned ipt = 2000000/numThreads;

	std::vector<double> xT(prob_dim);

	std::normal_distribution<double> *ndist = new std::normal_distribution<
			double>[this->numThreads];

	std::random_device rdev;

	double* ret = new double[ipt*numThreads];

	// initialize sums and brownian increments for each thread
#pragma omp parallel for shared(ret, ipt, ndist)
	for (int s = 0; s < numThreads; ++s)
	{
		std::normal_distribution<double> temp_ndist(0., 1.);
		ndist[s] = temp_ndist;

		std::mt19937_64 temp_mts;

		if (seed == std::numeric_limits<unsigned int>::max())
		{
			std::vector<int> seed_data(2 * std::mt19937_64::state_size);
			std::generate_n(seed_data.data(), seed_data.size(), std::ref(rdev));
			std::seed_seq seq(std::begin(seed_data), std::end(seed_data));

			temp_mts.seed(seq);
		}
		else
		{
			temp_mts.seed(seed + s);
		}

		MTS[s] = temp_mts;

		for (unsigned dd = s; dd < numThreads*ipt; dd+=numThreads)
		{
			ret[dd] = ndist[s](MTS[s]);
		}
	}

	return ret;
}

std::vector<double> EulerScheme::getCoefficients()
{
	return sf->getCoefficients();
}

unsigned long int EulerScheme::getLength()
{
	return this->overallSize;
}
